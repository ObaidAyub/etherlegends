![dissolveeffect](https://cloud.githubusercontent.com/assets/12746779/17116951/fa3707c6-52b1-11e6-89ff-c1012c589fa6.png)

Add this texture to the second slot. Set it to alpha from grayscale and alpha is transparency.
Your texture goes in the first one.

Make a new material with this shader and play with the sliders and colours. You can create cool animated effects if you change dissolution level and/or edge width gradually over time.

Example of the shader in action:

![anim](https://cloud.githubusercontent.com/assets/12746779/17117308/7ca85d80-52b3-11e6-8c59-4a446dd4f631.gif)
