﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissoveEffectRunner : MonoBehaviour
{

    public GameObject FireEffects, FireEffects2;
    public Action OnEffectComplete;

    Material material;

    bool DoingEffects = false;

    // Start is called before the first frame update
    void Start()
    {
        //var SpriteRenderer = GetComponent<SpriteRenderer>();
        //material = SpriteRenderer.material;
        //material.SetTexture("_NoiseTex", SpriteRenderer.sprite.texture);

    }

    internal void UpdateTexture()
    {
        var SpriteRenderer = GetComponent<SpriteRenderer>();
        material = SpriteRenderer.material;
        material.SetTexture("_NoiseTex", SpriteRenderer.sprite.texture);
    }


    float level , edges;

    // Update is called once per frame
    void Update()
    {
        if (DoingEffects)
        {
            level = Mathf.Lerp(level, 1, Time.smoothDeltaTime * 2.3F);
            edges = Mathf.Lerp(edges, 0F, Time.smoothDeltaTime * 1.2F);

            material.SetFloat("_Level", level);
            material.SetFloat("_Edges", edges);

            if (level > 0.94F)
                DoingEffects = false;
        }
    }
    /// <summary>
    /// after defeat call this funtion for showing removing card particles
    /// </summary>
    public void StartBurningEffect() // card removeing funtion
    {
        UpdateTexture();
        level = 0;
        edges = 0.6F;
        //OnEffectComplete = OnComplete;
        StartCoroutine(DestructionRotiens());
    }

    /// <summary>
    /// after defeat call this funtion for showing defeat card
    /// </summary>
    public void StartBurningEffect(Action onComplete = null)
    {
        UpdateTexture();

        level = 0;
        edges = 0.6F;

        OnEffectComplete = onComplete;
        StartCoroutine(DestructionRotiens());
    }



    IEnumerator DestructionRotiens()
    {
        yield return new WaitForSeconds(1F);
        if (FireEffects)
        {
            FireEffects.SetActive(true);
            yield return new WaitForSeconds(1);
            if (FireEffects2)
            {
                FireEffects2.SetActive(true);
                yield return new WaitForSeconds(0.1F);
            }
        }
        DoingEffects = true;


        yield return new WaitUntil(() => DoingEffects);
        FireEffects.SetActive(false);
        OnEffectComplete?.Invoke();
    }

    /// <summary>
    /// after defeat call this funtion for showing defeat card
    /// </summary>
    public void StartApperaing() 
    {
        UpdateTexture();

        StartCoroutine(AppearGradually());
    }

   

    IEnumerator AppearGradually()
    {
        if (FireEffects)
        {
            FireEffects.SetActive(false);
            if (FireEffects2)
            {
                FireEffects2.SetActive(false);
            }
        }

        level = 1;
        edges = 0.5F;

        material.SetFloat("_Level", level);
        material.SetFloat("_Edges", edges);

        while (level > 0)
        {

            level -= 0.05F;
            edges -= 0.09F;

            material.SetFloat("_Level", Mathf.Max(0, level));
            material.SetFloat("_Edges", Mathf.Max(0, edges));

            yield return new WaitForEndOfFrame();
        }
    }

    
}
