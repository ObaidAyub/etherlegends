using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using UnityEngine.Android;
using BarcodeScanner.Parser;
using BarcodeScanner.Scanner;
using BarcodeScanner;
using ZXing;
using System.IO;

public class UpdateProfilePic : MonoBehaviour
{

	
    public RawImage ProfilePic;

    public Texture2D DefaultProfilePic;

    public GameObject loadingSCreen;

    public GameObject ScanQRObj;

	
    public void ChangeProfilePic() 
    {
        NativeGallery.GetImageFromGallery((Path) =>
        {
            if (string.IsNullOrEmpty(Path))
            {
                return;
            }
            var resizedPath = NativeGallery.GetResizedImagePath(Path, 512);
            ImagePicked( resizedPath);
        }, "Select Image ", "image/*");
    }

    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }

    void ImagePicked(string path)
    {
        if (path != "")
        {
            ProfilePic.texture = LoadImageFromFile(path);
            StartCoroutine(transform.parent.GetComponent<ProfileManager>().UploadProfilePicAsync(path));
        }
    }

    public static Texture2D LoadImageFromFile(string path)
    {
        if (path == "Cancelled") return null;

        byte[] bytes;
        Texture2D texture = new Texture2D(290, 290, TextureFormat.RGB24, false);

        #if UNITY_WINRT

                bytes = UnityEngine.Windows.File.ReadAllBytes(path);
                texture.LoadImage(bytes);

        #else

                bytes = System.IO.File.ReadAllBytes(path);
                texture.LoadImage(bytes);

        #endif

        return texture;
    }

  
}
