﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SimpleJSON;
using System.IO;
using UnityEngine.SceneManagement;

public class EnableObjectDelay : MonoBehaviour
{

    [SerializeField] GameObject UserData, GameCardsDownload, BlockChainCards;


    [SerializeField] GameObject[] Gobjects;
    public static EnableObjectDelay Instance;

    public string Response;

    private void Awake()
    {
        //Debug.Log("first ");



        Instance = this;
        

        StartCoroutine(checkingOnNetwork());
        LoadingManager.Instance.ShowMainLoading("Initializing..");
       // Invoke("DownloadGameCards", 2);
        StartCoroutine(StartDelay());
    }
    public void CheckInternetConnection()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            ////Debug.Log("Error. Check internet connection!");
            LoadingManager.Instance.ShowNetworkErrorWindow(Restart);
            return;
        }
        else
        {
            ////Debug.Log("hide window");

            LoadingManager.Instance.HideNetworkErrorWindow();
        }

    }

    public async void CheckingDataVersion()
    {
        string response = await SessionManager.Instance.Post(ConstantManager._url_getCardsVersion, null, null, false);
        ////Debug.Log(response);


        if (string.IsNullOrEmpty(response) || JSON.Parse(response) == null || JSON.Parse(response).Count < 1)
        {
            StartCoroutine(StartDelay());
            return;
        }
        else
        {
            var Jnode = JSON.Parse(response);

            if (Jnode["status"] == 1)
            {

                if (UserDataController.Instance.GetVersion() == Jnode["data"]["version_no"])
                {

                }
                else
                {

                    string DIRpath = Path.Combine(Application.persistentDataPath, "GameCards");
                    if (Directory.Exists(DIRpath))
                    {
                        var directoryInfo = new DirectoryInfo(DIRpath);
                        directoryInfo.Delete(true);

                    }
                    UserDataController.Instance.SetVersion(Jnode["data"]["version_no"]);


                }
            }



            LoadingManager.Instance.ShowMainLoading("Fetching Characters..", true);

            DownloadGameCards();
        }
    }
    public IEnumerator checkingOnNetwork()
    {
        CheckInternetConnection();
        yield return new WaitForSeconds(5);
        StartCoroutine(checkingOnNetwork());
    }
    IEnumerator StartDelay()
    {
        //UserData = GameObject.Find("UserDataController");
        UserData.SetActive(true);      
        yield return new WaitForSeconds(2);
        CheckingDataVersion();

    }

    public void DownloadGameCards()
    {
        GameCardsDownload.SetActive(true);
    }

   

    public void StartOtherObject()
    {
        StartCoroutine(DelayObjectsEnable());
    }


    IEnumerator DelayObjectsEnable()
    {
        //LoadingManager.Instance.ShowMainLoading("Almost there..");
         
        for (int i = 0; i < Gobjects.Length; i++)
        {
            if(Gobjects[i])             
            Gobjects[i].SetActive(true);
            
        }

        yield return new WaitForEndOfFrame();
        //LoadingManager.Instance.HideMainLoading();
    }


    public void Restart()
    {
        //Debug.Log("Restrt");

        if (UserDataController.Instance != null)
        {
            Destroy(UserDataController.Instance.gameObject);
            UserDataController.Instance = null;
        }
        if (CardManagerSC.Instance != null)
        {
            Destroy(CardManagerSC.Instance.gameObject);
            CardManagerSC.Instance = null;
        }
        if (BattleLobbySC.Instance != null)
        {
            Destroy(BattleLobbySC.Instance.gameObject);
            BattleLobbySC.Instance = null;
        }
        if (ConnectOpenGame.Instance != null)
        {
            Destroy(ConnectOpenGame.Instance.gameObject);
            ConnectOpenGame.Instance = null;
        }


        SceneManager.LoadScene(0);
    }
}
