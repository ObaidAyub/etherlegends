﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LeaderBoardTabChange : MonoBehaviour
{

    public GameObject allLeaderboard;
    public GameObject seasonLeaderboard;

    public Button allLeaderBoardBtn;
    public Button seasonLeaderBoardBtn;


    public void OpenAllLeaderBoardTab()
    {
        LeaderboardMain.instance.AllLeaderBoardUI();
        //allLeaderboard.SetActive(true);
        //seasonLeaderboard.SetActive(false);
        //allLeaderBoardBtn.GetComponent<Button>().interactable = false;
        //seasonLeaderBoardBtn.GetComponent<Button>().interactable = true;

        //Debug.Log("Open all leader board ");
    }
    public void OpenAllSeasonLeaderBoardTab()
    {
       LeaderboardMain.instance.GetSeasonData();
        allLeaderBoardBtn.GetComponent<Button>().interactable = true;
        seasonLeaderBoardBtn.GetComponent<Button>().interactable = false;
        //LeaderboardMain.instance.ResetAll();
        allLeaderboard.SetActive(false);
        seasonLeaderboard.SetActive(true);
        //Debug.Log("Open season leader board ");

    }
}
