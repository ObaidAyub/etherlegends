﻿
using UnityEngine;
using UnityEngine.UI;

public class TabbingSC : MonoBehaviour
{
    public GameObject[] TabPanel;
    public Button[] Tabbutton;

    private void OnEnable()
    {
        TabButtonPressed(0);
    }

    public void TabButtonPressed(int tab)
    {
        for (int i = 0; i < TabPanel.Length; i++)
        {
            TabPanel[i].SetActive(false);
            Tabbutton[i].interactable = true;
        }
        TabPanel[tab].SetActive(true);
        Tabbutton[tab].interactable = false;
    }

}
