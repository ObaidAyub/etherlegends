﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class UserTransactionSc : MonoBehaviour
{
    public GameObject ItemObj,loadingObj;
    public Transform ParentObj;
   
    public ScrollRect ScrollPos;

    private void OnEnable()
    {
        FetchUserTrans();
    }

    

    public async void FetchUserTrans()
    {

        loadingObj.SetActive(true);

        for (int i = 0; i < ParentObj.childCount; i++)
        {
            Destroy(ParentObj.GetChild(i).gameObject);
        }

        string response = await SessionManager.Instance.Post(ConstantManager._url_GetAllUserTransaction, null, SessionManager.Instance.userToken, true);

        Debug.Log("Transaction----" + response);

        var res = JSON.Parse(response);

        if (res["status"]==1)
        {
            for (int i = 0; i < res["data"].Count; i++)
            {
                var Item = Instantiate(ItemObj, ParentObj);
                Debug.Log("TransactionHash" + res["data"][i]["transaction_hash"]);
                Item.GetComponent<BLCTransSC>().setValue(res["data"][i]["createdAt"], res["data"][i]["user_wallet_address"], res["data"][i]["trans_type"], 
                                            res["data"][i]["dest_wallet_address"], res["data"][i]["amount"], res["data"][i]["transaction_hash"]);

                Item.SetActive(true);
                //ItemObj.GetComponent<BLCTransSC>().
            }

        }
        loadingObj.SetActive(false);
    }


   
}
