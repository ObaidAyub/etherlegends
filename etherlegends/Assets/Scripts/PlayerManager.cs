﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using SG;
using System.Security.Cryptography;
using UnityEngine.UI;
using System.Linq;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance;

    //public Canvas[] CardCanvas;
    //public Canvas[] EnemyCardCanvas;

    public Canvas[] CardCanvas
    {
        get
        {
            return PlayerCardsObj.Select(c => c.canvas).ToArray();
        }
    }

    public Canvas[] EnemyCardCanvas
    {
        get
        {
            return enemyCardsObj.Select(c => c.canvas).ToArray();
        }
    }


    public Transform PlayerCardController, EnemyCardController;
    public List<CardObject> enemyCardsObj, PlayerCardsObj;

    public Animator[] powerBtns
    {
        get
        {
            return PlayerCardsObj.Select(c => c.powerAnim).ToArray();
        }
    }

   
    
    public Animator[] EnemypowerBtns
    {
        get
        {
            return PlayerCardsObj.Select(c => c.powerAnim).ToArray();
        }
    }

    

    [Space]
    [Space]

    public Animator mainAnim;

    public Animator winAnimator;

    public GameObject LostAnimator, TieObj, skirmishObj, VictoryObj, DefeatObj, DrawObj;

    public CardObject SelectedCard, SelectedHealCard;

    //public Transform enemyCardsObj[0, enemyCardsObj[1, enemyCardsObj[2;

    [Space]
    public GameObject MeIndicator, OppIndicator;
    public GameObject FinalMeIndicator, FinalOppIndicator;


    [Space]
    public GameObject clickParticlePrefab;

    public CardObject SelectedEnemyCard;
    public CardObject SelectedHealEnemyCard;

    public RuntimeAnimatorController AfterControl, GameplayAnim;

    public bool CanClick = true;
    public bool CanClickOnBG;
    [HideInInspector]
    public bool CanClickOnElemt, CanClickOnMeCard, CanClickOnEnemyCard, CanClickOnMeAttrib, CanClickonEnemyAttrib, CanClickonMeDice, CanClickonEnemyDice, CanClickOnSpecialPower;

    public GameObject TextCanvas;

    public TextMeshProUGUI TextMessage;



    //FinalBattleScreen
    [Header("FinalBattleScreen")]
    public GameObject FinalBattleEndScreen;
    public Image bgFinalEndScreen;
    public GameObject canvasFinalEndScreen;

    public TextMeshProUGUI MeResult, OppResult;

    public RawImage MyProfile, OppProfile;

    public TextMeshProUGUI MeName, OppName;

    public GameObject[] FinalObjToDisable;

    public GameObject ParticleParent;

    public string Message = "";

    //string MeSpUsed, OppSpUsed;

    public string PrevRes = "";
    public bool IsMeSp, IsOppSp;

    public GameObject MsgPopUP, NoInternet;
    public Text MsgText;

    public PlayerCompanionCardsAnim CompanionCards;
    public GameObject[] playerCompaionCards;

    public CardObject BattleCardPrefab;

    [Header("elementeumParticles")]
    public DigitalRuby.LightningBolt.LightningBoltScript[] elementeumParticle;
    public DigitalRuby.LightningBolt.LightningBoltScript[] elementeumParticleEnemy;

    [Header("animatedHeart Player")]
    public GameObject animatedHeart;
    public GameObject heartSlash;
    public TextMeshProUGUI animatedHeartPoint;

    [Header("animatedHeart Enemy")]
    public GameObject animatedHeartEnemy;
    public GameObject heartSlashEnemy;
    public TextMeshProUGUI animatedHeartPointEnemy;

    [Header("animatedHeart")]
    public GameObject spParticals;


    Vector3 PlayerRotEular = Vector3.right * 30F;
    Vector3 PlayerScale = Vector3.one * 0.54F;
    Vector3[] PlayerCardPos = new Vector3[] {
        new Vector3(3.3F,1.62f,15.45f),
        new Vector3(0F,1.62f,15.45f),
        new Vector3(-3.3F,1.62f,15.45f)
    };

    Vector3 EnemyScale = new Vector3(0.5F, 0.55f, 0.5f);
    Vector3 EnemyRotEular = Vector3.right * 25f;
    Vector3[] EnemyCardPos = new Vector3[] {
        new Vector3(3.03F, 5.33f,15.84f),
        new Vector3(0F,5.33f,15.84f),
        new Vector3(-3.03F,5.33f,15.84f)
    };

    

    private void Awake()
    {
        if (instance == null) instance = this;


        enemyCardsObj = new List<CardObject>();
        PlayerCardsObj = new List<CardObject>();

        int i = 0;
        BattleLobbySC.Instance.Finaldata.user_data.best_cards.ForEach(x =>
        {
            var cardObject = Instantiate(BattleCardPrefab, PlayerCardController);
            cardObject.IsEnemyCard = false;
            cardObject.CardDetail = Card.FromFetchedCard(x);
            cardObject.name = "Card" + i;
            cardObject.cardNumber = i;
            cardObject.transform.localPosition = PlayerCardPos[i++];
            cardObject.transform.localEulerAngles = PlayerRotEular;
            cardObject.RefreshCardDetails(x.card_contract_id);
            PlayerCardsObj.Add(cardObject);
            

            
        });

         i = 0;
        BattleLobbySC.Instance.Finaldata.opp_user_data.best_cards.ForEach(x =>
        {
            var cardObject = Instantiate(BattleCardPrefab, EnemyCardController);
            cardObject.IsEnemyCard = true;
            cardObject.CardDetail = Card.FromFetchedCard(x);
            cardObject.name = "Card" + i;
            cardObject.cardNumber = i;
            cardObject.transform.localPosition = EnemyCardPos[i++];
            cardObject.transform.localEulerAngles = EnemyRotEular;
            cardObject.RefreshCardDetails(x.card_contract_id);
            var btn = cardObject.SpAbility1.GetComponentInChildren<Button>();
            btn.gameObject.GetComponent<Image>().raycastTarget = false;

            btn = cardObject.SpAbility2.GetComponentInChildren<Button>();
            btn.gameObject.GetComponent<Image>().raycastTarget = false;
            enemyCardsObj.Add(cardObject);
        });



        //for (int i = 0; i < 3; i++)
        //{
        //    //public Transform PlayerCardController, EnemyCardController;
        //    var card = Instantiate(BattleCardPrefab, PlayerCardController);
        //    card.IsEnemyCard = false;
        //    card.transform.localPosition = PlayerCardPos[i];
        //    card.transform.localEulerAngles = PlayerRotEular;
        //    PlayerCardsObj.Add(card);

        //    card = Instantiate(BattleCardPrefab, EnemyCardController);
        //    card.IsEnemyCard = true;
        //    card.transform.localPosition = EnemyCardPos[i];
        //    card.transform.localEulerAngles = EnemyRotEular;
        //    enemyCardsObj.Add(card);
        //}


        SelectedCard = null;
        SelectedHealCard = null;
        SelectedEnemyCard = null;
        SelectedHealEnemyCard = null;
    }

    public void DisableParticle()
    {
        for (int i = 1; i < ParticleParent.transform.childCount; i++)
        {
            ParticleParent.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void setSp(bool isMe, AttributeType SpName)
    {

        if (isMe)
        {
            if (SpName != AttributeType.None)
            {
                //MeSpUsed = SpName;

                IsMeSp = true;
            }
            else
            {
                //MeSpUsed = SpName;
                IsMeSp = false;
            }
        }
        else if (IsMeSp && IsOppSp)
        {

        }
        else
        {
            if (SpName != AttributeType.None)
            {
                //OppSpUsed = SpName;
                IsOppSp = true;
            }
            else
            {
                //OppSpUsed = SpName;
                IsOppSp = false;
            }
        }
    }

    public void UnsetSp(bool isme)
    {
        if (isme)
        {
            //MeSpUsed = "";
            IsMeSp = false;
        }
        else if (isme)
        {
            //OppSpUsed = "";
            IsOppSp = false;
        }
    }

    public bool getSpme()
    {
        return IsMeSp;
    }

    public bool getSpOpp()
    {
        return IsOppSp;
    }


    private void Update()
    {
        if (Message == "clickonbg")
        {
            Debug.Log("Click on bg" + CanClick + " canclick " + CanClickOnBG);
            Message = "";
            DiceManager.instance.playerDiceNum = 0;
            DiceManager.instance.enemyDiceNum = 0;

            SelectPowerManager.instance.checkAttrAnim = 0;
            if (SelectedCard != null)
            {
                Debug.Log(SelectedCard.isUseSpAbility);
                if (SelectedCard.isUseSpAbility)
                {
                    SelectedCard.Cur_Elemt = 0;
                    SelectedCard.AddElementeum(SelectedCard.CardDetail.elementeum);
                    SelectedCard.isSpAbility = true;
                    BattleLobbySC.Instance.CancelSpecialAbility(SelectedCard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName);
                }

                if (SelectedCard.debuffValueAttack >= 1 && !SelectedCard.debuffAtk)
                {
                    SelectedCard.debuffAtk = true;
                    SelectedCard.debuffValueAttack -= 1;

                }
                if (SelectedCard.debuffValueHeal >= 1 && !SelectedCard.debuffHl)
                {
                    SelectedCard.debuffHl = true;
                    SelectedCard.debuffValueHeal -= 1;

                }
                SelectedCard.RemoveDebuffToPlayerAttribute(SelectPowerManager.instance.selectedPowerName);
                SelectedCard.HealthAnimatedsHeartHide();
                for (int i = 0; i < SelectedCard.elementeumChild.Length; i++)
                {

                    SelectedCard.elementeumChild[i].canClickElementeum = true;
                    SelectedCard.elementeumChild[i].selectEle = true;

                }

                SelectedCard.RevertPlayerSpecialAbilityAnimation();
            }
            if (SelectedEnemyCard != null)
            {

                //Debug.Log(SelectedEnemyCard.isUseSpAbility);
                if (SelectedEnemyCard.isUseSpAbility)
                {
                    SelectedEnemyCard.Cur_Elemt = 0;
                    SelectedEnemyCard.AddElementeum(SelectedEnemyCard.CardDetail.elementeum);
                    SelectedEnemyCard.isSpAbility = true;
                    InGameCards.instance.enemyCompanion.text = "0";

                }
                SelectedEnemyCard.RemoveDebuffToEnemyAttribute(SelectPowerManager.instance.selectedEnemyPowerName);
                SelectedEnemyCard.HealthAnimatedsHeartHide();

                SelectedEnemyCard.RevertEnemySpAbiAnim();
            }

            CompanionCards.canClickCompanion = true;

            DiceManager.instance.ElEarnMe.SetActive(false);

            DiceManager.instance.ELEarnOpp.SetActive(false);

            TextCanvas.SetActive(false);

            TextMessage.text = "";

            IsMeSp = false;

            IsOppSp = false;

            mainAnim.enabled = true;

            SelectPowerManager.instance.PlayerTwo = false;

            SelectPowerManager.instance.selectedPowerName = AttributeType.None;

            SelectPowerManager.instance.selectedEnemyPowerName = AttributeType.None;



            DisableParticle();

            Debug.Log("------------->>>>>>>>");

            if (DiceManager.instance)
            {
                DiceManager.instance.DisableDice();
                //DiceManager.instance.OppDisableDice();

            }

            InGameCards.instance.SetMyAttribImage(AttributeType.None);

            InGameCards.instance.SetOppAttribImage(AttributeType.None);

            InGameCards.instance.AddOrRemovePlayerElementeum();//Remove Elementeum from here 
            InGameCards.instance.AddOrRemoveOppElementeum();//Remove Elementeum from here 
            if (SelectedCard == null)
            {

            }
            else
            {
                SelectedCard.remain_Elemt = 0;

            }
            if (SelectedEnemyCard == null)
            {

            }
            else
            {
                SelectedEnemyCard.remain_Elemt = 0;

            }

            OnMainAnim();

            mainAnim.SetBool("Card0", false);
            mainAnim.SetBool("Card1", false);
            mainAnim.SetBool("Card2", false);
            mainAnim.SetBool("CardPosBack", true);

            DiceManager.instance.dice2.DOScale(new Vector3(0f, 0f, 0f), 0.3f);
            DiceManager.instance.dice.DOScale(new Vector3(0f, 0f, 0f), 0.3f);

            //   DiceManager.instance.GetComponent<AudioSource>().clip = DiceManager.instance.CardReset;
            //  DiceManager.instance.GetComponent<AudioSource>().Play();

            for (int i = 0; i < CardCanvas.Length; i++)
            {
                CardCanvas[i].sortingOrder = 2;
            }

            // mainAnim.SetBool("CardPosBack", true);

            winAnimator.enabled = false;

            for (int i = 0; i < PlayerCardsObj.Count; i++)
            {
                PlayerCardsObj[i].ElementeumStatus.Clear();
                enemyCardsObj[i].ElementeumStatus.Clear();
                PlayerCardsObj[i].HideElementeum();
                enemyCardsObj[i].HideElementeum();

                PlayerCardsObj[i].SetElemtDeault();

                PlayerCardsObj[i].SetDownSort();

                enemyCardsObj[i].SetDownSort();

                PlayerCardsObj[i].ResetAttributeValue();
                enemyCardsObj[i].ResetAttributeValue();

                PlayerCardsObj[i].UnsetSpecialAbilityPosition();
                enemyCardsObj[i].UnsetSpecialAbilityPosition();

                PlayerCardsObj[i].SetAllPowerOff();
                enemyCardsObj[i].SetAllPowerOff();

                PlayerCardsObj[i].HideSpecialAbility();
                enemyCardsObj[i].HideSpecialAbility();
                enemyCardsObj[i].debuffValueAttack = 0;
                enemyCardsObj[i].debuffValueHeal = 0;
                PlayerCardsObj[i].DiceValue = 0;
                enemyCardsObj[i].DiceValue = 0;
                //MyCardsObj[i].GetComponent<CardObject>().UsingElementeum = 0;
                //enemyCardsObj[i].UsingElementeum = 0;

                PlayerCardsObj[i].isSpActive = false;
                enemyCardsObj[i].isSpActive = false;

                PlayerCardsObj[i].SpAbilityObj.gameObject.SetActive(false);
                PlayerManager.instance.spParticals.SetActive(false);
                PlayerManager.instance.spParticals.SetActive(false);
            }

            EnemyCardAndCanvasOrderSet();

            EnemyCardPosSet(enemyCardsObj[0].transform, new Vector3(-3.03f, 5.9f, 14.9f), 0.3f, new Vector3(0.5f, 0.55f, 0.5f), Quaternion.Euler(25f, 0f, 0));
            EnemyCardPosSet(enemyCardsObj[1].transform, new Vector3(0, 5.9f, 14.9f), 0.3f, new Vector3(0.5f, 0.55f, 0.5f), Quaternion.Euler(25f, 0f, 0));
            EnemyCardPosSet(enemyCardsObj[2].transform, new Vector3(3.03f, 5.9f, 14.9f), 0.3f, new Vector3(0.5f, 0.55f, 0.5f), Quaternion.Euler(25f, 0f, 0));

            SelectPowerManager.instance.selectedCardAnimator = null;
            SelectPowerManager.instance.EnemyselectedCardAnimator = null;
            SelectPowerManager.instance.selectedPowerName = AttributeType.None;
            SelectPowerManager.instance.selectedEnemyPowerName = AttributeType.None;



            InGameCards.instance.revertAllAttributeVisualsValue();
            InGameCards.instance.companion.text = "0";
            InGameCards.instance.comp = 0;
            InGameCards.instance.Elemtium.text = "0";
            InGameCards.instance.elem = 0;
            SelectedCard = null;
            SelectedEnemyCard = null;
            SelectedHealCard = null;
            SelectedHealEnemyCard = null;
            for (int i = 0; i < CompanionCards.playerCompanionCards.Length; i++)
            {
                CompanionCards.AutoDeselectCompanionCardsForBattle(CompanionCards.playerCompanionCards[i]);
            }
            for (int i = 0; i < CompanionCards.enemyCompanionCards.Length; i++)
            {
                CompanionCards.AutoDeselectEnemyCompanionCardsForBattle(CompanionCards.enemyCompanionCards[i]);
                //Debug.Log("DeSelect Enemey companion from here ! ");
            }
            for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
            {

                PlayerManager.instance.enemyCardsObj[i].companionData.Clear();
                PlayerManager.instance.enemyCardsObj[i].companionIDs.Clear();
            }

        }

    }


    public void ShowPopUP(string MsgTxt)
    {
        MsgPopUP.SetActive(true);
        MsgText.text = MsgTxt;
        StartCoroutine(HidePopUp());
    }

    IEnumerator HidePopUp()
    {
        yield return new WaitForSeconds(0.9f);
        MsgPopUP.SetActive(false);
    }

    IEnumerator DisableIsStart(float tim)
    {
        yield return new WaitForSeconds(tim);
        mainAnim.runtimeAnimatorController = AfterControl;
    }

    private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        StartCoroutine(DisableIsStart(2f));
    }

    public void ClickOnBG()
    {
        Debug.Log("Click on bg" + Message);
        Debug.Log("canclick " + CanClick + "  Click on bg" + CanClickOnBG);
        if (!CanClick || !CanClickOnBG)
        {
            return;
        }
        if (BattleLobbySC.Instance)
            BattleLobbySC.Instance.SocketMessage("clickonbg");


        Debug.Log("Click on bg working ");
        DiceManager.instance.playerDiceNum = 0;
        DiceManager.instance.enemyDiceNum = 0;

        SelectPowerManager.instance.checkAttrAnim = 0;
        if (SelectedCard != null)
        {
            Debug.Log(SelectedCard.isUseSpAbility);
           
            if (SelectedCard.isUseSpAbility)
            {
                SelectedCard.Cur_Elemt = 0;
                SelectedCard.AddElementeum(SelectedCard.CardDetail.elementeum);
                SelectedCard.isSpAbility = true;
                BattleLobbySC.Instance.CancelSpecialAbility(SelectedCard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName);
            }

            if (SelectedCard.debuffValueAttack >= 1 && !SelectedCard.debuffAtk)
            {
                SelectedCard.debuffAtk = true;
                SelectedCard.debuffValueAttack -= 1;

            }
            if (SelectedCard.debuffValueHeal >= 1 && !SelectedCard.debuffHl)
            {
                SelectedCard.debuffHl = true;
                SelectedCard.debuffValueHeal -= 1;

            }

            SelectedCard.RemoveDebuffToPlayerAttribute(SelectPowerManager.instance.selectedPowerName);
            SelectedCard.HealthAnimatedsHeartHide();
            for (int i = 0; i < SelectedCard.elementeumChild.Length; i++)
            {

                SelectedCard.elementeumChild[i].canClickElementeum = true;
                SelectedCard.elementeumChild[i].selectEle = true;

            }

            SelectedCard.RevertPlayerSpecialAbilityAnimation();
        }

        if (SelectedEnemyCard != null)
        {
            Debug.Log(SelectedEnemyCard.isUseSpAbility);
            var SeletedCardObject = SelectedEnemyCard;
            if (SeletedCardObject.isUseSpAbility)
            {
                SeletedCardObject.AddElementeum(SeletedCardObject.CardDetail.elementeum);
            }

            if (SeletedCardObject.debuffValueAttack >= 1 && !SeletedCardObject.debuffAtk)
            {
                SeletedCardObject.debuffAtk = true;
                SeletedCardObject.debuffValueAttack -= 1;

            }
            if (SeletedCardObject.debuffValueHeal >= 1 && !SeletedCardObject.debuffHl)
            {
                SeletedCardObject.debuffHl = true;
                SeletedCardObject.debuffValueHeal -= 1;

            }
            SeletedCardObject.RemoveDebuffToEnemyAttribute(SelectPowerManager.instance.selectedEnemyPowerName);
            SeletedCardObject.HealthAnimatedsHeartHide();


            SeletedCardObject.RevertEnemySpAbiAnim();
        }



        //InGameCards.instance.revertAllAttributeVisualsValue();

        CompanionCards.canClickCompanion = true;
        SelectPowerManager.instance.PlayerTwo = false;
        DiceManager.instance.ElEarnMe.SetActive(false);
        DiceManager.instance.ELEarnOpp.SetActive(false);
        TextCanvas.SetActive(false);
        TextMessage.text = "";



        InGameCards.instance.SetMyAttribImage(AttributeType.None);

        InGameCards.instance.SetOppAttribImage(AttributeType.None);





        InGameCards.instance.AddOrRemovePlayerElementeum();//Remove Elementeum from here 
        InGameCards.instance.AddOrRemoveOppElementeum();//Remove Elementeum from here 





        DisableParticle();

        if (DiceManager.instance)
        {
            DiceManager.instance.DisableDice();
            //DiceManager.instance.OppDisableDice();

        }

        IsMeSp = false;
        IsOppSp = false;

        mainAnim.enabled = true;

        SelectPowerManager.instance.PlayerTwo = false;

        SelectPowerManager.instance.selectedPowerName = AttributeType.None;

        SelectPowerManager.instance.selectedEnemyPowerName = AttributeType.None;



        DisableParticle();

        Debug.Log("------------->>>>>>>>");



        OnMainAnim();

        AllAnimCardFalse();

        DiceManager.instance.GetComponent<AudioSource>().clip = DiceManager.instance.CardReset;
        DiceManager.instance.GetComponent<AudioSource>().Play();
        DiceManager.instance.dice2.DOScale(new Vector3(0f, 0f, 0f), 0.3f);
        DiceManager.instance.dice.DOScale(new Vector3(0f, 0f, 0f), 0.3f);

        for (int i = 0; i < CardCanvas.Length; i++)
        {
            CardCanvas[i].sortingOrder = 2;
        }

        mainAnim.SetBool("CardPosBack", true);

        winAnimator.enabled = false;

        EnemyCardAndCanvasOrderSet();

        Debug.Log("Onlclick");

        for (int i = 0; i < PlayerCardsObj.Count; i++)
        {

            PlayerCardsObj[i].ElementeumStatus.Clear();
            enemyCardsObj[i].ElementeumStatus.Clear();

            PlayerCardsObj[i].HideElementeum();
            PlayerCardsObj[i].powerAnim.transform.localScale = Vector3.one;
            PlayerCardsObj[i].SetElemtDeault();
            enemyCardsObj[i].HideElementeum();

            PlayerCardsObj[i].SetDownSort();
            enemyCardsObj[i].SetDownSort();

            PlayerCardsObj[i].UnsetSpecialAbilityPosition();
            enemyCardsObj[i].UnsetSpecialAbilityPosition();

            PlayerCardsObj[i].SetAllPowerOff();
            enemyCardsObj[i].SetAllPowerOff();

            PlayerCardsObj[i].ResetAttributeValue();
            enemyCardsObj[i].ResetAttributeValue();

            PlayerCardsObj[i].HideSpecialAbility();
            enemyCardsObj[i].HideSpecialAbility();

            enemyCardsObj[i].debuffValueAttack = 0;
            enemyCardsObj[i].debuffValueHeal = 0;

            PlayerCardsObj[i].DiceValue = 0;
            enemyCardsObj[i].DiceValue = 0;
            //MyCardsObj[i].GetComponent<CardObject>().UsingElementeum = 0;
            //enemyCardsObj[i].UsingElementeum = 0;

            PlayerCardsObj[i].isSpActive = false;
            enemyCardsObj[i].isSpActive = false;

            PlayerCardsObj[i].SpAbilityObj.gameObject.SetActive(false);

            PlayerManager.instance.spParticals.SetActive(false);
            PlayerManager.instance.spParticals.SetActive(false);

        }

        if (SelectedCard == null)
        {

        }
        else
        {
            SelectedCard.remain_Elemt = 0;
            for (int i = 0; i < SelectedCard.CardDetail.elementeum; i++)
            {
                //SelectedCard.elementeumChild[i].GetComponent<RectTransform>().DOAnchorPos(new Vector3(0,0,0),0.1f);
                SelectedCard.elementeumChild[i].canClickElementeum = true;
                SelectedCard.elementeumChild[i].selectEle = true;
                SelectedCard.elementeumChild[i].GetComponent<Button>().interactable = true;
            }


        SelectPowerManager.instance.selectedCardAnimator = null;
        }
        if (SelectedEnemyCard == null)
        {

        }
        else
        {
            SelectedEnemyCard.remain_Elemt = 0;

        SelectPowerManager.instance.EnemyselectedCardAnimator = null;
        }

        EnemyCardPosSet(enemyCardsObj[0].transform, new Vector3(-3.03f, 5.9f, 14.9f), 0.3f, new Vector3(0.5f, 0.55f, 0.5f), Quaternion.Euler(25f, 0f, 0));
        EnemyCardPosSet(enemyCardsObj[1].transform, new Vector3(0, 5.9f, 14.9f), 0.3f, new Vector3(0.5f, 0.55f, 0.5f), Quaternion.Euler(25f, 0f, 0));
        EnemyCardPosSet(enemyCardsObj[2].transform, new Vector3(3.03f, 5.9f, 14.9f), 0.3f, new Vector3(0.5f, 0.55f, 0.5f), Quaternion.Euler(25f, 0f, 0));



        InGameCards.instance.revertAllAttributeVisualsValue();
        InGameCards.instance.companion.text = "0";
        InGameCards.instance.comp = 0;
        InGameCards.instance.Elemtium.text = "0";
        InGameCards.instance.elem = 0;
        SelectedCard = null;
        SelectedEnemyCard = null;
        SelectedHealCard = null;
        SelectedHealEnemyCard = null;
        for (int i = 0; i < CompanionCards.playerCompanionCards.Length; i++)
        {
            CompanionCards.AutoDeselectCompanionCardsForBattle(CompanionCards.playerCompanionCards[i]);

        }
        for (int i = 0; i < CompanionCards.enemyCompanionCards.Length; i++)
        {
            CompanionCards.AutoDeselectEnemyCompanionCardsForBattle(CompanionCards.enemyCompanionCards[i]);

        }
        for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
        {

            PlayerManager.instance.enemyCardsObj[i].companionData.Clear();
            PlayerManager.instance.enemyCardsObj[i].companionIDs.Clear();
        }
    }

    void EnemyCardPosSet(Transform card, Vector3 pos, float v, Vector3 scale, Quaternion rot)
    {
        // Debug.Log("=========");


        card.localScale = scale;

        card.rotation = rot;

        card.DOMove(pos, v);

        //  Debug.Log(card.localPosition);
    }

    public void SocketClickOnBG()
    {

        Message = "clickonbg";
    }


    public void CheckCardsHeart() //Final check every cards health to see who wins and who lost
    {
        Debug.Log("--------Card Health Check------------");

        if (PlayerCardsObj[0].GetComponent<CardObject>().CardDetail.heart <= 0 && PlayerCardsObj[1].GetComponent<CardObject>().CardDetail.heart <= 0 && PlayerCardsObj[2].GetComponent<CardObject>().CardDetail.heart <= 0)
        {
            BattleLobbySC.Instance.GameComplete("opp");
        }
        else if (enemyCardsObj[0].GetComponent<CardObject>().CardDetail.heart <= 0 && enemyCardsObj[1].GetComponent<CardObject>().CardDetail.heart <= 0 && enemyCardsObj[2].GetComponent<CardObject>().CardDetail.heart <= 0)
        {
            BattleLobbySC.Instance.GameComplete("me");
        }

        int meVal = 0;

        int OppVal = 0;

        for (int i = 0; i < 3; i++)
        {
            if (PlayerCardsObj[i].GetComponent<CardObject>().CardDetail.heart <= 0)
            {
                meVal += 1;
            }
            if (enemyCardsObj[i].CardDetail.heart <= 0)
            {
                OppVal += 1;
            }
        }

        if (OppVal >= 2)
        {
            FinalOppIndicator.SetActive(true);
        }

        if (meVal >= 2)
        {
            FinalMeIndicator.SetActive(true);
        }

    }


    public void FinalBattleScreenSHow(string whowon)
    {
        for (int i = 0; i < FinalObjToDisable.Length; i++)
        {
            FinalObjToDisable[i].SetActive(false);
        }

        FinalBattleEndScreen.SetActive(true);

        MyProfile.texture = BattleLobbySC.Instance.Mytext;

        OppProfile.texture = BattleLobbySC.Instance.opText;

        MeName.text = BattleLobbySC.Instance.Finaldata.user_data.player_name;
        OppName.text = BattleLobbySC.Instance.Finaldata.opp_user_data.player_name;

        if (whowon == "me")
        {
            MeResult.text = "Won";
            OppResult.text = "Lost";
        }
        else if (whowon == "draw")
        {
            OppResult.text = "draw";
            MeResult.text = "draw";
        }
        else if (whowon == "opp")
        {
            OppResult.text = "Won";
            MeResult.text = "Lost";
        }

    }


    // If Not Selected Any Power Btn than..
    public void EnemyCardAndCanvasOrderSet()
    {

        for (int i = 0; i < EnemyCardCanvas.Length; i++)
        {
            enemyCardsObj[i].GetComponent<SpriteRenderer>().sortingOrder = 1;
            EnemyCardCanvas[i].sortingOrder = 3;
            enemyCardsObj[i].GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    private void AllAnimCardFalse()
    {
        mainAnim.SetBool("Card0", false);
        mainAnim.SetBool("Card1", false);
        mainAnim.SetBool("Card2", false);
        mainAnim.SetBool("CardPosBack", false);

    }

    void OnMainAnim()
    {
        mainAnim.enabled = true;
    }

    public void Home()
    {


        if (BattleLobbySC.Instance)
        {
            BattleLobbySC.Instance.UserGameQuit();
            Debug.Log("Home");
            BattleLobbySC.Instance.DisconnectSocket();
        }
        RestartGame();


    }

    public void FinalHome()
    {

    }

    IEnumerator DelayHome()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(0);
    }

    public void victory()
    {
        //BattleLobbySC.Instance.message = "victory";
        BattleLobbySC.Instance.MessageQuoue.Enqueue(new KeyValuePair<string, string>("victory", ""));

    }
    public void RestartGame()
    {

        Destroy(UserDataController.Instance.gameObject);
        Destroy(CardManagerSC.Instance.gameObject);
        Destroy(BattleLobbySC.Instance.gameObject);
        Destroy(ConnectOpenGame.Instance.gameObject);

        UserDataController.Instance = null;
        CardManagerSC.Instance = null;
        BattleLobbySC.Instance = null;
        ConnectOpenGame.Instance = null;

        SceneManager.LoadScene(0);
    }
}
