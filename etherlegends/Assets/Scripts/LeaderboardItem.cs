﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardItem : MonoBehaviour
{
    public Text Id,Txt_name,Txt_Battle,Txt_win,Txt_Loss,Txt_Streak,Txt_EP;
    string UserId;

    //public GameObject ProfilePanel;
    public Image CardImage;

    byte[] bytes = new byte[0];
    public Texture2D profile_imgTemp;
    [SerializeField]
    public class UserID
    {
       public string user_id;
    }


    public void SetValue(string id,Texture2D ProfileImg, string name,string Userid, string battle,string win,string loss,string streak,string EP)
    {



        //Clone of Texture
        bytes = ProfileImg.EncodeToPNG();
        profile_imgTemp = new Texture2D(1, 1);
        profile_imgTemp.LoadImage(bytes);

        profile_imgTemp.name = ProfileImg.name;


        Sprite sprite = Sprite.Create(profile_imgTemp, new Rect(0, 0, profile_imgTemp.width, profile_imgTemp.height), new Vector2(0.5f, 0.5f));
        CardImage.sprite = sprite;

        UserId = Userid;

        Id.text = id;
        //Img_Image.texture  = ProfileImg;
        Txt_name.text = name;
        Txt_Battle.text = battle;
        Txt_win.text = win;
        Txt_Loss.text = loss;
        Txt_Streak.text = streak;
        Txt_EP.text = EP;

       
    }

    private void OnDestroy()
    {
        if (CardImage.sprite.texture!=null && CardImage.sprite.name!= "default")
        {
            bytes = null;
            Destroy(CardImage.sprite.texture);
            System.GC.Collect();
        }

    }


    public async void OpenProfilePanel()
    {
        
        LeaderboardMain.instance.profilePanel.SetActive(true);
        LoadingManager.Instance.ShowLoader("Please Wait...");
        UserID Jobj = new UserID();

        Jobj.user_id = UserId;

        string json = JsonUtility.ToJson(Jobj);
        string response = await SessionManager.Instance.Post(ConstantManager._url_getOtherUserProfile, json, SessionManager.Instance.userToken, true);



        JSONNode data = JSON.Parse(response);
        List<Card> Tempcard = new List<Card>();

        for (int i = 0; i < data["data"]["best_card"].Count; i++)
        {
            var newCard = Newtonsoft.Json.JsonConvert.DeserializeObject<Card>(data["data"]["best_card"][i]);
            Tempcard.Add(newCard);
        }


        //Clone of Texture : Detination will handle memory itselt.
        Texture2D textureTemp = new Texture2D(1, 1);
        textureTemp.LoadImage(bytes);
        textureTemp.name = (profile_imgTemp) ? profile_imgTemp.name : "default";
        
        LeaderboardMain.instance.profilePanel.GetComponent<OtherUserProfile>().SetValue(textureTemp, UserId, Txt_name.text, Txt_Battle.text, Txt_win.text, Txt_Streak.text, Txt_EP.text, Tempcard);
        LoadingManager.Instance.HideLoader();
        LeaderboardMain.instance.ResetAll();

        LeaderboardMain.instance.gameObject.SetActive(false);


    }

}
