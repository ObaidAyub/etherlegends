﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardInfoSc : MonoBehaviour
{

    public GameObject CardInfoObj;
    public RawImage CardImg,WalletQRCode;
    public Text CardName,cardBalance,walletAddress;
    Texture2D CardImage;
    Card CardData;
            

    public void SetCardvalue(Texture2D CdImage,Card cardInfo)
    {
        CardImage = CdImage;
        CardData = cardInfo;
    }

    public void showInfoPanel()
    {
        if (CardData.value==0)
        {
            return;
        }
        CardInfoObj.SetActive(true);
        CardImg.texture = CardImage;
        CardName.text = CardData.name;
        cardBalance.text = CardData.value.ToString();
        CardInfoObj.GetComponent<SendErcCard>().CardData = CardData;
        walletAddress.text = UserDataController.Instance.GetWalletAddress();
        WalletQRCode.texture = UserDataController.Instance.GenerateBarcode(walletAddress.text, WalletQRCode.mainTexture.width, WalletQRCode.mainTexture.height);
    }


    public void HideInfoPanel()
    {
        CardInfoObj.SetActive(false);
    }

}
