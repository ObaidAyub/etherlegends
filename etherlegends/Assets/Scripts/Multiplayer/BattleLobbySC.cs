﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System.Threading.Tasks;
using System;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.Networking;
using System.Linq;
using EtherSocketPackets;

[Serializable]
public class stplay
{
    public string socket_id;
    public string message;
}

[Serializable]
public class SocketResponse
{
    public string status;
    public string message;
    public string data;
}

public class BattleLobbySC : MonoBehaviour
{

    public static BattleLobbySC Instance;
    public Socket socket;


    public string socketid, Oppsocketid;

    string userid, OppUserid;
    public GameObject MatchmakingWindow, bestCardWindow;
    public Text txt_CountDown;

    public GenericPopUp genericPopup;

    bool startCount;

    int count;
    float timecount;

    public FetchedData Finaldata;

    //BothPlayer Data After MatchMaking
    public RawImage MyImage, OpponentImage;
    public Text MyName, OpponentName;
    public GameObject BattleScreen;
    public Texture2D Mytext, opText;
    public Texture2D DefaultTexture;

    public InGameCards ingamecards;

    //Round End Values
    [HideInInspector]
    public int ElementMe, ElementOpp, MeValue, OppValue, dice_numberOpp, dice_numberMe, companionMe, companionOpp, debuffME, debuffOpp, heartValME, healCardHeartVal, heartValOpp;
    bool oppSpecialAb, MeSpecialAb;


    //public string message, FData, gameCompleteData;
    //string Fdata;

    public Queue<KeyValuePair<string, string>> MessageQuoue = new Queue<KeyValuePair<string, string>>();


    bool showList = false;

    [Header("Trending Player")]
    public GameObject TrendingPanel, TrendItemToSpawn;
    public GameObject NoPlFound;
    public Transform TrendTransform;

    //public delegate void Gotlist(string Data);

    public static event Action<string> FoundList;

    int attrib_valueme;
    int attrib_valueOpp;
    bool IsDisconnected;
    string SyncMainTurn, SyncTurn, SyncTimer, SyncLastAction;
    public DiceRollPacket syncLastActionResponse;
    public bool IsComplete = true;

    public EtherSocketPackets.DiceRollPacket LastAction;

    private void Awake()
    {
        IsComplete = true;
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {

            Instance.MatchmakingWindow = this.MatchmakingWindow;
            Instance.bestCardWindow = this.bestCardWindow;
            Instance.txt_CountDown = this.txt_CountDown;
            Instance.BattleScreen = this.BattleScreen;
            Instance.MyImage = this.MyImage;
            Instance.OpponentImage = this.OpponentImage;
            Instance.MyName = this.MyName;
            Instance.OpponentName = this.OpponentName;
            Instance.TrendingPanel = this.TrendingPanel;
            Instance.TrendItemToSpawn = this.TrendItemToSpawn;
            Instance.TrendTransform = this.TrendTransform;
            Instance.NoPlFound = this.NoPlFound;
            Destroy(this.gameObject);
        }
        FoundList += FillList;
    }



    private void OnDisable()
    {
        FoundList -= FillList;
    }

    private void Start()
    {
        ConnectSocketid();

    }


    bool showPopup = false;
    string popupMsg = "";

    public void ShowEventPopUp(string message = "")
    {
        //MessageText.text = message;
        Debug.LogError(message + " PoPUp");
        showPopup = true;
        popupMsg = message;



    }




    void ConnectSocketid()
    {

        if (socket == null)
        {
            Debug.Log("In socket condition");
            IO.Options options = new IO.Options();

            socket = IO.Socket(ConstantManager._url_SocketUrl, options);

            socket.On(Socket.EVENT_CONNECT, () =>
            {
                Debug.Log("connection");

                socket.On("connected", (data) =>
                {
                    Debug.Log("Inside COnnected --" + data.ToString());
                    JSONNode Jnode = JSON.Parse(data.ToString());
                    socketid = Jnode["socket_id"];
                    //Fdata = data.ToString();
                    //message = "connect";

                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("connect", data.ToString()));
                });
            });

        }
        else
        {
            Debug.Log("socket" + socket);
        }


    }

    public void ConnectSocket()
    {
        if (socket == null)
        {

            Debug.Log("AfterConnect Start");

            IO.Options options = new IO.Options();
            options.Reconnection = false;
            options.Timeout = 50;

            socket = IO.Socket(ConstantManager._url_SocketUrl, options);
            Debug.Log(socket);
            socket.On(Socket.EVENT_CONNECT, () =>
            {
                socket.On("connected", (data) =>
                {

                    Debug.Log("Inside Socket COnnected --" + data.ToString());

                    //Fdata = data.ToString();
                    //message = "connect";

                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("connect", data.ToString()));

                });
            });
        }
        else
        {
            Debug.Log("Else COnnect");
            Debug.Log(socket);
        }
    }

    public void LogoutImmediately()
    {
        //socket.Off("logoutImmediately");


        socket.On("logoutImmediately", (data) =>
        {
            Debug.Log(" logout Immediately from Remote " + data.ToString());

            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {
                if (BattleLobbySC.Instance != null && BattleLobbySC.Instance.IsComplete)
                {
                    //message = "logoutCall";
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("logoutCall", data.ToString()));
                    AuthenticationManager.Instance.logoutAndBlockText.text = Jnode["message"];
                }
            }
        });
    }


    public void UserQuitResponse()
    {
        socket.On("afterQuitUser", (data) =>
        {

            Debug.Log(data.ToString());

            Debug.Log("Opponent User Quit");

            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {
                Debug.Log("IsComplete-->" + IsComplete);
                //message = "oppquit";
                MessageQuoue.Enqueue(new KeyValuePair<string, string>("oppquit", data.ToString()));

            }

        });
    }

    public void UserLostFocusResponse()
    {
        //Debug.Log("UserLostFocusResponse " + message);
        socket.On("afterPlayerFocus", (data) =>
        {
            Debug.Log("User Lost FOcus" + data.ToString());
            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1 && Jnode["user_id"] != userid)
            {

                if (Jnode["player_status"] == "1")
                {
                    Debug.Log("User Lost Response");
                    //message = "UserLost";
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("UserLost", data.ToString()));

                }

                else if (Jnode["player_status"] == "2")
                {
                    //message = "UserFound";
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("UserFound", data.ToString()));

                }

            }
        });

        socket.On("afterReconnectUser", (data) =>
        {
            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {

                InGameCards.instance.PlayCountTimer();

                Oppsocketid = Jnode["opp_user_socket_id"];

                //message = "UserFound";
                MessageQuoue.Enqueue(new KeyValuePair<string, string>("UserFound", data.ToString()));

                SendUserSync();
            }

        });

        socket.On("afterOppositeUserDisconnect", (data) =>
        {
            if (/*message == "" &&*/ !IsComplete)
            {
                Debug.Log("--------Complete   -->" + IsComplete);
                Debug.Log("AfterOppositeUserDisconnect--->" + data.ToString());

                JSONNode Jnode = JSON.Parse(data.ToString());

                if (Jnode["status"] == 1)
                {
                    //InGameCards.instance.PauseCountTimer();

                    //message = "UserLost";
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("UserLost", data.ToString()));
                }
            }

        });

        AfterUserSync();
        ReceiveUserSync();
    }


    public void ReceiveUserSync() //Opponent will receive this socket to sync timer
    {
        socket.On("afterUserSync", (data) =>
        {
            Debug.LogError("AfterUserSync " + data.ToString());
            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {
                UserSyncResponse();
            }
        });
    }

    public void SendUserSync() //Method to Call When User Reconnects after disconnected while playing game
    {
        //Debug.LogError("SendUserSync Method called");
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            return;
        }

        JObject Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        if (InGameCards.instance.GetMainTurn())
        {
            Jobj.Add("main_turn", "opp");
        }
        else
        {
            Jobj.Add("main_turn", "me");
        }

        if (InGameCards.instance.PTurn())
        {
            Jobj.Add("turn", "opp");
            //Jobj.Add("user_timer", InGameCards.instance.OppTimer.text.ToString());
        }
        else
        {
            Jobj.Add("turn", "me");
            //Jobj.Add("user_timer", InGameCards.instance.MyTimer.text.ToString());
        }

        if (LastAction == null)
            LastAction =null;

        Jobj.Add("last_action", LastAction.ToJObject());

        //Jobj.Add("uset_turn", );
        Debug.LogError("userSync " +  Jobj);


        socket.Emit("userSync", Jobj);

        socket.On("afterUserSync", (data) =>
        {
            Debug.Log("Me UserSync " + data.ToString());
            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {

            }
        });

    }

    public void UserSyncResponse()
    {
        socket.Off("afterUserSyncResponse");

        JObject Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        if (InGameCards.instance.PTurn())
        {
            if (InGameCards.instance.GetMainTurn())
            {
                Jobj.Add("main_turn", "opp");
                Jobj.Add("turn", "opp");
            }
            else
            {
                Jobj.Add("main_turn", "me");
                Jobj.Add("turn", "opp");
            }

            //Jobj.Add("user_timer", InGameCards.instance.OppTimer.text.ToString());
        }
        else
        {
            if (InGameCards.instance.GetMainTurn())
            {
                Jobj.Add("main_turn", "opp");
                Jobj.Add("turn", "me");
            }
            else
            {
                Jobj.Add("main_turn", "me");
                Jobj.Add("turn", "me");
            }

            //Jobj.Add("user_timer", InGameCards.instance.MyTimer.text.ToString());
        }

        if (LastAction == null)
            LastAction = null;

        Jobj.Add("last_action", LastAction.ToJObject());
        Debug.LogError("userSyncResponse " + Jobj);

        //Jobj.Add("uset_turn", );
        socket.Emit("userSyncResponse", Jobj);

        socket.On("afterUserSyncResponse", (data) =>
        {
            //Debug.Log("me UserSyncResponse " + data.ToString());

            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {
                //InGameCards.instance.PauseCountTimer();
                //PlayerManager.instance.CanClick = false;
            }
        }
       );

    }

    public void AfterUserSync()
    {
        socket.On("afterUserSyncResponse", (data) =>
        {
            Debug.LogError("Opp UserSyncResponse " + data.ToString());

            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {
                SyncMainTurn = Jnode["main_turn"];
                SyncTurn = Jnode["turn"];

                SyncLastAction = Jnode["last_action"].ToString();

                syncLastActionResponse = JsonConvert.DeserializeObject<DiceRollPacket>(SyncLastAction);
                
                MessageQuoue.Enqueue(new KeyValuePair<string, string>("sync", data.ToString()));

            }
        }
       );
    }

    public void DisconnectSocket()
    {

        Debug.Log("Socket Disconnected From Function");

        Time.timeScale = 1;

        if (socket == null)
            return;

        socket.Disconnect();
        socket = null;

    }

    public void OffSocket(string Sname)
    {

        socket.Off("afterSearchingPlayer");

        Debug.Log(Sname);
        socket.Off(Sname);
        startCount = false;

        JObject Job = new JObject();

        Job.Add("user_id", userid);
        Job.Add("socket_id", socketid);

        socket.Emit("escapeEvent", Job);

        socket.On("afterEscapeEvent", (data) =>
        {
            Debug.Log("Escape Event ---" + data.ToString());
        });

        NoPlayerFound();
    }

    public void NoPlayerFound()
    {
        socket.Off("afterNoPlayerFound");

        JObject Job = new JObject();
        Job.Add("user_id", userid);
        Job.Add("socket_id", socketid);
        socket.Emit("noPlayerFound", Job);

        socket.On("afterNoPlayerFound", (data) =>
        {
            Debug.Log(data.ToString());
        });

    }

    public void OnDestroy()
    {

        Debug.Log("Destroyeddd");
        //logoutAsync();
        if (socket != null)
            socket.Disconnect();
    }



    public void OnApplicationQuit()
    {
        Debug.Log("ApplicationQuit");

        // logoutAsync();
        if (socket != null)
            socket.Disconnect();

    }

    public void SocketMessage(string Messg)
    {


        var Jobj = new JObject();

        Jobj.Add("user_id", userid);
        Jobj.Add("socket_id", socketid);
        Jobj.Add("player_message", Messg);

        socket.Off("afterSendMessage");

        //Debug.Log("SendMessage " + Messg);
        socket.Emit("sendMessage", Jobj);

        socket.On("afterSendMessage", (data) =>
        {
            //Debug.Log("afterSendMessage" + data);
            JSONNode Jnode = JSON.Parse(data.ToString());
            if (Jnode["player_message"] == "turnchange")
            {
                Debug.Log("TurnChange");
                ingamecards.oldEnemyAnimUpdate("msg", "turnchange");
            }
            if (Jnode["player_message"] == "mainturnchange")
            {
                Debug.Log("mainTurnChange");
                ingamecards.oldEnemyAnimUpdate("msg", "mainturnchange");
            }
        });

        //Debug.Log(Jobj);

    }
    public void UserTime(string Time)
    {
        socket.Off("afterSendTimer");
        var Jobj = new JObject();

        Jobj.Add("user_id", userid);
        Jobj.Add("socket_id", socketid);
        Jobj.Add("time", Time);


        socket.Emit("sendTimer", Jobj);

        socket.On("afterSendTimer", (data) =>
        {
        });


    }


    public void GameComplete(string WhoWon)
    {

        Debug.Log("Game Completed " + WhoWon);
        if (socket != null)
            socket.Off("afterCompleteGame");
        var Jobj = new JObject();

        if (WhoWon == "me")
        {

            Jobj.Add("win_user_id", userid);
            Jobj.Add("win_socket_id", socketid);
            Jobj.Add("lost_user_id", OppUserid);
            Jobj.Add("lost_socket_id", Oppsocketid);
            Jobj.Add("isDraw", 1);
            //message = "victory";
            
        }
        else if (WhoWon == "draw")
        {
            Jobj.Add("win_user_id", OppUserid);
            Jobj.Add("win_socket_id", Oppsocketid);
            Jobj.Add("lost_user_id", userid);
            Jobj.Add("lost_socket_id", socketid);
            Jobj.Add("isDraw", 2);
            //message = "draw";
        }
        else
        {

            Jobj.Add("win_user_id", OppUserid);
            Jobj.Add("win_socket_id", Oppsocketid);
            Jobj.Add("lost_user_id", userid);
            Jobj.Add("lost_socket_id", socketid);
            Jobj.Add("isDraw", 1);
           

            //message = "defeat";
        }

        Debug.Log(Jobj);

        socket.Emit("completeGame", Jobj);

        socket.On("afterCompleteGame", (data) =>
        {

            Debug.Log(data.ToString());
            
            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {
                if (Jnode["data"]["win_user_id"] == userid)
                {
                    if (Jnode["data"]["isDraw"] == 2)
                    {
                        MessageQuoue.Enqueue(new KeyValuePair<string, string>("draw", Jobj.ToString()));
                    }
                    else
                    {

                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("victory", data.ToString()));
                    }

                }

                else if (Jnode["data"]["win_user_id"] == OppUserid)
                {
                    if (Jnode["data"]["isDraw"] == 2)
                    {
                        MessageQuoue.Enqueue(new KeyValuePair<string, string>("draw", Jobj.ToString()));
                    }
                    else
                    {

                        MessageQuoue.Enqueue(new KeyValuePair<string, string>("defeat", data.ToString()));
                    }
                }
            }

            //socket.Disconnect();
        });

    }

    //------------------------Timer Logics------------------

    public void StopAllTimer()
    {
        ingamecards.StopAllTimer();
    }

    public void PauseCountTimer()
    {
        ingamecards.PauseCountTimer();
    }

    public void PlayCountTimer()
    {
        ingamecards.PlayCountTimer();
    }


    //--------------------------

    //new structure of data sending is implement 
    public void PlayerOneDiceNumber(string PlyCardContract, string PlyHealedCardContract,AttributeType Attrib, string deBuffValue,string OppCardContract,
        Dictionary<string, int> ElementeumsKV, List<string> CompanionCardId,
        string dicenumber,string AttribNum , bool elementeum, string earn_elementeum, string elementeumNumber, string companionValue,  string isSkirmish)
    {

        socket.Off("afterSelectPlayerDiceNumber");

        Debug.Log("Player One Dice....");


        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;

        packet.PlayerCardId = PlyCardContract;
        packet.healing_card_contract_id = PlyHealedCardContract;
        packet.PlayerAttribute = Attrib;
        packet.debuff_value = deBuffValue;
        packet.EnemyCardId = OppCardContract;
        packet.ElementeumData = ElementeumsKV;
        packet.CompanionData = CompanionCardId;

        packet.PlayerDice = dicenumber;
        packet.earn_elementeum = earn_elementeum;
        packet.elementeum_number = elementeumNumber;

        packet.isSkirmish = isSkirmish;
        packet.attribute_number = AttribNum;

        Debug.Log(packet.ToJObject());

        socket.Emit("selectPlayerDiceNumber", packet.ToJObject());



        socket.On("afterSelectPlayerDiceNumber", (data) =>
        {

            Debug.Log("afterDice" + data.ToString());

            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.None)
                {
                    Debug.Log("Already ");
                }
                else
                {
                    if (true)
                    {

                    }
                    StartCoroutine(DiceManager.instance.TieTUrnChange(false));
                }

            }

        });

    }


    //--------------------------Player Two Socket---------------------------
    //new structure of data sending is implement 
    public void PlayerTwoAttribSel(string cardContract, AttributeType Attrib)
    {

        socket.Off("afterSelectPlayerTwoAttribute");

        Debug.Log("Player Two Card....");
        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.PlayerCardId = cardContract;
        packet.PlayerTwoAttribute = Attrib;

        Debug.Log(packet.ToJObject());

        socket.Emit("selectPlayerTwoAttribute", packet.ToJObject());


        socket.On("afterSelectPlayerTwoAttribute", (data) =>
        {
            Debug.Log("afterSelectPlayerTwoAttribute" + data);

            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {

                Debug.Log(data.ToString());

                DiceManager.instance.dice2.DOScale(new Vector3(0.38f, 0.38f, .038f), 0.5f);
                PlayerManager.instance.CanClickonEnemyDice = true;

            }

        });

    }

    //[TODO: new structure of data sending is remaining ]
    public void PlayerTwoDiceNumber(string playeroneCardID, string PlayerOneHealedCard,AttributeType PlayerOneAttri, string PlayerOneAttriNum, string PlayerOnedeBuffValue, 
        Dictionary<string, int> PlayerOneElementeumsKV, List<string> PlayerOneCompanionCardId,
        string PlayerOnedicenumber, string PlayerOnedEarn_elementeum, string PlayerOnedElementeumNumber, string PlayerOnedCompanionValue, bool PlayerOneSpecialAbilityUsed,
        string PlayerOneHeartValue,string PlayerOneMaxHealth, string HealCardHeartValue,
        string playerTwoCardID, AttributeType PlayerTwoAttri, string PlayerTwoAttriNum, string PlayerTwodeBuffValue,
        Dictionary<string, int> PlayerTwoElementeumsKV, List<string> PlayerTwoCompanionCardId,
        string PlayerTwodicenumber, string PlayerTwodEarn_elementeum, string PlayerTwodElementeumNumber, string PlayerTwodCompanionValue, bool PlayerTwoSpecialAbilityUsed,
        string PlayerTwoHeartValue, string PlayerTwoMaxHealth, string IsSkirmish)
    {

        socket.Off("afterSelectPlayerTwoDiceNumber");
        Debug.Log("afterSelectPlayerTwoDiceNumber....");

        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;

        packet.PlayerCardId = playeroneCardID;
        packet.healing_card_contract_id = PlayerOneHealedCard;
        packet.PlayerAttribute = PlayerOneAttri; // opp att
        packet.attribute_number = PlayerOneAttriNum;
        packet.debuff_value = PlayerOnedeBuffValue;
        packet.ElementeumData = PlayerOneElementeumsKV;
        packet.CompanionData = PlayerOneCompanionCardId;
        packet.PlayerDice = PlayerOnedicenumber;
        packet.earn_elementeum = PlayerOnedEarn_elementeum;
        packet.elementeum_number = PlayerOnedElementeumNumber;
        packet.companion_value = PlayerOnedCompanionValue;
        packet.special_power_used = PlayerOneSpecialAbilityUsed;
        packet.player_one_heart_value = PlayerOneHeartValue;
        packet.player_one_max_health = PlayerOneMaxHealth;
        packet.heal_card_max_health = HealCardHeartValue;

        packet.EnemyCardId = playerTwoCardID;
        packet.PlayerTwoAttribute = PlayerTwoAttri; // opp att
        packet.player_two_attribute_number = PlayerTwoAttriNum;
        packet.player_Two_debuff_value = PlayerTwodeBuffValue;
        packet.Player_two_ElementeumData = PlayerTwoElementeumsKV;
        packet.Player_two_CompanionData = PlayerTwoCompanionCardId;
        packet.PlayerTwoDice = PlayerTwodicenumber;
        packet.player_two_earn_elementeum = PlayerTwodEarn_elementeum;
        packet.player_two_elementeum_number = PlayerTwodElementeumNumber;
        packet.player_two_companion_value = PlayerTwodCompanionValue;
        packet.player_two_special_power_used = PlayerTwoSpecialAbilityUsed;
        packet.player_two_heart_value = PlayerTwoHeartValue;
        packet.player_two_max_health = PlayerTwoMaxHealth;

        packet.isSkirmish = IsSkirmish;


        Debug.Log(packet.ToJObject());

        socket.Emit("selectPlayerTwoDiceNumber", packet.ToJObject());

        socket.On("afterSelectPlayerTwoDiceNumber", (data) =>
        {
            Debug.Log(">>>>>>>>>>>EMIT>>>>>>>>>>>>");
            Debug.Log("afterDice>>" + data.ToString());
        EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                dice_numberMe = int.Parse(ReceivePacket.data.PlayerTwoDice);
                attrib_valueme = int.Parse(ReceivePacket.data.player_two_attribute_number); 
                ElementMe = int.Parse(ReceivePacket.data.player_two_elementeum_number);     
                companionMe = int.Parse(ReceivePacket.data.player_two_companion_value);     
                debuffME = int.Parse(ReceivePacket.data.player_Two_debuff_value);           

                heartValME = int.Parse(ReceivePacket.data.player_two_heart_value);

                oppSpecialAb = ReceivePacket.data.special_power_used;
                MeSpecialAb = ReceivePacket.data.player_two_special_power_used; 


                Debug.Log("ME--" + dice_numberMe + " " + (oppSpecialAb ? 0 : (attrib_valueme - debuffME)) + " " + ElementMe   + " heart value " + heartValME );

                MeValue = dice_numberMe + (oppSpecialAb ? 0 : (attrib_valueme - debuffME)) + ElementMe + companionMe;

                //Opp Data
                dice_numberOpp = int.Parse(ReceivePacket.data.PlayerDice);
                attrib_valueOpp = int.Parse(ReceivePacket.data.attribute_number);
                ElementOpp = int.Parse(ReceivePacket.data.elementeum_number); 
                companionOpp = int.Parse(ReceivePacket.data.companion_value);
                debuffOpp = int.Parse(ReceivePacket.data.debuff_value);


                heartValOpp = int.Parse(ReceivePacket.data.player_one_heart_value);

                Debug.Log("Opp--" + dice_numberOpp + " " + (MeSpecialAb ? 0 : (attrib_valueOpp - debuffOpp)) + " " + ElementOpp + " heart value " + heartValOpp);

                OppValue = dice_numberOpp + (MeSpecialAb ? 0 : (attrib_valueOpp - debuffOpp)) + ElementOpp + companionOpp;

                if (PlayerManager.instance.SelectedHealEnemyCard != null)
                    healCardHeartVal = int.Parse(ReceivePacket.data.heal_card_heart_value);
                

                

                if (MeValue > OppValue)
                {
                    Debug.Log("Me won");
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("won", ""));

                }

                else
                {
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("lost", ""));

                    Debug.Log("Me Lost");
                }

            }
            else if (ReceivePacket.status == 2)
            {
                heartValME = int.Parse(ReceivePacket.data.player_two_heart_value);
                heartValOpp = int.Parse(ReceivePacket.data.player_one_heart_value);
                Debug.Log("Tie");
                MessageQuoue.Enqueue(new KeyValuePair<string, string>("tie", ""));

            }
            else if (ReceivePacket.status == 3)
            {
                Debug.Log("skirmish Mode is Enalble ");

                dice_numberMe = int.Parse(ReceivePacket.data.PlayerTwoDice);
                heartValME = int.Parse(ReceivePacket.data.player_two_heart_value);
                MeValue = dice_numberMe;

                dice_numberOpp = int.Parse(ReceivePacket.data.PlayerDice);
                heartValOpp = int.Parse(ReceivePacket.data.player_one_heart_value);
                OppValue = dice_numberOpp;

                if (MeValue > OppValue)
                {
                    Debug.Log("Me won");
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("won", ""));
                }

                else
                {
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("lost", ""));
                    Debug.Log("Me Lost");
                }

            }


        });
    }



    IEnumerator SendMesgOFTie() // Send a tie message and mainTurn Change after 2 sec
    {
        yield return new WaitForSeconds(3f);
        SocketMessage("mainturnchange");
    }


    //public JObject ToJObject(object obj) {
    //    return JObject.Parse(JsonConvert.SerializeObject(obj));
    //}

    //new structure of data sending is implement 
    public void PlayerOneCardSelect(string cardContract)
    {
        socket.Off("afterSelectPlayerOneCard");
        Debug.Log(" selectPlayerOneCard   " + cardContract);

        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.PlayerCardId = cardContract;
        Debug.Log("Player Onr Card...." + packet.ToString());
        BattleLobbySC.Instance.LastAction = packet;
        socket.Emit("selectPlayerOneCard", packet.ToJObject());// DataPacket.ToString());
       

        socket.On("afterSelectPlayerOneCard", (data) =>
        {
            Debug.Log("afterSelectPlayerOneCard " + data.ToString());
            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                PlayerManager.instance.CanClickOnMeAttrib = true;
              
               
            }
            else
            {
                Debug.LogError("TODO: afterSelectPlayerOneCard is null or wrong do something if data not send to other player. ");
            }
        });
    }

    //new structure of data sending is implement 
    public void PlayerOneAttributeSelect(AttributeType Attrib, string cardContract, string DebuffValue = "0")
    {
        socket.Off("afterSelectPlayerOneAttribute");

        Debug.Log(" selectPlayerOneCard   " + cardContract);

        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.PlayerCardId = cardContract;
        packet.PlayerAttribute = Attrib;
        packet.debuff_value = DebuffValue;

        Debug.Log(packet.ToJObject());
        BattleLobbySC.Instance.LastAction = packet;

        socket.Emit("selectPlayerOneAttribute", packet.ToJObject());

        socket.On("afterSelectPlayerOneAttribute", (data) =>
        {
            Debug.Log("afterSelectPlayerOneAttribute " + data.ToString());
            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                if (Attrib != AttributeType.Heal)
                    PlayerManager.instance.CanClickOnEnemyCard = true;
            }
            else
            {
                Debug.LogError("TODO: afterSelectPlayerOneAttribute is null or wrong do something if data not send to other player. ");
            }
        });
    }

    //new structure of data sending is implement 
    public void OppositeCardSelect(string OppCardContract, string PlyCardContract, AttributeType Attrib, string DebuffValue, Dictionary<string, int> ElementeumsKV, List<string> CompanionCardId)
    {
        socket.Off("afterSelectOppositePlayerCard");

        Debug.Log(" afterSelectOppositePlayerCard   ");
        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.PlayerCardId = PlyCardContract;
        packet.PlayerAttribute = Attrib;
        packet.debuff_value = DebuffValue;
        packet.EnemyCardId = OppCardContract;
        packet.ElementeumData = ElementeumsKV;
        packet.CompanionData = CompanionCardId;

        Debug.Log(packet.ToJObject());
        BattleLobbySC.Instance.LastAction = packet;

        socket.Emit("selectOppositePlayerCard", packet.ToJObject());

        socket.On("afterSelectOppositePlayerCard", (data) =>
        {
            Debug.Log("afterSelectOppositePlayerCard" + data.ToString());

            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                PlayerManager.instance.CanClickonMeDice = true;
            }
            else
            {
                Debug.LogError("TODO: afterSelectOppositePlayerCard is null or wrong do something if data not send to other player. ");
            }

        });
    }

    //new structure of data sending is implement 
    public void HealCardSelect(string HealcardContract, string HealerCardContractId, AttributeType Attrib, string DebuffValue)
    {
        socket.Off("afterSelectPlayerOneHealCard");

        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.healing_card_contract_id = HealcardContract;
        packet.PlayerCardId = HealerCardContractId;
        packet.PlayerAttribute = Attrib;
        packet.debuff_value = DebuffValue;

        Debug.Log(packet.ToJObject());
        BattleLobbySC.Instance.LastAction = packet;

        socket.Emit("selectPlayerOneHealCard", packet.ToJObject());


        socket.On("afterSelectPlayerOneHealCard", (data) =>
        {

            Debug.Log("afterSelectPlayerOneHealCard" + data.ToString());

            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                PlayerManager.instance.CanClickonMeDice = true;
            }
            else
            {
                Debug.LogError("TODO: afterSelectPlayerOneHealCard is null or wrong do something if data not send to other player. ");
            }

        });
    }


    //new structure of data sending is implement 
    public void DisruptCardSelect(string cardContract)
    {
        socket.Off("afterSelectPlayerTwoDisruptCard");
        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.disrupt_card_contract_id = cardContract;

        Debug.Log(packet.ToJObject());
        BattleLobbySC.Instance.LastAction = packet;

        socket.Emit("selectPlayerTwoDisruptCard", packet.ToJObject());

        
        socket.On("afterSelectPlayerTwoDisruptCard", (data) =>
        {
            Debug.Log("afterSelectPlayerTwoDisruptCard" + data.ToString());
            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
            }
            else
            {
                Debug.LogError("TODO: afterSelectPlayerTwoDisruptCard is null or wrong do something if data not send to other player. ");

                Debug.Log(ReceivePacket.message);
            }
        });
    }

    //new structure of data sending is implement 
    public void CompanionCardSelected(List<string> CompanionCardId, string cardContract, AttributeType Attrib, string debuffVal, string EnemtCardContract, Dictionary<string, int> ElementeumsKV)
    {
        socket.Off("afterSelectPlayerCompanionCard");

        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.PlayerCardId = cardContract;
        packet.PlayerAttribute = Attrib;
        packet.debuff_value = debuffVal;
        packet.EnemyCardId = EnemtCardContract;
        packet.ElementeumData = ElementeumsKV;
        packet.CompanionData = CompanionCardId;

        Debug.Log(packet.ToJObject());
        BattleLobbySC.Instance.LastAction = packet;

        socket.Emit("selectPlayerCompanionCard", packet.ToJObject());

        socket.On("afterSelectPlayerCompanionCard", (data) =>
        {
            Debug.Log("afterSelectPlayerCompanionCard" + data.ToString());
            
            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                Debug.Log(ReceivePacket.message.ToString());

            }
            else
            {
                Debug.LogError("TODO: afterSelectPlayerCompanionCard is null or wrong do something if data not send to other player. ");

            }
        });
    }

    //[TODO: new structure of data sending is remaining ]
    public void SendLoginEmit(string userid)
    {
        var Jobj = new JObject();
        socket.Emit("login", userid);
        Debug.Log(userid + " emit is working ");
    }

    //Socket to Send Quit Request
    //[TODO: new structure of data sending is remaining ]
    public void UserGameQuit()
    {
        if (socket == null)
        {
            return;
        }
        socket.Off("afterQuitUser");

        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        Debug.Log(Jobj);

        socket.Emit("quitUser", Jobj);

        socket.On("afterQuitUser", (data) =>
        {
            Debug.Log(data.ToString());
            Debug.Log("Opponent User Quit");
            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)
            {
                Debug.Log("-----" + Jnode["status"]);

            }
        });
    }

    //new structure of data sending is implement 
    public void ElementeumSelected(string cardContract, AttributeType Attrib, string diceNum, string debuffVal, string EnemtCardContract, Dictionary<string, int> ElementeumsKV, List<string> CompanionId)
    {
        socket.Off("afterElementeumUsedNumber");

        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.PlayerCardId = cardContract;
        packet.PlayerAttribute = Attrib;
        packet.debuff_value = debuffVal;
        packet.EnemyCardId = EnemtCardContract;
        packet.ElementeumData = ElementeumsKV;
        packet.CompanionData = CompanionId;

        Debug.Log(packet.ToJObject());
        BattleLobbySC.Instance.LastAction = packet;

        socket.Emit("elementeumUsedNumber", packet.ToJObject());


        socket.On("afterElementeumUsedNumber", (data) =>
        {
            Debug.Log(data.ToString());
            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                Debug.Log(ReceivePacket.message.ToString());

            }
            else
            {
                Debug.LogError("TODO: afterSelectOppositePlayerCard is null or wrong do something if data not send to other player. ");

            }
        });
    }

    //[TODO: new structure of data sending is remaining ]
    async void GetTrendingPlayer()
    {
        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        Debug.Log(Jobj);

        socket.Emit("topTrendingMMRNumber", Jobj);


        socket.On("afterTopTrendingMMRNumber", (data) =>
        {
            JSONNode Jnode = JSON.Parse(data.ToString());

            Debug.Log(data);

            if (Jnode["status"] == 1)
            {
                //FoundList(data.ToString());
                //message = "found";
                //Fdata = data.ToString();
                MessageQuoue.Enqueue(new KeyValuePair<string, string>("found", data.ToString()));
            }



        });



    }

    public void FillList(string ListData)
    {

        Debug.Log("found");
        TrendingPanel.SetActive(true);
        JSONNode Jnode = JSON.Parse(ListData.ToString());

        // Jnode = JSON.Parse(Jnode["data"]);
        for (int i = 0; i < Jnode["data"].Count; i++)
        {
            Debug.Log("--------");
            GameObject NewListItem = Instantiate(TrendItemToSpawn, TrendTransform);
            NewListItem.transform.GetChild(0).GetComponent<Text>().text = (i + 1).ToString();
            NewListItem.transform.GetChild(1).GetComponent<Text>().text = Jnode["data"][i][0];
            NewListItem.transform.GetChild(2).GetComponent<Text>().text = Jnode["data"][i][1];
            NewListItem.SetActive(true);
        }
    }

    //---------------------------------------------------Socket off for Player One---------------------------

    public void PlayerOneUnsubscribeEvents()
    {
        socket.Off("afterSelectPlayerOneCard");
        socket.Off("afterSelectPlayerOneAttribute");
        socket.Off("afterSelectPlayerDiceNumber");
        socket.Off("afterSelectOppositePlayerCard");
        //socket.Off("afterSelectPlayerTwoAttribute");
        socket.Off("afterSelectPlayerCompanionCard");
        socket.Off("afterSelectPlayerCardDebuffValue");
    }

    //---------------------------------------------------Socket On events to Opposite Player---------------------------

    public void PlayerTwoAnim()
    {
        //socket.Off("afterReconnectUser");

        socket.Off("afterSelectPlayerOneCard");
        socket.Off("afterSelectPlayerOneAttribute");
        socket.Off("afterSelectPlayerDiceNumber");
        socket.Off("afterSelectOppositePlayerCard");
        socket.Off("afterCompleteGame");
        socket.Off("afterElementeumUsedNumber");
        socket.Off("afterSendMessage");
        socket.Off("afterSendTimer");
        socket.Off("afterSelectPlayerTwoAttribute");
        socket.Off("afterSelectPlayerTwoDiceNumber");
        socket.Off("afterSelectPlayerOneHealCard");
        socket.Off("afterSelectPlayerTwoDisruptCard");
        socket.Off("afterSpecialPowerCancel");
        socket.Off("afterSpecialPowerUsed");
        socket.Off("afterSelectPlayerCompanionCard");
        socket.Off("afterSelectPlayerCardDebuffValue");
        //messtatusz; playertwo";            

        Debug.Log("---My turn--->" + ingamecards.PTurn());


        // socket event for card selection from the oppoenet
        //new structure of data reciving is implement 
        socket.On("afterSelectPlayerOneCard", (data) =>
        {
            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }
        });

        // socket event for attribute selection from the oppoenet
        //new structure of data reciving is implement 
        socket.On("afterSelectPlayerOneAttribute", (data) =>
        {
            Debug.Log("afterSelectPlayerOneAttribute--" + data.ToString());
            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }
        });

        // socket event for player card selection from the oppoenet for attacked
        //new structure of data reciving is implement 
        socket.On("afterSelectOppositePlayerCard", (data) =>
        {
            Debug.Log("afterSelectOppositePlayerCard " + data.ToString());

            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }
        });


        //new structure of data reciving is implement 
        socket.On("afterElementeumUsedNumber", (data) =>
        {

            Debug.Log("afterElementeumUsedNumber " + data.ToString());
            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }
        });

      
        socket.On("afterSendMessage", (data) =>
        {

            //Debug.Log("afterSendMessage " + data);
            JSONNode Jnode = JSON.Parse(data.ToString());

            if (Jnode["status"] == 1)// && !ingamecards.PTurn())
            {


                if (Jnode["player_message"] == "clickonbg")
                {
                    Debug.Log("clicked on bg");
                    PlayerManager.instance.SocketClickOnBG();
                }
                else if (Jnode["player_message"] == "turnchange")
                {
                    Debug.Log("socket event call for TurnChange");
                    ingamecards.oldEnemyAnimUpdate("msg", "turnchange");
                }
               else if (Jnode["player_message"] == "mainturnchange")
                {
                    Debug.Log("socket event call for mainturnchange");
                    ingamecards.oldEnemyAnimUpdate("msg", "mainturnchange");
                }
                else
                {
                    //Debug.Log(int.Parse(Jnode["player_message"]));
                   //ingamecards.Oppcounter = int.Parse(Jnode["player_message"]);
                   
                }

            }
        });
        socket.On("afterSendTimer", (data) =>
        {
            JSONNode Jnode = JSON.Parse(data.ToString());
            
            if (Jnode["status"] == 1)
            {
                string time = Jnode["data"];
                ingamecards.Oppcounter = int.Parse(Jnode["data"]);
            }
        });


        //socket response of PLayer Two Attribute Selection
        //new structure of data reciving is implement 
        socket.On("afterSelectPlayerTwoAttribute", (data) =>
        {
            Debug.Log("afterSelectPlayerTwoAttribute" + data.ToString());

            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
                DiceManager.instance.dice2.DOScale(new Vector3(0.38f, 0.38f, .038f), 0.5f);

            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }
        });

        //socket response of Player Heal Card Select
        //new structure of data reciving is implement 
        socket.On("afterSelectPlayerOneHealCard", (data) =>
        {

            Debug.Log("afterSelectPlayerOneHealCard" + data.ToString());

            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }

        });


        //socket response of Player Disrupt Card
        //new structure of data reciving is implement 
        socket.On("afterSelectPlayerTwoDisruptCard", (data) =>
        {
            Debug.Log("afterSelectPlayerTwoDisruptCard" + data);
            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }

        });

        //[TODO: new structure of data reciving is remaining ]
        socket.On("afterSelectPlayerTwoDiceNumber", (data) =>
         {

             Debug.Log("afterDiceTwo Roll --" + data.ToString());

             EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
             if (packet.status == 1)
             {
                 ingamecards.RollDicePacketHandler(packet.data);

                 dice_numberMe = int.Parse(packet.data.PlayerDice);         
                 attrib_valueme = int.Parse(packet.data.attribute_number);  
                 MeSpecialAb = (packet.data.special_power_used);            
                 ElementMe = int.Parse(packet.data.elementeum_number);      
                 companionMe = int.Parse(packet.data.companion_value);      
                 debuffME = int.Parse(packet.data.debuff_value);
                 heartValME = int.Parse(packet.data.player_one_heart_value);


                 oppSpecialAb = packet.data.player_two_special_power_used;

                 Debug.Log("ME--" + dice_numberMe + " " + (oppSpecialAb ? 0 : attrib_valueme) + " " + debuffME + " " + ElementMe + " " + companionMe);

                 Debug.Log("ME--" + dice_numberMe + " " + (oppSpecialAb ? 0 : (attrib_valueme - debuffME)) + " " + ElementMe + " " + companionMe);


                 MeValue = dice_numberMe + (oppSpecialAb ? 0 : (attrib_valueme - debuffME)) + ElementMe + companionMe;

                 dice_numberOpp = int.Parse(packet.data.PlayerTwoDice);
                 attrib_valueOpp = int.Parse(packet.data.player_two_attribute_number); 
                 ElementOpp = int.Parse(packet.data.player_two_elementeum_number);     
                 companionOpp = int.Parse(packet.data.player_two_companion_value);     
                 debuffOpp = int.Parse(packet.data.player_Two_debuff_value);
                 heartValOpp = int.Parse(packet.data.player_two_heart_value);

                 if (PlayerManager.instance.SelectedHealCard != null)
                     healCardHeartVal = int.Parse(packet.data.heal_card_heart_value);

                 Debug.Log("Opp--" + dice_numberOpp + " " + (MeSpecialAb ? 0 : attrib_valueOpp) + " " + debuffOpp + " " + ElementOpp + " " + companionOpp);

                 Debug.Log("Opp--" + dice_numberOpp + " " + (MeSpecialAb ? 0 : (attrib_valueOpp - debuffOpp)) + " " + ElementOpp + " " + companionOpp);



                 OppValue = dice_numberOpp + (MeSpecialAb ? 0 : (attrib_valueOpp - debuffOpp)) + ElementOpp + companionOpp;


                 Debug.Log("Opp--" + OppValue + " Me " + MeValue);


                 if (MeValue > OppValue)
                 {
                     Debug.Log("Me won");
                     MessageQuoue.Enqueue(new KeyValuePair<string, string>("won", data.ToString()));
                 }

                 else
                 {
                     MessageQuoue.Enqueue(new KeyValuePair<string, string>("lost", data.ToString()));
                     Debug.Log("Me Lost");
                 }
             }
             else if (packet.status == 2)
             {
                 ingamecards.RollDicePacketHandler(packet.data);

                 heartValME = int.Parse(packet.data.player_one_heart_value);
                 heartValOpp = int.Parse(packet.data.player_two_heart_value);

                 MessageQuoue.Enqueue(new KeyValuePair<string, string>("tie", data.ToString()));

             }
             else if (packet.status == 3)
             {

                 ingamecards.RollDicePacketHandler(packet.data);

                 Debug.Log("skirmish Mode is Enable ");

                 dice_numberMe = int.Parse(packet.data.PlayerDice);
                 heartValME = int.Parse(packet.data.player_one_heart_value);
                 MeValue = dice_numberMe;



                 dice_numberOpp = int.Parse(packet.data.PlayerTwoDice);
                 heartValOpp = int.Parse(packet.data.player_two_heart_value);
                 OppValue = dice_numberOpp;
                 Debug.Log("skirmish Mode is Enable 2 mevalue  " + MeValue + " " + OppValue);

                 if (MeValue > OppValue)
                 {
                     Debug.Log("Me won");
                     MessageQuoue.Enqueue(new KeyValuePair<string, string>("won", data.ToString()));
                 }

                 else
                 {
                     MessageQuoue.Enqueue(new KeyValuePair<string, string>("lost", data.ToString()));
                     Debug.Log("Me Lost");
                 }
             }
             else
             {
                 Debug.LogError("TODO: packet.status = 0");
             }
         });

        //new structure of data reciving is implement
        //[TODO: add more data in sending if required  ]
        socket.On("afterSelectPlayerDiceNumber", (data) =>
        {

            Debug.Log("afterSelectPlayerDiceNumber " + data.ToString());
            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
                if (SelectPowerManager.instance.selectedPowerName == AttributeType.None)
                {
                    PlayerManager.instance.CanClickOnMeAttrib = true;
                }
                else
                {
                    PlayerManager.instance.CanClickonEnemyDice = true;
                }
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }

        });

        //[TODO: new structure of data reciving is remaining ]
        socket.On("afterCompleteGame", (data) =>
        {

            Debug.Log("after game complete socket event data " + data.ToString());
            var gameCompleteData = data.ToString();
            JSONNode Jnode = JSON.Parse(gameCompleteData);

            if (Jnode["status"] == 1)
            {
                if (Jnode["data"]["win_user_id"] == userid)
                {
                    //message = "victory";
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("victory", data.ToString()));
                }

                else if (Jnode["data"]["win_user_id"] == OppUserid)
                {
                    //message = "defeat";
                    MessageQuoue.Enqueue(new KeyValuePair<string, string>("defeat", data.ToString()));
                }
            }
            //socket.Disconnect();
        });


        //[TODO: new structure of data reciving is remaining ]
        socket.On("afterSpecialPowerUsed", (data) =>
        {
            Debug.Log("afterSpecialPowerUsed--" + data.ToString());
            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: " + packet.message);
            }
        });

        socket.On("afterSpecialPowerCancel", (data) =>
        {
            Debug.Log("After Special cancle--" + data.ToString());
           
            JSONNode jNode = JSON.Parse(data.ToString());

            if (jNode["status"] == 1)
            {

                //Fdata = data.ToString();
                //message = "specialabilitycancel";
                MessageQuoue.Enqueue(new KeyValuePair<string, string>("specialabilitycancel", data.ToString()));
            }
            else
            {
                Debug.Log(data.ToString());
            }
        });


        /// Companion data socket On
        /// 
        #region Companion Data Socket on

        //new structure of data reciving is implement
        socket.On("afterSelectPlayerCompanionCard", (data) =>
        {
            Debug.Log("afterSelectPlayerCompanionCard " + data.ToString());

            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }
        });


        #endregion
        //new structure of data reciving is implement
        socket.On("afterSelectPlayerCardDebuffValue", (data) =>
        {
            EtherSocketPackets.SocketEventPacket packet = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());
            if (packet.status == 1)
            {
                ingamecards.RollDicePacketHandler(packet.data);
            }
            else
            {
                Debug.LogError("TODO: packet.status = 0");
            }
        });

    }
    //new structure of data reciving is implement
    public void UseSpecialAbility(string cardContract, AttributeType AbilityName)
    {
        socket.Off("afterSpecialPowerUsed");

        Debug.Log("Special Ability------" + AbilityName);


        EtherSocketPackets.DiceRollPacket packet = new EtherSocketPackets.DiceRollPacket();
        packet.user_id = userid;
        packet.socket_id = socketid;
        packet.PlayerCardId = cardContract;
        packet.special_power = AbilityName;  // its  attribute name just for special power its now special_power
        Debug.Log(packet.ToJObject());
        BattleLobbySC.Instance.LastAction = packet;

        socket.Emit("specialPowerUsed", packet.ToJObject());


        socket.On("afterSpecialPowerUsed", (data) =>
        {
            Debug.Log("afterSpecialPowerUsed " + data.ToString());
            EtherSocketPackets.SocketEventPacket ReceivePacket = Newtonsoft.Json.JsonConvert.DeserializeObject<EtherSocketPackets.SocketEventPacket>(data.ToString());

            if (ReceivePacket.status == 1)
            {
                Debug.Log(ReceivePacket.message);
            }
            else
            {
                Debug.LogError("TODO: afterSelectPlayerOneAttribute is null or wrong do something if data not send to other player. ");
            }
        });

    }

    public void PlayerLostFocus(string status)
    {

        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        Jobj.Add("player_status", status);

        socket.Emit("playerFocus", Jobj);

        socket.On("afterPlayerFocus", (data) =>
        {
            Debug.Log("Player Lost Focus---->>" + data.ToString());
        });

    }

    public void PlayerReconnected(string SocketId)
    {
        //socket.Off("afterReconnectUser");
        Debug.Log("Reconnected " + SocketId);
        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        socket.Emit("reconnectUser", Jobj);

        socket.On("afterReconnectUser", (data) =>
        {
            Debug.Log("afterReconnectUser----->" + data.ToString());

        });

    }

    public void CancelSpecialAbility(string cardContract, AttributeType AbilityName)
    {
        socket.Off("afterSpecialPowerCancel");

        Debug.Log("---------Special Ability Cancel ------");

        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);
        socket.Off("afterSpecialPowerCancel");

        Jobj.Add("card_contract_id", PlayerManager.instance.SelectedCard.CardDetail.card_contract_id);

        socket.Emit("specialPowerCancel", Jobj);


        socket.On("afterSpecialPowerCancel", (data) =>
        {
            Debug.Log("After Special cancle--" + data.ToString());

            JSONNode jNode = JSON.Parse(data.ToString());

            if (jNode["status"] == 1)
            {
                Debug.Log(data.ToString());
            }
            else
            {
                Debug.Log(data.ToString());
            }
        });

    }

    public void StartMatchMaking()
    {
        Debug.Log("In MatchMaching()" + socket + " " + socketid);

        if (socket == null || string.IsNullOrEmpty(socketid))
        {
            Debug.Log("Connect Socket In MatchMaking");
            ConnectSocket();
            return;
        }
        bestCardWindow.SetActive(false);
        MatchmakingWindow.SetActive(true);
        startCount = true;
        timecount = 0;
        showList = false;
        userid = UserDataController.Instance.profileData._id;
        Debug.Log("UserId " + userid);

        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        Debug.Log(Jobj);

        socket.Emit("startPlay", Jobj);

        socket.On("connect_failed", (data) =>
        {
            Debug.Log("Error " + data);
        });

        socket.On("error", (data) =>
        {
            Debug.Log("Another Error " + data);
            if (data.ToString().Contains("Network subsystem is down"))
            {
                Time.timeScale = 0;
            }
        });


        socket.On("afterStartPlay", (data) =>
        {
            Debug.Log(data.ToString());
            Debug.Log("afterStartPlay");
            JSONNode Jnode = JSON.Parse(data.ToString());
            Debug.Log(Jnode["status"]);

            if (Jnode["status"] == 1)
            {
                SearchPlayer();
            }

        });

    }
   

    private void Update()
    {

        if (showPopup)
        {
            showPopup = false;


            Instantiate(genericPopup).MessageText.text = popupMsg;
            popupMsg = "";
        }


        if (startCount)
        {

            timecount += Time.deltaTime;

            txt_CountDown.text = "" + timecount.ToString("0");

            if (timecount >= 25 && !showList)
            {
                GetTrendingPlayer();
                showList = true;
            }

            if (timecount >= 30)
            {

                MatchmakingWindow.GetComponent<BackButtonSc>().BackBtn.SetActive(true);
                //socket.Off("afterStartPlay");
                // LoadingManager.Instance.ShowLoader("Request Timeout!" +
                //   "Try Again");
                // bestCardWindow.SetActive(true);
                //MatchmakingWindow.SetActive(false);
                //startCount = false;
                // timecount = 0;
                //StartCoroutine(DisableLoading());
            }

        }
        if (IsComplete == false)
        {


            if (Application.internetReachability == NetworkReachability.NotReachable)
            {

                if (!IsDisconnected)
                {
                    InGameCards.instance.SetCounter(2, false);
                    PlayerManager.instance.NoInternet.SetActive(true);
                    InGameCards.instance.PauseCountTimer();
                    PlayerManager.instance.CanClick = false;
                    IsDisconnected = true;
                    Debug.Log("INTERNET IS NOT WORKING ");
                }

            }
            else
            {
                if (IsDisconnected)
                {
                    Invoke("PlayCountTimer", 4f);
                    PlayerManager.instance.NoInternet.SetActive(false);

                    if (!InGameCards.instance.PTurn())
                        PlayerManager.instance.CanClick = true;

                    IsDisconnected = false;
                }
            }
        }
        //if (string.IsNullOrEmpty(message))
        //{
        //    return;
        //}

        if (MessageQuoue.Count < 1) {
            return;
        }

        var nextMessage = MessageQuoue.Dequeue();
        var message = nextMessage.Key;
        var Fdata = nextMessage.Value;

        Debug.Log(Fdata);
        if (message == "battle")
        {
            message = "";

            startCount = false;

            timecount = 0;

            //Debug.LogError(FData);
            //FetchedInfo inffo = JsonUtility.FromJson<FetchedInfo>(FData);
            FetchedInfo inffo = JsonConvert.DeserializeObject<FetchedInfo>(Fdata);


            Finaldata = inffo.data;

            OppUserid = inffo.data.opp_user_data.user_id;

            Oppsocketid = inffo.data.opp_user_data.socket_id;

            MyName.text = inffo.data.user_data.player_name;

            OpponentName.text = inffo.data.opp_user_data.player_name;

            Debug.Log("My Profile----" + inffo.data.user_data.profile_picture);

            StartCoroutine(setMyImage(inffo.data.user_data.profile_picture));
            StartCoroutine(setOpponentImage(inffo.data.opp_user_data.profile_picture));

            message = "";

        }

        else if (message == "decide")
        {

            socket.Off("afterFirstPlayerSelected");

            Debug.Log("Decide Start--------------------");

            JSONNode node = JSON.Parse(Fdata);

            if (node["status"] == 2)
            {
                ingamecards.DecideWhichPlayerWon("equal", node["dice_number"], node["dice_number"]);
            }
            else
            {
                FetchedInfo inffo = JsonUtility.FromJson<FetchedInfo>(Fdata);
                int msc, osc;


                msc = int.Parse(inffo.data.user_data.decide_dice_number);
                osc = int.Parse(inffo.data.opp_user_data.decide_dice_number);

                if (msc > osc)
                {
                    ingamecards.DecideWhichPlayerWon("me", msc, osc);
                }
                else if (msc == osc)
                {
                    ingamecards.DecideWhichPlayerWon("equal", msc, osc);
                }
                else
                {
                    ingamecards.DecideWhichPlayerWon("opp", msc, osc);
                }
            }
            message = "";
            Debug.Log("Decide End--------------------");
        }

        else if (message == "connect")
        {
            JSONNode Jnode = JSON.Parse(Fdata.ToString());

            string socket_id = Jnode["socket_id"];
            Debug.Log("---Socket Id " + socket_id);

            if (socket_id == socketid && !string.IsNullOrEmpty(socketid))//&& SceneManager.GetActiveScene().buildIndex != 0)
            {
                Debug.Log("-------Reconnect---");
                Time.timeScale = 1;
                message = "reconnect";
            }
            else
            {
                message = "";
            }

            socketid = socket_id;

            Debug.Log(" New Socket Id " + socketid);
            userid = UserDataController.Instance.profileData._id;

        }

        else if (message == "won")
        {
            UserWonRoundHandling();

            message = "";
        }
        else if (message == "lost")
        {
            UserLostRoundHandling();
            message = "";
        }

        else if (message == "tie")
        {
            StartCoroutine(TieDelay());
            message = "";
        }

        else if (message == "victory")
        {
            Debug.Log("Victory");
            Debug.Log(Fdata);
            StartCoroutine(DiceManager.instance.WinAnim(Fdata));
            message = "";
        }

        else if (message == "defeat")
        {
            Debug.Log("Defeat");
            Debug.Log(Fdata);
            StartCoroutine(DiceManager.instance.DefeatAnim(Fdata));
            message = "";
        }

        else if (message == "draw")
        {
            StartCoroutine(DiceManager.instance.DrawAnim());
            message = "";
        }
       

        if (message == "found")
        {
            TrendingPanel.SetActive(true);
            JSONNode Jnode = JSON.Parse(Fdata.ToString());

            if (Jnode["data"].Count == 0)
            {
                NoPlFound.SetActive(true);
            }
            else
            {
                // Jnode = JSON.Parse(Jnode["data"]);
                for (int i = 0; i < Jnode["data"].Count; i++)
                {
                    Debug.Log("--------");
                    GameObject NewListItem = Instantiate(TrendItemToSpawn, TrendTransform);
                    NewListItem.transform.GetChild(0).GetComponent<Text>().text = (i + 1).ToString();
                    NewListItem.transform.GetChild(1).GetComponent<Text>().text = Jnode["data"][i][0];
                    NewListItem.transform.GetChild(2).GetComponent<Text>().text = Jnode["data"][i][1];
                    NewListItem.SetActive(true);
                }
            }

            message = "";
        }
        if (message == "specialabilitycancel")
        {

            Debug.Log("Message SpecialAbility Cancel");

            JSONNode Jnode = JSON.Parse(Fdata);

            for (int i = 0; i < 3; i++)
            {

                if (PlayerManager.instance.enemyCardsObj[i].CardDetail.card_contract_id == Jnode["data"]["card_contract_id"])
                {
                    Debug.Log("Found Special Card");
                    PlayerManager.instance.enemyCardsObj[i].CancelSpecialAbilityRPC();
                }

            }
            message = "";
        }





        if (message == "oppquit")
        {
            Debug.Log("Opponenttt Quittt");
            IsComplete = true;
            Time.timeScale = 1;
            StartCoroutine(UserQuit());
            message = "";
        }


        if (message == "UserLost")
        {

            Debug.Log("Lost Opponenttt");

            if (!IsComplete)
                InGameCards.instance.UserLost();
            message = "";
        }

        else if (message == "UserFound")
        {
            InGameCards.instance.UserFound();
            message = "";
        }

        if (message == "reconnect")
        {
            Debug.Log("Message Reconnected");
            Time.timeScale = 1;
            PlayerReconnected(socketid);
            SendUserSync();
            message = "";
        }

        if (message == "sync")
        {

            //int iSyncTimer = 0;
            //int.TryParse(SyncTimer.Split('\\')[0], out iSyncTimer);

            StartCoroutine(InGameCards.instance.SyncTimer(SyncMainTurn, SyncTurn, syncLastActionResponse));
            message = "";


        }

        if (message == "logoutCall")
        {
            Debug.Log("Logout pop up is apper in the game");
            AuthenticationManager.Instance.LogoutPopup.SetActive(true);

            message = "";
        }

    }

    private void UserLostRoundHandling()
    {
        int Meval = 0;
        int Oppval = 0;

        if (dice_numberMe == 3 || dice_numberMe == 4)
        {
            Meval = 1;
        }
        else if (dice_numberMe == 5 || dice_numberMe == 6)
        {
            Meval = 2;
        }
        if (dice_numberOpp == 3 || dice_numberOpp == 4)
        {
            Oppval = 1;
        }
        else if (dice_numberOpp == 5 || dice_numberOpp == 6)
        {
            Oppval = 2;
        }

        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {

            SelectPowerManager.instance.selectedPowerName = AttributeType.Attack;
            SelectPowerManager.instance.selectedEnemyPowerName = AttributeType.Attack;
        }
        else
        {



            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
            {

                if (PlayerManager.instance.SelectedEnemyCard == null)
                {
                    var objCollection = PlayerManager.instance.enemyCardsObj.Where(x => x != null);
                    if (objCollection.Any())
                    {
                        PlayerManager.instance.SelectedEnemyCard = objCollection.First();
                    }

                    SelectPowerManager.instance.selectedEnemyPowerName = AttributeType.Disrupt;
                }
            }

            if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
            {
                if (PlayerManager.instance.SelectedCard == null)
                {
                    var objCollection = PlayerManager.instance.PlayerCardsObj.Where(x => x != null);
                    if (objCollection.Any())
                    {
                        PlayerManager.instance.SelectedCard = objCollection.First();
                    }
                    SelectPowerManager.instance.selectedPowerName = AttributeType.Disrupt;
                }
            }
        }
        StartCoroutine(DiceManager.instance.LostParticle_Fun_Active(Meval, Oppval, MeSpecialAb, oppSpecialAb));
        StartCoroutine(LostAnimCoroutine());
    }

    private void UserWonRoundHandling()
    {
        int Meval = 0;
        int Oppval = 0;

        Debug.Log(dice_numberMe);
        if (dice_numberMe == 3 || dice_numberMe == 4)
        {
            Meval = 1;
        }
        else if (dice_numberMe == 5 || dice_numberMe == 6)
        {
            Meval = 2;
        }
        Debug.Log(dice_numberOpp);

        if (dice_numberOpp == 3 || dice_numberOpp == 4)
        {
            Oppval = 1;
        }
        else if (dice_numberOpp == 5 || dice_numberOpp == 6)
        {
            Oppval = 2;
        }

        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {

            SelectPowerManager.instance.selectedPowerName = AttributeType.Attack;
            SelectPowerManager.instance.selectedEnemyPowerName = AttributeType.Attack;


        }
        else
        {
            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
            {
                if (PlayerManager.instance.SelectedEnemyCard == null)
                {
                    var objCollection = PlayerManager.instance.enemyCardsObj.Where(x => x != null);
                    if (objCollection.Any())
                    {
                        PlayerManager.instance.SelectedEnemyCard = objCollection.First();
                    }
                    SelectPowerManager.instance.selectedEnemyPowerName = AttributeType.Disrupt;
                }
            }

            if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
            {
                if (PlayerManager.instance.SelectedEnemyCard == null)
                {
                    var objCollection = PlayerManager.instance.PlayerCardsObj.Where(x => x != null);
                    if (objCollection.Any())
                    {
                        PlayerManager.instance.SelectedCard = objCollection.First();
                    }
                    SelectPowerManager.instance.selectedPowerName = AttributeType.Disrupt;
                }
            }
        }
        Debug.Log("After win condition from socket player comes here and its selected power is  " + SelectPowerManager.instance.selectedPowerName);
        Debug.Log("DIceNumber -" + dice_numberMe + " ROund Up -" + Meval);
        StartCoroutine(DiceManager.instance.AttackParticle_Fun_Active(Meval, Oppval, MeSpecialAb, oppSpecialAb));

        StartCoroutine(WinAnimCoroutine());
    }

    IEnumerator TieDelay()
    {
        yield return new WaitForSeconds(2f);

        ingamecards.oldEnemyAnimUpdate("finalresult", "tie");
        yield return new WaitForSeconds(1f);
        InGameCards.instance.MeTieRefreshAttrib(attrib_valueme, ElementMe);
        InGameCards.instance.OppTieRefreshAttrib(attrib_valueOpp, ElementOpp);
    }

    IEnumerator UserQuit()
    {
        IsComplete = true;
        PlayerManager.instance.TextCanvas.SetActive(true);         PlayerManager.instance.TextMessage.text = "Opponent Quit's Game";
         yield return new WaitForSeconds(2f);         int meVal = 0;         int opVal = 0;          for (int i = 0; i < 3; i++)         {
            if (PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().CardDetail.heart <= 0)             {                 meVal += 1;             }             if (PlayerManager.instance.enemyCardsObj[i].CardDetail.heart <= 0)             {                 opVal += 1;             }         }

        GameComplete("draw");          PlayerManager.instance.TextCanvas.SetActive(false); 
    }

    IEnumerator WinAnimCoroutine()
    {
        yield return new WaitForSeconds(4f);

        if (ElementMe > 0)
        {
            Debug.Log("Remove Elementium " + ElementMe + " " + (PlayerManager.instance.SelectedCard.isSpActive ? PlayerManager.instance.SelectedCard.CardDetail.elementeum : PlayerManager.instance.SelectedCard.UsingElementeum));
            PlayerManager.instance.SelectedCard.gameObject.GetComponent<CardObject>().RemoveElementeum(PlayerManager.instance.SelectedCard.isSpActive ? PlayerManager.instance.SelectedCard.CardDetail.elementeum : PlayerManager.instance.SelectedCard.UsingElementeum);
        }
        if (ElementOpp > 0)
        {
            Debug.Log("Remove Elementium " + ElementOpp + " " + (PlayerManager.instance.SelectedEnemyCard.isSpActive ? PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum : PlayerManager.instance.SelectedEnemyCard.UsingElementeum));
            PlayerManager.instance.SelectedEnemyCard.gameObject.GetComponent<CardObject>().RemoveElementeum(PlayerManager.instance.SelectedEnemyCard.isSpActive ? PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum : PlayerManager.instance.SelectedEnemyCard.UsingElementeum);
        }

        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Attack)
        {
            PlayerManager.instance.CanClickOnBG = true;
            PlayerManager.instance.SelectedEnemyCard.gameObject.GetComponent<CardObject>().ApplyDamage(MeValue - OppValue);
        }
        else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {
            PlayerManager.instance.CanClickOnBG = true;
            PlayerManager.instance.SelectedEnemyCard.gameObject.GetComponent<CardObject>().ApplyDamage(1);
            PlayerManager.instance.SelectedHealCard.gameObject.GetComponent<CardObject>().AddHitPoint(MeValue - OppValue);

        }
        DiceManager.instance.ElEarnMe.SetActive(true);

        DiceManager.instance.ElEarnMe.GetComponent<Rotate>().enabled = true;
        PlayerManager.instance.SelectedCard.gameObject.GetComponent<CardObject>().AddElementeum(1);


        PlayerManager.instance.SelectedCard.gameObject.GetComponent<CardObject>().HideElementeum();
        PlayerManager.instance.CheckCardsHeart();
        PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().EndSkirmishMode();


    }

    IEnumerator LostAnimCoroutine()
    {
        yield return new WaitForSeconds(4f);

        if (ElementMe > 0)
        {
            Debug.Log("Remove Elementium " + ElementMe + " " + (PlayerManager.instance.SelectedCard.isSpActive ? PlayerManager.instance.SelectedCard.CardDetail.elementeum : PlayerManager.instance.SelectedCard.UsingElementeum));

            PlayerManager.instance.SelectedCard.gameObject.GetComponent<CardObject>().RemoveElementeum(PlayerManager.instance.SelectedCard.isSpActive ? PlayerManager.instance.SelectedCard.CardDetail.elementeum : PlayerManager.instance.SelectedCard.UsingElementeum);
        }

        if (ElementOpp > 0)
        {
            Debug.Log("Remove Elementium " + ElementOpp + " " + (PlayerManager.instance.SelectedEnemyCard.isSpActive ? PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum : PlayerManager.instance.SelectedEnemyCard.UsingElementeum));

            PlayerManager.instance.SelectedEnemyCard.gameObject.GetComponent<CardObject>().RemoveElementeum(PlayerManager.instance.SelectedEnemyCard.isSpActive ? PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum : PlayerManager.instance.SelectedEnemyCard.UsingElementeum);
        }

        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {
            PlayerManager.instance.SelectedCard.gameObject.GetComponent<CardObject>().ApplyDamage(MeValue - OppValue);
        }
        else
        {


            if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Attack)
            {
                PlayerManager.instance.SelectedCard.gameObject.GetComponent<CardObject>().ApplyDamage(MeValue - OppValue);
            }
            else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
            {
                PlayerManager.instance.CanClickOnBG = true;
                PlayerManager.instance.SelectedHealEnemyCard.gameObject.GetComponent<CardObject>().AddHitPoint(MeValue - OppValue);
                PlayerManager.instance.SelectedCard.gameObject.GetComponent<CardObject>().ApplyDamage(1);


            }
        }


        DiceManager.instance.ELEarnOpp.SetActive(true);
        DiceManager.instance.ELEarnOpp.GetComponent<Rotate>().enabled = true;
        PlayerManager.instance.SelectedEnemyCard.gameObject.GetComponent<CardObject>().AddElementeum(1);


        PlayerManager.instance.SelectedEnemyCard.gameObject.GetComponent<CardObject>().HideElementeum();
        PlayerManager.instance.CheckCardsHeart();

        PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().EndSkirmishMode();

    }

    public void SendDiceNumber(int dicenum, InGameCards igame)
    {
        Debug.Log("SendDiceNumber-->>>");
        ingamecards = igame;

        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        Jobj.Add("decide_dice_number", dicenum); //rm dicenum

        socket.Emit("rollDiceToDecideTurn", Jobj);

        socket.On("afterRollDiceToDecideTurn", (data) =>
        {
            Debug.Log("afterRoll Dice---" + data.ToString());
            JSONNode Jnode = JSON.Parse(data.ToString());
            if (Jnode["status"] == 1)
            {

                // StartCoroutine(ShowBattleScreen(data.ToString()));
                Debug.Log("rolled dice");

            }
        });

        socket.On("afterFirstPlayerSelected", (data2) =>
        {
            Debug.Log("after first player--- " + data2.ToString());
            JSONNode Jnode2 = JSON.Parse(data2.ToString());
            if (Jnode2["status"] == 1 || Jnode2["status"] == 2)
            {
                startCount = false;
                //FData = data2.ToString();
                //Debug.Log("FInal--" + FData);
                //message = "decide";

                MessageQuoue.Enqueue(new KeyValuePair<string, string>("decide", data2.ToString()));


            }
        });
    }

    public void aftermatchStart()
    {
        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        socket.Emit("startPlayingAfterMatchPlayer", Jobj);

        socket.On("afterStartPlayingAfterMatchPlayer", (data) =>
        {
            Debug.Log(data.ToString());
            JSONNode Jnode = JSON.Parse(data.ToString());
            if (Jnode["status"] == 1)
            {
                // StartCoroutine(ShowBattleScreen(data.ToString()));
                Debug.Log("matchstart");
            }
        });

    }

    IEnumerator setMyImage(string url)
    {
        Mytext = DefaultTexture;

        if (string.IsNullOrEmpty(url))
        {
            Mytext = DefaultTexture;

            Debug.Log("Mytext" + Mytext);

            MyImage.texture = Mytext;
        }
        else
        {

            Texture2D texture = new Texture2D(2, 2);

            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);

            if (www.isNetworkError || www.isHttpError)
            {
                opText = DefaultTexture;

            }
            else
            {
                yield return www.SendWebRequest();
                while (!www.isDone)
                {
                    yield return new WaitForEndOfFrame();
                }
                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                    Mytext = DefaultTexture;
                }
                else
                {
                    Mytext = DownloadHandlerTexture.GetContent(www);
                }

            }

            yield return new WaitForSeconds(1f);

            MyImage.texture = Mytext;

            // File.WriteAllBytes(ProfilePicPath, www.bytes);
            //end show Image in texture 2D
        }
    }

    IEnumerator setOpponentImage(string url)
    {
        if (string.IsNullOrEmpty(url))
        {
            opText = DefaultTexture;

            Debug.Log("Mytext" + Mytext);

            OpponentImage.texture = opText;
        }
        else
        {

            Texture2D texture = new Texture2D(2, 2);

            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);

            if (www.isNetworkError || www.isHttpError)
            {
                opText = DefaultTexture;

            }
            else
            {
                yield return www.SendWebRequest();
                while (!www.isDone)
                {
                    yield return new WaitForEndOfFrame();
                }

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                    opText = DefaultTexture;
                }
                else
                {

                    opText = DownloadHandlerTexture.GetContent(www);
                }
            }


        }

        yield return new WaitForSeconds(2f);
        OpponentImage.texture = opText;
        BattleScreen.SetActive(true);
        MatchmakingWindow.SetActive(false);
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Game");
    }

    IEnumerator DisableLoading()
    {
        yield return new WaitForSeconds(2f);
        LoadingManager.Instance.HideLoader();
    }

    public void SearchPlayer()
    {


        Debug.Log("SearchPlayer");

        var Jobj = new JObject();

        Jobj.Add("user_id", userid);

        Jobj.Add("socket_id", socketid);

        socket.Emit("searchingPlayer", Jobj);


        socket.On("afterSearchingPlayer", (data) =>
        {
            Debug.Log(data.ToString());
            JSONNode Jnode = JSON.Parse(data.ToString());
            if (Jnode["status"] == 2)
            {
                // StartCoroutine(ShowBattleScreen(data.ToString()));
                Debug.Log("Found Player");
                //message = "battle";
                //FData = data.ToString();

                MessageQuoue.Enqueue(new KeyValuePair<string, string>("battle", data.ToString()));


                IsComplete = false;
            }

        });
    }
    

}

