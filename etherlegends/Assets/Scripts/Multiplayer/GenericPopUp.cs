﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericPopUp : MonoBehaviour
{
    public static GenericPopUp instance;

    public Text MessageText;

    public Button CloseBtn;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        CloseBtn.onClick.AddListener(DestroyPopUp);
    }
    public void DestroyPopUp()
    {
        Destroy(this.gameObject);
    }
}
