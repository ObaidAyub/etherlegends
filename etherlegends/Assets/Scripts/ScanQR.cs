﻿using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Wizcorp.Utils.Logger;
using SimpleJSON;
using UnityEngine.Android;
using BarcodeScanner.Parser;
using TMPro;
using Nethereum.Contracts;
using Nethereum.JsonRpc.UnityClient;
using Nethereum.Web3;
using Nethereum.Util;
using System.Threading.Tasks;

public class ScanQR : ForgeAndRedeemCard
{


    private IScanner BarcodeScanner;
    public Text TextHeader;
    public RawImage QrImage;
    private ParserResult Result;
    public AudioSource Audio;

    public bool IsGalleryImage;
    public GameObject SuccessSCreen,FailScreen,AskBtns,WaitObj,FinalImageObj;

    public RawImage FinalCardImage; // After QR Redeemed card is confirmed on blockchain
    public Text FoundCardName;

    bool StartTimer;
    public Text HashText;
    string HashLink;
    bool GotData;

    public float TimeToWait;

    public TMP_Text TimerTxt;

    [System.Serializable]
    public class HashSent
    {
        public string txHash;
    }


    private void OnEnable()
    {
        // ScanWithCam();
        // Create a basic scanner
        AskBtns.SetActive(true);
        SuccessSCreen.SetActive(false);
        FailScreen.SetActive(false);
        QrImage.gameObject.SetActive(false);
    }

    public void ScanWithCam()
    {

        AskBtns.SetActive(false);

        QrImage.gameObject.SetActive(true);

        BarcodeScanner = new Scanner();

        BarcodeScanner.Camera.Play();

        // Display the camera texture through a RawImage
        BarcodeScanner.OnReady += (sender, arg) => {
            // Set Orientation & Texture
            QrImage.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
            QrImage.transform.localScale = BarcodeScanner.Camera.GetScale();
            QrImage.texture = BarcodeScanner.Camera.Texture;
            // Keep Image Aspect Ratio
            var rect = QrImage.GetComponent<RectTransform>();
            var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
            rect.sizeDelta = new UnityEngine.Vector2(rect.sizeDelta.x, newHeight);
        };

        // Track status of the scanner
        BarcodeScanner.StatusChanged += (sender, arg) =>
        {
            TextHeader.text = "Status: " + BarcodeScanner.Status;
            Debug.Log("status" + BarcodeScanner.Status);
        };

        Invoke("ClickStart", 2f);

    }

    // Disable Screen Rotation on that screen
    void Awake()
    {

        Application.RequestUserAuthorization(UserAuthorization.WebCam);

        #if PLATFORM_ANDROID

                if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
                {
                    Permission.RequestUserPermission(Permission.Camera);

                }

                if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
                {
                    Permission.RequestUserPermission(Permission.ExternalStorageRead);

                }

                if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
                {
                    Permission.RequestUserPermission(Permission.ExternalStorageWrite);

                }

        #endif

        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
    }

    public void sendQRDataAsync()
    {
        try
        {
            if (TextHeader.text.Contains("redemptionCode"))
            {
                JSONNode Receiveparam = JSON.Parse(TextHeader.text);

                QrImage.gameObject.SetActive(false);
                WaitObj.SetActive(true);
                StartTimer = true;
                StartCoroutine(CountDown());

                StartCoroutine(StartRedeemingCard(Receiveparam["redemptionCode"], Receiveparam["setAddress"]));
            }
            else
            {
                InvalidImage();
            }
           
        }
        catch (Exception ex)
        {
            AskBtns.SetActive(true);
            QrImage.gameObject.SetActive(false);
            FailScreen.SetActive(true);
        }     
    }   


    public void OpenLink()
    {
        Application.OpenURL(HashLink);
    }

    public void Back()
    {
        if (AskBtns.activeInHierarchy)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            AskBtns.SetActive(true);
            QrImage.gameObject.SetActive(false);
        }
    }


    IEnumerator CountDown()
    {
        float Counttime = TimeToWait;

        while(Counttime > 0 && StartTimer)
        {

            TimeSpan t = TimeSpan.FromSeconds(Counttime);
            if (Counttime<=1)
            {                    
                WaitObj.SetActive(false);
                SuccessSCreen.SetActive(true);
                FailScreen.SetActive(false);
                StartTimer = false;
                StopCoroutine(CountDown());
            }
            TimerTxt.text = t.Minutes.ToString("00") + ":" + t.Seconds.ToString("00");
            yield return new WaitForSeconds(1f);
            Counttime--;
        }

    }

   

    

    public void GetQRFromGallery()
    {

        BarcodeScanner = new Scanner();

        if (Application.isEditor)
        {
            if (BarcodeScanner == null)
            {               
                Debug.Log("null Scanner");
            }           
            BarcodeScanner.ScanImageQR((Texture2D)QrImage.mainTexture);
            return;
        }

        ClickStop();
        //GetComponent<NativeToolkitExample>().OnQRImagePress();
    }

    /// <summary>
    /// The Update method from unity need to be propagated to the scanner
    /// </summary>
    
    void Update()
    {

        if (IsGalleryImage && QrImage != null && BarcodeScanner.QRValue == null)
        {
            IsGalleryImage = false;
            print("ScanQR ISGallery");
            if (QrImage.mainTexture)
                BarcodeScanner.ScanImageQR((Texture2D)QrImage.mainTexture);
        }

        if (BarcodeScanner == null)
        {
            return;
        }

        if (BarcodeScanner.QRValue == null)
        {
            BarcodeScanner.Update();

        }

        else if (!GotData)
        {
            TextHeader.text = BarcodeScanner.QRValue;
            sendQRDataAsync();
            GotData = true;
        }


    }


    #region UI Buttons

    public void ClickStart()
    {     

        GotData = false;

        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Start");
            return;
        }
        
        // Start Scanning
        BarcodeScanner.Scan((barCodeType, barCodeValue) => {
            BarcodeScanner.Stop();
            // QRData QRCode = JsonUtility.FromJson<QRData>(barCodeValue);
            TextHeader.text = barCodeValue;
          
            sendQRDataAsync();
            Debug.Log("result" + TextHeader.text);

        #if UNITY_ANDROID || UNITY_IOS
                    Handheld.Vibrate();
        #endif

        });
    }

    public void ClickStop()
    {
        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Stop");
            return;
        }

        // Stop Scanning
        BarcodeScanner.Stop();
    }


    /// <summary>
    /// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
    /// Trying to stop the camera in OnDestroy provoke random crash on Android
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    public IEnumerator StopCamera(Action callback)
    {
        // Stop Scanning
        QrImage = null;
        BarcodeScanner.Destroy();
        BarcodeScanner = null;

        // Wait a bit
        yield return new WaitForSeconds(0.1f);

        callback.Invoke();
    }

    #endregion

    async void CheckQRCodeRedeem(string transactionHash)
    {     
        HashSent HashS = new HashSent();

        HashS.txHash = transactionHash;

        string JsBody = JsonUtility.ToJson(HashS);

        Debug.Log("Check Send ---------" + JsBody);

        string Response = await SessionManager.Instance.Post(ConstantManager._url_getTransactionConfirmation, JsBody, SessionManager.Instance.userToken, true);

        Debug.Log("Check Response ---" + Response);

        JSONNode Jnode = JSON.Parse(Response);

        if (Jnode["status"] == 1)
        {

            Debug.Log(Jnode["data"]);
            string contId = UserDataController.Instance.GetCardContract(Jnode["data"]);
            Debug.Log("ID" + contId);
            FoundCardName.text = Jnode["data"];

            TransactionClass transactionClass = new TransactionClass();
            transactionClass.tx_hash = transactionHash;
            transactionClass.user_wallet_add = UserDataController.Instance.Myprefs.Address;
            transactionClass.dest_wallet_add = UserDataController.Instance.Myprefs.Address;
            transactionClass.amount = "1";
            transactionClass.trans_type = "RedeemCard:" + FoundCardName.text;

            string Jsonbody = JsonUtility.ToJson(transactionClass);

            string StoreResponse = await SessionManager.Instance.Post(ConstantManager._url_StoreTransaction, Jsonbody, SessionManager.Instance.userToken, true);

            Debug.Log("Save Transaction-->" + StoreResponse);

            FinalCardImage.texture = BlockChainCardSC.Instance.GetCardTexture(contId);
            FinalImageObj.SetActive(true);
        }

    }

    #region RedeemAndForgeCard


    public void InvalidImage()
    {
        StartTimer = false;        
        WaitObj.SetActive(false);
        FailScreen.SetActive(true);
    }

    IEnumerator StartRedeemingCard(string RedeemCode, string ContAddress)
    {

        var url = ConstantManager._url_ETHMainNet;

        var privateKey = UserDataController.Instance.GetPrivateKey(); //rm //"8D2F14C97D6B4AD21B1B4E25FF18606F5B8D3EC891C03C6D35F21DA7F70B2A55";

        var account =UserDataController.Instance.Myprefs.Address; //rm //"0x5B7415F8f61C29Fc7A01B7c67e4B5067ea26e3C9";

        var transactionPurchaseRequest = new TransactionSignedUnityRequest(url, privateKey);

        transactionPurchaseRequest.EstimateGas = false;

        Debug.Log("Redeem COde " + RedeemCode + " Cont Address " + ContAddress);

        var Gas_Price = Web3.Convert.ToWei(5, UnitConversion.EthUnit.Gwei);

        var transactionMessage = new RedeemPhysicalCard
        {
            Beneficiary = account,
            RedemtionCode = RedeemCode,
            GasPrice = Gas_Price,
            Gas = 4_300_00,            
        };

        Debug.Log("GasEstimation--->" + transactionMessage.GasPrice);

        yield return transactionPurchaseRequest.SignAndSendTransaction(transactionMessage, ContAddress);

        var transactionHash = transactionPurchaseRequest.Result;
        HashText.text = transactionHash;
        HashLink = "https://etherscan.io/tx/" + HashText.text;

        if (transactionPurchaseRequest.Exception != null)
        {
            StartTimer = false;
            StopCoroutine(CountDown());
            WaitObj.SetActive(false);
            FailScreen.SetActive(true);
            Debug.Log("Gass Estimate" + transactionPurchaseRequest.EstimateGas);
            Debug.Log("Error " + transactionPurchaseRequest.Exception.Message);
            yield break;
        }

        Debug.Log("Purchase Hash " + transactionHash);

        var transactionReceiptPolling = new TransactionReceiptPollingRequest(url);

        yield return transactionReceiptPolling.PollForReceipt(transactionHash, 2);
       
        var transferReceipt = transactionReceiptPolling.Result;

        if (transactionReceiptPolling.Exception!=null)
        {
            Debug.Log("Transaction Failed ");
        }

        var transferEvent = transferReceipt.DecodeAllEvents<TransferEventDTO>();

        if (transferEvent.Count <= 0)
        {
            Debug.Log("Redeem Failed");
            StartTimer = false;
            StopCoroutine(CountDown());
            WaitObj.SetActive(false);
            FailScreen.SetActive(true);
            yield break;
        }
        else
        {

          

            yield return new WaitForSeconds(2f);
            CheckQRCodeRedeem(transactionHash);

            /*
                WaitObj.SetActive(false);
                StartTimer = false;
                StopCoroutine(CountDown());
                FailScreen.SetActive(false);
                SuccessSCreen.SetActive(true);
                HashText.text = transactionHash;
                HashLink = "https://etherscan.io/tx/" + HashText.text;
            */


        }

        Debug.Log("Transferd amount from event: " + transferEvent[0].Event.Value);

        var getLogsRequest = new EthGetLogsUnityRequest(url);

        var eventTransfer = TransferEventDTO.GetEventABI();
        yield return getLogsRequest.SendRequest(eventTransfer.CreateFilterInput(transactionReceiptPolling.Result.ContractAddress, account));

        var eventDecoded = getLogsRequest.Result.DecodeAllEvents<TransferEventDTO>();

        Debug.Log("Transferd amount from get logs event: " + eventDecoded[0].Event.Value);

    }

    public async void OnSendComplete(TransactionClass transactionClass)
    {

        string Jsonbody = JsonUtility.ToJson(transactionClass);

        string Response = await SessionManager.Instance.Post(ConstantManager._url_StoreTransaction, Jsonbody, SessionManager.Instance.userToken, true);

        Debug.Log("Save Transaction-->" + Response);

        if (!string.IsNullOrEmpty(Response))
        {

        }
    }

    #endregion
}

