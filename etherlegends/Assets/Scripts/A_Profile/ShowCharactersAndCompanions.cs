﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ShowCharactersAndCompanions : MonoBehaviour
{
    public static ShowCharactersAndCompanions instance;
    public Transform characterCardScrollView;
    public SelectedCardSc cardPrefab;
    public Transform companionScrollView;
    public SelectedCardSc companionPrefab;

    public Transform characterTypeScroller;
    public CharacterTab cardtypePrefab;

    public List<GameObject> tabs;

    public Button[] Btn_Panel;
    public GameObject[] Panels;

    public void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        ChangePanel(0);
    }
    public void ChangePanel(int no)
    {
        if (no == 0)
        {
            if (companionScrollView.gameObject.activeInHierarchy)
            {
                ClearCard(companionScrollView);
            }
            else if (characterCardScrollView.gameObject.activeInHierarchy)
            {
                ClearCard(characterCardScrollView);
            }
        }
        for (int i = 0; i < Btn_Panel.Length; i++)
        {
            Btn_Panel[i].interactable = true;
        }
        Btn_Panel[no].interactable = false;
        for (int i = 0; i < Panels.Length; i++)
        {
            Panels[i].SetActive(false);
        }
        Panels[no].SetActive(true);

    }


    public void Characters()
    {
        if (UserDataController.Instance.cardDeck.Count <= 0)
        {
            return;
        }
        if (UserDataController.Instance.sortedCards.Count <= 0)
        {
            return;
        }

        AddPanelInCharacters();
        PopulateCardsData(0);
        tabs[0].GetComponent<Button>().interactable = false;

    }
    public void Companions()
    {
        if (UserDataController.Instance.cardDeck.Count <= 0)
        {
            return;
        }
        if (UserDataController.Instance.sortedCards.Count <= 0)
        {
            return;
        }

        ClearCard(characterCardScrollView);
        if (UserDataController.Instance.cardDeck.Count > 0 || !string.IsNullOrEmpty(UserDataController.Instance.cardDeck[0].s3_Image))
        {

            if (true)
            {
                for (int i = 0; i < UserDataController.Instance.sortedCards.Count; i++)
                {
                    if (UserDataController.Instance.sortedCards[i].IsCompanion())
                    {
                        var CompanionCards = UserDataController.Instance.sortedCards[i].Cards;
                        for (int j = 0; j < CompanionCards.Count; j++)
                        {
                            SelectedCardSc NewCard = Instantiate(companionPrefab, companionScrollView);
                            NewCard.gameObject.SetActive(true);
                            NewCard.CardDetail = CompanionCards[j];


                            if (NewCard.CardDetail.heal > 0)
                            {
                                NewCard.txt_Heal.gameObject.SetActive(true);
                                NewCard.txt_Attack.gameObject.SetActive(false);
                                NewCard.txt_Defense.gameObject.SetActive(false);
                                NewCard.txt_Disrupt.gameObject.SetActive(false);

                            }
                            if (NewCard.CardDetail.attack > 0)
                            {
                                NewCard.txt_Heal.gameObject.SetActive(false);
                                NewCard.txt_Attack.gameObject.SetActive(true);
                                NewCard.txt_Defense.gameObject.SetActive(false);
                                NewCard.txt_Disrupt.gameObject.SetActive(false);

                            }
                            if (NewCard.CardDetail.defence > 0)
                            {
                                NewCard.txt_Heal.gameObject.SetActive(false);
                                NewCard.txt_Attack.gameObject.SetActive(false);
                                NewCard.txt_Defense.gameObject.SetActive(true);
                                NewCard.txt_Disrupt.gameObject.SetActive(false);

                            }
                            if (NewCard.CardDetail.disrupt > 0)
                            {
                                NewCard.txt_Heal.gameObject.SetActive(false);
                                NewCard.txt_Attack.gameObject.SetActive(false);
                                NewCard.txt_Defense.gameObject.SetActive(false);
                                NewCard.txt_Disrupt.gameObject.SetActive(true);

                            }
                            NewCard.txt_CardNumber.gameObject.SetActive(false);
                            NewCard.txt_Level.gameObject.SetActive(false);
                            NewCard.txt_Life.gameObject.SetActive(false);


                            NewCard.RefreshCardDetails(true);
                        }
                    }
                }

            }


        }

    }
    public void ClearCard(Transform Content)
    {
        while (Content.childCount > 0 )
        {
            var child = Content.GetChild(0);
            child.SetParent(null);
            Destroy(child.gameObject);
        }
    }

    public void AddPanelInCharacters()
    {
        if (UserDataController.Instance.sortedCards.Count <= 0)
        {
            return;
        }

        if (tabs != null)
        {
            tabs.Clear();
        }
        ClearCard(characterTypeScroller);
        //Debug.Log(UserDataController.Instance.sortedCards.Count);
        for (int i = 0; i < UserDataController.Instance.sortedCards.Count; i++)
        {
            if (!UserDataController.Instance.sortedCards[i].IsCompanion())
            {
                ////Debug.Log(UserDataController.Instance.sortedCards[i].Name + " " + i);
                CharacterTab NewCard = Instantiate(cardtypePrefab, characterTypeScroller);
               string tabname = UserDataController.Instance.sortedCards[i].Name.Replace("_", " ");

                NewCard.name = tabname;
                tabs.Add(NewCard.gameObject);
                NewCard.tabName = i.ToString();
                NewCard.headingText.text = tabname;
            }

        }
    }


    public void PopulateCardsData(int typeNum)
    {

        ClearCard(companionScrollView);
        ClearCard(characterCardScrollView);

        if (UserDataController.Instance.cardDeck.Count > 0 || !string.IsNullOrEmpty(UserDataController.Instance.cardDeck[0].s3_Image))
        {
            var CharacterCards = UserDataController.Instance.sortedCards[typeNum].Cards;
            for (int j = 0; j < CharacterCards.Count; j++)
            {

                SelectedCardSc NewCard = Instantiate(cardPrefab, characterCardScrollView);
                //NewCard.gameObject.SetActive(true);
                NewCard.CardDetail = CharacterCards[j];
                NewCard.RefreshCardDetails(true);
            }
        }
    }
}
