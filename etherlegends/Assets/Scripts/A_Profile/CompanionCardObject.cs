﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CompanionCardObject : MonoBehaviour
{

    public Vector3 cardPosition;
    public int cardNumber;

    public Card CardDetail;
    public GameObject txt_Attack, txt_Defense, txt_Disrupt, txt_Heal, txt_CardName;
    public SpriteRenderer CardImage;

    public Canvas canvas;
    public SpriteRenderer[] StrengthAttributes;

    public ParticleSystem attackParticles;
    public ParticleSystem defenceParticles;
    public ParticleSystem disruptParticles;
    public ParticleSystem HealParticles;


    public ParticleSystem[] AllParticles
        () {
        return new ParticleSystem[]{attackParticles,
        defenceParticles,
        disruptParticles,
        HealParticles};
    }

    public void OnClick() {
        PlayerCompanionCardsAnim.instance.CompanionCards(this);
    }

    public bool IsCurrentAbilityLessThan(int ElementiumVal) {

        switch (this.CardDetail.strength_1)
        {
            case AttributeType.None:
                return false;
            case AttributeType.Attack:
                return CardDetail.attack <= ElementiumVal;
            case AttributeType.Defense:
                return CardDetail.defence <= ElementiumVal;
            case AttributeType.Disrupt:
                return CardDetail.disrupt <= ElementiumVal;
            case AttributeType.Heal:
                return CardDetail.heal <= ElementiumVal;
        }
        return false;
    }

    public bool isUsed;
    public GameObject lockImage;

    public bool isSelected;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        GetAttributeValue();
    }


    public  void SortingOrder(int sortingOrder)
    {
        CardImage.sortingOrder = sortingOrder;
        StrengthAttributes.ForEach(x => x.sortingOrder = sortingOrder + 1);
        canvas.sortingOrder = sortingOrder + 2;
    }


    public void GetAttributeValue() //AfterAttack Reset Attribute Value
    {

        txt_Attack.GetComponent<TextMeshProUGUI>().text = this.CardDetail.attack.ToString();
        txt_Defense.GetComponent<TextMeshProUGUI>().text = this.CardDetail.defence.ToString();
        txt_Disrupt.GetComponent<TextMeshProUGUI>().text = this.CardDetail.disrupt.ToString();
        txt_Heal.GetComponent<TextMeshProUGUI>().text = this.CardDetail.heal.ToString();
        txt_CardName.GetComponent<TextMeshProUGUI>().text = this.CardDetail.name.ToString();
        //HideElementeum();
        populateCards();
        ////Debug.Log(this.CardDetail.card_contract_id);
        ImageFromDataPath(this.CardDetail.card_contract_id);
    }
    public void populateCards()
    {
        
        if (this.CardDetail.heal > 0)
        {
            this.txt_Heal.gameObject.SetActive(true);
            this.txt_Attack.gameObject.SetActive(false);
            this.txt_Defense.gameObject.SetActive(false);
            this.txt_Disrupt.gameObject.SetActive(false);
            this.StrengthAttributes[0].gameObject.SetActive(false);
            this.StrengthAttributes[1].gameObject.SetActive(false);
            this.StrengthAttributes[2].gameObject.SetActive(false);
            this.StrengthAttributes[3].gameObject.SetActive(true);

            attackParticles.gameObject.SetActive(false);
            defenceParticles.gameObject.SetActive(false);
            disruptParticles.gameObject.SetActive(false);
            HealParticles.gameObject.SetActive(true);

            //HealParticles.Play();
            ////Debug.Log(CardStrengthType.Heal);

        }
        else if (this.CardDetail.attack > 0)
        {
            this.txt_Heal.gameObject.SetActive(false);
            this.txt_Attack.gameObject.SetActive(true);
            this.txt_Defense.gameObject.SetActive(false);
            this.txt_Disrupt.gameObject.SetActive(false);
            this.StrengthAttributes[0].gameObject.SetActive(true);
            this.StrengthAttributes[1].gameObject.SetActive(false);
            this.StrengthAttributes[2].gameObject.SetActive(false);
            this.StrengthAttributes[3].gameObject.SetActive(false);

            attackParticles.gameObject.SetActive(true);
            defenceParticles.gameObject.SetActive(false);
            disruptParticles.gameObject.SetActive(false);
            HealParticles.gameObject.SetActive(false);

            //attackParticles.Play();
            ////Debug.Log(CardStrengthType.Attack);

        }
        else if (this.CardDetail.defence > 0)
        {
            this.txt_Heal.gameObject.SetActive(false);
            this.txt_Attack.gameObject.SetActive(false);
            this.txt_Defense.gameObject.SetActive(true);
            this.txt_Disrupt.gameObject.SetActive(false);
            this.StrengthAttributes[0].gameObject.SetActive(false);
            this.StrengthAttributes[1].gameObject.SetActive(true);
            this.StrengthAttributes[2].gameObject.SetActive(false);
            this.StrengthAttributes[3].gameObject.SetActive(false);

            attackParticles.gameObject.SetActive(false);
            defenceParticles.gameObject.SetActive(true);
            disruptParticles.gameObject.SetActive(false);
            HealParticles.gameObject.SetActive(false);

            //defenceParticles.Play();
            ////Debug.Log(CardStrengthType.Defense);

        }
        else if (this.CardDetail.disrupt > 0)
        {
            this.txt_Heal.gameObject.SetActive(false);
            this.txt_Attack.gameObject.SetActive(false);
            this.txt_Defense.gameObject.SetActive(false);
            this.txt_Disrupt.gameObject.SetActive(true);
            this.StrengthAttributes[0].gameObject.SetActive(false);
            this.StrengthAttributes[1].gameObject.SetActive(false);
            this.StrengthAttributes[2].gameObject.SetActive(true);
            this.StrengthAttributes[3].gameObject.SetActive(false);

            attackParticles.gameObject.SetActive(false);
            defenceParticles.gameObject.SetActive(false);
            disruptParticles.gameObject.SetActive(true);
            HealParticles.gameObject.SetActive(false);

            //distruptParticles.Play();
            ////Debug.Log("disrupt");

        }
        
    }

    private Sprite cardImage;
    public Texture2D textureImg;
    private void ImageFromDataPath(string card_Contract_Id)
    { 
        if (string.IsNullOrEmpty(card_Contract_Id)) return;
        string path = Application.persistentDataPath + "/GameCards" + "/" + card_Contract_Id + ".png";
        string ImgLink = CardDetail.s3_Image;
        string card_contract_id = card_Contract_Id;
        string DIRPath = Path.Combine(Application.persistentDataPath, "GameCards");
        StartCoroutine(setimage(ImgLink, card_contract_id, DIRPath, path));

    }

    IEnumerator setimage(string ImgLink, string card_contract_id, string DIRPath, string FilePath, System.Action onComplete = null)
    {
        if (System.IO.File.Exists(FilePath))
        {
            byte[] bytes = System.IO.File.ReadAllBytes(FilePath);
            textureImg = new Texture2D(1, 1);
            textureImg.LoadImage(bytes);

            if (textureImg != null)
            {
                Sprite sprite = Sprite.Create(textureImg, new Rect(0, 0, textureImg.width, textureImg.height), new Vector2(0.5f, 0.5f));
                this.CardImage.sprite = sprite;
               
            }
            onComplete?.Invoke();
            yield break;
        }


        UnityWebRequest www = UnityWebRequestTexture.GetTexture(ImgLink);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.Log(www.error);
        }
        else
        {
            textureImg = new Texture2D(1, 1);
            textureImg = DownloadHandlerTexture.GetContent(www);

            if (!Directory.Exists(DIRPath))
            {

                Directory.CreateDirectory(DIRPath);

                ////Debug.Log("Created Dir" + DIRPath);
            }

            byte[] bytes = textureImg.EncodeToPNG();
            File.WriteAllBytes(FilePath, bytes);

            Destroy(textureImg);
            bytes = System.IO.File.ReadAllBytes(FilePath);
            yield return new WaitForSeconds(2);
            textureImg = new Texture2D(1, 1);
            textureImg.LoadImage(bytes);

            if (textureImg != null)
            {
                Sprite sprite = Sprite.Create(textureImg, new Rect(0, 0, textureImg.width, textureImg.height), new Vector2(0.5f, 0.5f));

                cardImage = sprite;
                this.CardImage.sprite = sprite;
            }


            www.Dispose();
            bytes = new byte[0];
            System.GC.Collect();


        }
    }


    //public int BaseAttack { get => companionData.Sum(x => x); }


}
