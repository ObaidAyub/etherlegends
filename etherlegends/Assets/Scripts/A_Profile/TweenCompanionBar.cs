﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class TweenCompanionBar : MonoBehaviour
{

    public Animator anim;
    bool startAnim = true;
    
    public void Down()
    {
        if (!startAnim)
        {

        anim.SetBool("Up", false);
        anim.SetBool("Down",true);
            startAnim = true;
        }


    }

    public void Up()
    {
        if (startAnim)
        {

        anim.SetBool("Down", false);
        anim.SetBool("Up", true);
            startAnim = false;
        }
    }
}
