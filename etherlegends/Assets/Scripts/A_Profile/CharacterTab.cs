﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class CharacterTab : MonoBehaviour
{
    public string tabName;
    public TextMeshProUGUI headingText;
    public Button button;

    private void Start()
    {
        button.onClick.AddListener(PopulateCards);
    }
    public void PopulateCards()
    {
        ShowCharactersAndCompanions.instance.PopulateCardsData(int.Parse(tabName));

        for (int i = 0; i < ShowCharactersAndCompanions.instance.tabs.Count; i++)
        {
            if (ShowCharactersAndCompanions.instance.tabs[i].name.ToString() == headingText.text.ToString())
            {
                ShowCharactersAndCompanions.instance.tabs[i].GetComponent<Button>().interactable = false;

            }
            else
            {
                ShowCharactersAndCompanions.instance.tabs[i].GetComponent<Button>().interactable = true;
            }
        }
    }
}
