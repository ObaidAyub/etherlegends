﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Nethereum.JsonRpc.UnityClient;
using TMPro;
using BarcodeScanner;
using BarcodeScanner.Scanner;
using Wizcorp.Utils.Logger;
using System;
//using SimpleJSON;
using Nethereum.Util;
using System.Numerics;
using System.Runtime.InteropServices;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.ABI.Model;
using Nethereum.Contracts;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts.Extensions;
using Nethereum.RPC.Eth.DTOs;

public class TransferEther : MonoBehaviour
{

    public TMP_InputField InpAddress, InpAmt1;   

    string playerEthereumAccount = "";

    string playerEthereumAccountPK = ""; 

    public Text TxtEtBal ,EtherBalance,MainEtherBalance;
    public GameObject TransProgress;
    public Image ProgressBarr;
    private IScanner BarcodeScanner;
    public GameObject[] disableObj;
    public Text TxHash;
    bool isProg;

    public decimal Amount = 1.1m;
    public decimal GasPriceGwei = 2;
    public string TransactionHash = "";
    public decimal BalanceAddressTo = 0m;
    string AddressTo = "";


    #region Barcode QRCode
    public RawImage Qrcode;
    public GameObject QRWindow;
    #endregion
    string HashLink;

    [Event("Transfer")]
    public class TransferEventDTOBase : IEventDTO
    {

        [Parameter("address", "_from", 1, true)]
        public virtual string From { get; set; }
        [Parameter("address", "_to", 2, true)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_value", 3, false)]
        public virtual BigInteger Value { get; set; }
    }

    public partial class TransferEventDTO : TransferEventDTOBase
    {
        public static EventABI GetEventABI()
        {
            return EventExtensions.GetEventABI<TransferEventDTO>();
        }
    }


    public void OnEnable()
    {
       // EthereumNetworkConnection.Instance.ConnectToNetwork("Rinkeby");

        playerEthereumAccount = UserDataController.Instance.GetWalletAddress();
        playerEthereumAccountPK = UserDataController.Instance.GetPrivateKey();
        TransProgress.SetActive(false);
        TxHash.gameObject.SetActive(false);
        TxtEtBal.gameObject.SetActive(false);

        InpAddress.text = "";
        InpAmt1.text = "";

        getBalance();
    }

    public void TransferAuthCheck() //Authenticating before transfering
    {

        if (string.IsNullOrEmpty(InpAmt1.text) || string.IsNullOrEmpty(InpAddress.text))
        {
            TxtEtBal.gameObject.SetActive(true);
            TxtEtBal.text = "Please Enter value ";
            StartCoroutine(ErrorTest());
            return;

        }
        if (float.Parse(InpAmt1.text) > float.Parse( EtherBalance.text) )
        {
            TxtEtBal.gameObject.SetActive(true);
            TxtEtBal.text = "Not Enough Balance..";
            StartCoroutine(ErrorTest());
            return;
        }

        LoadingManager.Instance.ShowAuthentication(FinalTransfer);       
    }

    public void FinalTransfer()
    {
        //Debug.Log("Calledddd");
        StartCoroutine(TransEther());
    }

    public void OpenLink()
    {
        Application.OpenURL(HashLink);
    }

    IEnumerator ErrorTest()
    {
        yield return new WaitForSeconds(3f);
        TxtEtBal.gameObject.SetActive(false);

    }


    public IEnumerator TransEther()
    {
        playerEthereumAccount = UserDataController.Instance.GetWalletAddress();
        playerEthereumAccountPK = UserDataController.Instance.GetPrivateKey();

        Debug.Log("net--" + ConstantManager._url_ETHMainNet);
        Debug.Log("address--" + playerEthereumAccountPK);
        isProg = true;

        yield return new WaitForSeconds(0.1f);

        for (int i = 0; i < disableObj.Length; i++)
        {
            disableObj[i].SetActive(false);
        }

        TxtEtBal.gameObject.SetActive(true);
        TxtEtBal.text = "Transferring..";
        ProgressBarr.gameObject.SetActive(true);
        TransProgress.SetActive(true);
        StartCoroutine(Progress());

       // SendEtherBalance();  //SS Transaction

        var ethTransfer = new EthTransferUnityRequest(ConstantManager._url_ETHMainNet, playerEthereumAccountPK);
#pragma warning disable CS1701 // Assuming assembly reference matches identity
       // EthEstimateGasUnityRequest gasEstimate = new EthEstimateGasUnityRequest(ConstantManager._url_ETHMainNet);
        //Debug.Log("Gas Value " + gasEstimate.Result.Value);
#pragma warning restore CS1701 // Assuming assembly reference matches identity


        yield return ethTransfer.TransferEther(InpAddress.text, (decimal)float.Parse(InpAmt1.text));

        if (ethTransfer.Exception != null)
        {
            Debug.Log(ethTransfer.Exception.Message);
            TxtEtBal.text = "Transfer Error : Please Check Wallet Address ";
            TransProgress.SetActive(false);
            isProg = false;

            StopCoroutine(Progress());
            ProgressBarr.color = Color.red;
            for (int i = 0; i < disableObj.Length; i++)
            {
                disableObj[i].SetActive(true);
            }
            yield break;
        }

        var transactionHash = ethTransfer.Result;

        HashLink = "https://etherscan.io/tx/" + transactionHash;

        Debug.Log("Transfer transaction hash:" + transactionHash);

        TxtEtBal.text = "Transaction Broadcast Successful. Wait For Transaction Hash..";
        //create a poll to get the receipt when mined
        var transactionReceiptPolling = new TransactionReceiptPollingRequest(ConstantManager._url_ETHMainNet);

        //checking every 2 seconds for the receipt
        yield return transactionReceiptPolling.PollForReceipt(transactionHash, 2);

        TransactionClass transactionClass = new TransactionClass();
        transactionClass.tx_hash = transactionHash;
        transactionClass.user_wallet_add = UserDataController.Instance.Myprefs.Address;
        transactionClass.dest_wallet_add = InpAddress.text;
        transactionClass.amount = InpAmt1.text;
        transactionClass.trans_type = "TransferEther:ETHER";

        var transferReceipt = transactionReceiptPolling.Result;

        var transferEvent = transferReceipt.DecodeAllEvents<TransferEventDTO>();

        if (transferEvent.Count > 0)
        {
            OnSendComplete(transactionClass);
        }


        TxtEtBal.text = "Transfer Complete";
        TxHash.gameObject.SetActive(true);
        TxHash.text=transactionHash;
        getBalance();
        StopCoroutine(Progress());
        isProg = false;
        TransProgress.gameObject.SetActive(false);
        ProgressBarr.fillAmount = 1;
       
        for (int i = 0; i < disableObj.Length; i++)
        {
            disableObj[i].SetActive(true);
        }
        
    //  Debug.Log("Balance of account:" + UnitConversion.Convert.FromWei(balanceRequest.Result.Value));
    }

    public async void OnSendComplete(TransactionClass transactionClass)
    {

        string Jsonbody = JsonUtility.ToJson(transactionClass);

        string Response = await SessionManager.Instance.Post(ConstantManager._url_StoreTransaction, Jsonbody, SessionManager.Instance.userToken, true);

        Debug.Log("Save Transaction-->" + Response);

        if (!string.IsNullOrEmpty(Response))
        {

        }
    }

    //public void TransferRequest()
    //{

    //    StartCoroutine(TransferEthereum());
    //}

    //public IEnumerator TransferEthereum()
    //{
    //    playerEthereumAccount = UserDataController.Instance.GetWalletAddress();
    //    playerEthereumAccountPK = UserDataController.Instance.GetPrivateKey();
    //    //  Url = InputUrl.text;
    //    //  PrivateKey = InputPrivateKey.text;
    //    AddressTo = InpAddress.text;
    //    Amount = System.Decimal.Parse(InpAmt1.text);

    //    TxtEtBal.gameObject.SetActive(true);
    //    TxtEtBal.text = "Transferring..";
    //    ProgressBarr.gameObject.SetActive(true);
    //    TransProgress.SetActive(true);
    //    StartCoroutine(Progress());

    //    //initialising the transaction request sender
    //    var ethTransfer = new EthTransferUnityRequest(ConstantManager._url_ETHMainNet, playerEthereumAccountPK);


    //    var receivingAddress = AddressTo;

    //    yield return ethTransfer.TransferEther(receivingAddress, Amount, GasPriceGwei);


    //    if (ethTransfer.Exception != null)
    //    {
    //        Debug.Log(ethTransfer.Exception.Message);
    //        TxtEtBal.text = "Transfer Error : Please Check Wallet Address ";
    //        TransProgress.SetActive(false);
    //        isProg = false;

    //        StopCoroutine(Progress());
    //        ProgressBarr.color = Color.red;
    //        for (int i = 0; i < disableObj.Length; i++)
    //        {
    //            disableObj[i].SetActive(true);
    //        }
    //        yield break;
    //    }

    //    TransactionHash = ethTransfer.Result;


    //    Debug.Log("Transfer transaction hash:" + TransactionHash);

    //    TxtEtBal.text = "Transaction Broadcast Successful. Wait For Transaction Hash..";
    //    //create a poll to get the receipt when mined
    //    var transactionReceiptPolling = new TransactionReceiptPollingRequest(ConstantManager._url_ETHMainNet);
    //    //checking every 2 seconds for the receipt
    //    yield return transactionReceiptPolling.PollForReceipt(TransactionHash, 2);

    //    Debug.Log("Transaction mined");
    //    TxtEtBal.text = "Transfer Complete";
    //    TxHash.gameObject.SetActive(true);
    //    TxHash.text = TransactionHash;

    //    getBalance();

    //    StopCoroutine(Progress());
    //    isProg = false;
    //    TransProgress.gameObject.SetActive(false);
    //    ProgressBarr.fillAmount = 1;

    //    for (int i = 0; i < disableObj.Length; i++)
    //    {
    //        disableObj[i].SetActive(true);
    //    }
    //    //GetEthBalance();
    //}

    IEnumerator Progress()
    {
        float i = 0;

        while (i <= 1f)
        {
            if (!isProg)
            {
                TransProgress.gameObject.SetActive(false);
                break;
            }
            yield return new WaitForSeconds(0.02f);
            i = i + 0.001f;
            ProgressBarr.fillAmount = i;
        }

        for (int j = 0; j < disableObj.Length; j++)
        {
            disableObj[j].SetActive(true);
        }
    }



    public async void GetEtherBalance()
    {
       
        //string bodyJson = "";
               
        string GetTokenBalResponse = await SessionManager.Instance.Post(ConstantManager._url_getAllTokenBalance, null, SessionManager.Instance.userToken, true);

        GetTokenBal TokenResponse = JsonUtility.FromJson<GetTokenBal>(GetTokenBalResponse);

        Debug.Log("Token Response" + GetTokenBalResponse);

        if (TokenResponse.status == 1)
        {
            EtherBalance.text = TokenResponse.data.ether.ToString();// Math.Round(double.Parse(TokenResponse.data.ether), 8).ToString();
            MainEtherBalance.text = EtherBalance.text;

            UserDataController.Instance.setTOken20(float.Parse(TokenResponse.data.elementeum), float.Parse(TokenResponse.data.enjin), float.Parse(TokenResponse.data.ether), TokenResponse.data.ep, float.Parse(TokenResponse.data.lp), float.Parse(TokenResponse.data.arena));
        }

       
    }
   

    public void getBalance()
    {
      
        StartCoroutine(GetEthBalance());       

    }

    public IEnumerator GetEthBalance()
    {
        
        var balanceRequest = new EthGetBalanceUnityRequest(ConstantManager._url_ETHMainNet);
        yield return balanceRequest.SendRequest(playerEthereumAccount, BlockParameter.CreateLatest());

        var BalanceAddressTo = UnitConversion.Convert.FromWei(balanceRequest.Result.Value);
        EtherBalance.text = BalanceAddressTo.ToString();
    }





    public IEnumerator GetAccountBalanceCoroutine()
    {
        Debug.Log("net--" + ConstantManager._url_ETHMainNet);
        Debug.Log("address--" + playerEthereumAccountPK);
        playerEthereumAccount = UserDataController.Instance.Myprefs.Address;      

#pragma warning disable CS1701 // Assuming assembly reference matches identity
        var getBalanceRequest = new EthGetBalanceUnityRequest(ConstantManager._url_ETHMainNet);           // 1
#pragma warning restore CS1701 // Assuming assembly reference matches identity
        Debug.Log("Balance Request--" + getBalanceRequest);                                                                  // Send balance request with player's account, asking for balance in latest block
        yield return getBalanceRequest.SendRequest(playerEthereumAccount, BlockParameter.CreateLatest());  // 2

        if (getBalanceRequest.Exception == null)                                     // 3
        {
            Debug.Log("balance--" + getBalanceRequest.Result.Value);           
            var balance = getBalanceRequest.Result.Value;                             // 4
            Debug.Log(balance);                                                                  // Convert the balance from wei to ether and round to 8 decimals for display                                
            EtherBalance.text = UnitConversion.Convert.FromWei(balance, 18).ToString("n8");    // 5
        }
        else
        {
            Debug.Log("RW: Get Account Balance gave an exception: " + getBalanceRequest.Exception.Message);
        }
    }



    //--------------------------Scan Wallet Address QR---------------------------

    public void StartScan()
    {
        QRWindow.SetActive(true);
        BarcodeScanner = new Scanner();

        BarcodeScanner.Camera.Play();

        // Display the camera texture through a RawImage
        BarcodeScanner.OnReady += (sender, arg) => {
            // Set Orientation & Texture
            Qrcode.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
            Qrcode.transform.localScale = BarcodeScanner.Camera.GetScale();
            Qrcode.texture = BarcodeScanner.Camera.Texture;

            // Keep Image Aspect Ratio
            var rect = Qrcode.GetComponent<RectTransform>();
            var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
            rect.sizeDelta = new UnityEngine.Vector2(rect.sizeDelta.x, newHeight);
        };

        // Track status of the scanner
        BarcodeScanner.StatusChanged += (sender, arg) => {
            Debug.Log("Status: " + BarcodeScanner.Status);

        };
        Invoke("ClickStart", 2);
    }


    private void Update()
    {
        if (BarcodeScanner == null)
        {
            return;
        }
        if (BarcodeScanner.QRValue == null)
        {
            BarcodeScanner.Update();

        }
    }

    #region 
    public void ClickStart()
    {


        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Start");
            return;
        }

        // Start Scanning
        BarcodeScanner.Scan((barCodeType, barCodeValue) => {
            BarcodeScanner.Stop();
            QRWindow.SetActive(false);
            // QRData QRCode = JsonUtility.FromJson<QRData>(barCodeValue);
            InpAddress.text = barCodeValue;

#if UNITY_ANDROID || UNITY_IOS
            Handheld.Vibrate();
#endif
        });
    }

    public void ClickStop()
    {
        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Stop");
            return;
        }

        // Stop Scanning
        BarcodeScanner.Stop();
    }


    /// <summary>
    /// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
    /// Trying to stop the camera in OnDestroy provoke random crash on Android
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    public IEnumerator StopCamera(Action callback)
    {
        // Stop Scanning

        BarcodeScanner.Destroy();
        BarcodeScanner = null;

        // Wait a bit
        yield return new WaitForSeconds(0.1f);

        callback.Invoke();
    }
    #endregion
}