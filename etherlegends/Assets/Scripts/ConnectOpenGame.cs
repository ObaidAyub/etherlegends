﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System.Threading.Tasks;
using System;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.Networking;

public class ConnectOpenGame : MonoBehaviour
{

   public Socket socket;
   public string socketid;
    public GameObject AlertCanvas;
    public static ConnectOpenGame Instance;
    string Message = "";


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {            
            Destroy(this.gameObject);
        }
    }



    private void Start()
    {
        ConnectSocket();
    }

    void ConnectSocket()
    {

        if (socket==null)
        {
            IO.Options options = new IO.Options();

            socket = IO.Socket(ConstantManager._url_OpenGameSocketUrl, options);

            socket.On(Socket.EVENT_CONNECT, () =>
            {

                socket.On("connected", (data) =>
                {
                    JSONNode Jnode = JSON.Parse(data.ToString());
                    socketid = Jnode["socket_id"];


                });




            });


        }
        else
        {
        }

        if(socket!=null)
        {
            AlertMessage();
        }
       
    }

    public void AlertMessage()
    {
        //Debug.Log("LogIn Alert Called");
        socket.On("afterLoginAlert", (data2) =>
        {
           
            //Debug.Log("After Login Alert-->" + data2);
            Message = "loginalert";
        });
        LoadingManager.Instance.HideMainLoading();
    }

    private void Update()
    {

        if (Message=="loginalert")
        {
            if (SceneManager.GetActiveScene().buildIndex==0)
            {
                AuthenticationManager.Instance.Logout();
            }
            else
            {
                BattleLobbySC.Instance.UserGameQuit();
                AlertCanvas.SetActive(true);
                Time.timeScale = 0;
            }           
            Message = "";
        }

    }

    public void CheckUserAdddress(string Address) //when importing  address to account check if that account is already imported to another account.
    {
        JObject Jobj = new JObject();

        Jobj.Add("user_address", Address);

        Jobj.Add("socket_id", socketid);

        //Debug.Log("CHeck User Address--->" + Jobj.ToString());

        socket.Emit("checkUserAddress", Jobj);

        socket.On("afterCheckUserAddress", (data) =>
        {
            //Debug.Log("CheckUserAddress" + data.ToString());
        });

    }

    public void MainMenu() //go to main menu.

    {
        Time.timeScale = 1;
        AlertCanvas.SetActive(false);
        UserDataController.Instance.deleteAllData();
        SceneManager.LoadScene(0);
    }

    public void ForceSocketUpdate()
    {
        //Debug.Log("Force Socket Update-------------");
        JObject Jobj = new JObject();

        Jobj.Add("email", UserDataController.Instance.Myprefs.UName);

        Jobj.Add("socket_id", socketid);

        //Debug.Log(Jobj.ToString());

        socket.Emit("forceSocketUpdate", Jobj);

        socket.On(" ", (data) =>
        {
            //Debug.Log("AfterOpenGame" + data.ToString());
        });

    }

    public void OpenGame()
    {
        //Debug.Log("Open Game Login-------------");
        JObject Jobj = new JObject();

        Jobj.Add("email",UserDataController.Instance.Myprefs.UName);

        Jobj.Add("socket_id",socketid);

        //Debug.Log(Jobj.ToString());

        socket.Emit("openGame", Jobj);

        socket.On("afterOpenGame",(data)=>
        {
            //Debug.Log("AfterOpenGame" + data.ToString());
        });     

    }


}
