﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectSc : MonoBehaviour
{

    public Vector3 loc1, loc2;
    bool moveup;
    public float speed;

    private void Start()
    {
        transform.localPosition = loc1;
    }


    private void FixedUpdate()
    {
        if (moveup)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, loc1, Time.deltaTime * speed);

        }
        else
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, loc2, Time.deltaTime * speed);
        }

        if(transform.localPosition.y >= loc1.y)
        {
            moveup = false;
        }
        else if(transform.localPosition.y <= loc2.y)
        {
            moveup = true;
        }
    }


}
