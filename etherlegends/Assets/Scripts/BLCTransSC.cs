﻿using UnityEngine;
using UnityEngine.UI;
using System;


public class BLCTransSC : MonoBehaviour
{

    public Text DateTxt,FromTxt, DepositTxt, ToTxt,AmtTxt,TxHashTxt;

    public RawImage ItemImage;

    string fromLink, Tolink, HashLink;

    public Texture2D EnjinTexture,ElementeumTexture,EtherTexture;


    public void setValue(string DateTx,string FromTx, string Deposit, string ToTx, string AmtTx,string TxHash)
    {
        Debug.Log("Value Set " + DateTx + " from " + FromTx + " Deposit " + Deposit + " TO  " + ToTx + " Amt " + AmtTx + " txhash " + TxHash);


       
        DateTxt.text = DateTx;
        
        FromTxt.text = FromTx;

        Debug.Log(Deposit);

        if (Deposit.ToLower().Contains("deposit"))
        {

            DepositTxt.text = "IN";

            string[] TransType = Deposit.Split(':');

            if (TransType.Length<=1)
            {
                ItemImage.GetComponent<RawImage>().enabled = false;
                return;
            }
           
            switch (TransType[1].ToLower().Trim())
            {
                case "elet":
                    ItemImage.texture = ElementeumTexture;
                    break;

                case "enj":
                    ItemImage.texture = EnjinTexture;
                    break;

                case "ether":
                     ItemImage.texture = EtherTexture;
                break;

                default:
                    Debug.Log("Card Deposit:" + TransType[1]);
                    string contId = UserDataController.Instance.GetCardContract(TransType[1]);
                    Debug.Log("ID" + contId);
                    ItemImage.texture = BlockChainCardSC.Instance.GetCardTexture(contId);
                break;

            }
        }

        else
        {
            DepositTxt.text = "OUT";

            string[] TransType = Deposit.Split(':');

            if (TransType.Length<=1)
            {
                ItemImage.GetComponent<RawImage>().enabled = false;
            }
            Debug.Log(TransType[0].ToLower());

            switch (TransType[0].ToLower().Trim())
            {
                case "forgecard":
                
                case "redeemcard":
                
                case "transfererc721":

                    string contId = UserDataController.Instance.GetCardContract(TransType[1]);
                    Debug.Log("ID" + contId);
                    ItemImage.texture = BlockChainCardSC.Instance.GetCardTexture(contId);

                break;

                case "transfererc20":
                        Debug.Log("transfer--> " + TransType[1].ToLower());
                        if (TransType[1].ToLower().Equals("enjin"))
                        {
                            ItemImage.texture = EnjinTexture;
                        }
                        else if (TransType[1].ToLower().Equals("elementeum"))
                        {
                            ItemImage.texture = ElementeumTexture;
                        }                    

                    break;

                case "transfererc1155":

                    break;

                case "transferether":
                    ItemImage.texture = EtherTexture;
                    break;

                default:
                    ItemImage.GetComponent<RawImage>().enabled = false;
                    break;

            }

        }
      
        // ItemImage.texture = ItemText;

        fromLink = "https://etherscan.io/address/" + FromTx;
        Tolink = "https://etherscan.io/address/" + ToTx;

        HashLink = TxHash;

        Debug.Log(HashLink);
        ToTxt.text = ToTx;

        AmtTxt.text = AmtTx;

        TxHashTxt.text = "View On EtherScan";

    }

    public void OpenHash()
    {
        Debug.Log("OpenHash" + HashLink);
        Application.OpenURL(HashLink);
    }

    public void FromLinkOpen()
    {
        Debug.Log("FromLink" + fromLink);
        Application.OpenURL(fromLink);
    }

    public void ToLinkOpen()
    {
        Debug.Log("ToLink" + Tolink);
        Application.OpenURL(Tolink);  
    }

}
