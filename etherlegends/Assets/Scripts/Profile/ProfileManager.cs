﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;
using TMPro;
using System.Collections;
using SimpleJSON;

public class ProfileManager : MonoBehaviour
{
    public static ProfileManager Instance;

    [Header("Token Balance")]
    public Text text_eth;
    public Text text_elet;
    public Text text_enj;
    public Text text_ep;

    [Header("Profile")]
    public RawImage img_profilePic;
    public Text text_displayName;
    public InputField name;
    public Text text_title;
    public Text text_publicAddress;
    public Text text_Email;
    public Text text_totBattles,text_totWin,text_totWinStreak;
    public Texture2D DefaultProfile;
    Image pic;
    public GameObject profilePanel;
    public GameObject BestCard;

    public string ProfilePicPath = null;

   

    [Header("Change Display Name")]
    public GameObject changeDisplayNamePanel;
    public TMP_InputField newDisplayName;
    //public Text ErrorTxt;

    private void Awake()
    {
        Instance = this;
        ProfilePicPath = Application.persistentDataPath + "/Profilepic.png";
    }

    public void SetProfile()
    {

        //text_displayName.text = UserDataController.Instance.profileData.displayName;
        name.text = UserDataController.Instance.profileData.displayName;

        text_title.text = UserDataController.Instance.profileData.title;
        text_publicAddress.text = UserDataController.Instance.profileData.publicAddress;
        text_Email.text = UserDataController.Instance.profileData.email;

        StartCoroutine(setImage(UserDataController.Instance.profileData.profile_pic));

    }

    public void ChangeDisplayName()
    {

        changeDisplayNamePanel.SetActive(true);

    }
    public async void ConfirmDisPlayName()
    {
        LoadingManager.Instance.ShowLoader("Loading...");
        string Displayname;
        Displayname = newDisplayName.text;

        //Debug.Log(Displayname);

        ChangeDispName changeDispName = new ChangeDispName();
        changeDispName.displayName = Displayname;
        string bodyJson = JsonUtility.ToJson(changeDispName);
        //Debug.Log("confirm -->" + bodyJson);
        string ConfirmResponse = await SessionManager.Instance.Post(ConstantManager._url_UpdateDisplayName, bodyJson, null, true);
        LoadingManager.Instance.HideLoader();
        //Debug.Log("Response" + ConfirmResponse);
        var Jnode = JSON.Parse(ConfirmResponse);

        if (string.IsNullOrEmpty(ConfirmResponse))
        {
        LoadingManager.Instance.ShowInfoPopUp(Jnode["message"]);
            return;

        }
        else
        {
            LoadingManager.Instance.ShowInfoPopUp(Jnode["message"]);


        }

        if (Jnode["status"] == 1)
        {
            name.text = Displayname;
            UserDataController.Instance.profileData.displayName = Displayname;
        }
        else
        {
            //Debug.Log("Error" + Jnode["message"]);
        }

        
    }
    



    IEnumerator setImage(string url)
    {

        WWW www = new WWW(url);

        yield return www;

        if (www.texture ==null)
        {
            img_profilePic.texture = DefaultProfile;
        }
        else
        {
            img_profilePic.texture = www.texture;
            File.WriteAllBytes(ProfilePicPath, www.bytes);
        }


        //yield return new WaitForSeconds(1f);
        LoadingManager.Instance.HideMainLoading();
        //Debug.Log("Hide Loader");
        //end show Image in texture 2D
    }



    public IEnumerator UploadProfilePicAsync(string path)
    {

        byte[] imageData = File.ReadAllBytes(path);
       
        WWWForm form = new WWWForm();
        form.AddBinaryData("profilePic", imageData, "ProfilePic.png", "image/png");


        var upload = UnityWebRequest.Post(ConstantManager._url_baseApi + ConstantManager._url_UploadProfilePic,form);

        upload.SetRequestHeader("Authorization", SessionManager.Instance.userToken);

        yield return upload.SendWebRequest();

        if (upload.isHttpError) { }
        //Debug.Log(upload.error);
        else
            //Debug.Log("Uploaded Successfully");

            //Debug.Log(upload.downloadHandler.text);

            File.WriteAllBytes(ProfilePicPath, imageData);

    }

   
    public void SetTokenBalance()
    {
      
        if (text_totBattles)
        {
            text_totBattles.text = UserDataController.Instance.profileData.battles.ToString();
            text_totWin.text = UserDataController.Instance.profileData.wins.ToString();
            text_totWinStreak.text = UserDataController.Instance.profileData.max_streak.ToString();
        }
      
    }

    public void Btn_Play()
    {
        SceneManager.LoadScene("Game");
    }

   
}
