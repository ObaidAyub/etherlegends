﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Enjin.SDK.DataTypes;
//using static Enjin.SDK.Core.Enjin;
using TMPro;
using UnityEngine.UI;
using BarcodeScanner;
using BarcodeScanner.Scanner;
using Wizcorp.Utils.Logger;
using System;

public class TransferERC20 : MonoBehaviour
{

    public TMP_InputField InpAddress, InpAmt1;

    public Text TxtEtBal, Col_Name, Col_Bal,TransText;

    string Name;
    float bal;
    public GameObject TransProgress;
    public Image ProgressBarr;
    private IScanner BarcodeScanner;
    public GameObject[] disableObj;

    #region Transfer Panel
    public GameObject TransferPanel;
    public Text TxHash;
    #endregion

    #region Barcode QRCode
    public RawImage Qrcode;
    public GameObject QRWindow;
    #endregion
    bool isProg;
    string Col_Id;
    string HashLink;


    public void setValues(string ItemName, float Balance)
    {

        Name = ItemName;
        bal = Balance;

        Col_Name.text = "Send " + ItemName;

        Col_Bal.text = Balance.ToString();

    }

   
    private void OnEnable()
    {
        TransferPanel.SetActive(false);
        TxtEtBal.gameObject.SetActive(false);
        disableObj[0].SetActive(true);
        TransProgress.SetActive(false);
        TxHash.gameObject.SetActive(false);
        InpAddress.text = "";
        TxHash.text = "";
        InpAmt1.text = "";
    }

    public void TransferToken()
    {

        if (UserDataController.Instance.Myprefs.Ether<=0)
        {
            LoadingManager.Instance.ShowInfoPopUp("Not Enough Ethereum To Transfer.");
            return; ;
        }
        TransText.gameObject.SetActive(true);
        disableObj[0].SetActive(false);
        float TransferBal = float.Parse(InpAmt1.text);

        if (InpAddress.text == "" || InpAmt1.text == "")
        {
            Debug.Log("Please Enter Proper Value");
            return;
        }

        if (TransferBal > bal)
        {
            TxtEtBal.gameObject.SetActive(true);
            TxtEtBal.text = "Not Enough Balance";

            disableObj[0].SetActive(true);
            return;
        }

        else if (TransferBal == 0 && InpAddress.text =="")
        {

            TxtEtBal.gameObject.SetActive(true);

            TxtEtBal.text = "Please Enter Proper Value";

            disableObj[0].SetActive(true);

            return;
        }
        isProg = true;
        TransferPanel.SetActive(true);
        ProgressBarr.gameObject.SetActive(true);
        TransProgress.SetActive(true);
        StartCoroutine(Progress());

        if (Name.ToLower().Equals("enjin"))
        {
            GetComponent<EnjinTransferSc>().StartTransfer();
        }
        else if (Name.ToLower().Equals("elementeum"))
        {
            GetComponent<ERC20TransferSc>().StartTransfer();
        }
        //AuthenticationManager.Instance.SendERC20Token(int.Parse(InpAmt1.text), InpAddress.text, Name.ToLower());
        StartCoroutine(delayit());
    }

    IEnumerator delayit()
    {
        yield return new WaitForSeconds(3f);
        TxtEtBal.gameObject.SetActive(true);
    }


    public void DoneTransfer(string Data)
    {

        TxtEtBal.text = "Transfer Complete";
        TxtEtBal.gameObject.SetActive(true);
        TxHash.gameObject.SetActive(true);
        TxHash.text = Data;
        HashLink = "https://etherscan.io/tx/" + Data;
        TransProgress.gameObject.SetActive(false);      
        TransText.gameObject.SetActive(false);
        StopCoroutine(Progress());
        isProg = false;
       
    }

    public void OpenLink()
    {
        Application.OpenURL(HashLink);
    }
    //--------------------------Scan Wallet Address QR---------------------------

    public void StartScan()
    {
        QRWindow.SetActive(true);
        BarcodeScanner = new Scanner();

        BarcodeScanner.Camera.Play();

        // Display the camera texture through a RawImage
        BarcodeScanner.OnReady += (sender, arg) => {
            // Set Orientation & Texture
            Qrcode.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
            Qrcode.transform.localScale = BarcodeScanner.Camera.GetScale();
            Qrcode.texture = BarcodeScanner.Camera.Texture;

            // Keep Image Aspect Ratio
            var rect = Qrcode.GetComponent<RectTransform>();
            var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);
        };

        // Track status of the scanner
        BarcodeScanner.StatusChanged += (sender, arg) => {
            Debug.Log("Status: " + BarcodeScanner.Status);

        };

        Invoke("ClickStart", 2);
    }


    IEnumerator Progress()
    {
        float i = 0;

        while (i <= 1f)
        {
            if (!isProg)
            {
                TransProgress.gameObject.SetActive(false);
                break;
            }
            yield return new WaitForSeconds(0.02f);
            i = i + 0.001f;
            ProgressBarr.fillAmount = i;
        }

        for (int j = 0; j < disableObj.Length; j++)
        {
            disableObj[j].SetActive(true);
        }
    }


    private void Update()
    {
        if (BarcodeScanner == null)
        {
            return;
        }
        if (BarcodeScanner.QRValue == null)
        {
            BarcodeScanner.Update();

        }
    }

    #region 
    public void ClickStart()
    {


        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Start");
            return;
        }

        // Start Scanning
        BarcodeScanner.Scan((barCodeType, barCodeValue) => {
            BarcodeScanner.Stop();
            QRWindow.SetActive(false);
            // QRData QRCode = JsonUtility.FromJson<QRData>(barCodeValue);
            InpAddress.text = barCodeValue;

#if UNITY_ANDROID || UNITY_IOS
            Handheld.Vibrate();
#endif
        });
    }

    public void ClickStop()
    {
        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Stop");
            return;
        }

        // Stop Scanning
        BarcodeScanner.Stop();
    }


    /// <summary>
    /// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
    /// Trying to stop the camera in OnDestroy provoke random crash on Android
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    public IEnumerator StopCamera(Action callback)
    {
        // Stop Scanning

        BarcodeScanner.Destroy();
        BarcodeScanner = null;

        // Wait a bit
        yield return new WaitForSeconds(0.1f);

        callback.Invoke();
    }
    #endregion



}
