﻿using System.Collections;
using Nethereum.Contracts;
using Nethereum.JsonRpc.UnityClient;
using UnityEngine;
using Nethereum.Web3;
using TMPro;
using Nethereum.ABI.FunctionEncoding.Attributes;
using System.Numerics;
using Nethereum.ABI.Model;
using Nethereum.Util;
using System;
using System.Text;
using Nethereum.Web3.Accounts;
using Nethereum.Signer;
using Nethereum.ABI.FunctionEncoding;
using System.Threading.Tasks;

public class EnjinTransferDemo : MonoBehaviour
{
    //Deployment contract object definition

      

    [Function("transfer", "bool")]
    public class TransferFunctionBase : FunctionMessage
    {
        [Parameter("address", "_to", 1)]
        public string To { get; set; }
        [Parameter("uint256", "_value", 2)]
        public BigInteger Value { get; set; }
    }

    public partial class TransferFunction : TransferFunctionBase
    {

    }

    [Function("balanceOf", "uint256")]
    public class BalanceOfFunction : FunctionMessage
    {
        [Parameter("address", "_owner", 1)]
        public string Owner { get; set; }
        [Parameter("uint256", "_id", 1)]
        public string Id { get; set; }
    }


    [FunctionOutput]
    public class BalanceOfFunctionOutput : IFunctionOutputDTO
    {
        [Parameter("uint256", 1)]
        public int balances { get; set; }
    }

    [Function("setApprovalForAll")]
    public class SetApprovalForAll : FunctionMessage
    {
        [Parameter("address", "_operator", 1)]
        public string _operator { get; set; }
        [Parameter("bool", "_approved", 2)]
        public bool _approved { get; set; }
    }

    [Function("isApprovedForAll")]
    public class isApprovedForAll : FunctionMessage
    {
        [Parameter("address", "_owner", 1)]
        public string _owner { get; set; }
        [Parameter("address", "_operator", 2)]
        public string _operator { get; set; }
    }


    [FunctionOutput]
    public class SetApprovalForAllOutput : IFunctionOutputDTO
    {
        [Parameter("bool", 1)]
        public bool IsApproved { get; set; }
    }

    public partial class safeTransferFrom : safeTransferFrombase { }

    [Function("safeTransferFrom")]
    public class safeTransferFrombase : TransferFunction
    {
        [Parameter("address", "_from", 1)]
        public string from { get; set; }
        [Parameter("address", "_to", 2)]
        public string to { get; set; }
        [Parameter("uint256", "_id", 3)]
        public string Id { get; set; }
        [Parameter("uint256", "_value", 4)]
        public int value { get; set; }
        [Parameter("bytes", "_data", 5)]
        public Byte[] calldata { get; set; }

    }



    [Event("Transfer")]
    public class TransferEventDTOBase : IEventDTO
    {

        [Parameter("address", "_from", 1, true)]
        public virtual string From { get; set; }
        [Parameter("address", "_to", 2, true)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_value", 3, false)]
        public virtual BigInteger Value { get; set; }
    }   

    public partial class TransferEventDTO : TransferEventDTOBase
    {
        public static EventABI GetEventABI()
        {
            return EventExtensions.GetEventABI<TransferEventDTO>();
        }
    }

    public string TokenAddress = "0x6c37Bf4f042712C978A73e3fd56D1F5738dD7C43";

    public TMP_InputField ToAddress;
    public TMP_InputField value;
    private string url = "";
    private string privateKey = "";
    private string account = "";
    private string newAddress = "";

    public void StartTransfer()
    {
        StartCoroutine(DeployAndTransferToken());
    }

    public  void TestEnjin()
    {
         // StartCoroutine(Getbalance());
        // StartCoroutine(SafeTransferFrom());
       RequestApproval();
    }   


    public IEnumerator Getbalance()
    {
        var url = ConstantManager._url_ETHMainNet;

        var account = "0x5B7415F8f61C29Fc7A01B7c67e4B5067ea26e3C9"; //"0xD77ABD0de441B9F81CdcE8663aD64bAaaD3d8FF2";

        var TokenId = "0x1800000000001289000000000000000000000000000000000000000000000000";

        var queryRequest = new QueryUnityRequest<BalanceOfFunction, BalanceOfFunctionOutput>(url, account);

        yield return queryRequest.Query(new BalanceOfFunction() { Owner = account,Id=TokenId }, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");

        //Getting the dto response already decoded
        if (queryRequest.Exception!=null)
        {
            Debug.Log(queryRequest.Exception.Message);
        }
      
        var dtoResult = queryRequest.Result;

        Debug.Log("Result of BalanceOf--->" + dtoResult.balances);

        if (dtoResult.balances == 0)
        {
            yield break;
        }

    }

  
    async void RequestApproval()
    {
        var contractByteCode = "0x6080604052600436106100555760003560e01c806348ff15b3146100ef5780637457bbf7146101065780638d0a3a0814610137578063a0a2daf01461014c578063ba0e930a14610180578063d5009584146101b3575b600080356001600160e01b0319168152600260205260409020546001600160a01b0316806100ca576040805162461bcd60e51b815260206004820152601860248201527f46756e6374696f6e20646f6573206e6f742065786973742e0000000000000000604482015290519081900360640190fd5b60405136600082376000803683855af43d806000843e8180156100eb578184f35b8184fd5b3480156100fb57600080fd5b506101046101c8565b005b34801561011257600080fd5b5061011b61029a565b604080516001600160a01b039092168252519081900360200190f35b34801561014357600080fd5b506101046102a9565b34801561015857600080fd5b5061011b6004803603602081101561016f57600080fd5b50356001600160e01b0319166102de565b34801561018c57600080fd5b50610104600480360360208110156101a357600080fd5b50356001600160a01b03166102f9565b3480156101bf57600080fd5b5061011b61037f565b6001546001600160a01b03163314610227576040805162461bcd60e51b815260206004820152601f60248201527f53656e646572206d75737420626520746865206e6577206d616e616765722e00604482015290519081900360640190fd5b600054600154604080516001600160a01b03938416815292909116602083015280517fbe4cc281795971a471c980e842627a7f1ea3892ddfce8c5b6357cd2611c197329281900390910190a160018054600080546001600160a01b03199081166001600160a01b03841617909155169055565b6001546001600160a01b031690565b6000546001600160a01b031633146102c057600080fd5b600080546001600160a01b0319908116909155600180549091169055565b6002602052600090815260409020546001600160a01b031681565b6000546001600160a01b0316331461031057600080fd5b6000546001600160a01b038281169116141561035d5760405162461bcd60e51b815260040180806020018281038252602281526020018061038f6022913960400191505060405180910390fd5b600180546001600160a01b0319166001600160a01b0392909216919091179055565b6000546001600160a01b03169056fe4e6577206d616e61676572206e6565647320746f20626520646966666572656e742ea265627a7a7230582026f253e11c72e4cff992d5c2e60d57f74d33086c62ccec5250b44336533e3a0c64736f6c63430005090032";
        var abi = @"[{ ""anonymous"": false, ""inputs"": [ { ""indexed"": true, ""name"": ""_id"", ""type"": ""uint256"" }, { ""indexed"": true, ""name"": ""_creator"", ""type"": ""address"" } ], ""name"": ""AcceptAssignment"", ""type"": ""event"" }, { ""anonymous"": false, ""inputs"": [ { ""indexed"": true, ""name"": ""_owner"", ""type"": ""address"" }, { ""indexed"": true, ""name"": ""_operator"", ""type"": ""address"" }, { ""indexed"": false, ""name"": ""_approved"", ""type"": ""bool"" } ], ""name"": ""ApprovalForAll"", ""type"": ""event"" }, { ""anonymous"": false, ""inputs"": [ { ""indexed"": true, ""name"": ""_id"", ""type"": ""uint256"" }, { ""indexed"": true, ""name"": ""_from"", ""type"": ""address"" }, { ""indexed"": true, ""name"": ""_to"", ""type"": ""address"" } ], ""name"": ""Assign"", ""type"": ""event"" }, { ""anonymous"": false, ""inputs"": [ { ""indexed"": true, ""name"": ""_operator"", ""type"": ""address"" }, { ""indexed"": true, ""name"": ""_from"", ""type"": ""address"" }, { ""indexed"": true, ""name"": ""_to"", ""type"": ""address"" }, { ""indexed"": false, ""name"": ""_ids"", ""type"": ""uint256[]"" }, { ""indexed"": false, ""name"": ""_values"", ""type"": ""uint256[]"" } ], ""name"": ""TransferBatch"", ""type"": ""event"" }, { ""anonymous"": false, ""inputs"": [ { ""indexed"": true, ""name"": ""_operator"", ""type"": ""address"" }, { ""indexed"": true, ""name"": ""_from"", ""type"": ""address"" }, { ""indexed"": true, ""name"": ""_to"", ""type"": ""address"" }, { ""indexed"": false, ""name"": ""_id"", ""type"": ""uint256"" }, { ""indexed"": false, ""name"": ""_value"", ""type"": ""uint256"" } ], ""name"": ""TransferSingle"", ""type"": ""event"" }, { ""anonymous"": false, ""inputs"": [ { ""indexed"": false, ""name"": ""_value"", ""type"": ""string"" }, { ""indexed"": true, ""name"": ""_id"", ""type"": ""uint256"" } ], ""name"": ""URI"", ""type"": ""event"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_id"", ""type"": ""uint256"" } ], ""name"": ""acceptAssignment"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_id"", ""type"": ""uint256"" }, { ""name"": ""_creator"", ""type"": ""address"" } ], ""name"": ""assign"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": true, ""inputs"": [ { ""name"": ""_owner"", ""type"": ""address"" }, { ""name"": ""_id"", ""type"": ""uint256"" } ], ""name"": ""balanceOf"", ""outputs"": [ { ""name"": """", ""type"": ""uint256"" } ], ""payable"": false, ""stateMutability"": ""view"", ""type"": ""function"" }, { ""constant"": true, ""inputs"": [ { ""name"": ""_owners"", ""type"": ""address[]"" }, { ""name"": ""_ids"", ""type"": ""uint256[]"" } ], ""name"": ""balanceOfBatch"", ""outputs"": [ { ""name"": """", ""type"": ""uint256[]"" } ], ""payable"": false, ""stateMutability"": ""view"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_name"", ""type"": ""string"" }, { ""name"": ""_totalSupply"", ""type"": ""uint256"" }, { ""name"": ""_initialReserve"", ""type"": ""uint256"" }, { ""name"": ""_supplyModel"", ""type"": ""address"" }, { ""name"": ""_meltValue"", ""type"": ""uint256"" }, { ""name"": ""_meltFeeRatio"", ""type"": ""uint16"" }, { ""name"": ""_transferable"", ""type"": ""uint8"" }, { ""name"": ""_transferFeeSettings"", ""type"": ""uint256[3]"" }, { ""name"": ""_nonFungible"", ""type"": ""bool"" } ], ""name"": ""create"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": true, ""inputs"": [ { ""name"": ""_owner"", ""type"": ""address"" }, { ""name"": ""_operator"", ""type"": ""address"" } ], ""name"": ""isApprovedForAll"", ""outputs"": [ { ""name"": """", ""type"": ""bool"" } ], ""payable"": false, ""stateMutability"": ""view"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_ids"", ""type"": ""uint256[]"" }, { ""name"": ""_values"", ""type"": ""uint256[]"" } ], ""name"": ""melt"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_id"", ""type"": ""uint256"" }, { ""name"": ""_to"", ""type"": ""address[]"" }, { ""name"": ""_values"", ""type"": ""uint256[]"" } ], ""name"": ""mintFungibles"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_id"", ""type"": ""uint256"" }, { ""name"": ""_to"", ""type"": ""address[]"" } ], ""name"": ""mintNonFungibles"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_from"", ""type"": ""address"" }, { ""name"": ""_to"", ""type"": ""address"" }, { ""name"": ""_ids"", ""type"": ""uint256[]"" }, { ""name"": ""_values"", ""type"": ""uint256[]"" }, { ""name"": ""_data"", ""type"": ""bytes"" } ], ""name"": ""safeBatchTransferFrom"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_from"", ""type"": ""address"" }, { ""name"": ""_to"", ""type"": ""address"" }, { ""name"": ""_id"", ""type"": ""uint256"" }, { ""name"": ""_value"", ""type"": ""uint256"" }, { ""name"": ""_data"", ""type"": ""bytes"" } ], ""name"": ""safeTransferFrom"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_operator"", ""type"": ""address"" }, { ""name"": ""_approved"", ""type"": ""bool"" } ], ""name"": ""setApprovalForAll"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": false, ""inputs"": [ { ""name"": ""_id"", ""type"": ""uint256"" }, { ""name"": ""_uri"", ""type"": ""string"" } ], ""name"": ""setURI"", ""outputs"": [], ""payable"": false, ""stateMutability"": ""nonpayable"", ""type"": ""function"" }, { ""constant"": true, ""inputs"": [ { ""name"": ""_interfaceID"", ""type"": ""bytes4"" } ], ""name"": ""supportsInterface"", ""outputs"": [ { ""name"": """", ""type"": ""bool"" } ], ""payable"": false, ""stateMutability"": ""pure"", ""type"": ""function"" }, { ""constant"": true, ""inputs"": [ { ""name"": ""_id"", ""type"": ""uint256"" } ], ""name"": ""uri"", ""outputs"": [ { ""name"": """", ""type"": ""string"" } ], ""payable"": false, ""stateMutability"": ""view"", ""type"": ""function"" } ]";
        var Pkey = "8D2F14C97D6B4AD21B1B4E25FF18606F5B8D3EC891C03C6D35F21DA7F70B2A55";
        var senderAddress = "0x5B7415F8f61C29Fc7A01B7c67e4B5067ea26e3C9";
        string ToAddress = "0x3c04EA3B7CAf12b6463f7923F1aC806EA7912364";
        var url = ConstantManager._url_ETHMainNet;
        var account = new Account(Pkey);
        var web3 = new Web3(account, url);

        // var senderAddress = account.Address;
        //estimating the gas
        // var estimatedGas = 40000;// await web3.Eth.DeployContract.EstimateGasAsync(abi, contractByteCode, senderAddress);
        // deploying the contract


        var contract = web3.Eth.GetContract(abi, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");
        var balanceFun = contract.GetFunction("balanceOf");
        Debug.Log(balanceFun.ContractAddress);
        //Debug.Log(TransferFun.CallAsync);
        var balan = await balanceFun.CallDeserializingToObjectAsync<BalanceOfFunctionOutput>(senderAddress, "0x1800000000001289000000000000000000000000000000000000000000000000");

        Debug.Log("Balance-->" + balan.balances.ToString());


       // var TransferFun = contract.GetFunction("safeTransferFrom");


        //var result = await TransferFun.CallDeserializingToObjectAsync<safeTransferFrom>(senderAddress, ToAddress, "0x1800000000001289000000000000000000000000000000000000000000000000", 2, "");

      
        var transferHandler = web3.Eth.GetContractTransactionHandler<safeTransferFrom>();
        var TransferFun = contract.GetFunction("safeTransferFrom");
        var Gas_Price = Web3.Convert.ToWei(8, UnitConversion.EthUnit.Gwei);
        var bytesValue = new byte[1];
        var transactionMessage = new safeTransferFrom()
        {
            // AmountToSend=value,
            // FromAddress=FromAddress,
            from = senderAddress,
            to = ToAddress,
            Id = "0x1800000000001289000000000000000000000000000000000000000000000000",
            value = 2,
            calldata = Encoding.UTF8.GetBytes("from"),

        };

         transactionMessage.GasPrice = Gas_Price;
         transactionMessage.Gas = 4_300_00;

        var data = TransferFun.GetData(transactionMessage);

         // var encoded = web3.TransactionManager.SendTransactionAsync(data, senderAddress,ToAddress,transactionMessage.Gas, transactionMessage.GasPrice,2);
        //return await web3.Eth.Transactions.SendRawTransaction.SendRequestAsync(encoded).ConfigureAwait(false);

        var encoder = new ConstructorCallEncoder();

        var result = encoder.EncodeRequest<safeTransferFrom>(transactionMessage, "");
        var transactionReceipt = await transferHandler.SendRequestAndWaitForReceiptAsync("0xfaaFDc07907ff5120a76b34b731b278c38d6043C", transactionMessage);
       
        Debug.Log("Hash--->"+transactionReceipt.TransactionHash);
        //string ToAddress = "0x3c04EA3B7CAf12b6463f7923F1aC806EA7912364";
        //var TokenId = "0x1800000000001289000000000000000000000000000000000000000000000000";


        // var contractHandler = web3.Eth.GetContractHandler("0xfaaFDc07907ff5120a76b34b731b278c38d6043C");



        // const alice = '0xb414031Aa4838A69e27Cb2AE31E709Bcd674F0Cb';

        // web3.Eth.Accounts.add('0x3a0ce9a362c73439adb38c595e739539be1e34d19c5e9f04962c101c86bd7616');

        // var req = contractHandler.QueryAsync<SetApprovalForAll, data>(transactionMessage);


        //Debug.Log("Request Response---->" + req);
    }

    public IEnumerator SafeTransferFrom()
    {

        string FromAddress = "0x5B7415F8f61C29Fc7A01B7c67e4B5067ea26e3C9";
        string ToAddress = "0x3c04EA3B7CAf12b6463f7923F1aC806EA7912364";
        string TokenId= "0x1800000000001289000000000000000000000000000000000000000000000000";
        int value = 5;
        var url = ConstantManager._url_ETHMainNet;

        /*
        var ApprovalRequest = new TransactionSignedUnityRequest(url, "8D2F14C97D6B4AD21B1B4E25FF18606F5B8D3EC891C03C6D35F21DA7F70B2A55");

        var ApprovalMessage = new SetApprovalForAll
        {
            _operator= "0xfaaFDc07907ff5120a76b34b731b278c38d6043C",
            _approved=true,
        };
     
        yield return ApprovalRequest.SignAndSendTransaction<SetApprovalForAll>(ApprovalMessage, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");

        if (ApprovalRequest.Exception != null)
        {
            Debug.Log("Error Sending " + ApprovalRequest.Exception.Message.ToString());
        }

        var Result = ApprovalRequest.Result;

        Debug.Log("Hashhh--->" + Result);
        */
        
        var queryRequest = new QueryUnityRequest<isApprovedForAll, SetApprovalForAllOutput>(url, FromAddress);

        yield return queryRequest.Query(new isApprovedForAll() { _owner = FromAddress, _operator = "0xfaaFDc07907ff5120a76b34b731b278c38d6043C" }, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");

        //Getting the dto response already decoded
        if (queryRequest.Exception != null)
        {
            Debug.Log(queryRequest.Exception.Message);
        }

        var dtoResult = queryRequest.Result;

        Debug.Log("Result approval--->" + dtoResult.IsApproved);

        //Transfer Method
        var transactionTransferRequest = new TransactionSignedUnityRequest(url, "8D2F14C97D6B4AD21B1B4E25FF18606F5B8D3EC891C03C6D35F21DA7F70B2A55");

        transactionTransferRequest.EstimateGas = false;

        var Gas_Price = Web3.Convert.ToWei(8, UnitConversion.EthUnit.Gwei);

        var transactionMessage = new safeTransferFrom
        {
           // AmountToSend=value,
           // FromAddress=FromAddress,
            from = FromAddress,
            to = ToAddress,
            Id = TokenId,
            value = value,
//            calldata = Encoding.UTF8.GetBytes("from:"+FromAddress),

        };
        
        transactionMessage.GasPrice = Gas_Price;
        transactionMessage.Gas = 4_300_00;

        yield return transactionTransferRequest.SignAndSendTransaction(transactionMessage, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");


        if (transactionTransferRequest.Exception != null)
        {
            Debug.Log("Error Sending " + transactionTransferRequest.Exception.Message.ToString());
        }

        var transactionTransferHash = transactionTransferRequest.Result;

        Debug.Log("Hashhh--->" + transactionTransferHash);

        var transactionReceiptPolling = new TransactionReceiptPollingRequest(url);
        yield return transactionReceiptPolling.PollForReceipt(transactionTransferHash, 2);

        var transferReceipt = transactionReceiptPolling.Result;

        var transferEvent = transferReceipt.DecodeAllEvents<TransferEventDTO>();

        if (transferEvent.Count > 0)
        {
            Debug.Log(transferEvent[0].Log.ToString());
            // OnSendComplete(transactionClass);
        }
        else
        {
            Debug.Log("Failed Transaction");
            Debug.Log(transferReceipt.Logs.ToString());
        }

        var getLogsRequest = new EthGetLogsUnityRequest(url);
        var eventTransfer = TransferEventDTO.GetEventABI();
        yield return getLogsRequest.SendRequest(eventTransfer.CreateFilterInput(transferReceipt.ContractAddress, FromAddress));

        var eventDecoded = getLogsRequest.Result.DecodeAllEvents<TransferEventDTO>();
        Debug.Log("Transferd amount from get logs event: " + eventDecoded[0].Event.Value);
      
    }

    //Sample of new features / requests
    public IEnumerator DeployAndTransferToken()
    {

        url = ConstantManager._url_ETHMainNet;
        newAddress = ToAddress.text;
        account = UserDataController.Instance.GetWalletAddress();
        privateKey = UserDataController.Instance.GetPrivateKey();

        var transactionTransferRequest = new TransactionSignedUnityRequest(url, privateKey);

        var transactionMessage = new TransferFunction
        {
            FromAddress = account,
            To = newAddress,
            Value = Web3.Convert.ToWei(value.text),
        };

        yield return transactionTransferRequest.SignAndSendTransaction(transactionMessage, TokenAddress);

        if (transactionTransferRequest.Exception != null)
        {
            Debug.Log("Error Sending " + transactionTransferRequest.Exception.Message.ToString());
        }

        var transactionTransferHash = transactionTransferRequest.Result;

        TransactionClass transactionClass = new TransactionClass();
        transactionClass.tx_hash = transactionTransferHash;
        transactionClass.user_wallet_add = UserDataController.Instance.Myprefs.Address;
        transactionClass.dest_wallet_add = ToAddress.text;
        transactionClass.amount = value.text;
        transactionClass.trans_type = "TransferERC20:ELEMENTEUM";

        Debug.Log("Transfer txn hash:" + transactionTransferHash);

        GetComponent<TransferERC20>().DoneTransfer(transactionTransferHash);

        var transactionReceiptPolling = new TransactionReceiptPollingRequest(url);
        yield return transactionReceiptPolling.PollForReceipt(transactionTransferHash, 2);
        var transferReceipt = transactionReceiptPolling.Result;

        var transferEvent = transferReceipt.DecodeAllEvents<TransferEventDTO>();

        if (transferEvent.Count > 0)
        {
            OnSendComplete(transactionClass);
        }

        var getLogsRequest = new EthGetLogsUnityRequest(url);
        var eventTransfer = TransferEventDTO.GetEventABI();
        yield return getLogsRequest.SendRequest(eventTransfer.CreateFilterInput(transferReceipt.ContractAddress, account));

        var eventDecoded = getLogsRequest.Result.DecodeAllEvents<TransferEventDTO>();
        Debug.Log("Transferd amount from get logs event: " + eventDecoded[0].Event.Value);
    }

    public async void OnSendComplete(TransactionClass transactionClass)
    {
        string Jsonbody = JsonUtility.ToJson(transactionClass);
        string Response = await SessionManager.Instance.Post(ConstantManager._url_StoreTransaction, Jsonbody, SessionManager.Instance.userToken, true);
        Debug.Log("Save Transaction-->" + Response);
        if (!string.IsNullOrEmpty(Response))
        {
        }
    }
}
