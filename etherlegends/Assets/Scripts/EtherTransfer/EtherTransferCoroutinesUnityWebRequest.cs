﻿using System.Collections;
using Nethereum.JsonRpc.UnityClient;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Util;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EtherTransferCoroutinesUnityWebRequest : MonoBehaviour {

    public string Url = "https://rinkeby.infura.io/v3/1b57fe1cae5e4973ba7b15eef3a2768e";
    string PrivateKey = "0xb5b1870957d373ef0eeffecc6e4812c0fd08f554b37b233526acc331bf1544f7";
    string AddressTo = "0xde0B295669a9FD93d5F28D9Ec85E40f4cb697BAe";
    public decimal Amount = 1.1m;
    public decimal GasPriceGwei = 2;
    public string TransactionHash = "";
    public decimal BalanceAddressTo = 0m;

    public TMP_InputField InputUrl;
    public TMP_InputField InputPrivateKey;
    public TMP_InputField InputAddressTo;
    public TMP_InputField InputAmount;

    public Text ResultBalanceAddressTo;
    public Text ResultTxnHash;

   
    private void OnEnable()
    {
        InitializeEther();
    }

    public void InitializeEther()
    {
        InputAddressTo.text = "";
        InputAmount.text = "";
    }

    public IEnumerator GetEthBalance()
    {
        var balanceRequest = new EthGetBalanceUnityRequest(Url);
        yield return balanceRequest.SendRequest(AddressTo, BlockParameter.CreateLatest());
        BalanceAddressTo = UnitConversion.Convert.FromWei(balanceRequest.Result.Value);
        ResultBalanceAddressTo.text = BalanceAddressTo.ToString();
    }



    public async void TransferRequest()
    {
        //GasPriceOracleData GasClass = await EtherLegendsAPI.APIProxy.LookupNetworkGasPricesAsync();
        //GasPriceGwei = (decimal)GasClass.average;
        StartCoroutine(TransferEther());
    }

    //Sample of new features / requests
    public IEnumerator TransferEther()
    {
      //  Url = InputUrl.text;
      //  PrivateKey = InputPrivateKey.text;
        AddressTo = InputAddressTo.text;
        Amount = System.Decimal.Parse(InputAmount.text);
       

        //initialising the transaction request sender
        var ethTransfer = new EthTransferUnityRequest(Url, PrivateKey);


        var receivingAddress = AddressTo;

        yield return ethTransfer.TransferEther(receivingAddress, Amount, GasPriceGwei);

        if (ethTransfer.Exception != null)
        {
            Debug.Log(ethTransfer.Exception.Message);
            yield break;
        }

        TransactionHash = ethTransfer.Result;
        ResultTxnHash.text = TransactionHash;
        Debug.Log("Transfer transaction hash:" + TransactionHash);

        //create a poll to get the receipt when mined
        var transactionReceiptPolling = new TransactionReceiptPollingRequest(Url);
        //checking every 2 seconds for the receipt
        yield return transactionReceiptPolling.PollForReceipt(TransactionHash, 2);
        
        Debug.Log("Transaction mined");

        GetEthBalance();
    }



 
}
