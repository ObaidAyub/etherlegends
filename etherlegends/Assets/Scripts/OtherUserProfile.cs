﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OtherUserProfile : MonoBehaviour
{

    public Text Txt_name, Txt_Battle, Txt_win, Txt_Streak, Txt_EP;

    public RawImage Img_Image;
    string userId;
    public GameObject CardItem;
    public GameObject CompanionItem;
    public Transform CardContainer;

    public List<Card> UserCards = new List<Card>();

    public void SetValue(Texture2D ProfileImg, string UserId, string name, string battle, string win, string streak, string EP, List<Card> cards)
    {

        for (int i = 0; i < CardContainer.childCount; i++)
        {
            Destroy(CardContainer.GetChild(i).gameObject);
        }

        Img_Image.texture = ProfileImg;
        Txt_name.text = name;
        Txt_Battle.text = battle;
        Txt_win.text = win;
        Txt_Streak.text = streak;
        userId = UserId;
        Txt_EP.text = EP;
        UserCards = cards;



        foreach (var item in UserCards)
        {
            if (item.type.ToString().ToLower() != "companion" && item.type.ToString().ToLower() != "taming" && item.type.ToString().ToLower() != "forged item")
            {
                GameObject InCard = Instantiate(CardItem, CardContainer);
                InCard.GetComponent<SelectedCardSc>().CardDetail = item;
                //Debug.Log(InCard.GetComponent<SelectedCardSc>().CardDetail.type);
                InCard.SetActive(true);
                InCard.GetComponent<SelectedCardSc>().RefreshCardDetails();
            }
            else
            {
                GameObject InCard = Instantiate(CompanionItem, CardContainer);
                InCard.GetComponent<SelectedCardSc>().CardDetail = item;
                //Debug.Log(InCard.GetComponent<SelectedCardSc>().CardDetail.type);
                InCard.SetActive(true);
                InCard.GetComponent<SelectedCardSc>().RefreshCardDetails();
            }
        }

    }
    public void BackToLeaderBoard()
    {
        clearCard(CardContainer);
        this.gameObject.SetActive(false);
        LeaderboardMain.instance.gameObject.SetActive(true);
        if (Img_Image.texture.name != "default")
        {

            Destroy(Img_Image.texture);
        }
    }

    public void clearCard(Transform Content)
    {
        while (Content.childCount > 0)
        {
            var child = Content.GetChild(0);
            child.SetParent(null);
            Destroy(child.gameObject);
        }
    }
}


