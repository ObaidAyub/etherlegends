﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ERCTokenItem : MonoBehaviour
{
    public Text Name, Balance;
    public GameObject TransferObj;
    string TokenName;
    float TokenBal;

    public void setValue(string ItemName,float Bal)
    {
        TokenName = ItemName;
        TokenBal = Bal;
        Name.text = ItemName;
        Balance.text = Bal.ToString();

    }

    public void showTransfer()
    {
        TransferObj.SetActive(true);
        TransferObj.GetComponent<TransferERC20>().setValues(TokenName, TokenBal);
    }


    public void HideTransfer()
    {
        TransferObj.SetActive(false);
    }

}
