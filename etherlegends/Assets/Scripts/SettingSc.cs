﻿
using UnityEngine;
using UnityEngine.UI;

public class SettingSc : MonoBehaviour
{
    public Sprite OnSprite, OffSprite;
    public Button btn_sound, btn_vibration;

    [SerializeField]
    GameObject PanelExportPkey, PanelExportSeedPhrase,Btn_Pkey,Btn_SeedPhrase;

    [SerializeField]
    Text txt_pkey, txt_SeedPhrase;

    int sound, vib;

    private void Start()
    {
        sound = PlayerPrefs.GetInt("sound");
        vib = PlayerPrefs.GetInt("vibration");
      
        if (sound ==0)
        {
            btn_sound.GetComponent<Image>().sprite = OnSprite;
            AudioListener.volume = 1;
        }
        else
        {
            btn_sound.GetComponent<Image>().sprite = OffSprite;
            AudioListener.volume = 0;
        }

        if (vib == 0)
        {
            btn_vibration.GetComponent<Image>().sprite = OnSprite;        
        }
        else
        {
            btn_vibration.GetComponent<Image>().sprite = OffSprite;         
        }

        //if (string.IsNullOrEmpty(UserDataController.Instance.GetPrivateKey()))
        //{
        //    Btn_Pkey.SetActive(false);
        //}
        //else
        //{
        //    Btn_Pkey.SetActive(true);
        //}

        //if (string.IsNullOrEmpty(UserDataController.Instance.GetSeedPhrase()))
        //{
        //    Btn_SeedPhrase.SetActive(false);
        //}
        //else
        //{
        //    Btn_SeedPhrase.SetActive(true);
        //}


    }


    public void ExportPkeyAuth(bool IsPkey)
    {
        if (IsPkey)
        {
            LoadingManager.Instance.ShowAuthentication(ExportPkey);
        }
        else
        {
            LoadingManager.Instance.ShowAuthentication(ExportSeedPhrase);
        }
    }

    public void ExportPkey()
    {
        PanelExportPkey.SetActive(true);

        txt_pkey.text = UserDataController.Instance.GetPrivateKey();
    }

    public void ExportSeedPhrase()
    {
        PanelExportSeedPhrase.SetActive(true);

        txt_SeedPhrase.text = UserDataController.Instance.GetSeedPhrase();
    }

    public void ToggleSound()
    {
        if (sound == 0)
        {
            sound = 1;
            btn_sound.GetComponent<Image>().sprite = OffSprite;
            AudioListener.volume = 0;
        }
        else
        {
            sound = 0;
            btn_sound.GetComponent<Image>().sprite = OnSprite;
            AudioListener.volume = 1;
        }
        PlayerPrefs.SetInt("sound", sound);
    }

    public void ToggleVibration()
    {
        if (vib == 0)
        {
            vib = 1;
            btn_vibration.GetComponent<Image>().sprite = OffSprite;
        }
        else
        {
            btn_vibration.GetComponent<Image>().sprite = OnSprite;
            vib = 0;
        }
        PlayerPrefs.SetInt("vibration", vib);
    }

}
