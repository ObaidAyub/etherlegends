﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using SimpleJSON;
using System;
using TMPro;
using Newtonsoft.Json.Linq;
using UnityEngine.Networking;
using System.IO;

public class SelectedCardSc : MonoBehaviour
{
    [Serializable]
    public class DefaultCardDetail
    {
        public int card_attack;
        public int card_defence;
        public int card_disrupt;
        public int card_heal;
        public int card_heart;
    }

    public Card CardDetail;
    public DefaultCardDetail DefaultDetails;
    

    public GameObject txt_CardLvl, txt_Attack, txt_Defense, txt_Disrupt, txt_Heal, txt_CardName, txt_Life, txt_Level, txt_CardNumber;

    public Image CardImage, Img_attack, Img_Defense, Img_Disrupt, Img_Heal, Img_Life;

    public GameObject btn_talentPts, Btn_save, Btn_Reset;

    public TextMeshProUGUI TalentPts;

    public GameObject BGPanel; //Disable background Clicks and focus on card attributes

    public GameObject GobjSorting; //Gameobject to change sorting layer for attributes

    public GameObject Elementeums; //Gameobjects for 

    int attackVal, Defense_val, Disrupt_val, Heal_val, Life_val; //Life val means Heart Value

    int CardLevel;

    int TalPts_value;

    public GameObject Bestcard;

    public GameObject[] SpecialAbility;

    public Text PopUpCardLvl;

    [Header("Select card Objects")]

    public GameObject selectedGobj, NotSelectedObj;
    public GameObject PopUpGobj, PopUpLevel;
    public Text AttribType, AttribValue;
    public int DefAttribValue;
    public GameObject card1, card2, card3;
    public GameObject[] companion_Card;

    public GameObject Btn_battle;
   
    public bool allocated,isSaveOrReset;

    bool IspopUp;

    public GameObject selectionText;
    public GameObject red, green, blue, yellow;
    public GameObject CardSelectiomImage;
    public int cardSortNum;
    public bool compSelected;

    public GameObject imagePlaceHolder;
    public GameObject Loader;


    //Temperory variables for CardUpdate
    int tempEP;    

    private void Awake()
    {
        if (Btn_save)
        {
            Btn_save.SetActive(false);
            Btn_Reset.SetActive(false);
            btn_talentPts.SetActive(false);
        }

    }
  
    public void CardLevelChange(bool IsIncrease)
    {       

        int CurEP = UserDataController.Instance.GetEP();

        if (IsIncrease)
        {

            int TotalEP = (CardLevel + 1) * 20;

            if (CardLevel < 10 && CurEP >= TotalEP)
            {

                CurEP -= TotalEP;
                UserDataController.Instance.SetEPower(CurEP);
                tempEP = CurEP;
                Bestcard.GetComponent<BestCardSelectSc>().EPText.text = CurEP.ToString();
                CardLevel += 1;
                txt_CardLvl.GetComponent<Text>().text = CardLevel.ToString();
                PopUpCardLvl.text = CardLevel.ToString();
                TalPts_value += 1;
                TalentPts.text = TalPts_value.ToString();
                btn_talentPts.SetActive(true);

            }

            else if (CardLevel == 10)
            {
                LoadingManager.Instance.ShowInfoPopUp("Max Card Level Reached!");
            }

            else
            {
                LoadingManager.Instance.ShowInfoPopUp("Need " + TotalEP + " EP You Have " + CurEP + " EP!");
            }

        }
        else
        {
            if (CardLevel > 0)
            {
                int TotalEP = (CardLevel) * 20;
                CurEP += TotalEP;
                UserDataController.Instance.SetEPower(CurEP);
                Bestcard.GetComponent<BestCardSelectSc>().EPText.text = CurEP.ToString();
                tempEP = CurEP;
                CardLevel -= 1;
                txt_CardLvl.GetComponent<Text>().text = CardLevel.ToString();
                PopUpCardLvl.text = CardLevel.ToString();
                TalPts_value -= 1;
                TalentPts.text = TalPts_value.ToString();

                if (TalPts_value == 0)
                {
                    btn_talentPts.SetActive(false);
                }
                else
                {
                    btn_talentPts.SetActive(true);
                }
            }
        }
    }

  
    [Serializable]
    public class BaseValueclass
    {        
        public string contract_id;
    }

    public async void GetBaseCardValue(string contract_id)
    {
        
        BaseValueclass Jobj = new BaseValueclass();

        Jobj.contract_id = contract_id;
       
        string bodyJson = JsonUtility.ToJson(Jobj);

        //////Debug.Log(bodyJson);

        string response = await SessionManager.Instance.Post(ConstantManager._url_GetbaseValue, bodyJson, SessionManager.Instance.userToken, true);

        ////////Debug.Log("response" + response);

        if (string.IsNullOrEmpty(response))
            return;


        JSONNode Jnode = JSON.Parse(response);

        if (Jnode["status"] == 1)
        {

            
            DefaultDetails.card_attack =(int) Jnode["data"]["card_attack"];
            DefaultDetails.card_defence =(int) Jnode["data"]["card_defence"];
            DefaultDetails.card_heal =(int) Jnode["data"]["card_heal"];
            DefaultDetails.card_disrupt =(int) Jnode["data"]["card_disrupt"];
            DefaultDetails.card_heart =(int) Jnode["data"]["card_heart"];
        }


        //getCardBaseValue {  }

    }



    public void UseTokenUI()
    {
        BGPanel.SetActive(true);
        GobjSorting.GetComponent<Canvas>().sortingOrder = 5;
    }

    public void SaveAttrib()
    {

        switch (System.Enum.Parse(typeof(AttributeType), AttribType.text))
        {

            case AttributeType.Attack:

                txt_Attack.GetComponent<Text>().text = attackVal.ToString();
                break;

            case AttributeType.Defense:
                txt_Defense.GetComponent<Text>().text = Defense_val.ToString();
                break;

            case AttributeType.Disrupt:
                txt_Disrupt.GetComponent<Text>().text = Disrupt_val.ToString();
                break;

            case AttributeType.Heal:
                txt_Heal.GetComponent<Text>().text = Heal_val.ToString();
                break;
            case "Heart"://Heart Value
                txt_Life.GetComponent<Text>().text = Life_val.ToString();
                break;

        }

        IspopUp = false;
        PopUpGobj.GetComponent<Animator>().SetTrigger("reverse");
        StartCoroutine(DisableAttribPopup());

        if (attackVal != CardDetail.attack || Defense_val != CardDetail.defence || Disrupt_val != CardDetail.disrupt || Heal_val != CardDetail.heal || Life_val != CardDetail.heart)
        {
            isSaveOrReset = true;
            Btn_save.SetActive(true);
            Btn_Reset.SetActive(true);
        }
        else
        {
            isSaveOrReset = false;
            Btn_save.SetActive(false);
            Btn_Reset.SetActive(false);
        }

    }



    public async void FinalSave()
    {

        if (TalPts_value < 0)
        {
            LoadingManager.Instance.ShowLoader("Please Reduce value of Attribute");
            return;
        }

        UpdateCardDetail _CardDetail = new UpdateCardDetail();

        _CardDetail.card_type = CardDetail.Card_Type;
        _CardDetail.contract_id = CardDetail.card_contract_id;
        _CardDetail.update_level = CardLevel.ToString();

        if (attackVal==0)
        {
            attackVal = CardDetail.attack;
        }
        _CardDetail.attack = attackVal;
        //------------
        if (Defense_val == 0)
        {
            Defense_val = CardDetail.defence;
        }

        _CardDetail.defence = Defense_val;
        //---------
        if (Heal_val == 0)
        {
            Heal_val = CardDetail.heal;
        }
        _CardDetail.heal = Heal_val;
        //----------
        if (Disrupt_val == 0)
        {
            Disrupt_val = CardDetail.disrupt;
        }
        _CardDetail.disrupt = Disrupt_val;
        if (Life_val == 0)
        {
            Life_val = CardDetail.heart;
        }
        _CardDetail.heart = Life_val;

        _CardDetail.ep = UserDataController.Instance.GetEP().ToString();

        string bodyJson = JsonUtility.ToJson(_CardDetail);

        //////Debug.Log(bodyJson);

        print(SessionManager.Instance.userToken);

        string response = await SessionManager.Instance.Post(ConstantManager._url_UpdateCard, bodyJson, SessionManager.Instance.userToken, true);

        LoadingManager.Instance.ShowLoader("Updating Card");

        UpdateCardResponse UpdateResponse = JsonUtility.FromJson<UpdateCardResponse>(response);

        //////Debug.Log(UpdateResponse.message);

        if (UpdateResponse.status == 1)
        {
            LoadingManager.Instance.ShowLoader("CardUpdated");
            int mmr =Bestcard.GetComponent<BestCardSelectSc>().mMR_Val;

            if (CardDetail.level>0)
            {
                mmr -= CardDetail.level;
            }

            CardDetail.level = int.Parse(_CardDetail.update_level);

            CardDetail.attack = _CardDetail.attack;
            CardDetail.defence = _CardDetail.defence;
            CardDetail.heal = _CardDetail.heal;
            CardDetail.disrupt = _CardDetail.disrupt;
            CardDetail.heart = _CardDetail.heart;

            mmr += CardDetail.level;
            Bestcard.GetComponent<BestCardSelectSc>().EPText.text = _CardDetail.ep.ToString();
            Bestcard.GetComponent<BestCardSelectSc>().MMRText.text = "MMR: " + mmr.ToString();

        }
        else
        {
            ResetAttrib();
            LoadingManager.Instance.ShowLoader(UpdateResponse.message);
        }

        //////Debug.Log(UpdateResponse.message);

        card1.GetComponent<SelectedCardSc>().CardDetail = this.CardDetail;
        card1.GetComponent<SelectedCardSc>().RefreshCardDetails();

        Btn_save.SetActive(false);
        Btn_Reset.SetActive(false);
        if (TalPts_value <= 0)
        {
            btn_talentPts.SetActive(false);
        }
        Invoke("HideLoader", 1f);
    }

    public void HideLoader()
    {
        LoadingManager.Instance.HideLoader();
    }

    public void ResetAttrib()
    {

        CardLevel = CardDetail.level;
        txt_CardLvl.GetComponent<Text>().text = CardDetail.level.ToString();
        txt_Attack.GetComponent<Text>().text = CardDetail.attack.ToString();
        attackVal = CardDetail.attack;
        txt_Defense.GetComponent<Text>().text = CardDetail.defence.ToString();
        Defense_val = CardDetail.defence;
        txt_Disrupt.GetComponent<Text>().text = CardDetail.disrupt.ToString();
        Disrupt_val = CardDetail.disrupt;
        txt_Heal.GetComponent<Text>().text = CardDetail.heal.ToString();
        Heal_val = CardDetail.heal;
        txt_CardName.GetComponent<Text>().text = CardDetail.name;
        txt_Life.GetComponent<Text>().text = CardDetail.heart.ToString();
        Life_val = CardDetail.heart;
        txt_Level.GetComponent<Text>().text = CardDetail.level.ToString();
        txt_CardNumber.GetComponent<Text>().text = CardDetail.number.ToString();
        HideLevelPopUP();
        HideAttribPopUP();
        TalPts_value = 0;
        TalentPts.text = "0";
        btn_talentPts.SetActive(false);
        Btn_save.SetActive(false);
        Btn_Reset.SetActive(false);

    }

    public async void ResetDefaultAttrib()
    {

        LoadingManager.Instance.HidePopUp();

        int EP = 0;

        if (CardLevel>CardDetail.level)
        {
            for (int i = CardLevel; i > CardDetail.level; i--)
            {
                //////Debug.Log("i=" + i);
                EP += i * 20;
                //////Debug.Log(" EP = " + EP);
            }
        }

        for (int i = CardDetail.level; i > 0 ; i--)
        {
            //////Debug.Log("i=" + i);
            EP += i * 20;
            //////Debug.Log(" EP = " + EP);
        }



        CardLevel = 0;

        txt_CardLvl.GetComponent<Text>().text = "0";

        txt_Attack.GetComponent<Text>().text = DefaultDetails.card_attack.ToString();
        attackVal = DefaultDetails.card_attack;
        txt_Defense.GetComponent<Text>().text = DefaultDetails.card_defence.ToString();
        Defense_val = DefaultDetails.card_defence;
        txt_Disrupt.GetComponent<Text>().text = DefaultDetails.card_disrupt.ToString();
        Disrupt_val = DefaultDetails.card_disrupt;
        txt_Heal.GetComponent<Text>().text = DefaultDetails.card_heal.ToString();
        Heal_val = DefaultDetails.card_heal;
        txt_CardName.GetComponent<Text>().text = CardDetail.name;
        txt_Life.GetComponent<Text>().text = DefaultDetails.card_heart.ToString();
        Life_val = DefaultDetails.card_heart;
        txt_Level.GetComponent<Text>().text = "0";
        txt_CardNumber.GetComponent<Text>().text = CardDetail.number.ToString();

        HideLevelPopUP();

        HideAttribPopUP();

        TalPts_value = 0;

        TalentPts.text = "0";

        btn_talentPts.SetActive(false);
        Btn_save.SetActive(false);
        Btn_Reset.SetActive(false);
        //////Debug.Log("EP___"+UserDataController.Instance.GetEP());
        EP = EP + UserDataController.Instance.GetEP();

        UserDataController.Instance.SetEP(EP);

        UpdateCardDetail _CardDetail = new UpdateCardDetail();

        _CardDetail.card_type = CardDetail.Card_Type;
        _CardDetail.contract_id = CardDetail.card_contract_id;
        _CardDetail.update_level = CardLevel.ToString();
        _CardDetail.attack = DefaultDetails.card_attack;
        _CardDetail.defence = Defense_val;
        _CardDetail.heal = Heal_val;
        _CardDetail.disrupt = Disrupt_val;
        _CardDetail.heart = Life_val;
        _CardDetail.ep = EP.ToString();

        string bodyJson = JsonUtility.ToJson(_CardDetail);

        //////Debug.Log(bodyJson);

        print(SessionManager.Instance.userToken);

        string response = await SessionManager.Instance.Post(ConstantManager._url_UpdateCard, bodyJson, SessionManager.Instance.userToken, true);

        LoadingManager.Instance.ShowLoader("Reseting Card");

        UpdateCardResponse UpdateResponse = JsonUtility.FromJson<UpdateCardResponse>(response);

        //////Debug.Log(UpdateResponse.message);

        if (UpdateResponse.status == 1)
        {

            LoadingManager.Instance.ShowLoader("Sucessfully Reset");

            int mmr = Bestcard.GetComponent<BestCardSelectSc>().mMR_Val;

            if (CardDetail.level > 0)
            {
                mmr -= CardDetail.level;
            }

            CardDetail.level = int.Parse(_CardDetail.update_level);
            CardDetail.attack = _CardDetail.attack;
            CardDetail.defence = _CardDetail.defence;
            CardDetail.heal = _CardDetail.heal;
            CardDetail.disrupt = _CardDetail.disrupt;
            CardDetail.heart = _CardDetail.heart;

            mmr += CardDetail.level;
            Bestcard.GetComponent<BestCardSelectSc>().EPText.text = _CardDetail.ep.ToString();
            Bestcard.GetComponent<BestCardSelectSc>().MMRText.text = "MMR: " + mmr.ToString();

        }
        else
        {
            ResetAttrib();
            LoadingManager.Instance.ShowLoader(UpdateResponse.message);
        }

        //////Debug.Log(UpdateResponse.message);

        card1.GetComponent<SelectedCardSc>().CardDetail = this.CardDetail;
        card1.GetComponent<SelectedCardSc>().RefreshCardDetails();

        Btn_save.SetActive(false);
        Btn_Reset.SetActive(false);
        if (TalPts_value <= 0)
        {
            btn_talentPts.SetActive(false);
        }
        Invoke("HideLoader", 1f);



    }

    public void ShowAttribPopUP(string AtType) //Attribute increase or Decrease Value
    {

        if (!BGPanel.activeSelf)
        {
            return;
        }

        AttribType.text = AtType;

        DefAttribValue = TalPts_value;

        
        switch (System.Enum.Parse(typeof(AttributeType), AtType))
        {
            case AttributeType.Attack:
                attackVal = int.Parse(txt_Attack.GetComponent<Text>().text);
                AttribValue.GetComponent<Text>().text = attackVal.ToString();
                TalentPts.text = TalPts_value.ToString();
                break;

            case AttributeType.Defense:
                print(txt_Defense.GetComponent<Text>().text);
                Defense_val = int.Parse(txt_Defense.GetComponent<Text>().text);
                AttribValue.GetComponent<Text>().text = Defense_val.ToString();
                TalentPts.text = TalPts_value.ToString();
                break;

            case AttributeType.Disrupt:
                Disrupt_val = int.Parse(txt_Disrupt.GetComponent<Text>().text);
                AttribValue.GetComponent<Text>().text = Disrupt_val.ToString();
                TalentPts.text = TalPts_value.ToString();
                break;

            case AttributeType.Heal:
                Heal_val = int.Parse(txt_Heal.GetComponent<Text>().text);
                AttribValue.GetComponent<Text>().text = Heal_val.ToString();
                TalentPts.text = TalPts_value.ToString();
                break;
            case "Heart"://Heart Value
                Life_val = int.Parse(txt_Life.GetComponent<Text>().text);
                AttribValue.GetComponent<Text>().text = Life_val.ToString();
                TalentPts.text = TalPts_value.ToString();
                break;

        }
        PopUpGobj.SetActive(true);
        IspopUp = true;
    }


    public void OnBGClick() //To HidePopUP and Card Attribute Edit
    {
        print(IspopUp);
        if (IspopUp)
        {
            HideAttribPopUP();
            HideLevelPopUP();
        }
        else
        {
            BGPanel.SetActive(false);
            GobjSorting.GetComponent<Canvas>().sortingOrder = 2;
        }
    }

    public void OpenLevelPopUP()
    {
        PopUpLevel.SetActive(true);
        PopUpCardLvl.text = txt_CardLvl.GetComponent<Text>().text;
        IspopUp = true;
    }

    public void HideLevelPopUP()
    {       
        PopUpLevel.GetComponent<Animator>().SetTrigger("reverse");
        StartCoroutine(DisableLevelPopup());
        IspopUp = false;
    }

    IEnumerator DisableLevelPopup()
    {
        yield return new WaitForSeconds(.7f);
        PopUpLevel.SetActive(false);
    }
    public void HideAttribPopUP()
    {
        IspopUp = false;
        TalentPts.text = DefAttribValue.ToString();
        TalPts_value = DefAttribValue;
        attackVal = 0;
        PopUpGobj.GetComponent<Animator>().SetTrigger("reverse");
        StartCoroutine(DisableAttribPopup());
    }

    IEnumerator DisableAttribPopup()
    {
        yield return new WaitForSeconds(.7f);
        PopUpGobj.SetActive(false);
    }

    public void Increase_Attrib() //Increase Attribute Value
    {
        switch (System.Enum.Parse(typeof(AttributeType), AttribType.text))
        {
            case AttributeType.Attack:
                if (TalPts_value > 0 && attackVal < 10)
                {
                    attackVal += 1;
                    TalPts_value -= 1;
                    AttribValue.GetComponent<Text>().text = attackVal.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;

            case AttributeType.Defense:
                if (TalPts_value > 0 && Defense_val < 10)
                {
                    Defense_val += 1;
                    TalPts_value -= 1;
                    AttribValue.GetComponent<Text>().text = Defense_val.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;

            case AttributeType.Disrupt:
                if (TalPts_value > 0 && Disrupt_val < 10)
                {
                    Disrupt_val += 1;
                    TalPts_value -= 1;
                    AttribValue.GetComponent<Text>().text = Disrupt_val.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;

            case AttributeType.Heal:
                if (TalPts_value > 0 && Heal_val < 10)
                {
                    Heal_val += 1;
                    TalPts_value -= 1;
                    AttribValue.GetComponent<Text>().text = Heal_val.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;
            case "Heart"://Heart Value
                if (TalPts_value > 0 && Life_val < 20)
                {
                    Life_val += 1;
                    TalPts_value -= 1;
                    AttribValue.GetComponent<Text>().text = Life_val.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;
        }
    }


    public void Decrease_Attrib()
    {

        switch (System.Enum.Parse(typeof(AttributeType), AttribType.text))
        {
            case AttributeType.Attack:
                if (attackVal > DefaultDetails.card_attack)
                {
                    attackVal -= 1;
                    TalPts_value += 1;
                    AttribValue.GetComponent<Text>().text = attackVal.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;

            case AttributeType.Defense:
                if (Defense_val > DefaultDetails.card_defence)
                {
                    Defense_val -= 1;
                    TalPts_value += 1;
                    AttribValue.GetComponent<Text>().text = Defense_val.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;

            case AttributeType.Disrupt:
                if (Disrupt_val > DefaultDetails.card_disrupt)
                {
                    Disrupt_val -= 1;
                    TalPts_value += 1;
                    AttribValue.GetComponent<Text>().text = Disrupt_val.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;

            case AttributeType.Heal:
                if (Heal_val > DefaultDetails.card_heal)
                {
                    Heal_val -= 1;
                    TalPts_value += 1;
                    AttribValue.GetComponent<Text>().text = Heal_val.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;
            case "Heart": //Heart Value
                if (Life_val > DefaultDetails.card_heart)
                {
                    Life_val -= 1;
                    TalPts_value += 1;
                    AttribValue.GetComponent<Text>().text = Life_val.ToString();
                    TalentPts.text = TalPts_value.ToString();
                }
                break;

        }

    }
    //public GameObject card;
    private Sprite cardImage;
    public Texture2D texture;

    private void ImageFromDataPath()
    {
        if (string.IsNullOrEmpty(this.CardDetail.card_contract_id)) return;
        string path = Application.persistentDataPath + "/GameCards" + "/" + this.CardDetail.card_contract_id + ".png";
        string ImgLink = CardDetail.s3_Image;
        string card_contract_id = this.CardDetail.card_contract_id;
        string DIRPath = Path.Combine(Application.persistentDataPath, "GameCards");
        string FilePath = Path.Combine(DIRPath, card_contract_id + ".png");
        StartCoroutine(setimage(ImgLink, card_contract_id, DIRPath, path, ShowPlaceHolder)) ; 
    }



    IEnumerator setimage(string ImgLink, string card_contract_id, string DIRPath, string FilePath, System.Action<bool> onPlaceHolder = null)
    {
        if (string.IsNullOrEmpty(ImgLink))
        {
            onPlaceHolder?.Invoke(true);
            yield break;
        }


        if (System.IO.File.Exists(FilePath))
        {
            byte[] bytes = System.IO.File.ReadAllBytes(FilePath);
             texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            CardImage.sprite = sprite;
            onPlaceHolder?.Invoke(false);
            yield break;
        }



        UnityWebRequest www = UnityWebRequestTexture.GetTexture(ImgLink);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            ////Debug.Log(www.error);
             onPlaceHolder?.Invoke(true);
        }
        else
        {
             texture = new Texture2D(1, 1);
            texture = DownloadHandlerTexture.GetContent(www);

            if (!Directory.Exists(DIRPath))
            {

                Directory.CreateDirectory(DIRPath);

                //////Debug.Log("Created Dir" + DIRPath);
            }

            byte[] bytes = texture.EncodeToPNG();
            File.WriteAllBytes(FilePath, bytes);

            Destroy(texture);
            bytes = System.IO.File.ReadAllBytes(FilePath);
            //yield return new WaitForSeconds(2);
            texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            cardImage = sprite;
            CardImage.sprite = sprite;
            onPlaceHolder?.Invoke(false);
            CardImage.enabled = false;
            CardImage.enabled = true;

            www.Dispose();
            bytes = new byte[0];
            System.GC.Collect();

        }
    }
    private void OnDestroy()
    {
        Destroy(texture);
    }
    public void RefreshCardDetails(bool isDelay) //displaying details of card in 3 Selected Card
    {
        RefreshCardDetails();
    }


    public void RefreshCardDetails() //displaying details of card in 3 Selected Card
    {

        ImageFromDataPath();



        CardLevel = CardDetail.level;
        txt_CardLvl.GetComponent<Text>().text = CardLevel.ToString();
        txt_Attack.GetComponent<Text>().text = CardDetail.attack.ToString();
        attackVal = CardDetail.attack;
        txt_Defense.GetComponent<Text>().text = CardDetail.defence.ToString();
        Defense_val = CardDetail.defence;
        txt_Disrupt.GetComponent<Text>().text = CardDetail.disrupt.ToString();
        Disrupt_val = CardDetail.disrupt;
        txt_Heal.GetComponent<Text>().text = CardDetail.heal.ToString();
        Heal_val = CardDetail.heal;
        txt_CardName.GetComponent<Text>().text = CardDetail.name;
        txt_Life.GetComponent<Text>().text = CardDetail.heart.ToString();
        Life_val = CardDetail.heart;
        txt_Level.GetComponent<Text>().text = CardDetail.level.ToString();
        txt_CardNumber.GetComponent<Text>().text = CardDetail.number.ToString();
        TalPts_value = 0;

        if (SpecialAbility.Length > 0)
        {
            for (int i = 0; i < SpecialAbility.Length; i++)
            {
                SpecialAbility[i].SetActive(false);
            }

            switch (CardDetail.strength_1)
            {
                case AttributeType.Attack:
                    SpecialAbility[0].SetActive(true);
                    break;
                case AttributeType.Defense:
                    SpecialAbility[1].SetActive(true);
                    break;
                case AttributeType.Disrupt:
                    SpecialAbility[2].SetActive(true);
                    break;
                case AttributeType.Heal:
                    SpecialAbility[3].SetActive(true);
                    break;
            }
            switch (CardDetail.strength_2)
            {
                case AttributeType.Attack:
                    SpecialAbility[0].SetActive(true);
                    break;
                case AttributeType.Defense:
                    SpecialAbility[1].SetActive(true);
                    break;
                case AttributeType.Disrupt:
                    SpecialAbility[2].SetActive(true);
                    break;
                case AttributeType.Heal:
                    SpecialAbility[3].SetActive(true);
                    break;
            }

        }

        if (TalentPts)
        {
            TalentPts.text = TalPts_value.ToString();
            btn_talentPts.SetActive(false);
            Btn_save.SetActive(false);
            Btn_Reset.SetActive(false);
        }

        if (Elementeums)
        {
            if (CardDetail.elementeum > 0)
            {
                Elementeums.SetActive(true);
            }

            for (int i = 0; i < Elementeums.transform.childCount; i++)
            {
                Elementeums.transform.GetChild(i).gameObject.SetActive(false);
            }

            for (int i = 0; i < CardDetail.elementeum; i++)
            {
                Elementeums.transform.GetChild(i).gameObject.SetActive(true);
            }
        }

    }

    //IEnumerator DelayRefresh()
    //{
    //    yield return new WaitForSeconds(3f);


    //    Texture2D Cardtexture = CardManagerSC.Instance.GetImageTexture(this.CardDetail.card_contract_id, CardDetail.s3_Image);

    //    CardImage.sprite = Sprite.Create(Cardtexture, new Rect(0, 0, Cardtexture.width, Cardtexture.height), new Vector2(0, 0));


    //    CardLevel = CardDetail.level;
    //    txt_CardLvl.GetComponent<Text>().text = CardLevel.ToString();
    //    txt_Attack.GetComponent<Text>().text = CardDetail.attack.ToString();
    //    attackVal = CardDetail.attack;
    //    txt_Defense.GetComponent<Text>().text = CardDetail.defence.ToString();
    //    Defense_val = CardDetail.defence;
    //    txt_Disrupt.GetComponent<Text>().text = CardDetail.disrupt.ToString();
    //    Disrupt_val = CardDetail.disrupt;
    //    txt_Heal.GetComponent<Text>().text = CardDetail.heal.ToString();
    //    Heal_val = CardDetail.heal;
    //    txt_CardName.GetComponent<Text>().text = CardDetail.name;
    //    txt_Life.GetComponent<Text>().text = CardDetail.heart.ToString();
    //    Life_val = CardDetail.heart;
    //    txt_Level.GetComponent<Text>().text = CardDetail.level.ToString();
    //    txt_CardNumber.GetComponent<Text>().text = CardDetail.number.ToString();
    //    TalPts_value = 0;

    //    if (SpecialAbility.Length > 0)
    //    {
    //        for (int i = 0; i < SpecialAbility.Length; i++)
    //        {
    //            SpecialAbility[i].SetActive(false);
    //        }

    //        switch (CardDetail.strength_1.ToLower())
    //        {
    //            case CardStrengthType.Attack:
    //                SpecialAbility[0].SetActive(true);
    //                break;
    //            case CardStrengthType.Defense:
    //                SpecialAbility[1].SetActive(true);
    //                break;
    //            case CardStrengthType.Disrupt:
    //                SpecialAbility[2].SetActive(true);
    //                break;
    //            case CardStrengthType.Heal:
    //                SpecialAbility[3].SetActive(true);
    //                break;
    //        }
    //        switch (CardDetail.strength_2)
    //        {
    //            case CardStrengthType.Attack:
    //                SpecialAbility[0].SetActive(true);
    //                break;
    //            case CardStrengthType.Defense:
    //                SpecialAbility[1].SetActive(true);
    //                break;
    //            case CardStrengthType.Disrupt:
    //                SpecialAbility[2].SetActive(true);
    //                break;
    //            case CardStrengthType.Heal:
    //                SpecialAbility[3].SetActive(true);
    //                break;
    //        }

    //    }

    //    if (TalentPts)
    //    {
    //        TalentPts.text = TalPts_value.ToString();
    //        btn_talentPts.SetActive(false);
    //        Btn_save.SetActive(false);
    //        Btn_Reset.SetActive(false);
    //    }

    //    if (Elementeums)
    //    {
    //        if (CardDetail.elementeum > 0)
    //        {
    //            Elementeums.SetActive(true);
    //        }

    //        for (int i = 0; i < Elementeums.transform.childCount; i++)
    //        {
    //            Elementeums.transform.GetChild(i).gameObject.SetActive(false);
    //        }

    //        for (int i = 0; i < CardDetail.elementeum; i++)
    //        {
    //            Elementeums.transform.GetChild(i).gameObject.SetActive(true);
    //        }
    //    }
    //}

    
    public void SelectCard() //this function is used to allocate value to any card from those 3
    {

        if (CardDetail == card1.GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == card2.GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == card3.GetComponent<SelectedCardSc>().CardDetail)
        {
            return;
        }
        if (!card1.GetComponent<SelectedCardSc>().allocated)
        {

            BestCardSelectSc.Instance.charCardSeleNum += 1;
            card1.GetComponent<SelectedCardSc>().CardDetail = this.CardDetail;
            card1.GetComponent<SelectedCardSc>().RefreshCardDetails();
            card1.GetComponent<SelectedCardSc>().GetBaseCardValue(CardDetail.card_contract_id);
            card1.GetComponent<SelectedCardSc>().AssignCard(this.gameObject);
            card1.GetComponent<SelectedCardSc>().card1 = this.gameObject;

            //card1.GetComponent<SelectedCardSc>().CardSelectiomImage.SetActive(false);

        }
        else if (!card2.GetComponent<SelectedCardSc>().allocated)
        {
            BestCardSelectSc.Instance.charCardSeleNum += 1;

            card2.GetComponent<SelectedCardSc>().CardDetail = this.CardDetail;
            card2.GetComponent<SelectedCardSc>().RefreshCardDetails();
            card2.GetComponent<SelectedCardSc>().GetBaseCardValue(CardDetail.card_contract_id);
            card2.GetComponent<SelectedCardSc>().AssignCard(this.gameObject);
            card2.GetComponent<SelectedCardSc>().card1 = this.gameObject;

            //card2.GetComponent<SelectedCardSc>().CardSelectiomImage.SetActive(false);
        }
        else if (!card3.GetComponent<SelectedCardSc>().allocated)
        {
            BestCardSelectSc.Instance.charCardSeleNum += 1;

            card3.GetComponent<SelectedCardSc>().CardDetail = this.CardDetail;
            card3.GetComponent<SelectedCardSc>().RefreshCardDetails();
            card3.GetComponent<SelectedCardSc>().GetBaseCardValue(CardDetail.card_contract_id);
            card3.GetComponent<SelectedCardSc>().AssignCard(this.gameObject);
            card3.GetComponent<SelectedCardSc>().card1 = this.gameObject;

            //card3.GetComponent<SelectedCardSc>().CardSelectiomImage.SetActive(false);
        }

        if (card1.GetComponent<SelectedCardSc>().allocated && card2.GetComponent<SelectedCardSc>().allocated && card3.GetComponent<SelectedCardSc>().allocated)
        {
            Btn_battle.SetActive(true);
        }
        else
        {
            Btn_battle.SetActive(false);
        }

        Bestcard.GetComponent<BestCardSelectSc>().CalculateMMR();

    }

    public void SelectCampanionCards(GameObject cardObject)
    {
        var cardobject = cardObject.GetComponent<SelectedCardSc>().CardDetail;
        //////Debug.Log(cardobject.name);
        if (cardObject.GetComponent<SelectedCardSc>().compSelected)
        {


            if (companion_Card[cardObject.GetComponent<SelectedCardSc>().cardSortNum].activeInHierarchy)
            {


                companion_Card[cardObject.GetComponent<SelectedCardSc>().cardSortNum].GetComponent<SelectedCardSc>().UnassignCompanionCard(cardobject);
                companion_Card[cardObject.GetComponent<SelectedCardSc>().cardSortNum].GetComponent<SelectedCardSc>().allocated = false;
                companion_Card[cardObject.GetComponent<SelectedCardSc>().cardSortNum].GetComponent<SelectedCardSc>().CardDetail = new Card();
                companion_Card[cardObject.GetComponent<SelectedCardSc>().cardSortNum].GetComponent<SelectedCardSc>().selectedGobj.SetActive(false);
                companion_Card[cardObject.GetComponent<SelectedCardSc>().cardSortNum].GetComponent<SelectedCardSc>().NotSelectedObj.SetActive(true);
                selectionText.SetActive(false);
                yellow.SetActive(false);
                red.SetActive(false);
                green.SetActive(false);
                blue.SetActive(false);
                companion_Card[cardObject.GetComponent<SelectedCardSc>().cardSortNum].gameObject.SetActive(false);
                cardObject.GetComponent<SelectedCardSc>().compSelected = false;
            }    return;

        }
        for (int i = 0; i < UserDataController.Instance.profileData.companion_card.Count; i++)
        {

            //////Debug.Log(cardobject.name);
            //////Debug.Log(UserDataController.Instance.profileData.companion_card[i].name.ToString());

            if (UserDataController.Instance.profileData.companion_card[i].name == cardobject.name)
            {
                //////Debug.Log("Card Matched");
                //////Debug.Log(cardobject.name + " " + i + " card Removed");

                //companion_Card[i].GetComponent<SelectedCardSc>().RemoveCardFromCompanionCard(this.CardDetail);
                companion_Card[i].GetComponent<SelectedCardSc>().UnassignCompanionCard(cardobject);
                companion_Card[i].GetComponent<SelectedCardSc>().allocated = false;
                companion_Card[i].GetComponent<SelectedCardSc>().CardDetail = new Card();
                companion_Card[i].GetComponent<SelectedCardSc>().selectedGobj.SetActive(false);
                companion_Card[i].GetComponent<SelectedCardSc>().NotSelectedObj.SetActive(true);
                selectionText.SetActive(false);
                yellow.SetActive(false);
                red.SetActive(false);
                green.SetActive(false);
                blue.SetActive(false);
                companion_Card[i].gameObject.SetActive(false);
                return;
            }
           
        }
        if (CardDetail == companion_Card[0].GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == companion_Card[1].GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == companion_Card[2].GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == companion_Card[3].GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == companion_Card[4].GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == companion_Card[5].GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == companion_Card[6].GetComponent<SelectedCardSc>().CardDetail ||
            CardDetail == companion_Card[7].GetComponent<SelectedCardSc>().CardDetail
            )
        {
            return;
        }

        if (BestCardSelectSc.Instance.selectedCompanionCardNum < 8)
        {
            
            selectionText.SetActive(true);
            BestCardSelectSc.Instance.selectedCompanionCardNum += 1;
            if (!companion_Card[0].GetComponent<SelectedCardSc>().allocated)
            {
                companion_Card[0].SetActive(true);
                SelectedCompanionCard(companion_Card[0], cardObject);
                cardObject.GetComponent<SelectedCardSc>().cardSortNum = companion_Card[0].GetComponent<SelectedCardSc>().cardSortNum;
                cardObject.GetComponent<SelectedCardSc>().compSelected = true;
            }
            else if (!companion_Card[1].GetComponent<SelectedCardSc>().allocated)
            {
                companion_Card[1].SetActive(true);
                SelectedCompanionCard(companion_Card[1], cardObject);
                cardObject.GetComponent<SelectedCardSc>().cardSortNum = companion_Card[1].GetComponent<SelectedCardSc>().cardSortNum;
                cardObject.GetComponent<SelectedCardSc>().compSelected = true;
            }
            else if (!companion_Card[2].GetComponent<SelectedCardSc>().allocated)
            {
                companion_Card[2].SetActive(true);
                SelectedCompanionCard(companion_Card[2], cardObject);
                cardObject.GetComponent<SelectedCardSc>().cardSortNum = companion_Card[2].GetComponent<SelectedCardSc>().cardSortNum;
                cardObject.GetComponent<SelectedCardSc>().compSelected = true;
            }
            else if (!companion_Card[3].GetComponent<SelectedCardSc>().allocated)
            {
                companion_Card[3].SetActive(true);
                SelectedCompanionCard(companion_Card[3], cardObject);
                cardObject.GetComponent<SelectedCardSc>().cardSortNum = companion_Card[3].GetComponent<SelectedCardSc>().cardSortNum;
                cardObject.GetComponent<SelectedCardSc>().compSelected = true;
            }
            else if (!companion_Card[4].GetComponent<SelectedCardSc>().allocated)
            {
                companion_Card[4].SetActive(true);
                SelectedCompanionCard(companion_Card[4], cardObject);
                cardObject.GetComponent<SelectedCardSc>().cardSortNum = companion_Card[4].GetComponent<SelectedCardSc>().cardSortNum;
                cardObject.GetComponent<SelectedCardSc>().compSelected = true;
            }
            else if (!companion_Card[5].GetComponent<SelectedCardSc>().allocated)
            {
                companion_Card[5].SetActive(true);
                SelectedCompanionCard(companion_Card[5], cardObject);
                cardObject.GetComponent<SelectedCardSc>().cardSortNum = companion_Card[5].GetComponent<SelectedCardSc>().cardSortNum;
                cardObject.GetComponent<SelectedCardSc>().compSelected = true;
            }
            else if (!companion_Card[6].GetComponent<SelectedCardSc>().allocated)
            {
                companion_Card[6].SetActive(true);
                SelectedCompanionCard(companion_Card[6], cardObject);
                cardObject.GetComponent<SelectedCardSc>().cardSortNum = companion_Card[6].GetComponent<SelectedCardSc>().cardSortNum;
                cardObject.GetComponent<SelectedCardSc>().compSelected = true;
            }
            else if (!companion_Card[7].GetComponent<SelectedCardSc>().allocated)
            {
                companion_Card[7].SetActive(true);
                SelectedCompanionCard(companion_Card[7], cardObject);
                cardObject.GetComponent<SelectedCardSc>().cardSortNum = companion_Card[7].GetComponent<SelectedCardSc>().cardSortNum;
                cardObject.GetComponent<SelectedCardSc>().compSelected = true;
            }
        }
        else
        {
            //////Debug.Log("Companion adding range in full you can not add more Companions in the list");
        }
    }
    public void SelectedCompanionCard(GameObject companion_Cards, GameObject cardDetail)
    {
        var cardobject = cardDetail.GetComponent<SelectedCardSc>().CardDetail;

        companion_Cards.GetComponent<SelectedCardSc>().CardDetail = cardobject;
        companion_Cards.GetComponent<SelectedCardSc>().RefreshCardDetails();
        companion_Cards.GetComponent<SelectedCardSc>().GetBaseCardValue(CardDetail.card_contract_id);
        companion_Cards.GetComponent<SelectedCardSc>().AssignCompanionCard(cardDetail);
        companion_Cards.GetComponent<SelectedCardSc>().companion_Card[0] = this.gameObject;

        if (companion_Cards.GetComponent<SelectedCardSc>().CardDetail.heal > 0)
        {
            //////Debug.Log("Heal is selected ");
            companion_Cards.GetComponent<SelectedCardSc>().txt_Heal.gameObject.SetActive(true);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Attack.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Defense.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Disrupt.gameObject.SetActive(false);

            yellow.SetActive(false);
            red.SetActive(false);
            green.SetActive(false);
            blue.SetActive(true);
        }
        if (companion_Cards.GetComponent<SelectedCardSc>().CardDetail.attack > 0)
        {
            //////Debug.Log("attack is selected ");
            companion_Cards.GetComponent<SelectedCardSc>().txt_Heal.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Attack.gameObject.SetActive(true);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Defense.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Disrupt.gameObject.SetActive(false);
            yellow.SetActive(false);
            red.SetActive(true);
            green.SetActive(false);
            blue.SetActive(false);
        }
        if (companion_Cards.GetComponent<SelectedCardSc>().CardDetail.defence > 0)
        {
            //////Debug.Log("defence is selected ");
            companion_Cards.GetComponent<SelectedCardSc>().txt_Heal.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Attack.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Defense.gameObject.SetActive(true);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Disrupt.gameObject.SetActive(false);
            yellow.SetActive(false);
            red.SetActive(false);
            green.SetActive(true);
            blue.SetActive(false);

        }
        if (companion_Cards.GetComponent<SelectedCardSc>().CardDetail.disrupt > 0)
        {
            //////Debug.Log("disrupt is selected ");
            companion_Cards.GetComponent<SelectedCardSc>().txt_Heal.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Attack.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Defense.gameObject.SetActive(false);
            companion_Cards.GetComponent<SelectedCardSc>().txt_Disrupt.gameObject.SetActive(true);
            yellow.SetActive(true);
            red.SetActive(false);
            green.SetActive(false);
            blue.SetActive(false);
        }
        companion_Cards.GetComponent<SelectedCardSc>().txt_CardNumber.gameObject.SetActive(false);
        companion_Cards.GetComponent<SelectedCardSc>().txt_Level.gameObject.SetActive(false);
        companion_Cards.GetComponent<SelectedCardSc>().txt_Life.gameObject.SetActive(false);


    }

    public void AddCardToBestCard()
    {
        //UserDataController.Instance.profileData.best_card.Add(this.CardDetail);
    }
   
    public void RemoveCardFromBestCard()
    {
        //UserDataController.Instance.profileData.best_card.Remove(this.CardDetail);
        Btn_battle.SetActive(false);

    }
   
    public void AssignCard(GameObject card) //this function is used to assign carddetails to selected cards
    {
        allocated = true;
        selectedGobj.SetActive(true);
        NotSelectedObj.SetActive(false);
        card.GetComponent<SelectedCardSc>().CardSelectiomImage.SetActive(true);
        AddCardToBestCard();
        
    }
    public void UnassignCard()
    {
        for (int i = 0; i < BestCardSelectSc.Instance.cardScrollView.childCount; i++)
        {
            if (this.CardDetail.card_contract_id == BestCardSelectSc.Instance.cardScrollView.GetChild(i).GetComponent<SelectedCardSc>().CardDetail.card_contract_id)
            {
                BestCardSelectSc.Instance.cardScrollView.GetChild(i).GetComponent<SelectedCardSc>().CardSelectiomImage.SetActive(false);
            }

        }

        allocated = false;
        CardDetail = new Card();
        selectedGobj.SetActive(false);
        NotSelectedObj.SetActive(true);

        RemoveCardFromBestCard();

        Bestcard.GetComponent<BestCardSelectSc>().CalculateMMR();

        BestCardSelectSc.Instance.charCardSeleNum -= 1;

        Destroy(texture);
       
    }

    public void AddCardToCompanioncCard(GameObject cardObject)
    {
        var cardobject = cardObject.GetComponent<SelectedCardSc>().CardDetail;
        UserDataController.Instance.profileData.companion_card.Add(cardobject);
    }
    public void RemoveCardFromCompanionCard(Card cardDetail)
    {
        //////Debug.Log(cardDetail.name);

        UserDataController.Instance.profileData.companion_card.Remove(cardDetail);
        BestCardSelectSc.Instance.selectedCompanionCardNum -= 1;
        //Btn_battle.SetActive(false);
    }
    public void AssignCompanionCard(GameObject cardobject) //this function is used to assign carddetails to selected cards
    {
        allocated = true;
        selectedGobj.SetActive(true);
        NotSelectedObj.SetActive(false);
        AddCardToCompanioncCard(cardobject);
    }
    public void UnassignCompanionCard(Card cardDetail)
    {
        allocated = false;
        CardDetail = new Card();
        selectedGobj.SetActive(false);
        NotSelectedObj.SetActive(true);
        RemoveCardFromCompanionCard(cardDetail);
        Destroy(texture);
        //UserDataController.Instance.profileData.companion_card.Clear();
    }



    public void ShowPlaceHolder(bool _isShow)
    {
       
            imagePlaceHolder.gameObject.SetActive(_isShow);
            Loader.gameObject.SetActive(_isShow);

       

    }
    private void Update()
    {
        Loader.transform.Rotate(0, 0, -45 * Time.deltaTime * 10);
    }
}
