﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

//[JsonProperty(ItemConverterType = typeof(AttributeTypeEnumConverter))]

[Serializable]
public class Card
{

    [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
    public string name { get; set; }

    [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
    public string type { get; set; }

    [JsonProperty("rarity", NullValueHandling = NullValueHandling.Ignore)]
    public string rarity { get; set; }

    [JsonProperty("element_1", NullValueHandling = NullValueHandling.Ignore)]
    public string element_1 { get; set; }

    [JsonProperty("element_2", NullValueHandling = NullValueHandling.Ignore)]
    public string element_2 { get; set; }

    [JsonProperty(ItemConverterType = typeof(AttributeTypeEnumConverter), NullValueHandling = NullValueHandling.Ignore)]
    public AttributeType strength_1 { get; set; }

    [JsonProperty(ItemConverterType = typeof(AttributeTypeEnumConverter), NullValueHandling = NullValueHandling.Ignore)]
    public AttributeType strength_2 { get; set; }

    [JsonProperty("generation", NullValueHandling = NullValueHandling.Ignore)]
    public string generation { get; set; }



    //[JsonProperty(ItemConverterType = typeof(IntStringConverter), NullValueHandling = NullValueHandling.Ignore)]

    [JsonProperty("number", NullValueHandling = NullValueHandling.Ignore)]
    private string number1;
    [JsonIgnore]
    public int number
    {
        get
        {
            int temp = 0;
            int.TryParse(number1, out temp);
            return temp;
        }
        set
        {
            number1 = value.ToString();
        }
    }


    [JsonProperty("elementeum", NullValueHandling = NullValueHandling.Ignore)]
    private string elementeum1;
    [JsonIgnore]
    public int elementeum
    {
        get
        {
            int temp = 0;
            int.TryParse(elementeum1, out temp);
            return temp;
        }
        set
        {
            elementeum1 = value.ToString();
        }
    }

    [JsonProperty("attack", NullValueHandling = NullValueHandling.Ignore)]
    private string attack1;
    [JsonIgnore]
    public int attack
    {
        get
        {
            int temp = 0;
            int.TryParse(attack1, out temp);
            return temp;
        }
        set
        {
            attack1 = value.ToString();
        }
    }

    [JsonProperty("defence", NullValueHandling = NullValueHandling.Ignore)]
    private string defence1;
    [JsonIgnore]
    public int defence
    {
        get
        {
            int temp = 0;
            int.TryParse(defence1, out temp);
            return temp;
        }
        set
        {
            defence1 = value.ToString();
        }
    }

    [JsonProperty("heal", NullValueHandling = NullValueHandling.Ignore)]
    private string heal1;
    [JsonIgnore]
    public int heal
    {
        get
        {
            int temp = 0;
            int.TryParse(heal1, out temp);
            return temp;
        }
        set
        {
            heal1 = value.ToString();
        }
    }

    [JsonProperty("disrupt", NullValueHandling = NullValueHandling.Ignore)]
    private string disrupt1;
    [JsonIgnore]
    public int disrupt
    {
        get
        {
            int temp = 0;
            int.TryParse(disrupt1, out temp);
            return temp;
        }
        set
        {
            disrupt1 = value.ToString();
        }
    }

    [JsonProperty("heart", NullValueHandling = NullValueHandling.Ignore)]
    private string heart1;
    [JsonIgnore]
    public int heart
    {
        get
        {
            int temp = 0;
            int.TryParse(heart1, out temp);
            return temp;
        }
        set
        {
            heart1 = value.ToString();
        }
    }

    [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
    private string value1;
    [JsonIgnore]
    public int value
    {
        get
        {
            int temp = 0;
            int.TryParse(value1, out temp);
            return temp;
        }
        set
        {
            value1 = value.ToString();
        }
    }

    [JsonProperty("card_contract_id", NullValueHandling = NullValueHandling.Ignore)]
    public string card_contract_id { get; set; }

    [JsonProperty(ItemConverterType = typeof(IntStringConverter), NullValueHandling = NullValueHandling.Ignore)] //[JsonProperty("rarity_value")]
    public int rarity_value { get; set; }

    [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
    public string Image { get; set; }

    [JsonProperty("s3_image", NullValueHandling = NullValueHandling.Ignore)]
    public string s3_Image { get; set; }

    [JsonProperty("card_contract_add", NullValueHandling = NullValueHandling.Ignore)]
    public string card_contract_add { get; set; }


    public string Card_Type;
    public string CardGroup;
    public int level;


    public int AbilityValue(bool companion = true)
    {
        if (companion)
        {
            return AbilityValue(strength_1);
        }
        else
        {
            return AbilityValue(strength_1) + AbilityValue(strength_2);
        }
    }

    public int AbilityValue(AttributeType strength)
    {
        switch (strength)
        {
            case AttributeType.None:
                return 0;
            case AttributeType.Attack:
                return attack;
            case AttributeType.Defense:
                return defence;
            case AttributeType.Disrupt:
                return disrupt;
            case AttributeType.Heal:
                return heal;
            default:
                return 0;
        }
    }


    public int GetCompanionPowerWRTAttribute() {
        switch (SelectPowerManager.instance.selectedEnemyPowerName)
        {
            case AttributeType.None:
                return 0;
            case AttributeType.Attack:
                return this.attack;
            case AttributeType.Defense:
                return this.defence;
            case AttributeType.Disrupt:
                return this.disrupt;
            case AttributeType.Heal:
                return this.heal;
        }
        return 0;
    }


    public static Card FromFetchedCard(FetchedBestCard fetchedBestCard)
    {
        Card newCard = new Card();



        //newCard.set = fetchedBestCard.card_set;
        newCard.card_contract_id = fetchedBestCard.card_contract_id;
        newCard.level = fetchedBestCard.level;
        newCard.rarity = fetchedBestCard.card_rarity;
        newCard.attack1 = fetchedBestCard.card_attack;
        newCard.defence1 = fetchedBestCard.card_defence;
        newCard.heal1 = fetchedBestCard.card_heal;
        newCard.disrupt1 = fetchedBestCard.card_disrupt;
        newCard.heart1 = fetchedBestCard.card_heart;
        newCard.elementeum1  = fetchedBestCard.card_elementeum;
        newCard.name        = fetchedBestCard.card_name;
        newCard.generation  = fetchedBestCard.card_generation;
        newCard.element_1   = fetchedBestCard.card_element_1;
        newCard.element_2   = fetchedBestCard.card_element_2;
        newCard.strength_1  = fetchedBestCard.card_strength_1;
        newCard.strength_2  = fetchedBestCard.card_strength_2;

        //newCard.elementeum = fetchedBestCard.earn_elementeum;

        newCard.s3_Image = fetchedBestCard.s3_image;
        return newCard;
    }
}


public class FetchCardsResponse
{

    public int status { get; set; }
    public string messagec { get; set; }

    public Data data;

    public class Data
    {
        public Dictionary<string, Dictionary<string, Card>> user_card_details { get; set; }
    }
}

//[JsonConverter(typeof(StringEnumConverter))]
//public enum EnrollmentState
//{

//    [EnumMember(Value = "incomplete")]
//    Incomplete,

//    [EnumMember(Value = "actionNeeded")]
//    ActionNeeded,

//    [EnumMember(Value = "complete")]
//    Complete
//}

public class IntStringConverter : JsonConverter
{


    public override bool CanConvert(Type objectType)
    {
        return typeof(int) == objectType
            || typeof(Int16) == objectType
            || typeof(Int32) == objectType
            || typeof(Int64) == objectType
            || typeof(UInt32) == objectType;
    }



    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        if (reader == null || reader.Value == null || objectType == null || existingValue == null || serializer == null)
            return 0;

        if (string.IsNullOrEmpty(reader.Value.ToString()) || string.IsNullOrWhiteSpace(reader.Value.ToString()))
            return 0;

        int val = 0;
        int.TryParse(reader.Value.ToString(), out val);
        return val;

        //return base.ReadJson(reader, objectType, existingValue, serializer);
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        writer.WriteValue(value.ToString());
    }
}

public class AttributeTypeEnumConverter : StringEnumConverter
{
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        if (string.IsNullOrEmpty(reader.Value.ToString()) || string.IsNullOrWhiteSpace(reader.Value.ToString()))
            return AttributeType.None;

        return base.ReadJson(reader, objectType, existingValue, serializer);
    }
}

[JsonConverter(typeof(AttributeTypeEnumConverter))]
public enum AttributeType
{
    [EnumMember(Value = "None")]
    None,
    [EnumMember(Value = "Attack")]
    Attack,
    [EnumMember(Value = "Defense")]
    Defense,
    [EnumMember(Value = "Disrupt")]
    Disrupt,
    [EnumMember(Value = "Heal")]
    Heal
};


public static class Utils
{
    public static void ForEach<T>(this IEnumerable<T> collection, Action<T> function)
    {
        if (collection == null)
            return;
        foreach (T obj in collection)
            function(obj);
    }
}