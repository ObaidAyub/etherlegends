﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardIndexCallback : MonoBehaviour
{
    public GameObject highlight;
    public Toggle tglSelect;
    public Image img_card;
    public Button btn_useCard;
    public int index;
    void ScrollCellIndex(int idx)
    {
        index = idx;
        highlight.SetActive(false);
        gameObject.name = name;
        tglSelect.onValueChanged.RemoveAllListeners();
        tglSelect.group = DeckScreenController.Instance.tglGroup;
        img_card.sprite = DeckScreenController.Instance.cardSprite[idx];
        tglSelect.onValueChanged.AddListener((isOn) =>
        {
            img_card.GetComponent<Canvas>().sortingOrder = 1;
            if (isOn)
            {
                highlight.SetActive(true);
                img_card.GetComponent<Canvas>().sortingOrder = 3;
                DeckScreenController.Instance.lvsCardList.SrollToCell(idx, 1000);
            }
            else
            {
                highlight.SetActive(false);
                //img_card.GetComponent<Canvas>().sortingOrder = 1;
            }
        });

        btn_useCard.onClick.AddListener(() => {

            DeckScreenController.Instance.OnStartUse(index,transform.position);
        });
    }


}
