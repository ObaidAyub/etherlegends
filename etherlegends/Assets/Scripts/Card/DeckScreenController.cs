﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DeckScreenController : Singleton<DeckScreenController>
{
    public LoopScrollRect lvsCardList;
    public ToggleGroup tglGroup;

    public GameObject cardToReplaceParent;
    public GameObject cardToReplace;
    public Vector3 initPos;
    public GameObject scrollPanel;

    public Sprite[] cardSprite;

    public Button[] deckCards;
    // Start is called before the first frame update
    void Start()
    {
        lvsCardList.totalCount = 15;
        lvsCardList.RefillCells();
        initPos = cardToReplace.transform.position;
    }

    // Update is called once per fram
    void Update()
    {

    }

    public void OnStartUse(int idx,Vector3 pos)
    {
        cardToReplace.transform.GetChild(1).GetComponent<Image>().sprite = cardSprite[idx];
        scrollPanel.SetActive(false);
        cardToReplace.transform.position = pos;
        cardToReplace.SetActive(true);
        cardToReplaceParent.SetActive(true);
        cardToReplace.transform.DOMove(initPos, 1);
    }   

    public void CancelSwap()
    {
        scrollPanel.SetActive(true);
        cardToReplace.SetActive(false);
        cardToReplaceParent.SetActive(false);
        lvsCardList.totalCount = 15;
        lvsCardList.RefillCells();
       
    }

    public void NoSwap()
    {
        cardToReplace.GetComponent<RectTransform>().transform.position = initPos; 
    }

}
