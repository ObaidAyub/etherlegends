﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;
using Nethereum.JsonRpc.UnityClient;
using Nethereum.Web3;
using Nethereum.Contracts;

public class ForgeCardSc : ForgeAndRedeemCard
{
    [Header("Main Screen Objects")]
    public GameObject ERC721Cat;
    public Transform ERC721CatParent;
    public GameObject ERC721;
    public Transform ERC721Parent;

    [Header("Forging Card Objects")]
    public GameObject FindCardPanel;
    public GameObject FindcardItem;
    public Transform FindcardTransform;
    public Text TimerTxt;
    public Text TxHash;
    string StHash;
    string HashLink;
    float CountDown=120f;

    [Header("Success Card Objects")]
    public GameObject SuccessPanel;
    public RawImage FoundImage;
    public Text FoundCardName;
    public GameObject[] ItemsTODisable;

    public bool StartForging;

    float confirmTime = 100f;

    public float MoveSpeed = -0.3f;

    public GameObject ErrorPanel;

    float ScrollThreshold = -502;
    float AddScrollThres = -502;

    [System.Serializable]
    public class Pkey
    {
        public string private_key;
    }

    [System.Serializable]
    public class HashSent
    {
        public string txHash;
    }

    private void OnEnable()
    {

        for (int i = 0; i < ItemsTODisable.Length; i++)
        {
            FindcardTransform.localPosition = Vector3.zero;
            FindCardPanel.SetActive(false);
            ItemsTODisable[i].SetActive(true);
            TxHash.gameObject.SetActive(false);
            SuccessPanel.SetActive(false);
        }

        StartCoroutine(GetCards());

    }


    public void StartForgeCard()
    {

        LoadingManager.Instance.btn_yes.onClick.RemoveAllListeners();

        LoadingManager.Instance.btn_yes.onClick.AddListener(() => ConfirmForgingCard());

        LoadingManager.Instance.ShowPopUp(" This Operation will Cost you 0.01 ETH " + Environment.NewLine + " Are You Sure ? ");

        //JSONNode jNode = JSON.Parse(Response);
        
    }

    public  void ConfirmForgingCard()
    {

        if (UserDataController.Instance.userTokenData.ether < 0.01)
        {

          //  LoadingManager.Instance.ShowInfoPopUp("Insufficient Funds!");
           // return;

        }
        
        LoadingManager.Instance.ShowAuthentication(FinalConfirm);

    }

    public void FinalConfirm()
    {

        CountDown = 120f;
        StartForging = true;
        ErrorPanel.SetActive(false);

        LoadingManager.Instance.HidePopUp();

        FindCardPanel.SetActive(true);

        StartCoroutine(StartForgingCard());


    }

    public void OpenLink()
    {
        Application.OpenURL(HashLink);
    }

      
    public void SortByCat(GameObject CatObj)
    {

        for (int i = 0; i < ERC721Parent.childCount; i++)
        {
            Destroy(ERC721Parent.GetChild(i).gameObject);
        }

        if (CatObj.name == "all")
        {
            for (int i = 0; i < UserDataController.Instance.AllsortedCards.Count; i++)
            {
                for (int j = 0; j < UserDataController.Instance.AllsortedCards[i].Cards.Count; j++)
                {
                    Texture2D NewTex = BlockChainCardSC.Instance.GetCardTexture(UserDataController.Instance.AllsortedCards[i].Cards[j].card_contract_id);
                    GameObject CardObj = Instantiate(ERC721, ERC721Parent);
                    CardObj.GetComponent<RawImage>().texture = NewTex;
                    CardObj.SetActive(true);
                }
            }
        }
        else
        {
            for (int i = 0; i < UserDataController.Instance.AllsortedCards.Count; i++)
            {
                if (UserDataController.Instance.AllsortedCards[i].Name == CatObj.name )
                {
                    for (int j = 0; j < UserDataController.Instance.AllsortedCards[i].Cards.Count; j++)
                    {
                        Texture2D NewTex = BlockChainCardSC.Instance.GetCardTexture(UserDataController.Instance.AllsortedCards[i].Cards[j].card_contract_id);
                        GameObject CardObj = Instantiate(ERC721, ERC721Parent);
                        CardObj.GetComponent<RawImage>().texture = NewTex;
                        CardObj.SetActive(true);
                    }
                }               
            }
        }            
    }


    IEnumerator GetCards()
    {
        if(LoadingManager.Instance)




        Debug.Log("LoadingManager.Instance " + LoadingManager.Instance);

        for (int i = 0; i < ERC721Parent.childCount; i++)
        {
            Destroy(ERC721Parent.GetChild(i).gameObject);
        }

        for (int i = 0; i < ERC721CatParent.childCount; i++)
        {
            Destroy(ERC721CatParent.GetChild(i).gameObject);
        }

        for (int i = 0; i < FindcardTransform.childCount; i++)
        {
            Destroy(FindcardTransform.GetChild(i).gameObject);
        }

        yield return new WaitForSeconds(0.2f);

       // GameObject AllCardCat = Instantiate(ERC721Cat, ERC721CatParent);
      //  AllCardCat.name = "all";

        //AllCardCat.transform.GetChild(0).GetComponent<Text>().text = "All Cards";
       // AllCardCat.SetActive(true);

        for (int i = 0; i < UserDataController.Instance.AllsortedCards.Count ; i++)
        {
            if (UserDataController.Instance.AllsortedCards[i].Name.Contains("Beasts"))
            {

                GameObject CardCat = Instantiate(ERC721Cat, ERC721CatParent);
                CardCat.name = UserDataController.Instance.AllsortedCards[i].Name;
                CardCat.transform.GetChild(0).GetComponent<Text>().text = UserDataController.Instance.AllsortedCards[i].Name.Replace('_', ' ');
                CardCat.SetActive(true);

                for (int j = 0; j < UserDataController.Instance.AllsortedCards[i].Cards.Count; j++)
                {

                    Texture2D NewTex = BlockChainCardSC.Instance.GetCardTexture(UserDataController.Instance.AllsortedCards[i].Cards[j].card_contract_id);

                    if (NewTex != null)
                    {
                        GameObject CardObj = Instantiate(ERC721, ERC721Parent);
                        CardObj.GetComponent<RawImage>().texture = NewTex;
                        CardObj.SetActive(true);

                        GameObject FindCardObj = Instantiate(FindcardItem, FindcardTransform);
                        FindCardObj.GetComponent<RawImage>().texture = NewTex;
                        FindCardObj.SetActive(true);
                    }
                    else
                    {
                        UnityWebRequest www = null;
                        string url = UserDataController.Instance.AllsortedCards[i].Cards[j].Image;

                        if (UserDataController.Instance.AllsortedCards[i].Cards[j].Image.Contains(".gif"))
                        {
                            /*
                            www = new UnityWebRequest(url);
                            www.downloadHandler = new DownloadHandlerBuffer();
                            yield return www.SendWebRequest();

                            byte[] results = www.downloadHandler.data;

                            //yield return wr.SendWebRequest();

                            if (!(www.isNetworkError || www.isHttpError))
                            {
                                Texture2D texture2d = new Texture2D(8, 8);
                                Sprite sprite = null;

                                if (texture2d.LoadImage(results))
                                {
                                    sprite = Sprite.Create(texture2d, new Rect(0, 0, texture2d.width, texture2d.height), Vector2.zero);
                                }

                                if (sprite != null)
                                {
                                    //imageToUpdate.sprite = sprite;
                                }
                                //   Texture2D t = texDl.texture;
                                //  Sprite s = Sprite.Create(t, new Rect(0, 0, t.width, t.height),
                                //                           Vector2.zero, 1f);
                                // _img.sprite = s;
                                BlockChainCardSC.Instance.SaveCard(UserDataController.Instance.AllsortedCards[i].Cards[j].card_contract_id, results, true);
                            }
                            */
                        }
                        else
                        {
                            www = UnityWebRequestTexture.GetTexture(url);

                            Texture2D CardTexture = DownloadHandlerTexture.GetContent(www);
                            yield return www.SendWebRequest();
                            GameObject CardObj = Instantiate(ERC721, ERC721Parent);
                            CardObj.GetComponent<RawImage>().texture = CardTexture;
                            CardObj.SetActive(true);

                            GameObject FindCardObj = Instantiate(FindcardItem, FindcardTransform);
                            FindCardObj.GetComponent<RawImage>().texture = CardTexture;
                            FindCardObj.SetActive(true);

                            BlockChainCardSC.Instance.SaveCard(UserDataController.Instance.AllsortedCards[i].Cards[j].card_contract_id, CardTexture, false);
                        }



                        if (www.isNetworkError || www.isHttpError)
                        {
                            Debug.Log(www.error);
                        }
                        else
                        {


                        }
                    }
                }
            }
              
        }
        LoadingManager.Instance.HideMainLoading();
    }

    // Update is called once per frame
    void Update()
    {

        if (StartForging)
        {

            CountDown -= Time.deltaTime;

            TimeSpan t = TimeSpan.FromSeconds(CountDown);

            TimerTxt.text = t.Minutes + ":" + t.Seconds;

            FindcardTransform.Translate(new Vector3(MoveSpeed, 0, 0));

            if (FindcardTransform.localPosition.x <= ScrollThreshold )
            {
                Debug.Log("Reached");
                ScrollThreshold += AddScrollThres;

                FindcardTransform.GetChild(0).SetAsLastSibling();
                FindcardTransform.GetChild(0).SetAsLastSibling();
            }

            if (CountDown<=confirmTime)
            {
                confirmTime -= 10;
                VerifyTransaction();              
            }

            if (CountDown<=0)
            {

                StartForging = false;
                FailedToFetch();
            }

        }
    }

    public void FailedToFetch()
    {
        for (int i = 0; i < ItemsTODisable.Length; i++)
        {
            ItemsTODisable[i].SetActive(false);
        }
        TxHash.gameObject.SetActive(true);

    }

    public async void VerifyTransaction()
    {
        HashSent HashS = new HashSent();
        Debug.Log("Hashhhh--->" + StHash);
        HashS.txHash = StHash;

        string JsBody = JsonUtility.ToJson(HashS);

        Debug.Log("FOrge Send ---------" + JsBody);

        string Response = await SessionManager.Instance.Post(ConstantManager._url_getTransactionConfirmation, JsBody, SessionManager.Instance.userToken, true);

        Debug.Log("forge Response ---" + Response);

        JSONNode Jnode = JSON.Parse(Response);

        if (Jnode["status"]==1)
        {

            StartForging = false;

            Debug.Log(Jnode["data"]);

            FoundCardName.text = Jnode["data"];
            string contId = UserDataController.Instance.GetCardContract(FoundCardName.text);
            Debug.Log("ID" + contId);
            FoundImage.texture = BlockChainCardSC.Instance.GetCardTexture(contId);

            TransactionClass transactionClass = new TransactionClass();

            transactionClass.tx_hash = StHash;
            transactionClass.user_wallet_add = UserDataController.Instance.Myprefs.Address;
            transactionClass.dest_wallet_add = UserDataController.Instance.Myprefs.Address;
            transactionClass.amount = "1";
            transactionClass.trans_type = "ForgeCard:" + FoundCardName.text;

            string Jsonbody = JsonUtility.ToJson(transactionClass);

            string ForgeResponse = await SessionManager.Instance.Post(ConstantManager._url_StoreTransaction, Jsonbody, SessionManager.Instance.userToken, true);

            Debug.Log("Save Transaction-->" + ForgeResponse);

            SuccessPanel.SetActive(true);

        }

    }


    IEnumerator StartForgingCard()
    {
         
        var url = ConstantManager._url_ETHMainNet;

        var privateKey = UserDataController.Instance.GetPrivateKey();

        var account = UserDataController.Instance.Myprefs.Address;

        var QueryRequest = new QueryUnityRequest<PricePerCard, pricePerCardOutput>(url, account);

        yield return QueryRequest.Query(new PricePerCard() { }, "0x880F820900363c01D103D4c46773216a1a65f5D1");

        var transactionPurchaseRequest = new TransactionSignedUnityRequest(url, privateKey);

        Debug.Log("Price --->" + QueryRequest.Result.Price);

        var transactionMessage = new PurchaseCard
        {
            Beneficiary = account,
            AmountToSend = QueryRequest.Result.Price,
            FromAddress = account,

        };

        yield return transactionPurchaseRequest.SignAndSendTransaction(transactionMessage, "0x880F820900363c01D103D4c46773216a1a65f5D1");

        var transactionHash = transactionPurchaseRequest.Result;

        if (transactionPurchaseRequest.Exception != null)
        {
            Debug.Log("Error " + transactionPurchaseRequest.Exception.Message);
            StartForging = false;
            ErrorPanel.SetActive(true);

            yield break;
        }

        Debug.Log("Purchase Hash " + transactionHash);
        StHash = transactionHash;
        var transactionReceiptPolling = new TransactionReceiptPollingRequest(url);

        yield return transactionReceiptPolling.PollForReceipt(transactionHash, 2);

        var transferReceipt = transactionReceiptPolling.Result;
        

         UserDataController.Instance.RefreshERC20();
        StartForging = true;          
           
        HashLink = "https://etherscan.io/tx/" + StHash;
        TxHash.text = StHash;
        VerifyTransaction();
        }

    }



