﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;




namespace EtherSocketPackets
{
    public class SyncTime
    {
        public Data data { get; set; }
        public class Data
        {
            public string time;
        }

    }

    [Serializable]
    public class SocketEventPacket : JConvertable
    {
        public SocketEventPacket()
        {
            this.data = new DiceRollPacket();
        }

        public SocketEventPacket(int status, string message, DiceRollPacket data)
        {
            this.status = status;
            this.message = message;
            this.data = data;
        }

        [JsonProperty("status")]
        public int status { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public DiceRollPacket data { get; set; }



    }
    [Serializable]
    public class LastTurnPacket : DiceRollPacket
    {


        public LastTurnPacket()
        {
            //this.socketEventPacket = new SocketEventPacket();
        }

        public override JObject ToJObject()
        {
            return JObject.Parse(JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            }));
        }

        public static T FromJObject<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }



        //REMOVE THESE when done from backend
        //public string user_id;
        //public string socket_id;

        //public SocketEventPacket socketEventPacket;

        public string main_turn;
        public string turn;
        public string user_timer;

        public DiceRollPacket ToDiceRollPacket()
        {
            return JsonConvert.DeserializeObject<DiceRollPacket>(
           JsonConvert.SerializeObject(
               this
               ));
        }

        internal static LastTurnPacket FromDiceRollPacket(DiceRollPacket lastAction)
        {
            return JsonConvert.DeserializeObject<LastTurnPacket>(
             JsonConvert.SerializeObject(
                 lastAction
                 ));

            //return new LastTurnPacket() {

            //    ElementeumData                     =lastAction.ElementeumData               ,      
            //    Player_two_ElementeumData          =lastAction.Player_two_ElementeumData    , 
            //    CompanionData                      =lastAction.CompanionData                , 
            //    Player_two_CompanionData           =lastAction.Player_two_CompanionData     ,
            //    EnemyCardId = lastAction.EnemyCardId,
            //    PlayerDice = lastAction.PlayerDice       ,
            //    PlayerTwoDice = lastAction.PlayerTwoDice       , 
            //    attribute_number                   =lastAction.attribute_number             , 
            //    player_two_attribute_number        =lastAction.player_two_attribute_number  , 
            //    earn_elementeum                    =lastAction.earn_elementeum              , 
            //    player_two_earn_elementeum         =lastAction.player_two_earn_elementeum   , 
            //    elementeum_number                  =lastAction.elementeum_number            , 
            //    player_two_elementeum_number       =lastAction.player_two_elementeum_number , 
            //    card_elementeum                    =lastAction.card_elementeum              , 
            //    player_two_card_elementeum         =lastAction.player_two_card_elementeum   , 
            //    companion_value                    =lastAction.companion_value              , 
            //    player_two_companion_value         =lastAction.player_two_companion_value   , 
            //    special_power_used                 =lastAction.special_power_used           , 
            //    player_two_special_power_used      =lastAction.player_two_special_power_used, 
            //    heal_card_heart_value              =lastAction.heal_card_heart_value        , 
            //    player_one_heart_value             =lastAction.player_one_heart_value       , 
            //    player_two_heart_value             =lastAction.player_two_heart_value       ,
            //    isSkirmish = lastAction.isSkirmish                     ,
            //    healing_card_contract_id = lastAction.healing_card_contract_id ,
            //    disrupt_card_contract_id = lastAction.disrupt_card_contract_id  ,
            //    PlayerAttribute = lastAction.PlayerAttribute                   ,
            //    PlayerTwoAttribute = lastAction.PlayerTwoAttribute        ,
            //    debuff_value                       =lastAction.debuff_value                     ,
            //    player_Two_debuff_value = lastAction.player_Two_debuff_value          ,
            //    special_power                      =lastAction.special_power                    ,
            //    player_two_special_power           =lastAction.player_two_special_power         ,
            //    user_id                            =lastAction.user_id                          ,
            //    socket_id                          =lastAction.socket_id                        ,
            //    PlayerCardId = lastAction.PlayerCardId                                    

            //};
        }
    }

    [Serializable]
    public class DiceRollPacket : HealPacket
    {
        [JsonProperty("ElementeumData")]
        public Dictionary<string, int> ElementeumData { get; set; }

        [JsonProperty("Player_two_ElementeumData")]
        public Dictionary<string, int> Player_two_ElementeumData { get; set; }

        [JsonProperty("CompanionData")]
        public List<string> CompanionData { get; set; }

        [JsonProperty("Player_two_CompanionData")]
        public List<string> Player_two_CompanionData { get; set; }

        [JsonProperty("opposite_card_contract_id")]
        public string EnemyCardId { get; set; }


        [JsonProperty("player_one_dice_number")]
        public string PlayerDice { get; set; }

        [JsonProperty("player_two_dice_number")]
        public string PlayerTwoDice { get; set; }

        [JsonProperty("attribute_number")]
        public string attribute_number { get; set; }

        [JsonProperty("player_two_attribute_number")]
        public string player_two_attribute_number { get; set; }

        [JsonProperty("earn_elementeum")]
        public string earn_elementeum { get; set; }

        [JsonProperty("player_two_earn_elementeum")]
        public string player_two_earn_elementeum { get; set; }

        [JsonProperty("elementeum_number")]
        public string elementeum_number { get; set; }

        [JsonProperty("player_two_elementeum_number")]
        public string player_two_elementeum_number { get; set; }

        [JsonProperty("card_elementeum")]
        public string card_elementeum { get; set; }

        [JsonProperty("player_two_card_elementeum")]
        public string player_two_card_elementeum { get; set; }

        [JsonProperty("companion_value")]
        public string companion_value { get; set; }

        [JsonProperty("player_two_companion_value")]
        public string player_two_companion_value { get; set; }

        [JsonProperty("special_power_used")]
        public bool special_power_used { get; set; }

        [JsonProperty("player_two_special_power_used")]
        public bool player_two_special_power_used { get; set; }


        [JsonProperty("heal_card_heart_value")]
        public string heal_card_heart_value { get; set; }

        [JsonProperty("heal_card_max_health")]
        public string heal_card_max_health { get; set; }

        [JsonProperty("player_one_heart_value")]
        public string player_one_heart_value { get; set; }

        [JsonProperty("player_two_heart_value")]
        public string player_two_heart_value { get; set; }

        [JsonProperty("player_one_max_health")]
        public string player_one_max_health { get; set; }

        [JsonProperty("player_two_max_health")]
        public string player_two_max_health { get; set; }


        [JsonProperty("skirmish")]
        public string isSkirmish { get; set; }



        public static T FromJObject<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }

    [Serializable]
    public class JConvertable
    {

        public virtual JObject ToJObject()
        {
            return JObject.Parse(JsonConvert.SerializeObject(this));
        }


    }

    [Serializable]
    public class CardPacket : JConvertable
    {
        [JsonProperty("user_id")]
        public string user_id { get; set; }

        [JsonProperty("socket_id")]
        public string socket_id { get; set; }

        [JsonProperty("card_contract_id")]
        public string PlayerCardId { get; set; }



    }

    [Serializable]
    public class AttributePacket : CardPacket
    {
        [JsonProperty("attribute_type")]
        public AttributeType PlayerAttribute { get; set; }

        [JsonProperty("player_two_attribute_type")]
        public AttributeType PlayerTwoAttribute { get; set; }

        [JsonProperty("debuff_value")]
        public string debuff_value { get; set; }

        [JsonProperty("player_two_debuff_value")]
        public string player_Two_debuff_value { get; set; }

        //special_power"
        [JsonProperty("special_power")]
        public AttributeType special_power { get; set; }
        [JsonProperty("player_two_special_power")]
        public AttributeType player_two_special_power { get; set; }

    }

    [Serializable]
    public class HealPacket : AttributePacket
    {
        [JsonProperty("player_heal_card_contract_id")]
        public string healing_card_contract_id { get; set; }

        [JsonProperty("player_disrupt_card_contract_id")]
        public string disrupt_card_contract_id { get; set; }
    }

}