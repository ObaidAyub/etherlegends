﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SeasonLeaderBoardPrefab : MonoBehaviour
{
    public Text Id, Txt_name, Txt_Battle, Txt_win, Txt_Loss, Txt_Streak, Txt_Trophies;
    string UserId;
   

    public Texture2D profile_img;
    public Image CardImage;

    byte[] bytes = new byte[0];
    public Texture2D profile_imgTemp;


    string defaultSpriteName = "";
    private void Awake()
    {
        if (CardImage.sprite)
            defaultSpriteName = CardImage.sprite.name;
    }


    public void SetValue(string id, Texture2D ProfileImg, string name, string battle, string win, string loss, string streak, string trophy)
    {
        if (gameObject==null || CardImage==null)
        {
            return;
        }

        //if (!this.gameObject || !CardImage)
        //{
        //    return;
        //}


        //Clone of Texture
        bytes = ProfileImg.EncodeToPNG();
        profile_imgTemp = new Texture2D(1, 1);
        profile_imgTemp.LoadImage(bytes);

        //profile_imgTemp.name = ProfileImg.name;


        Sprite sprite = Sprite.Create(profile_imgTemp, new Rect(0, 0, profile_imgTemp.width, profile_imgTemp.height), new Vector2(0.5f, 0.5f));
        sprite.name = profile_imgTemp.name;
        CardImage.sprite = sprite;


        Id.text = id;

        Txt_name.text = name;
        Txt_Battle.text = battle;
        Txt_win.text = win;
        Txt_Loss.text = loss;
        Txt_Streak.text = streak;
        Txt_Trophies.text = trophy;

    }
    private void OnDestroy()
    {
        if (CardImage.sprite.texture != null && ( CardImage.sprite.name != "default" && CardImage.sprite.name != defaultSpriteName) )
        {
            bytes = null;
            Destroy(CardImage.sprite.texture);
            System.GC.Collect();
        }

    }
}
