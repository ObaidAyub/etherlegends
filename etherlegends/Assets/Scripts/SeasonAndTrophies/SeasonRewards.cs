﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SeasonRewards : MonoBehaviour
{
    public static SeasonRewards Instance;
    [Header("Reward Data")]
    public GameObject rewardWindow;
    public float totalTrophies, seasonName, totalDistributedTriphies, user_trophiess;
    public TextMeshProUGUI trophiesTxt, seasonNameTxt, totalDistributedTriphiesTxt, user_trophiesTxt, distributionPresentage, contributionPersentage, userRewardTxt, status;
    public GameObject yellow, green;
    public Transform rewardContent;
    public SeasonRewardsPrefab rewardPrefab;

    JSONNode rewardData;

    public Text profileScrTrophies;

    public Slider trophiesSiders;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    //Lp stands for liquidity provider(LP) tokens
    public async void GetCurrentSeasonTrophies()
    {
        string response = await SessionManager.Instance.Post(ConstantManager._url_userTrophies, null, SessionManager.Instance.userToken, true);
        
        if (response != null)
        {


            JSONNode jnode = JSON.Parse(response);
            if (jnode["data"] != "0")
            {

                profileScrTrophies.text = jnode["data"];
            }
            else
            {
                profileScrTrophies.text = "0";
            }
        }
        else
        {
            //Debug.LogError("_url_userTrophies response is null");
            GetCurrentSeasonTrophies();
        }
    }

    public async void ShowSeasonInfoAsync()
    {
        for (int i = 0; i < rewardContent.childCount; i++)
        {
            Destroy(rewardContent.GetChild(i).gameObject);
        }

        LoadingManager.Instance.ShowLoader("Please Wait...");
        string response = await SessionManager.Instance.Post(ConstantManager._url_Get_Active_Season, null, SessionManager.Instance.userToken, true);

        JSONNode jnode = JSON.Parse(response);
        //Debug.Log(jnode.ToString());

        LoadingManager.Instance.HideLoader();
        rewardWindow.SetActive(true);

        if (jnode["status"] == 1)
        {
            trophiesTxt.text = jnode["data"]["season"]["trophies"];
            seasonNameTxt.text = jnode["data"]["season"]["name"] + " Rewards";
            totalDistributedTriphiesTxt.text = jnode["data"]["total_distributed_trophies"] + "/" + jnode["data"]["season"]["trophies"];

            totalTrophies = jnode["data"]["season"]["trophies"];

            totalDistributedTriphies = jnode["data"]["total_distributed_trophies"];
            float persentage = (totalDistributedTriphies / totalTrophies) * 100;
            distributionPresentage.text = persentage + "%";
            trophiesSiders.maxValue = 100;
            trophiesSiders.minValue = 0;
            trophiesSiders.value = persentage;
            user_trophiesTxt.text = jnode["data"]["user_trophies"];
            user_trophiess = jnode["data"]["user_trophies"];
            rewardData = jnode["data"]["reward"];

            float userPersentage = (user_trophiess / totalTrophies) * 100;
            contributionPersentage.text = userPersentage.ToString() + "%";
            for (int i = 0; i < rewardData.Count; i++)
            {
                SeasonRewardsPrefab reward = Instantiate(rewardPrefab, rewardContent);
                reward.Name.text = jnode["data"]["reward"][i]["name"];
                reward.value.text = jnode["data"]["reward"][i]["value"];
            }
            if (jnode["data"]["season"]["status"].ToString() == "10")
            {
                green.SetActive(true);
                yellow.SetActive(false);

                status.text = "Activated";

            }
            else if (jnode["data"]["season"]["status"].ToString() == "20")
            {
                yellow.SetActive(true);
                green.SetActive(false);

                status.text = "Completed";
            }
            else
            {
                status.text = "InProgress";
            }

            float myTrophy = jnode["data"]["user_trophies"];
            float seasonTotalTroph = jnode["data"]["season"]["trophies"];
            float totalDistributedTrophy = jnode["data"]["total_distributed_trophies"];
            string seasonRewardValue = jnode["data"]["reward"][0]["number"];
            //seasonRewardValue = seasonRewardValue.Replace(seasonRewardValue, "1000");

            //Debug.Log(myTrophy + " + " + seasonTotalTroph + " + " + float.Parse(seasonRewardValue));
            float rewardValue = (((totalDistributedTrophy / seasonTotalTroph) * float.Parse(seasonRewardValue)) * myTrophy / totalDistributedTrophy);
            //  (((11300/1000000)*1000)*(3700/11300)

            userRewardTxt.text = "You will get an estimated $" + rewardValue + " from the season reward";

        }
        else
        {
        }
    }

    public void CloseSeasonRewardScr()
    {
        rewardWindow.SetActive(false);
        ////Debug.Log("season screen clsoed");

    }
}
