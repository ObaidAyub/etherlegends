﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;
using SimpleJSON;
using UnityEngine.Networking;

public class SeasonBtnPrefab : MonoBehaviour
{

    public string season_address;
    public TextMeshProUGUI seasonName;
    public int status;
    public Button button;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(PopulateSeasonData);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public async void PopulateSeasonData()
    {
        LeaderboardMain.instance.seasonLeaderBoardLoader.SetActive(true);

        LeaderboardMain.instance.destroycontentData(LeaderboardMain.instance.seasonLeaderBoardScroll);
        ConfirmSeason confirmSeason = new ConfirmSeason();
        confirmSeason.season_address = season_address;
        string confirmBody = JsonUtility.ToJson(confirmSeason);

        //Debug.Log("send " + confirmBody + " Data");
        string response = await SessionManager.Instance.Post(ConstantManager._url_SeasonLeaderboard, confirmBody, SessionManager.Instance.userToken, true);
        JSONNode jnode = JSON.Parse(response);
        //Debug.Log(jnode);
        //Debug.Log(jnode["data"][0]["total_trophies"]);
        LeaderboardMain.instance.seasonLeaderBoardLoader.SetActive(false);

        for (int i = 0; i < jnode["data"].Count; i++)
        {

        SeasonLeaderBoardPrefab season = Instantiate(LeaderboardMain.instance.seasonLeaderBoardPrefab, LeaderboardMain.instance.seasonLeaderBoardScroll);
            string id = season.Id.text = (i + 1).ToString();
            string name = season.Txt_name.text = jnode["data"][i]["userData"]["displayName"];
            string battle = season.Txt_Battle.text = jnode["data"][i]["userData"]["battles"];
            string win = season.Txt_win.text = jnode["data"][i]["userData"]["wins"];
            string streak = season.Txt_Streak.text = jnode["data"][i]["userData"]["max_streak"];
            string loss = season.Txt_Loss.text = jnode["data"][i]["userData"]["draw"];
            string trophy = season.Txt_Trophies.text = jnode["data"][i]["userTrophies"];
            string ImageLink = season.Txt_Trophies.text = jnode["data"][i]["userData"]["profile_pic"];
            //Debug.Log(ImageLink);
            StartCoroutine(GetSeasonImage(season, id, ImageLink, name, battle, win, loss, streak, trophy));
        }
    }

    IEnumerator GetSeasonImage(SeasonLeaderBoardPrefab SpawnObj, string id, string ImgLink, string txname, string battle, string win, string loss, string streak, string trophy)
    {
        if (string.IsNullOrEmpty(ImgLink))
        {

            SpawnObj.SetValue(id,LeaderboardMain.instance.DefaultPic, txname, battle, win, loss, streak, trophy);
            SpawnObj.gameObject.SetActive(true);
        }

        else
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(ImgLink))
            {
                yield return www.SendWebRequest();

                while (!www.isDone)
                {
                    yield return new WaitForEndOfFrame();
                }
                if (www.isNetworkError || www.isHttpError)
                {
                    if (SpawnObj)
                    {
                        SpawnObj.SetValue(id, LeaderboardMain.instance.DefaultPic, txname, battle, win, loss, streak, trophy);

                        SpawnObj.gameObject.SetActive(true);
                        //Debug.Log(www.error.ToString());
                    }
                }
                else
                {
                    if (SpawnObj)
                    {
                        SpawnObj.SetValue(id, DownloadHandlerTexture.GetContent(www), txname, battle, win, loss, streak, trophy);

                        SpawnObj.gameObject.SetActive(true);
                    }
                }
            }
        }



    }
}
public class ConfirmSeason
{
    public string season_address;
}