﻿using UnityEngine;
using TMPro;
using System;
using System.Collections;
using DG.Tweening;
using System.Linq;

public class SelectPowerManager : MonoBehaviour
{
    public static SelectPowerManager instance;

    [HideInInspector]
    public Animator selectedCardAnimator, EnemyselectedCardAnimator;
    //[HideInInspector]
    public AttributeType selectedPowerName, selectedEnemyPowerName;
    public AudioClip attackSound, HealSound, DefenseSound, DisruptSound;

    public bool PlayerTwo;
    public PlayerCompanionCardsAnim PlayerCompanionCards;

    private void Awake()
    {
        if (instance == null) instance = this;
    }


    IEnumerator DisableCanvas()
    {
        yield return new WaitForSeconds(2f);

        PlayerManager.instance.TextCanvas.SetActive(false);
    }

    public void SelectedPowerName(AttributeType powerName)  // attribute select from this funtion on button 
    {
        Debug.Log(PlayerManager.instance.SelectedCard.IsEnemyCard + " " + PlayerManager.instance.SelectedCard.CardDetail.name);
        if (PlayerManager.instance.SelectedCard.IsEnemyCard)
        {
            return;
        }
        SyncSelectPowerName(true, powerName);
    }


    public void SyncSelectPowerName(bool CanSend, AttributeType powerName)
    {
        Debug.Log("Selected Power ----" + powerName + " canclick " + PlayerManager.instance.CanClick + " meattribclick " + PlayerManager.instance.CanClickOnMeAttrib);

        if (!PlayerManager.instance.CanClick || !PlayerManager.instance.CanClickOnMeAttrib)
        {
            return;
        }
        selectedPowerName = powerName;


        PlayerManager.instance.CanClickOnMeAttrib = false;
        PlayerManager.instance.CanClickOnElemt = true;

        if (PlayerManager.instance.getSpOpp())
        {
            PlayerManager.instance.TextCanvas.SetActive(true);
            PlayerManager.instance.TextMessage.text = "Opponent Used Special Power " + Environment.NewLine +
                "Your Base Ability Won't Count";
            StartCoroutine(DisableCanvas());
        }

        else if (PlayerManager.instance.getSpme())
        {
            PlayerManager.instance.TextCanvas.SetActive(true);
            PlayerManager.instance.TextMessage.text = "Special Power Used" + Environment.NewLine +
                "Opponent's Base Ability Won't Count";
            StartCoroutine(DisableCanvas());
        }

        AllSelectedPowerFalse();

        //Call Method TO show Image of MyAttribute image and value on canvas
        InGameCards.instance.SetMyAttribImage(powerName);

        var playerCard = PlayerManager.instance.SelectedCard;

        switch (powerName)
        {

            case AttributeType.Attack:
                GetComponent<AudioSource>().clip = attackSound;
                GetComponent<AudioSource>().Play();
                selectedCardAnimator.SetBool("isAttack", true);

                if (playerCard.isSpActive && playerCard.Cur_Elemt == playerCard.CardDetail.elementeum)
                {
                    if (playerCard.SpAbility1.attributeType == powerName)
                    {
                        //Debug.Log("This card sp1 ability is " + powerName);
                        playerCard.SpAbility1.gameObject.SetActive(true);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                    else if (playerCard.SpAbility2.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(true);
                    }
                    else
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                }
                else
                {

                    if (playerCard.SpAbility1.attributeType == powerName)
                    {
                        //Debug.Log("This card sp1 ability is " + powerName);
                        playerCard.SpAbility1.gameObject.SetActive(true);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                    else if (playerCard.SpAbility2.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(true);
                    }
                    else
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                }

                if (BattleLobbySC.Instance && !PlayerTwo && CanSend)
                {
                    if (CanSend)
                    {

                        int remainingCards = 0;
                        for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
                        {
                            if (PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().DestroyedCard == false)
                                remainingCards += 1;
                        }
                        if (remainingCards != 1)
                        {
                            if (playerCard.debuffAtk)
                            {
                                playerCard.ApplyDebuffToPlayerAttribute(powerName);
                                playerCard.debuffImage.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                                playerCard.debuffImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1.48f, 0), 1);
                                playerCard.debuffAttack.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1f, 0), 1);
                            }

                        }
                        else
                        {
                            playerCard.debuffValueAttack = 0;
                            playerCard.debuffValueHeal = 0;
                            playerCard.debuffHl = false;
                            playerCard.debuffAtk = false;

                        }
                        //BattleLobbySC.Instance.LastAction = "me:meattribute:" + AttributeType.Attack;

                        BattleLobbySC.Instance.PlayerOneAttributeSelect(AttributeType.Attack, playerCard.CardDetail.card_contract_id, playerCard.debuffValueAttack.ToString());

                    }

                    for (int i = 0; i < PlayerManager.instance.EnemyCardCanvas.Length; i++)
                    {
                        if (!PlayerManager.instance.enemyCardsObj[i].DestroyedCard)
                        {
                            PlayerManager.instance.enemyCardsObj[i].SortingOrder(6);

                            PlayerManager.instance.enemyCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
                        }

                    }

                }

                else if (BattleLobbySC.Instance && PlayerTwo && CanSend)
                {

                    BattleLobbySC.Instance.PlayerTwoAttribSel(playerCard.CardDetail.card_contract_id,AttributeType.Attack);//this event call is for opp player
                    PlayerTwo = false;
                }
                PlayerCompanionCards.AutoSelectCompanionCardsForBattle(AttributeType.Attack);
                break;

            case AttributeType.Heal:

                GetComponent<AudioSource>().clip = HealSound;
                GetComponent<AudioSource>().Play();
                selectedCardAnimator.SetBool("isHeal", true);

                if (playerCard.isSpActive && playerCard.Cur_Elemt == playerCard.CardDetail.elementeum)
                {
                    Debug.Log(playerCard.SpAbility1.attributeType + "  " + powerName);
                    Debug.Log(playerCard.SpAbility2.attributeType + "  " + powerName);
                    if (playerCard.SpAbility1.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(true);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                    else if (playerCard.SpAbility2.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(true);
                    }
                    else
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                }
                else
                {

                    if (playerCard.SpAbility1.attributeType == powerName)
                    {
                        //Debug.Log("This card sp1 ability is " + powerName);
                        playerCard.SpAbility1.gameObject.SetActive(true);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                    else if (playerCard.SpAbility2.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(true);
                    }
                    else
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                }

                PlayerManager.instance.mainAnim.enabled = false;

                for (int i = 0; i < PlayerManager.instance.CardCanvas.Length; i++)
                {
                    if (!PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().DestroyedCard)
                    {
                        if (PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().isDisable)
                        {
                            Debug.LogError("Working 1");
                            PlayerManager.instance.PlayerCardsObj[i].GetComponent<SpriteRenderer>().sortingOrder = 2;
                            PlayerManager.instance.CardCanvas[i].sortingOrder = 3;
                            PlayerManager.instance.PlayerCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
                        }
                        else
                        {
                            Debug.LogError("Working 2");
                            PlayerManager.instance.PlayerCardsObj[i].GetComponent<SpriteRenderer>().sortingOrder = 6;
                            PlayerManager.instance.CardCanvas[i].sortingOrder = 8;
                            PlayerManager.instance.PlayerCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
                        }
                    }
                }

                StartCoroutine(DelayHeal());

                if (BattleLobbySC.Instance && !PlayerTwo && CanSend)
                {

                    //BattleLobbySC.Instance.LastAction = "me:meattribute:" + AttributeType.Heal;

                    BattleLobbySC.Instance.PlayerOneAttributeSelect(AttributeType.Heal, playerCard.CardDetail.card_contract_id, playerCard.debuffValueHeal.ToString());

                    int remainingCards = 0;
                    for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
                    {

                        if (PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().DestroyedCard == false)
                        {
                            remainingCards += 1;
                        }

                    }
                    if (remainingCards != 1)
                    {
                        if (playerCard.debuffHl)
                        {
                            playerCard.ApplyDebuffToPlayerAttribute(powerName);
                            playerCard.debuffHealImage.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                            playerCard.debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1.48f, 0), 1);
                            playerCard.debuffHeal.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1f, 0), 1);
                        }
                    }
                    else
                    {
                        playerCard.debuffValueAttack = 0;
                        playerCard.debuffValueHeal = 0;
                        playerCard.debuffHl = false;
                        playerCard.debuffAtk = false;
                    }
                    PlayerManager.instance.CanClickOnMeCard = true;
                    playerCard.HealthAnimatedsHeart();
                }

                else if (BattleLobbySC.Instance && PlayerTwo && CanSend)
                {
                    BattleLobbySC.Instance.PlayerTwoAttribSel(playerCard.CardDetail.card_contract_id,AttributeType.Heal);
                    playerCard.HealthAnimatedsHeart();
                    PlayerTwo = false;
                }
                PlayerCompanionCards.AutoSelectCompanionCardsForBattle(AttributeType.Heal);

                break;

            case AttributeType.Defense:

                GetComponent<AudioSource>().clip = DefenseSound;

                GetComponent<AudioSource>().Play();

                selectedCardAnimator.SetBool("isDefense", true);


                if (playerCard.isSpActive && playerCard.Cur_Elemt == playerCard.CardDetail.elementeum)
                {
                    if (playerCard.SpAbility1.attributeType == powerName)
                    {
                        //Debug.Log("This card sp1 ability is " + powerName);
                        playerCard.SpAbility1.gameObject.SetActive(true);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                    else if (playerCard.SpAbility2.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(true);
                    }
                    else
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                }
                else
                {

                    if (playerCard.SpAbility1.attributeType == powerName)
                    {
                        //Debug.Log("This card sp1 ability is " + powerName);
                        playerCard.SpAbility1.gameObject.SetActive(true);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                    else if (playerCard.SpAbility2.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(true);
                    }
                    else
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                }

                if (BattleLobbySC.Instance && !PlayerTwo && CanSend)
                {
                    BattleLobbySC.Instance.PlayerOneAttributeSelect(AttributeType.Defense, playerCard.CardDetail.card_contract_id);
                    for (int i = 0; i < PlayerManager.instance.EnemyCardCanvas.Length; i++)
                    {
                        if (!PlayerManager.instance.enemyCardsObj[i].DestroyedCard)
                        {
                            PlayerManager.instance.enemyCardsObj[i].SortingOrder(6);


                            PlayerManager.instance.enemyCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
                        }
                    }

                }

                else if (BattleLobbySC.Instance && PlayerTwo && CanSend)
                {

                    BattleLobbySC.Instance.PlayerTwoAttribSel(playerCard.CardDetail.card_contract_id,AttributeType.Defense);

                    playerCard.HealthAnimatedsHeart();
                    PlayerTwo = false;
                    if (!playerCard.isUseSpAbility)
                    {

                        for (int i = 0; i < playerCard.elementeumChild.Length; i++)
                        {
                            playerCard.elementeumChild[i].GetComponent<Elementium>().canClickElementeum = true;
                        }
                    }
                }
                PlayerCompanionCards.AutoSelectCompanionCardsForBattle(AttributeType.Defense);

                break;

            case AttributeType.Disrupt:

                GetComponent<AudioSource>().clip = DisruptSound;

                GetComponent<AudioSource>().Play();

                selectedCardAnimator.SetBool("isDisrupt", true);


                if (playerCard.isSpActive && playerCard.Cur_Elemt == playerCard.CardDetail.elementeum)
                {
                    if (playerCard.SpAbility1.attributeType == powerName)
                    {
                        //Debug.Log("This card sp1 ability is " + powerName);
                        playerCard.SpAbility1.gameObject.SetActive(true);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                    else if (playerCard.SpAbility2.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(true);
                    }
                    else
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                }
                else
                {

                    if (playerCard.SpAbility1.attributeType == powerName)
                    {
                        //Debug.Log("This card sp1 ability is " + powerName);
                        playerCard.SpAbility1.gameObject.SetActive(true);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                    else if (playerCard.SpAbility2.attributeType == powerName)
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(true);
                    }
                    else
                    {
                        playerCard.SpAbility1.gameObject.SetActive(false);
                        playerCard.SpAbility2.gameObject.SetActive(false);
                    }
                }
                if (BattleLobbySC.Instance && !PlayerTwo)
                {
                    BattleLobbySC.Instance.PlayerOneAttributeSelect(AttributeType.Disrupt, playerCard.CardDetail.card_contract_id);
                    //Debug.Log(powerName + " is selected by player");


                    for (int i = 0; i < PlayerManager.instance.EnemyCardCanvas.Length; i++)
                    {
                        if (!PlayerManager.instance.enemyCardsObj[i].DestroyedCard)
                        {
                            PlayerManager.instance.enemyCardsObj[i].SortingOrder(6);

                            PlayerManager.instance.enemyCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
                        }
                    }
                }

                else if (BattleLobbySC.Instance && PlayerTwo && CanSend)
                {
                    BattleLobbySC.Instance.PlayerTwoAttribSel(playerCard.CardDetail.card_contract_id,AttributeType.Disrupt);
                    playerCard.HealthAnimatedsHeart();
                    PlayerTwo = false;
                }

                PlayerCompanionCards.AutoSelectCompanionCardsForBattle(AttributeType.Disrupt);

                break;

        }
        playerCard.AnimatePlayerAttributeValue(powerName);
    }


    public void AutoSlectedPowerName(AttributeType powerName, string cardContractId)
    {

        //Debug.Log("Selected Power ----" + powerName);


        PlayerManager.instance.CanClickOnElemt = true;

        if (PlayerManager.instance.getSpOpp())
        {

            PlayerManager.instance.TextCanvas.SetActive(true);

            PlayerManager.instance.TextMessage.text = "Opponent Used Special Power " + Environment.NewLine +
                "Your Base Ability Won't Count";

            StartCoroutine(DisableCanvas());

        }

        else if (PlayerManager.instance.getSpme())
        {

            PlayerManager.instance.TextCanvas.SetActive(true);

            PlayerManager.instance.TextMessage.text = "Special Power Used" + Environment.NewLine +
                "Opponent's Base Ability Won't Count";

            StartCoroutine(DisableCanvas());
        }


        AllSelectedPowerFalse();

        selectedPowerName = powerName;

        //Call Method TO show Image of MyAttribute image and value on canvas
        InGameCards.instance.SetMyAttribImage(powerName);


        switch (powerName)
        {



            case AttributeType.Defense:

                GetComponent<AudioSource>().clip = DefenseSound;

                GetComponent<AudioSource>().Play();

                selectedCardAnimator.SetBool("isDefense", true);

                if (BattleLobbySC.Instance && !PlayerTwo)
                {
                    BattleLobbySC.Instance.PlayerOneAttributeSelect(AttributeType.Defense, cardContractId);

                    //Debug.Log(powerName + " is selected by Enemy");


                    for (int i = 0; i < PlayerManager.instance.EnemyCardCanvas.Length; i++)
                    {
                        if (!PlayerManager.instance.enemyCardsObj[i].DestroyedCard)
                        {
                            PlayerManager.instance.enemyCardsObj[i].SortingOrder(6);
                            PlayerManager.instance.enemyCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
                        }
                    }

                }

                else if (BattleLobbySC.Instance && PlayerTwo)
                {
                    //BattleLobbySC.Instance.PlayerTwoAttribSel(AttributeType.Defense);

                    BattleLobbySC.Instance.PlayerTwoAttribSel(PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,AttributeType.Defense);
                    PlayerTwo = false;
                }

                break;
          
            case AttributeType.Disrupt:
                //TODO: Implement/(move implementation) of Disrupt over here
                Debug.LogError("TODO: Implement/(move implementation) of Disrupt over here");
                GetComponent<AudioSource>().clip = DefenseSound;

                GetComponent<AudioSource>().Play();

                selectedCardAnimator.SetBool("isDisrupt", true);
                if (BattleLobbySC.Instance && !PlayerTwo)
                {
                    BattleLobbySC.Instance.PlayerOneAttributeSelect(AttributeType.Disrupt, cardContractId);

                    //Debug.Log(powerName + " is selected by Enemy");


                    for (int i = 0; i < PlayerManager.instance.EnemyCardCanvas.Length; i++)
                    {
                        if (!PlayerManager.instance.enemyCardsObj[i].DestroyedCard)
                        {
                            PlayerManager.instance.enemyCardsObj[i].SortingOrder(6);
                            PlayerManager.instance.enemyCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
                        }
                    }

                }
                else if (BattleLobbySC.Instance && PlayerTwo)
                {
                    //BattleLobbySC.Instance.PlayerTwoAttribSel(AttributeType.Defense);

                    BattleLobbySC.Instance.PlayerTwoAttribSel(PlayerManager.instance.SelectedCard.CardDetail.card_contract_id, AttributeType.Disrupt);
                    PlayerTwo = false;
                }
                break;
          
        }

    }


    Vector3[] MyCardPosVectors = new Vector3[] { new Vector3(3.4f, 1.62f, 15.46f), new Vector3(0f, 1.62f, 15.46f), new Vector3(-3.4f, 1.62f, 15.46f) };
    Vector3[] MyCardScaleVectors = new Vector3[] { new Vector3(0.5f, 0.5f, 0.5f), new Vector3(0.5f, 0.5f, 0.5f), new Vector3(0.5f, 0.5f, 0.5f) };
    float[] MyCardDurations = new float[] { 0.3f, 0.3f, 0.3F };

    IEnumerator DelayHeal() //Select heal card animation
    {
        
        yield return new WaitForSeconds(0.8f);

        //PlayerManager.instance.PlayerCardsObj.Where(x => !(x.DestroyedCard || x.isDisable)).ForEach(y =>
        //SetCardPosition(PlayerManager.instance.PlayerCardsObj[0].transform, MyCardPosVectors[0], 0.3f, MyCardScaleVectors[0], Quaternion.Euler(Vector3.zero))
        //);

        for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
        {
            var y = PlayerManager.instance.PlayerCardsObj[i];
            if (y != null && !(y.DestroyedCard || y.isDisable)) {

                SetCardPosition(y.transform, MyCardPosVectors[i], MyCardDurations[i], MyCardScaleVectors[i], Quaternion.Euler(Vector3.zero));
            }

        }


        PlayerManager.instance.SelectedCard.powerAnim.transform.localScale = Vector3.zero;
        PlayerManager.instance.TextCanvas.SetActive(true);
        PlayerManager.instance.TextMessage.text = "Please Select Card to Heal";
        PlayerManager.instance.PlayerCardsObj.Where(x => !(x.DestroyedCard)).ForEach(x => x.SetStrengthSortOrder(7, 7, 7, 7));

        Debug.LogError("Please select card for heal ");
    }

    void SetCardPosition(Transform card, Vector3 pos, float v, Vector3 scale, Quaternion rot)
    {
        card.DOLocalMove(pos, v);
        card.localScale = scale;
        card.localRotation = rot;
    }

    public int checkAttrAnim = 0;
    public void SlectedEnemyPowerName(AttributeType powerName, int index)
    {
        Debug.Log("SlectedEnemyPowerName----" + EnemyselectedCardAnimator + " " + powerName);
        checkAttrAnim += index;
      
        AllSelectedEnemyPowerFalse();
        InGameCards.instance.OppAttribImage.gameObject.SetActive(true);
        selectedEnemyPowerName = powerName;
        CardObject EnemyObj = PlayerManager.instance.SelectedEnemyCard;

        if (EnemyObj != null)
        {
            //Call Method TO show Image of MyAttribute image and value on canvas
            InGameCards.instance.SetOppAttribImage(powerName);

            var audioSource = GetComponent<AudioSource>();

            switch (powerName)
            {

                case AttributeType.Attack:
                    audioSource.clip = attackSound;
                    break;

                case AttributeType.Heal:
                    audioSource.clip = HealSound;
                    break;

                case AttributeType.Defense:
                    audioSource.clip = DefenseSound;
                    break;

                case AttributeType.Disrupt:
                    audioSource.clip = DisruptSound;
                    break;
            }

            if (powerName != AttributeType.Attack)
                EnemyObj.HealthAnimatedsHeart();

            if (EnemyselectedCardAnimator)
                EnemyselectedCardAnimator.SetBool("is" + powerName, true);

            audioSource.Play();
            PlayerCompanionCards.AutoSelectEnemyCompanionCardsForBattle(powerName);


            if (checkAttrAnim == 1)
            {
                EnemyObj.AnimateEnemyAttributeValue(powerName);
            }

        }

    }


    public void AllSelectedPowerFalse()
    {

        if (!selectedCardAnimator)
        { return; }

        //selectedCardAnimator.transform.parent.parent.GetComponent<CardObject>().isPowerBtnShow = true;
        var cardRefHolder = SelectPowerManager.instance.selectedCardAnimator.GetComponent<CardObjectRefHolder>();

        if (cardRefHolder && cardRefHolder.cardObject && cardRefHolder.cardObject.isPowerBtnShow)
        {
            cardRefHolder.cardObject.isPowerBtnShow = true;
        }



        selectedCardAnimator.SetBool("Shopower", false);
        selectedCardAnimator.SetBool("isAttack", false);
        selectedCardAnimator.SetBool("isHeal", false);
        selectedCardAnimator.SetBool("isDefense", false);
        selectedCardAnimator.SetBool("isDisrupt", false);
        //Debug.Log("Power de slected");
    }


    public void AllSelectedEnemyPowerFalse()
    {

        if (!EnemyselectedCardAnimator)
        { return; }

        //Debug.Log("AllSelectedEnemyPowerFalse");
        EnemyselectedCardAnimator.transform.parent.parent.GetComponent<CardObject>().isPowerBtnShow = true;
        EnemyselectedCardAnimator.SetBool("Shopower", false);
        EnemyselectedCardAnimator.SetBool("isAttack", false);
        EnemyselectedCardAnimator.SetBool("isHeal", false);
        EnemyselectedCardAnimator.SetBool("isDefense", false);
        EnemyselectedCardAnimator.SetBool("isDisrupt", false);
        ///
        /// debug
        /// 
        ////Debug.Log("call comapanion card rest funtion here");
        //PlayerCompanionCards.AutoDeselectCompanionCardsForBattle();
    }


}