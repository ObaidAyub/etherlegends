﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.UI;

public class LeaderboardMain : MonoBehaviour
{
    public static LeaderboardMain instance;

    public GameObject UserItem;
    public Transform LeaderContainer;
    public Texture2D DefaultPic;

    public GameObject allLeaderboard;
    public GameObject seasonLeaderboard;

    public Button allTime, Season;

    public Transform seasonBtnContent;
    public SeasonBtnPrefab seasonBtnPrefab;

    public Transform seasonLeaderBoardScroll;
    public SeasonLeaderBoardPrefab seasonLeaderBoardPrefab;

    public GameObject seasonLeaderBoardLoader;
    public GameObject profilePanel;

    

    // all leasderboard inital and final data fecting 
    //public int all_Skip = 0;
    public int PageSize = 10;
    public int TotalCount = 0;

    // Season leasderboard inital and final data fecting 
    public int season_Skip = 0;
    public int season_Limit = 10;


    public ScrollRect scroller;


    List<JSONNode> LeaderBoardPlayers = new List<JSONNode>();

    public class JsonBody
    {
        public string skip;
        public string limit;
    }

    public Button exit;
    

    private void Start()
    {
        instance = this;

        if (DefaultPic != null)
            DefaultPic.name = "default";
    }
    private void OnEnable()
    {
        AllLeaderBoardUI();
        ResetAll();
        GetUserLeaderBoard();
    }


    public void ResetAll()
    {
        TotalCount = 0;
        LeaderBoardPlayers.Clear();
        destroycontentData(LeaderContainer);

        destroycontentData(seasonBtnContent);
        destroycontentData(seasonLeaderBoardScroll);
    }

    private void OnDisable()
    {
        ResetAll();
    }
    public void backToProfileScr()
    {
        ResetAll();
        this.gameObject.SetActive(false);
    }


    void onScrollValueChange(Vector2 value)
    {

        

        if (scroller.normalizedPosition.y < 0.01F && (scroller.content.childCount == 0 || scroller.content.childCount > 9) )
        {
            if (TotalCount == 0 || LeaderBoardPlayers.Count < TotalCount)
            {
                if (!isBusy)
                {
                    LoadPage();
                }
            }
        }
    }


    bool isBusy = false;
    public async void LoadPage()
    {
        //print(Time.realtimeSinceStartup);
        isBusy = true;
        JsonBody jsonbody = new JsonBody();
        jsonbody.skip = LeaderBoardPlayers.Count.ToString();
        jsonbody.limit = PageSize.ToString();
        string confirmBody = JsonUtility.ToJson(jsonbody);
        exit.interactable = false;
        seasonLeaderBoardLoader.SetActive(true);
        string response = await SessionManager.Instance.Post(ConstantManager._url_LeaderBoard, confirmBody, SessionManager.Instance.userToken, false);
        //Debug.Log(response);

        ////Debug.Log("LeaderBoard Response " + response);
        var data = JSON.Parse(response);
        exit.interactable = true;
        TotalCount = data["total"];
        seasonLeaderBoardLoader.SetActive(false);
        for (int i = 0; i < data["data"].Count; i++)
        {

            LeaderBoardPlayers.Add(data["data"][i]);
           
        }

        if (data["data"].Count > 0) {

            if (this.gameObject.activeInHierarchy)
            {

            PopulateData(int.Parse(jsonbody.skip));
            }
        }
       
    }

    public void GetUserLeaderBoard()
    {
        scroller.onValueChanged.RemoveListener(onScrollValueChange);
        scroller.onValueChanged.AddListener(onScrollValueChange);

        LoadPage();
    }

    void PopulateData(int skip) {

        StartCoroutine(LoadPlayers(skip));

    }

    IEnumerator LoadPlayers(int skip) {


        for (int i = skip; i < LeaderBoardPlayers.Count; i++)
        {
            yield return new WaitForEndOfFrame();

            GameObject SpawnObj = Instantiate(UserItem, LeaderContainer);
            StartCoroutine( GetImage(SpawnObj, i,
                LeaderBoardPlayers[i]["profile_pic"],
                LeaderBoardPlayers[i]["displayName"],
                LeaderBoardPlayers[i]["_id"],
                LeaderBoardPlayers[i]["battles"],
                LeaderBoardPlayers[i]["wins"],
                LeaderBoardPlayers[i]["wins"],
                LeaderBoardPlayers[i]["max_streak"],
                LeaderBoardPlayers[i]["ethereal_power"]));
        }
        isBusy = false;

    }




    IEnumerator GetImage(GameObject SpawnObj, int id, string ImgLink, string txname, string UserID, string battle, string win, string loss, string streak, string EP)
    {
        if (string.IsNullOrEmpty(ImgLink))
        {
           
            SpawnObj.GetComponent<LeaderboardItem>().SetValue((id+1).ToString(), DefaultPic, txname,UserID, battle, win, loss, streak, EP);
            SpawnObj.SetActive(true);
        }

        else
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(ImgLink))
            {
                yield return www.SendWebRequest();

                while (!www.isDone)
                {
                    yield return new WaitForEndOfFrame();
                    ////Debug.Log(www.downloadProgress);
                }
                if (www.isNetworkError || www.isHttpError)
                {
                    ////Debug.Log(www.error);
                    SpawnObj.GetComponent<LeaderboardItem>().SetValue((id + 1).ToString(), DefaultPic, txname, UserID, battle, win, loss, streak, EP);

                    SpawnObj.SetActive(true);

                }
                else
                {

                    SpawnObj.SetActive(true);
                    SpawnObj.GetComponent<LeaderboardItem>().SetValue((id + 1).ToString(), DownloadHandlerTexture.GetContent(www), txname, UserID, battle, win, loss, streak, EP);


                }
            }
        }
       


    }




    public void AllLeaderBoardUI()
    {
        destroycontentData(seasonBtnContent);
        destroycontentData(seasonLeaderBoardScroll);
        ////Debug.Log("All leaderboad enable ");
        allLeaderboard.SetActive(true);
        allTime.interactable = false;
        seasonLeaderboard.SetActive(false);
        Season.interactable = true;


        
    }

    public void GetSeasonLeaderBoard()
    {
        allLeaderboard.SetActive(false);
        allTime.interactable = true;

        seasonLeaderboard.SetActive(true);
        Season.interactable = false;

        ////Debug.Log("Season leaderboad enable ");

        GetSeasonData();
    }
    public async void GetSeasonData()
    {
        seasonLeaderBoardLoader.SetActive(true);
        destroycontentData(seasonBtnContent);

        JsonBody jsonbody = new JsonBody();
        jsonbody.skip = season_Skip.ToString();
        jsonbody.limit =season_Limit.ToString();

        string confirmBody = JsonUtility.ToJson(jsonbody);

        string response = await SessionManager.Instance.Post(ConstantManager._url_All_Season, confirmBody, null, false);
        Debug.Log(response);
        if (string.IsNullOrEmpty(response))
        {
            GetSeasonData();
            return;
        }
        else
        {

            JSONNode jnode = JSON.Parse(response);

            ////Debug.Log(jnode);
            ////Debug.Log(jnode["data"].Count);
            seasonLeaderBoardLoader.SetActive(false);

            for (int i = 0; i < jnode["data"].Count; i++)
            {
                SeasonBtnPrefab season = Instantiate(seasonBtnPrefab, seasonBtnContent);
                season.status = jnode["status"];
                 season.seasonName.text = jnode["data"][i]["season_name"];
                season.season_address = jnode["data"][i]["season_address"];


            }
            GetSeasonLeaderBoardData();
        }

    }

    public async void GetSeasonLeaderBoardData()
    {
        seasonLeaderBoardLoader.SetActive(true);
        exit.interactable = false;
        destroycontentData(seasonLeaderBoardScroll);

        string response = await SessionManager.Instance.Post(ConstantManager._url_SeasonLeaderboard, null, SessionManager.Instance.userToken, true);
        //Debug.Log(response);

        JSONNode jnode = JSON.Parse(response);
        //Debug.Log(jnode);
        
        seasonLeaderBoardLoader.SetActive(false);
        exit.interactable = true;

        for (int i = 0; i < jnode["data"].Count; i++)
        {

            SeasonLeaderBoardPrefab season = Instantiate(seasonLeaderBoardPrefab, seasonLeaderBoardScroll);
            

            string id = season.Id.text = (i + 1).ToString();
            string name = season.Txt_name.text = jnode["data"][i]["userData"]["displayName"];
            string battle = season.Txt_Battle.text = jnode["data"][i]["userData"]["battles"];
            string win = season.Txt_win.text = jnode["data"][i]["userData"]["wins"];
            string streak = season.Txt_Streak.text = jnode["data"][i]["userData"]["max_streak"];
            string loss = season.Txt_Loss.text = jnode["data"][i]["userData"]["draw"];
            string trophy = season.Txt_Trophies.text = jnode["data"][i]["userTrophies"];
            string ImageLink = season.Txt_Trophies.text = jnode["data"][i]["userData"]["profile_pic"];
            //Debug.Log(ImageLink);
            StartCoroutine(GetSeasonImage(season, id, ImageLink, name, battle, win, loss, streak, trophy));

        }
    }
    IEnumerator GetSeasonImage(SeasonLeaderBoardPrefab SpawnObj, string id, string ImgLink, string txname, string battle, string win, string loss, string streak, string trophy)
    {
        if (string.IsNullOrEmpty(ImgLink))
        {

            SpawnObj.SetValue(id, DefaultPic, txname, battle, win, loss, streak, trophy);
            SpawnObj.gameObject.SetActive(true);
        }

        else
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(ImgLink))
            {
                yield return www.SendWebRequest();

                while (!www.isDone)
                {
                    yield return new WaitForEndOfFrame();
                }
                if (www.isNetworkError || www.isHttpError)
                {
                    if (SpawnObj)
                    {
                        SpawnObj.SetValue(id, DefaultPic, txname, battle, win, loss, streak, trophy);

                        SpawnObj.gameObject.SetActive(true);
                        //Debug.Log(www.error.ToString());
                    }
                }
                else
                {
                    if (SpawnObj)
                    {
                        SpawnObj.SetValue(id, DownloadHandlerTexture.GetContent(www), txname, battle, win, loss, streak, trophy);
                        SpawnObj.gameObject.SetActive(true);
                    }

                }
            }
        }



    }
    public void destroycontentData(Transform Content)
    {

        while (Content.childCount > 0)
        {
            var child = Content.GetChild(0);
            child.SetParent(null);
            Destroy(child.gameObject);
        }

    }

}
