﻿using System;
using System.Collections;
using System.Collections.Generic;
using Wizcorp.Utils.Logger;
using System.Runtime.Serialization.Formatters.Binary;
using BarcodeScanner;
using BarcodeScanner.Scanner;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;
using TMPro;
using UnityEngine.Android;
using Nethereum.Web3;

using Nethereum.Util;
using Nethereum.HdWallet;
using NBitcoin;
using System.IO;
using System.Text.RegularExpressions;

public class AuthenticationManager : MonoBehaviour
{

    #region Variables

    public static AuthenticationManager Instance;

    [Header("Game Screens")]
    public GameObject[] screens;

    [Header("Signin")]
    public InputField inp_signIn_email;
    public InputField inp_signIn_password;

    
    public string tokenId;
    private string savedGames;

    public bool StartGame;
    BinaryFormatter bf = new BinaryFormatter();
    Button CopyButton;



    public GameObject LoginErrorText, SignUpErrorText;

    public GameObject Profile, BestCards, socketObj;

    public GameObject ERCToken721Obj, ERCToken20Obj, ERCToken1155Obj, ConfirmPopup, LogoutPopup;


    public TextMeshProUGUI logoutAndBlockText;
    public Text LinkCode, ErrorLink;
    public GameObject LinkObj;
    // public object SaveLoad { get; private set; }
    public GameObject RetryBtn, LoadingImg;

    public GameObject LoginLoading, loginBtn, createbtn;

    #endregion Variables


    #region Barcode QRCode
    private IScanner BarcodeScanner;
    public RawImage Qrcode;
    public GameObject QRWindow;
    #endregion

    string glyphs = "";

    public GameObject creatWalletPopUp;
    public TextMeshProUGUI connectWalletText;
    [Serializable]
    public class ConfirmToken
    {
        public string email;
        public string password;
        public string user_token;
    }

    public enum AuthenticationScreen
    {
        LOGIN,
        REGISTER,
        //CHANGEPASS,
        LINKWALLET,
        NEWWALLET,
        LINKPRIVATEKEY,
        LINKSEED,
        SPLASH,
        /*NETWORKERROR,*/
        NONE
    }


    public GameObject enableObjectDelay;
    private void Awake()
    {
        Instance = this;
        enableObjectDelay.SetActive(false);
        StartCoroutine(checkingOnNetwork());
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        glyphs = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


#if PLATFORM_ANDROID

        //if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        //{
        //    Permission.RequestUserPermission(Permission.Camera);
        //}

        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageRead);
        }

        //if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        //{
        //    Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        //}

#endif


        LoadingManager.Instance.ShowMainLoading("Logging you In...");


        int sound = PlayerPrefs.GetInt("sound");


        if (sound == 0)
        {
            AudioListener.volume = 1;

        }
        else
        {
            AudioListener.volume = 0;
        }

        Input.multiTouchEnabled = false;
        StartCoroutine(SetCollectibleDelay());

    }


    public IEnumerator checkingOnNetwork()
    {
        CheckInternetConnection();
        yield return new WaitForSeconds(5);
        StartCoroutine(checkingOnNetwork());
    }

    public void CheckInternetConnection()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            ////Debug.Log("Error. Check internet connection!");
            LoadingManager.Instance.ShowNetworkErrorWindow();
            return;
        }
        else
        {
            ////Debug.Log("hide window");

            LoadingManager.Instance.HideNetworkErrorWindow();
        }

    }
    IEnumerator SetCollectibleDelay()
    {

        yield return new WaitForSeconds(2f);
        if (UserDataController.Instance.IsNotToken())
        {
            UserDataController.Instance.setToken(GenGlyphs());
        }
       
        //yield return new WaitForSeconds(5f);

        //StartGame = true;
        //Start();
        //GetComponent<CollectibleSc>().enabled = true;



    }
    public string GenGlyphs()
    {
        string myString = "";

        for (int i = 0; i < 50; i++)
        {
            myString += glyphs[UnityEngine.Random.Range(0, glyphs.Length)];
        }

        return myString;

    }



    public async void RemoveProfilePic()
    {
        string Response = await SessionManager.Instance.Post(ConstantManager._url_RemoveProfile, null, SessionManager.Instance.userToken, true);


    }

    public void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            GetAuthentication();
        }
        else
        {
            LoadingManager.Instance.HideMainLoading();
            LoadingManager.Instance.HideNetworkErrorWindow();
        }


       
    }

    void GetAuthentication()
    {
        //Debug.Log("GET ATHENTICATION ");
        //if (!StartGame)
        //    return;

        //yield return new WaitForSeconds(0f);
        if (!String.IsNullOrEmpty(SessionManager.Instance.userToken))
        {
            SessionManager.Instance.userToken = UserDataController.Instance.GetUserToken();
            onAuthenticiation();
        }
        else
        {
            if (UserDataController.Instance.GetUserToken() == "")
            {
                LoadingManager.Instance.HideMainLoading();
                ChangeScreen(AuthenticationScreen.LOGIN);
            }
            else
            {
                SessionManager.Instance.userToken = UserDataController.Instance.GetUserToken();
            }
        }
    }

    public static class SaveLoad
    {

        public static List<Game> savedGames = new List<Game>();
        internal static object registerTokenId;
    }

    public void ChangeScreen(AuthenticationScreen screen)
    {
        foreach (GameObject g in screens)
        {
            g.SetActive(false);
        }

        if (screen != AuthenticationScreen.NONE)
            screens[(int)screen].SetActive(true);
    }

    public void Btn_openSignUpPopUp()
    {
        creatWalletPopUp.SetActive(true);
       
    }


    private void onAuthenticiation()
    {

        LoadingManager.Instance.ShowMainLoading("Loading Resources...");

        checkLogin();

        inp_signIn_email.text = "";
        inp_signIn_password.text = "";
        ChangeScreen(AuthenticationScreen.NONE);
        ProfileManager.Instance.profilePanel.SetActive(true);
        LoginLoading.SetActive(false);
        loginBtn.SetActive(true);
        createbtn.SetActive(true);
        if (socketObj)
            socketObj.SetActive(true);

        
    }

    public void PlayClickSound()
    {
        LoadingManager.Instance.Btn_ClickSound();
    }

    public void Logout()
    {
        logoutAsync();
    }

   

    public void ConfirmLogout()
    {

        GameObject obj = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;

        ////////Debug.Log(obj.transform.parent);
        LoadingManager.Instance.btn_yes.onClick.RemoveAllListeners();

        LoadingManager.Instance.btn_yes.onClick.AddListener(() => hideparent(obj.transform.parent.gameObject));
        LoadingManager.Instance.btn_yes.onClick.AddListener(() => logoutAsync());
        ProfileManager.Instance.BestCard.SetActive(false);
        LoadingManager.Instance.ShowPopUp("Are you sure you want to logout?");

    }

    public void hideparent(GameObject ParentObj)
    {
        LoadingManager.Instance.ShowMainLoading("LogOut");
        LoadingManager.Instance.HidePopUp();
        ParentObj.SetActive(false);
    }

    /// <summary>
    /// Dos the Logout async. Called from Logout button.
    /// </summary>
    public async void logoutAsync()
    {

        string response = await SessionManager.Instance.Post(ConstantManager._url_userlogout, null, tokenId, true);

        var data = JSON.Parse(response);

        UserDataController.Instance.SetUserToken("");
        SessionManager.Instance.userToken = "";
        ProfileManager.Instance.profilePanel.SetActive(false);
        ProfileManager.Instance.BestCard.SetActive(false);

        LogoutPopup.SetActive(false);
       
        ChangeScreen(AuthenticationScreen.LOGIN);
        LoadingManager.Instance.HideMainLoading();


        int currentVersion = UserDataController.Instance.GetVersion();
        PlayerPrefs.DeleteAll();
        UserDataController.Instance.SetVersion(PlayerPrefs.GetInt("version", currentVersion));



        string DIRPathfile = Path.Combine(Application.persistentDataPath, "file");
        if (Directory.Exists(DIRPathfile))
        {
           
                DirectoryInfo data_dir = new DirectoryInfo(DIRPathfile);
                data_dir.Delete(true);
           
        }

        RestartGame();
    }


    async void checkLogin()
    {
        string emailId = "";

        if (string.IsNullOrEmpty(inp_signIn_email.text) && string.IsNullOrEmpty(UserDataController.Instance.Myprefs.UName))
        {
            return;
        }
        if (!string.IsNullOrEmpty(inp_signIn_email.text))
        {
            emailId = inp_signIn_email.text;
        }

        else if (!string.IsNullOrEmpty(UserDataController.Instance.Myprefs.UName))
        {
            emailId = UserDataController.Instance.Myprefs.UName;
        }

        ConfirmToken confirmToken = new ConfirmToken();
        confirmToken.email = emailId;
        confirmToken.password = UserDataController.Instance.Myprefs.UPass;
        confirmToken.user_token = UserDataController.Instance.gettoken();
        string confirmBody = JsonUtility.ToJson(confirmToken);
        //Debug.Log(confirmBody);
        string ConfirmResponseLogin = await SessionManager.Instance.Post(ConstantManager._url_Login, confirmBody);

        if (ConfirmResponseLogin != null)
        {
            var LoginJson = JSON.Parse(ConfirmResponseLogin);
        }
        string ConfirmResponse = await SessionManager.Instance.Post(ConstantManager._url_confirmUserToken, confirmBody);

        //Debug.Log("confirm -->" + ConfirmResponse);

        if (string.IsNullOrEmpty(ConfirmResponse))
        {

        }
        else
        {


            var ConfirmData = JSON.Parse(ConfirmResponse);

            if (ConfirmData["status"] == 1)
            {

                if (ConfirmData["matched"] == "false")
                {

                    LogoutPopup.SetActive(true);
                    return;
                }
                else
                {
                    BattleLobbySC.Instance.SendLoginEmit(UserDataController.Instance.Myprefs.UserID);
                }

            }
            else if (ConfirmData["status"] == 0)
            {
                logoutAndBlockText.text = ConfirmData["message"];
                LogoutPopup.SetActive(true);

            }
        }
        UserDataController.Instance.DoGetAllCardTokenBalance();

        UserDataController.Instance.DoGetProfile();
    }

    /// <summary>
    /// Dos the login async. Called from Login button.
    /// </summary>
    public async void DoLoginAsync()
    {
        LoadingManager.Instance.ShowLoader("Logging You In...");
        //////Debug.Log("LoginCalled");
        if (inp_signIn_email.text == "" && inp_signIn_password.text == "")
        {
            LoginErrorText.SetActive(true);
            LoginErrorText.GetComponent<Text>().text = "Please Enter Email ID and Password";
            LoadingManager.Instance.HideLoader();

            return;
        }

        else if (inp_signIn_email.text == "")
        {
            LoginErrorText.SetActive(true);
            LoginErrorText.GetComponent<Text>().text = "Please Enter Email ID";
            LoadingManager.Instance.HideLoader();

            return;
        }

        else if (inp_signIn_password.text == "")
        {
            LoginErrorText.SetActive(true);
            LoginErrorText.GetComponent<Text>().text = "Please Enter Password";
            LoadingManager.Instance.HideLoader();

            return;
        }
        else
        {
            LoginErrorText.SetActive(false);
        }

        

        UserDataController.Instance.setUser(inp_signIn_email.text, inp_signIn_password.text);
        ConfirmToken confirmToken = new ConfirmToken();
        confirmToken.email = inp_signIn_email.text;
        confirmToken.password = inp_signIn_password.text;
        confirmToken.user_token = UserDataController.Instance.gettoken();
        //Debug.Log("User Id = " + confirmToken.email + " Password = " + confirmToken.password + " user token " + confirmToken.user_token);
        string confirmBody = JsonUtility.ToJson(confirmToken);

        string ConfirmResponseLogin = await SessionManager.Instance.Post(ConstantManager._url_Login, confirmBody);
        ////Debug.Log("confirm -->" + ConfirmResponseLogin);
        var LoginJson = JSON.Parse(ConfirmResponseLogin);
        if (!string.IsNullOrEmpty(LoginJson["data"]["data"]["publicAddress"]))
        {
            UserDataController.Instance.Myprefs.UserToken = LoginJson["data"]["token"];
            UserDataController.Instance.SetUserToken(UserDataController.Instance.Myprefs.UserToken);

            UserDataController.Instance.Myprefs.Address = LoginJson["data"]["data"]["publicAddress"];
            //CheckForPrivateKey();
        }
      
        ////Debug.Log(LoginJson["data"]["data"]["_id"]);
        if (string.IsNullOrEmpty(UserDataController.Instance.Myprefs.UserID))
        {

            UserDataController.Instance.SetUserID(LoginJson["data"]["data"]["_id"]);
        }
        LoadingManager.Instance.HideLoader();
        if (LoginJson["status"] == 0)
        {
            if (LoginJson["message"] == "User already login in other device, Please logout first")
            {
                LoadingManager.Instance.HideLoader();
                ConfirmPopup.SetActive(true);
                return;

            }
            else
            {
                LoadingManager.Instance.HideLoader();
                LoginErrorText.SetActive(true);
                LoginErrorText.GetComponent<Text>().text = LoginJson["message"];
                LoginLoading.SetActive(false);

                ////Debug.Log(LoginJson["data"]["data"]["_id"]);

                return;
            }
           
        }
       
        string ConfirmResponse = await SessionManager.Instance.Post(ConstantManager._url_confirmUserToken, confirmBody);

        ////Debug.Log("confirm -->" + ConfirmResponse);


        var ConfirmData = JSON.Parse(ConfirmResponse);

        int status = ConfirmData["status"];

        if (status == 0)
        {
            LoginLoading.SetActive(false);
            loginBtn.SetActive(true);
            createbtn.SetActive(true);
            LoginErrorText.gameObject.SetActive(true);
            LoginErrorText.GetComponent<Text>().text = ConfirmData["message"];
        }
        else
        {
            if (ConfirmData["matched"] == "true" )
            {
                ConfirmLogin();

            }
            else if (ConfirmData["matched"] == "false")
            {
                ConfirmPopup.SetActive(true);

            }

            else if(ConfirmData["matched"] == "blank")
            {
                ConfirmPopup.SetActive(true);
            }
        }

        LoadingManager.Instance.HideLoader();

    }




    public async void ConfirmLogin()
    {
        
        Login _loginObj = new Login();

        _loginObj.email = inp_signIn_email.text;
        _loginObj.password =   inp_signIn_password.text;
        _loginObj.user_token = UserDataController.Instance.gettoken();


        string bodyJson = JsonUtility.ToJson(_loginObj);


        string response = await SessionManager.Instance.Post(ConstantManager._url_ForceLogin, bodyJson);

        if (response == null)
        {
            LoginLoading.SetActive(false);
            loginBtn.SetActive(true);
            createbtn.SetActive(true);
            LoginErrorText.gameObject.SetActive(true);
            LoginErrorText.GetComponent<Text>().text = "Please Check Your Internet Connection";
            LoadingManager.Instance.HideLoader();
            return;
        }

        LoginResponse loginResponse = JsonUtility.FromJson<LoginResponse>(response);
            var LoginJson = JSON.Parse(response);
        if (string.IsNullOrEmpty(UserDataController.Instance.Myprefs.Address))
        {

            UserDataController.Instance.Myprefs.Address = LoginJson["data"]["data"]["publicAddress"];
        }
       

        if (loginResponse.status == 1)
        {
            ConfirmPopup.SetActive(false);
            BattleLobbySC.Instance.SendLoginEmit(LoginJson["data"]["data"]["_id"]);

            if (string.IsNullOrEmpty(UserDataController.Instance.Myprefs.UserID))
            {
                UserDataController.Instance.SetUserID(LoginJson["data"]["data"]["_id"]);
            }

            LoginErrorText.gameObject.SetActive(false);
            SessionManager.Instance.userToken = loginResponse.data.token;
            UserDataController.Instance.SetUserToken(loginResponse.data.token);
            UserDataController.Instance.setUser(inp_signIn_email.text, inp_signIn_password.text);
            onAuthenticiation();
        }
        else
        {
            LoginErrorText.SetActive(true);
            LoginErrorText.GetComponent<Text>().text = loginResponse.message;
        }

        LoginLoading.SetActive(false);
        LoadingManager.Instance.HideLoader();
    }


    /// <summary>
    /// in this funtion data is remove and intances is delete
    ///
    /// </summary>
    public void RestartGame()
    {
        Destroy(UserDataController.Instance.gameObject);
        Destroy(CardManagerSC.Instance.gameObject);
        Destroy(BattleLobbySC.Instance.gameObject);
        Destroy(ConnectOpenGame.Instance.gameObject);

        UserDataController.Instance = null;
        CardManagerSC.Instance = null;
        BattleLobbySC.Instance = null;
        ConnectOpenGame.Instance = null;

        SceneManager.LoadScene(0);
    }


    public void GetCopyButton(GameObject InpButton)
    {
        CopyButton = InpButton.GetComponent<Button>();
    }

    public void CopyToClipboard(Text ValueTocopy)
    {
        GUIUtility.systemCopyBuffer = ValueTocopy.text;
        if (CopyButton)
        {
            CopyButton.transform.GetChild(0).gameObject.SetActive(true);
            StartCoroutine(HideCopyText());
        }
    }

    IEnumerator HideCopyText()
    {
        yield return new WaitForSeconds(0.5f);
        if (CopyButton)
        {
            CopyButton.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

}
