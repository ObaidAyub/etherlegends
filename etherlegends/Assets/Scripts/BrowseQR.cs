﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXing;

public class BrowseQR : MonoBehaviour
{
    //public Text console;
   

    public Texture2D texture;
    public RawImage QRImage;

    string imagePath = "";   

    public GameObject loadingSCreen;

    public GameObject ScanQRObj;


    void OnEnable()
    {
       Debug.Log("Onenable");             
       //NativeToolkit.OnImagePicked += ImagePicked;      
    }

    void OnDisable()
    {
        Debug.Log("OnDisable");
        //NativeToolkit.OnImagePicked -= ImagePicked;
    }

   

    //=============================================================================
    // Button handlers
    //=============================================================================


    public void OnPickImagePress()
    {
        PlayerPrefs.SetString("picktype", "qrcode");
        GetComponent<ScanQR>().ClickStop();

        //NativeToolkit.PickImage();
    }

    IEnumerator ScanDelay()
    {
        Debug.Log("Scan Delay");
        yield return new WaitForSeconds(0.1f);
        try
        {
            texture = (Texture2D)QRImage.mainTexture;

            IBarcodeReader reader = new BarcodeReader();

            // get texture Color32 array
            var barcodeBitmap = texture.GetPixels32();
            // detect and decode the barcode inside the Color32 array
            var result = reader.Decode(barcodeBitmap, QRImage.mainTexture.width, QRImage.mainTexture.height);

            Debug.Log("Result " + result.Text);

            // do something with the result

            if (result != null)
            {
                GetComponent<ScanQR>().TextHeader.text = result.Text;
                GetComponent<ScanQR>().sendQRDataAsync();
                Debug.Log(result.BarcodeFormat.ToString());
                Debug.Log(result.Text);
            }
            else
            {
                GetComponent<ScanQR>().InvalidImage();
            }
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.ToString());
            GetComponent<ScanQR>().InvalidImage();
        }

     
        // BarcodeScanner.ScanImageQR(Bar2d);
    }

    //=============================================================================
    // Callbacks
    //=============================================================================


    void ImagePicked(Texture2D img, string path)
    {
        if (PlayerPrefs.GetString("picktype") == "qrcode")
        {
            if (img == null || string.IsNullOrEmpty(path))
            {
                return;
            }

            print("QRIMagePicked Second");

            imagePath = path;
            Debug.Log("\n Browse QR Image picked at: " + imagePath);


            if (!string.IsNullOrEmpty(imagePath))
            {
                Debug.Log("inside Path Check");
                //ProfilePic.gameObject.SetActive(true);
                QRImage.texture = LoadImageFromFile(imagePath);
                StartCoroutine(ScanDelay());
                 if (GetComponent<ScanQR>())
                {
                //rm StartCoroutine(Delay());
               // loadingSCreen.SetActive(true);
                  }
            }
             Destroy(img);
        }     
       
    }

    

    public static Texture2D LoadImageFromFile(string path)
    {
        if (path == "Cancelled") return null;

        byte[] bytes;
        Texture2D texture = new Texture2D(290, 290, TextureFormat.RGB24, false);

#if UNITY_WINRT

                bytes = UnityEngine.Windows.File.ReadAllBytes(path);
                texture.LoadImage(bytes);

#else

        bytes = System.IO.File.ReadAllBytes(path);
        texture.LoadImage(bytes);

#endif

        return texture;
    }

    

}
