﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public enum Axis {z,y,x };

    public float speed = 1;

    public Axis RotAxis;     

    public float TimForDisable;

    public Transform ItemToRotate;

    /*private void Awake()
    {
        if (!ItemToRotate)
        {
            ItemToRotate = this.transform;
        }

        if (TimForDisable > 0)
        {
            StartCoroutine(Destroyit());
        }

    }   */
       

    IEnumerator Destroyit()
    {
            yield return new WaitForSeconds(TimForDisable);

       
            ItemToRotate.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
     
            //this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                         
            this.enabled = false;

    }

    private void OnEnable()
    {
        if (!ItemToRotate)
        {
            ItemToRotate = this.transform;
        }

        if (TimForDisable > 0)
        {
            StartCoroutine(Destroyit());
        }
    }

    private void Update()
    {       

        switch (RotAxis)
        {

            case Axis.x:

                ItemToRotate.Rotate(speed, 0, 0);

                break;

            case Axis.y:
                ItemToRotate.Rotate(0,speed, 0);
                break;

            case Axis.z:
                ItemToRotate.Rotate(0, 0, -speed);

                break;

        }

      
    }


}
