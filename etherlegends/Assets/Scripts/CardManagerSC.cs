﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CardManagerSC : MonoBehaviour
{

    [System.Serializable]
    public struct ImgJson
    {
        public string CardContractId;
        public string ImgName;
    }

    [System.Serializable]
    public class FinalJson
    {
        public List<ImgJson> Images;
    }

    public static CardManagerSC Instance;

    public FinalJson Fjsonnew;

    [System.Serializable]
    public struct CardImage
    {
        public string Contract_id;
        public Sprite Card_Image;
        public Texture2D Card_Texture;
    }

    [SerializeField]
    public CardImage[] CardDetails;

    public List<CardImage> CardDetailsImage;

    string DIRPath = "";
    string MainResponse;

    public Text downloadingText;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
        GetAllCardImages();
    }
    async void GetAllCardImages()
    {

        string response = await SessionManager.Instance.Post(ConstantManager._url_GetAllCardDetailsFromStore, null, null, false);
        if (string.IsNullOrEmpty(response))
        {
            GetAllCardImages();
            return;
        }
        else
        {
            var Jnode = JSON.Parse(response);
            MainResponse = response;
            StartCoroutine(DownloadAllCards(Jnode));
        }
    }

    public IEnumerator DownloadAllCards(JSONNode CardData)
    {
        int cardsLoaded = 0;
        LoadingManager.Instance.CardLoadingBar.minValue = 0;
        LoadingManager.Instance.CardLoadingBar.maxValue = CardData["data"].Count;

        float total = CardData["data"].Count;
        float final;
        //for (int i = 0; i < CardData["data"].Count; i++)
        //{
        //    yield return SetItemImage(CardData["data"][i]["card_contract_id"], CardData["data"][i]["s3_card_image"], () =>
        //    {
        //        cardsLoaded++;


        //        LoadingManager.Instance.CardLoadingBar.value = cardsLoaded;
        //        final = (LoadingManager.Instance.CardLoadingBar.value / total) * 100;
        //        if (final < 100)
        //        {

        //            downloadingText.text = "Downloading " + string.Format("{0:0.00}", final) + " %";
        //        }
        //        else
        //        {
        //            downloadingText.text = "Downloading " + final + " %";
        //            CardManagerSC.Instance.downloadingText.gameObject.SetActive(false);
        //        }
        //    });
        //    if (Application.internetReachability == NetworkReachability.NotReachable)
        //    {
        //        //Debug.Log("Network Error");
        //        LoadingManager.Instance.ShowNetworkErrorWindow(EnableObjectDelay.Instance.Restart);
        //        yield break;
        //    }


        //}
        //yield return new WaitUntil(() => cardsLoaded == CardData["data"].Count);

        yield return 0;
        EnableObjectDelay.Instance.StartOtherObject();

    }
    


    public IEnumerator SetItemImage(string ContractId, string ImgLink, System.Action onComplete = null)
    {
        DIRPath = Path.Combine(Application.persistentDataPath, "GameCards");

        if (!Directory.Exists(DIRPath))
        {
            Directory.CreateDirectory(DIRPath);
        }

        string FilePath = Path.Combine(DIRPath, ContractId + ".png");

        if (!File.Exists(FilePath))
        {

            if (string.IsNullOrEmpty(ImgLink))
            {
                onComplete?.Invoke();
                yield break;
            }
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(ImgLink);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                //Debug.Log(www.error);
            }
            else
            {


                Texture2D tex = DownloadHandlerTexture.GetContent(www);

                if (!Directory.Exists(DIRPath))
                {

                    Directory.CreateDirectory(DIRPath);


                }

                byte[] Imgbytes = tex.EncodeToPNG();



                File.WriteAllBytes(FilePath, Imgbytes);


                Destroy(tex);
                www.Dispose();
                Imgbytes = new byte[0];
                System.GC.Collect();
            }
        }

        onComplete?.Invoke();
    }
}


