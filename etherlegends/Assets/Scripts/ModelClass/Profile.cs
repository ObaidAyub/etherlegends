﻿using System;
using System.Collections.Generic;

[Serializable]
public class GetProfileResponse : Response
{
    public ProfileData data;
}

[Serializable]
public class ProfileData
{
    public string _id;
    public string displayName;
    public string title;
    public string email;
    public string publicAddress;
    public string profile_pic;
    public string profile_pic_thumb;
    public int battles;
    public int wins;
    public int max_streak;
    public List<Card> best_card;
    public List<Card> companion_card;
}


[Serializable]
public class GetAllTokenBalanceResponse : Response
{
    public UserTokenData data;
}

[Serializable]
public class UserTokenData
{
    public double ether;
    public double elementeum;
    public double enjin;

}