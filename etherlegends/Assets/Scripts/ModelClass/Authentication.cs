﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

#region LOGIN
[Serializable]
public class Login
{
    public string email;
    public string password;

    public string user_token;
}

#region LoginResponse






[Serializable]
public class LoginResponse : Response
{
    public LoginData data;
}

[Serializable]
public class LoginData
{
    public string token;
    public LoginUserData data;
}

[Serializable]
public class LoginUserData
{
    public string _id;
    public string name;
    public string email;
}
#endregion LoginResponse

#endregion LOGIN

#region TransactionStore

[Serializable]
public class TransactionClass
{
    public string tx_hash;
    public string user_wallet_add;
    public string dest_wallet_add;
    public string amount;
    public string trans_type;
}


#endregion TransactionStore


#region REGISTER
[Serializable]
public class Register
{
    public string email;   
    public string displayName;
    public string token;
    public string user_token;
    public string password;
    //public string publicAddress;
}

[Serializable]
public class RegisterResponse : Response
{
    public Register data;
}

#endregion REGISTER




#region ChangePassword
[Serializable]
public class ChangePassword
{
    public string new_password;
    public string old_password;
}

[Serializable]
public class ChangePasswordResponse : Response
{

}

#endregion ChangePassword



#region QRCodeJson

[Serializable]
public class QRData
{
    public string setName;
    public string setAddress;
    public string cardName;
    public string redemptionCode;
}

[Serializable]
public class QRResponse : Response
{

}

#endregion QRCodeJson


[Serializable]
public class NewCreateImportWallet
{
    public string user_address;
}

[Serializable]
public class CheckforEmailWallet : NewCreateImportWallet
{
    public string email;
}

#region ConfirmWalletAddress

[Serializable]
public class WalletAddressConfirm
{
    public string user_address;
}

public class WalletAddressConfirmResponse : Response
{
    public string assigned;
}

#endregion ConfirmWalletAddress

#region ImportFromSeedPhraseConfirm
[Serializable]
public class ImportFromSeedPhraseConfirm
{
    public string mnemonic_word;
}

public class ImportFromSeedPhraseConfirmResponse : Response
{
    public string assigned;
}

#endregion ImportFromSeedPhraseConfirm

#region GetAllCardContractBalance

[Serializable]
    public class GetAllCardContractBalance
    {
        public string tokenid;
        
    }

    public class GetAllCardContractBalanceResponse: Response
    {
        public SpecialCardDetails data;
    }

    public class SpecialCardDetails //fetching details of how much cards they have
    {
        public List<CardId> MainCards = new List<CardId>();
    }

    public class CardId //fetching list of cards inside special edition cards
    {
       
        public List<GetAllCardDetails> CardDetail = new List<GetAllCardDetails>();
    }

    public class GetAllCardDetails //class for fetching card details from json structure
    {
        public string name;
        public string type;
        public string rarity;
        public int rarity_value;
        public string element_1;
        public string element_2;
        public string strength_1;
        public string strength_2;
        public string generation;
        public string number;
        public string elementeum;
        public string attack;
        public string defence;
        public string heal;
        public string disrupt;
        public string heart;
        public string value;
        public string card_contract_id;
        public string level; 
    }

#endregion GetAllCardContractBalance

#region UpdateCardAttribute

[Serializable]
public class UpdateCardDetail
{
    public string card_type;
    public string contract_id;
    public string update_level;
    public int attack;
    public int defence;
    public int heal;
    public int disrupt;
    public int heart;
    public string ep;
}

public class UpdateCardResponse : Response
{

}

#endregion UpdateCardAttribute



#region BestCards

[Serializable]
public class CardDetail
{
    public string card_set;
    public string card_contract_id;
    public int level;
    public string card_rarity;
    public string s3_image;


    public CardDetail(string Card_Set, string Card_contract_id, int Level, string Card_rarity, string S3_image) => (card_set, card_contract_id, level, card_rarity, s3_image) = (Card_Set, Card_contract_id, Level, Card_rarity, S3_image);
}
[Serializable]
public class CompanionCardDetail
{
    public string card_set;
    public string card_contract_id;
    public int level;
    public string card_rarity;
    public string s3_image;

    public CompanionCardDetail(string Card_Set, string Card_contract_id, int Level, string Card_rarity, string S3_image) => (card_set, card_contract_id, level, card_rarity, s3_image) = (Card_Set, Card_contract_id, Level, Card_rarity, S3_image);
}


[Serializable]
public class RootObject
{
    public List<CardDetail> card_details;
    public List<CompanionCardDetail> companionCardDetail;
}
#endregion



#region FetchedCards

[Serializable]
public class FetchedBestCard
{
    public string card_set;
    public string card_contract_id;
    public int level;
    public string card_rarity;
    public string card_attack;
    public string card_defence;
    public string card_heal;
    public string card_disrupt;
    public string card_heart;
    public string card_elementeum;
    public string card_name;
    public string card_generation;
    public string card_element_1;
    public string card_element_2;

    [JsonProperty(ItemConverterType = typeof(AttributeTypeEnumConverter), NullValueHandling = NullValueHandling.Ignore)]
    public AttributeType card_strength_1;

    [JsonProperty(ItemConverterType = typeof(AttributeTypeEnumConverter), NullValueHandling = NullValueHandling.Ignore)]
    public AttributeType card_strength_2;
    public string earn_elementeum;
    public string s3_image;

}

[Serializable]
public class BesttCardd
{
    public List<FetchedBestCard> best_cards;
}

[Serializable]
public class UserVal
{
    public bool inBattle;
    public bool bestThree;

    [JsonProperty("best_cards", NullValueHandling = NullValueHandling.Ignore)]
    public List<FetchedBestCard> best_cards;
    [JsonProperty("companionCardDetail", NullValueHandling = NullValueHandling.Ignore)]
    public List<FetchedBestCard> companionCardDetail;

    public bool isSearching;
    public string _id;
    public string user_id;
    public int __v;
    public string attribute_type;
    public string card_contract_id;
    public DateTime createdAt;
    public string decide_dice_number;
    public string dice_number;
    public int mmr_score;
    public string opposite_card_contract_id;
    public string opposite_user_id;
    public string profile_picture;
    public string socket_id;
    public string socket_room;
    public DateTime updatedAt;
    public string player_name;
}

[Serializable]
public class OppositeUserVal
{
    public string _id;
    public string user_id;
    public int __v;
    public string attribute_type;
    public bool bestThree;


    [JsonProperty("best_cards", NullValueHandling = NullValueHandling.Ignore)]
    public List<FetchedBestCard> best_cards;
    [JsonProperty("companionCardDetail", NullValueHandling = NullValueHandling.Ignore)]
    public List<FetchedBestCard> companionCardDetail;

    public string card_contract_id;
    public DateTime createdAt;
    public string decide_dice_number;
    public string dice_number;
    public bool inBattle;
    public bool isSearching;
    public int mmr_score;
    public string opposite_card_contract_id;
    public string opposite_user_id;
    public string profile_picture;
    public string socket_id;
    public string socket_room;
    public DateTime updatedAt;
    public string player_name;
}

[Serializable]
public class FetchedData
{
    public UserVal user_data;
    public OppositeUserVal opp_user_data;
}

[Serializable]
public class FetchedInfo
{
    public int status;
    public string message;
    public FetchedData data;
}
#endregion

#region oppositeAnimResponse

public class OppData
{
    public string player_one_card_contract_id;
    public string player_one_dice_number;
    public string player_one_attribute_type;
    public string opposite_card_contract_id;
    public bool elementeum;
    public int elementeum_number;
}


[Serializable]
public class OppDataResponse : Response
{
    public OppData data;
}

#endregion oppositeAnimResponse

#region GetTokenBalance

[Serializable]
public class GetTokenBal : Response
{
    public TokenData data;
}

[Serializable]
public class TokenData
{
    public string ether;
    public string elementeum;
    public string enjin;
    public string lp;
    public string arena;

    public int ep;
}

#endregion


[Serializable]
public class Response
{
    public int status;
    public string message;
}

[Serializable]
public class ChangePassResponse
{
    public string new_password;
    public string old_password;

}

public class ChangeDispName
{
    public string displayName;
    
}
