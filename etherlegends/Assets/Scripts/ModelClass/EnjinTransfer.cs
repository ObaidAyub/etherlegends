﻿using System.Collections;
using Nethereum.Contracts;
using Nethereum.JsonRpc.UnityClient;
using UnityEngine;
using Nethereum.Web3;
using TMPro;
using Nethereum.ABI.FunctionEncoding.Attributes;
using System.Numerics;
using Nethereum.ABI.Model;
using Nethereum.Util;
using System;
using System.Text;
using Nethereum.Web3.Accounts;
using Nethereum.Signer;
using Nethereum.ABI.FunctionEncoding;
using System.Threading.Tasks;

public class EnjinTransfer : MonoBehaviour
{
    //Deployment contract object definition


    [Function("transfer", "bool")]
    public class TransferFunctionBase : FunctionMessage
    {
        [Parameter("address", "_to", 1)]
        public string To { get; set; }
        [Parameter("uint256", "_value", 2)]
        public BigInteger Value { get; set; }
    }

    public partial class TransferFunction : TransferFunctionBase
    {

    }

    [Function("balanceOf", "uint256")]
    public class BalanceOfFunction : FunctionMessage
    {
        [Parameter("address", "_owner", 1)]
        public string Owner { get; set; }
        [Parameter("uint256", "_id", 1)]
        public string Id { get; set; }
    }


    [FunctionOutput]
    public class BalanceOfFunctionOutput : IFunctionOutputDTO
    {
        [Parameter("uint256", 1)]
        public int balances { get; set; }
    }

    [Function("setApprovalForAll")]
    public class SetApprovalForAll : FunctionMessage
    {
        [Parameter("address", "_operator", 1)]
        public string _operator { get; set; }
        [Parameter("bool", "_approved", 2)]
        public bool _approved { get; set; }
    }

    [Function("isApprovedForAll")]
    public class isApprovedForAll : FunctionMessage
    {
        [Parameter("address", "_owner", 1)]
        public string _owner { get; set; }
        [Parameter("address", "_operator", 2)]
        public string _operator { get; set; }
    }


    [FunctionOutput]
    public class SetApprovalForAllOutput : IFunctionOutputDTO
    {
        [Parameter("bool", 1)]
        public bool IsApproved { get; set; }
    }

    public partial class safeTransferFrom : safeTransferFrombase { }

    [Function("safeTransferFrom")]
    public class safeTransferFrombase : TransferFunction
    {
        [Parameter("address", "_from", 1)]
        public string from { get; set; }
        [Parameter("address", "_to", 2)]
        public string to { get; set; }
        [Parameter("uint256", "_id", 3)]
        public string Id { get; set; }
        [Parameter("uint256", "_value", 4)]
        public int value { get; set; }
        [Parameter("bytes", "_data", 5)]
        public Byte[] calldata { get; set; }

    }

    [Event("Transfer")]
    public class TransferEventDTOBase : IEventDTO
    {

        [Parameter("address", "_from", 1, true)]
        public virtual string From { get; set; }
        [Parameter("address", "_to", 2, true)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_value", 3, false)]
        public virtual BigInteger Value { get; set; }
    }

    public partial class TransferEventDTO : TransferEventDTOBase
    {
        public static EventABI GetEventABI()
        {
            return EventExtensions.GetEventABI<TransferEventDTO>();
        }
    }

    public string TokenAddress = "0x6c37Bf4f042712C978A73e3fd56D1F5738dD7C43";

    public TMP_InputField ToAddress;
    public TMP_InputField value;
    private string url = "";
    private string privateKey = "";
    private string account = "";
    private string newAddress = "";

    public void StartTransfer()
    {
        StartCoroutine(DeployAndTransferToken());
    }

    public void TestEnjin()
    {
        // StartCoroutine(Getbalance());
        StartCoroutine(SafeTransferFrom());

    }


    public IEnumerator Getbalance()
    {
        var url = ConstantManager._url_ETHMainNet;

        var account = "0x5B7415F8f61C29Fc7A01B7c67e4B5067ea26e3C9"; //"0xD77ABD0de441B9F81CdcE8663aD64bAaaD3d8FF2";

        var TokenId = "0x1800000000001289000000000000000000000000000000000000000000000000";

        var queryRequest = new QueryUnityRequest<BalanceOfFunction, BalanceOfFunctionOutput>(url, account);

        yield return queryRequest.Query(new BalanceOfFunction() { Owner = account, Id = TokenId }, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");

        //Getting the dto response already decoded
        if (queryRequest.Exception != null)
        {
            Debug.Log(queryRequest.Exception.Message);
        }

        var dtoResult = queryRequest.Result;

        Debug.Log("Result of BalanceOf--->" + dtoResult.balances);

        if (dtoResult.balances == 0)
        {
            yield break;
        }

    }


    public IEnumerator SafeTransferFrom()
    {

        string FromAddress = "0x5B7415F8f61C29Fc7A01B7c67e4B5067ea26e3C9";
        string ToAddress = "0x3c04EA3B7CAf12b6463f7923F1aC806EA7912364";
        string TokenId = "0x1800000000001289000000000000000000000000000000000000000000000000";
        int value = 5;
        var url = ConstantManager._url_ETHMainNet;

        /*   //This code is for address approval

        var ApprovalRequest = new TransactionSignedUnityRequest(url, "8D2F14C97D6B4AD21B1B4E25FF18606F5B8D3EC891C03C6D35F21DA7F70B2A55");

        var ApprovalMessage = new SetApprovalForAll
        {
            _operator= "0xfaaFDc07907ff5120a76b34b731b278c38d6043C",
            _approved=true,
        };
     
        yield return ApprovalRequest.SignAndSendTransaction<SetApprovalForAll>(ApprovalMessage, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");

        if (ApprovalRequest.Exception != null)
        {
            Debug.Log("Error Sending " + ApprovalRequest.Exception.Message.ToString());
        }

        var Result = ApprovalRequest.Result;

        Debug.Log("Hashhh--->" + Result);
        */

        var queryRequest = new QueryUnityRequest<isApprovedForAll, SetApprovalForAllOutput>(url, FromAddress);

        yield return queryRequest.Query(new isApprovedForAll() { _owner = FromAddress, _operator = "0xfaaFDc07907ff5120a76b34b731b278c38d6043C" }, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");

        //Getting the dto response already decoded
        if (queryRequest.Exception != null)
        {
            Debug.Log(queryRequest.Exception.Message);
        }

        var dtoResult = queryRequest.Result;

        Debug.Log("Result approval--->" + dtoResult.IsApproved);

        //Transfer Method
        var transactionTransferRequest = new TransactionSignedUnityRequest(url, "8D2F14C97D6B4AD21B1B4E25FF18606F5B8D3EC891C03C6D35F21DA7F70B2A55");

        transactionTransferRequest.EstimateGas = false;

        var Gas_Price = Web3.Convert.ToWei(8, UnitConversion.EthUnit.Gwei);

        var transactionMessage = new safeTransferFrom
        {
            // AmountToSend=value,
            // FromAddress=FromAddress,
            from = FromAddress,
            to = ToAddress,
            Id = TokenId,
            value = value,
            //            calldata = Encoding.UTF8.GetBytes("from:"+FromAddress),

        };

        transactionMessage.GasPrice = Gas_Price;
        transactionMessage.Gas = 4_300_00;

        yield return transactionTransferRequest.SignAndSendTransaction(transactionMessage, "0xfaaFDc07907ff5120a76b34b731b278c38d6043C");


        if (transactionTransferRequest.Exception != null)
        {
            Debug.Log("Error Sending " + transactionTransferRequest.Exception.Message.ToString());
        }

        var transactionTransferHash = transactionTransferRequest.Result;

        Debug.Log("Hashhh--->" + transactionTransferHash);

        var transactionReceiptPolling = new TransactionReceiptPollingRequest(url);
        yield return transactionReceiptPolling.PollForReceipt(transactionTransferHash, 2);

        var transferReceipt = transactionReceiptPolling.Result;

        var transferEvent = transferReceipt.DecodeAllEvents<TransferEventDTO>();

        if (transferEvent.Count > 0)
        {
            Debug.Log(transferEvent[0].Log.ToString());
            // OnSendComplete(transactionClass);
        }
        else
        {
            Debug.Log("Failed Transaction");
            Debug.Log(transferReceipt.Logs.ToString());
        }

        var getLogsRequest = new EthGetLogsUnityRequest(url);
        var eventTransfer = TransferEventDTO.GetEventABI();
        yield return getLogsRequest.SendRequest(eventTransfer.CreateFilterInput(transferReceipt.ContractAddress, FromAddress));

        var eventDecoded = getLogsRequest.Result.DecodeAllEvents<TransferEventDTO>();
        Debug.Log("Transferd amount from get logs event: " + eventDecoded[0].Event.Value);

    }

    //Sample of new features / requests
    public IEnumerator DeployAndTransferToken()
    {

        url = ConstantManager._url_ETHMainNet;
        newAddress = ToAddress.text;
        account = UserDataController.Instance.GetWalletAddress();
        privateKey = UserDataController.Instance.GetPrivateKey();

        var transactionTransferRequest = new TransactionSignedUnityRequest(url, privateKey);

        var transactionMessage = new TransferFunction
        {
            FromAddress = account,
            To = newAddress,
            Value = Web3.Convert.ToWei(value.text),
        };

        yield return transactionTransferRequest.SignAndSendTransaction(transactionMessage, TokenAddress);

        if (transactionTransferRequest.Exception != null)
        {
            Debug.Log("Error Sending " + transactionTransferRequest.Exception.Message.ToString());
        }

        var transactionTransferHash = transactionTransferRequest.Result;

        TransactionClass transactionClass = new TransactionClass();
        transactionClass.tx_hash = transactionTransferHash;
        transactionClass.user_wallet_add = UserDataController.Instance.Myprefs.Address;
        transactionClass.dest_wallet_add = ToAddress.text;
        transactionClass.amount = value.text;
        transactionClass.trans_type = "TransferERC20:ELEMENTEUM";

        Debug.Log("Transfer txn hash:" + transactionTransferHash);

        GetComponent<TransferERC20>().DoneTransfer(transactionTransferHash);

        var transactionReceiptPolling = new TransactionReceiptPollingRequest(url);
        yield return transactionReceiptPolling.PollForReceipt(transactionTransferHash, 2);
        var transferReceipt = transactionReceiptPolling.Result;

        var transferEvent = transferReceipt.DecodeAllEvents<TransferEventDTO>();

        if (transferEvent.Count > 0)
        {
            OnSendComplete(transactionClass);
        }

        var getLogsRequest = new EthGetLogsUnityRequest(url);
        var eventTransfer = TransferEventDTO.GetEventABI();
        yield return getLogsRequest.SendRequest(eventTransfer.CreateFilterInput(transferReceipt.ContractAddress, account));

        var eventDecoded = getLogsRequest.Result.DecodeAllEvents<TransferEventDTO>();
        Debug.Log("Transferd amount from get logs event: " + eventDecoded[0].Event.Value);
    }

    public async void OnSendComplete(TransactionClass transactionClass)
    {
        string Jsonbody = JsonUtility.ToJson(transactionClass);
        string Response = await SessionManager.Instance.Post(ConstantManager._url_StoreTransaction, Jsonbody, SessionManager.Instance.userToken, true);
        Debug.Log("Save Transaction-->" + Response);
        if (!string.IsNullOrEmpty(Response))
        {
        }
    }
}
