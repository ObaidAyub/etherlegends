﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Enjin;
using TMPro;
using UnityEngine.UI;
using BarcodeScanner;
using BarcodeScanner.Scanner;
using Wizcorp.Utils.Logger;
using System;
//using Enjin.SDK.DataTypes;
//using static Enjin.SDK.Core.Enjin;

public class TransferErc1155 : MonoBehaviour
{
    public TMP_InputField InpAddress, InpAmt1;

    public Text TxtEtBal, Col_Name, Col_Bal;
    public GameObject TransProgress;
    public Image ProgressBarr;
    private IScanner BarcodeScanner;
    public GameObject[] disableObj;

    #region Transfer Panel
    public GameObject TransferPanel;
    public Text TxHash;
    #endregion

    #region Barcode QRCode
    public RawImage Qrcode;
    public GameObject QRWindow;
    #endregion

    string Col_Id;
    bool isProg;

    private void OnEnable()
    {
        TransferPanel.SetActive(false);
        TxtEtBal.gameObject.SetActive(false);
        disableObj[0].SetActive(true);
        TransProgress.SetActive(false);

        InpAddress.text = "";
        InpAmt1.text = "";
    }

    public void setValues(string ItemId, string ItemName, string Balance)
    {
        Col_Id = ItemId;
        Col_Name.text = ItemName;
        Col_Bal.text = Balance;
    }


    public void TransferToken()
    {
        LoadingManager.Instance.ShowAuthentication(FinalTransfer);
    }

    public void FinalTransfer()
    {
        /*
       // User Player = UserDataController.Instance.LoginEnjin();

        if (InpAddress.text==""||InpAmt1.text=="")
        {
            Debug.Log("Please Enter Proper Value");
            return;
        }

        TransferPanel.SetActive(true);
        TxtEtBal.gameObject.SetActive(true);
        disableObj[0].SetActive(false);
        isProg = true;
        ProgressBarr.gameObject.SetActive(true);
        TransProgress.SetActive(true);

        StartCoroutine(Progress());

        SendCryptoItemRequest(Player.identities[0].id, Col_Id,int.Parse( InpAddress.text), int.Parse(InpAmt1.text), (requestEvent) =>
        {        
            if (requestEvent.event_type.Equals("txr_pending"))
            {
                Debug.Log("Pending--" + requestEvent.data);
            }
            else if (requestEvent.event_type.Equals("tx_broadcast"))
            {
                Debug.Log("Broadcast--" + requestEvent.data);
            }
            else if (requestEvent.event_type.Equals("balance_updated"))
            {
                Debug.Log("balanceUpdated--" + requestEvent.data);
            }
            else if (requestEvent.event_type.Equals("tx_executed"))
            {
                DoneTransfer(requestEvent.data.transaction_id.ToString());
                Debug.Log("Executed--" + requestEvent.data);
            }
        }, true);

        disableObj[0].SetActive(true);
        */
    }

    public void DoneTransfer(string Data)
    {
        TxtEtBal.text = "Transfer Complete";
        TxtEtBal.gameObject.SetActive(true);
        TxHash.gameObject.SetActive(true);
        TxHash.text = Data;

        StopCoroutine(Progress());
        isProg = false;
        TransProgress.gameObject.SetActive(false);

        InpAddress.text = "";
        InpAmt1.text = "";
    }

    IEnumerator Progress()
    {
        float i = 0;

        while (i <= 1f)
        {
            if (!isProg)
            {
                TransProgress.gameObject.SetActive(false);
                break;
            }
            yield return new WaitForSeconds(0.02f);
            i = i + 0.001f;
            ProgressBarr.fillAmount = i;
        }

        for (int j = 0; j < disableObj.Length; j++)
        {
            disableObj[j].SetActive(true);
        }
    }




    //--------------------------Scan Wallet Address QR---------------------------

    public void StartScan()
    {
        QRWindow.SetActive(true);
        BarcodeScanner = new Scanner();

        BarcodeScanner.Camera.Play();

        // Display the camera texture through a RawImage
        BarcodeScanner.OnReady += (sender, arg) => {
            // Set Orientation & Texture
            Qrcode.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
            Qrcode.transform.localScale = BarcodeScanner.Camera.GetScale();
            Qrcode.texture = BarcodeScanner.Camera.Texture;

            // Keep Image Aspect Ratio
            var rect = Qrcode.GetComponent<RectTransform>();
            var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);
        };

        // Track status of the scanner
        BarcodeScanner.StatusChanged += (sender, arg) => {
            Debug.Log("Status: " + BarcodeScanner.Status);

        };
        Invoke("ClickStart", 2);
    }


    private void Update()
    {
        if (BarcodeScanner == null)
        {
            return;
        }
        if (BarcodeScanner.QRValue == null)
        {
            BarcodeScanner.Update();

        }
    }

    #region 
    public void ClickStart()
    {


        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Start");
            return;
        }

        // Start Scanning
        BarcodeScanner.Scan((barCodeType, barCodeValue) => {
            BarcodeScanner.Stop();
            QRWindow.SetActive(false);
            // QRData QRCode = JsonUtility.FromJson<QRData>(barCodeValue);
            InpAddress.text = barCodeValue;

#if UNITY_ANDROID || UNITY_IOS
            Handheld.Vibrate();
#endif
        });
    }

    public void ClickStop()
    {
        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Stop");
            return;
        }

        // Stop Scanning
        BarcodeScanner.Stop();
    }


    /// <summary>
    /// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
    /// Trying to stop the camera in OnDestroy provoke random crash on Android
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    public IEnumerator StopCamera(Action callback)
    {
        // Stop Scanning

        BarcodeScanner.Destroy();
        BarcodeScanner = null;

        // Wait a bit
        yield return new WaitForSeconds(0.1f);

        callback.Invoke();
    }
    #endregion

}
