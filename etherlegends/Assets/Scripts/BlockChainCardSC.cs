﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using SimpleJSON;

public class BlockChainCardSC : MonoBehaviour
{

    [System.Serializable]
    public class BlockCards
    {
        public string CardContractID;
        public Texture2D CardTex;
    }

    public List<BlockCards> BlChainCards;

    string DIRpath = "";
    public static BlockChainCardSC Instance;
    int DownloadedCards = 0;
    int count = 0;

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }

        DIRpath = Path.Combine(Application.persistentDataPath , "ERC721Cards");        
        //Debug.Log("path-->"+ DIRpath);

    }
    void Start()
    {

        if (!Directory.Exists(DIRpath))
        {
            Directory.CreateDirectory(DIRpath);
        }
        else
        {
            string[] files = Directory.GetFiles(DIRpath, "*.png");

            for (int i = 0; i < files.Length; i++)
            {

                string FilePath = Path.Combine(DIRpath, files[i]);

                byte[] ImgBytes;

                ImgBytes = File.ReadAllBytes(FilePath);

                Texture2D CardTexture = new Texture2D(2, 2);

                CardTexture.LoadImage(ImgBytes);

                BlockCards TempBCard = new BlockCards();

                TempBCard.CardContractID = Path.GetFileNameWithoutExtension(files[i]);
                TempBCard.CardTex = CardTexture;

                if (!BlChainCards.Contains(TempBCard))
                {
                    BlChainCards.Add(TempBCard);
                }
            }

        }
        GetAllBlockChainCardImages();
    }




    public void SaveCard(string ContId,string ImgLink)
    {

        if (GetCardTexture(ContId) == null)
        {
            StartCoroutine(CardImage(ImgLink, ContId));
        }
        else
        {
            count -= 1;
            if (count<=1)
            {
                EnableObjectDelay.Instance.StartOtherObject();
            }            
        }

    }

    public void SaveCard(string ContId,Texture2D ImgText,bool IsGif)
    {
        
        string FilePath = Path.Combine(DIRpath, ContId + (IsGif? ".gif" : ".png"));
        
        byte[] ImgBytes = ImgText.EncodeToPNG();

        File.WriteAllBytes(FilePath, ImgBytes);

        BlockCards TempBCard = new BlockCards();

        TempBCard.CardContractID = ContId;

        TempBCard.CardTex = ImgText;

        if (!BlChainCards.Contains(TempBCard))
        {
            BlChainCards.Add(TempBCard);
        }

    }
   

    public Texture2D GetCardTexture(string CardID)
    {
        for (int i = 0; i < BlChainCards.Count; i++)
        {
            if (BlChainCards[i].CardContractID == CardID)
            {
                return BlChainCards[i].CardTex;
            }
        }
        return null;
    }

    void GetAllBlockChainCardImages()
    {

        var Jnode = JSON.Parse(EnableObjectDelay.Instance.Response);

        // //Debug.Log(" Get All Card Image ---" + response);
        count = Jnode["data"].Count;

        //Debug.Log("BlockChain Cards--->" + count);

        for (int i = 0; i < Jnode["data"].Count; i++)
        {
            SaveCard(Jnode["data"][i]["card_contract_id"], Jnode["data"][i]["card_image"]);
        }

        //EnableObjectDelay.Instance.DownloadBlockChainCards(response);
    }

    IEnumerator CardImage(string Imglink, string CardID)
    {   

        yield return new WaitForSeconds(0.1f);

        //Debug.Log("Img Link----" + Imglink);

        string FilePath = Path.Combine(DIRpath, CardID + ".png");

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(Imglink);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.Log(www.error);
        }

        else
        {

            Texture2D CardTexture = DownloadHandlerTexture.GetContent(www);

            byte[] ImgBytes = CardTexture.EncodeToPNG();

            File.WriteAllBytes(FilePath, ImgBytes);

            BlockCards TempBCard = new BlockCards();

            TempBCard.CardContractID = CardID;
            TempBCard.CardTex = CardTexture;

            if (!BlChainCards.Contains(TempBCard))
            {
                BlChainCards.Add(TempBCard);
            }

        }
        count -= 1;

        if (count<=1)
        {
           EnableObjectDelay.Instance.StartOtherObject();
            //Debug.Log("Downloaded Cards");

        }
    }

    // Start is called before the first frame update
    
 


}
