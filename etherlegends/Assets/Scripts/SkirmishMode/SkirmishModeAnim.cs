﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(AudioSource))]
public class SkirmishModeAnim : MonoBehaviour
{
    public static SkirmishModeAnim instance;
    public Transform upperClouds, lowerClouds;
    public ParticleSystem energyfield, FireBlast;
    public GameObject canvas;
    public TextMeshProUGUI text;
    public TextMeshProUGUI leftName;
    public TextMeshProUGUI rightName;

    public CameraShake cameraShake;
    public SpriteRenderer Bg;
   


    public AudioClip[] otherClip;

    public bool start;

    public bool inSkirmish;
    public int _inSkirmish;
    private void Start()
    {
        instance = this;
        start = true;
        text.text = "Skirmish";
        inSkirmish = false;
        _inSkirmish = 0;
        leftName.gameObject.SetActive(false);
        rightName.gameObject.SetActive(false);
    }
    public void StartSkirmishMode()
    {
        var PlayerCard = PlayerManager.instance.SelectedCard;
        var EnemyCard = PlayerManager.instance.SelectedEnemyCard;
        if (start)
        {
            for (int i = 0; i < 3; i++)
            {
                PlayerManager.instance.PlayerCardsObj[i].SetAllPowerOff();
                PlayerManager.instance.PlayerCardsObj[i].resetTrigger();
                PlayerManager.instance.enemyCardsObj[i].SetAllPowerOff();
                //PlayerManager.instance.eneMyCardsObj[i].resetTrigger();
            }
            for (int i = 0; i < 4; i++)
            {
                PlayerCard.powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = true;
                EnemyCard.powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = true;

                PlayerCard.powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Image>().raycastTarget = false;
                EnemyCard.powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Image>().raycastTarget = false;


            }

            if (PlayerCard.isSpActive)
            {
                PlayerCard.Cur_Elemt = 0;
                PlayerCard.RemoveElementeum(PlayerCard.CardDetail.elementeum);
                PlayerCard.RevertPlayerSpecialAbilityAnimation();
                for (int i = 0; i < PlayerManager.instance.elementeumParticle.Length; i++)
                {
                    PlayerManager.instance.elementeumParticle[i].gameObject.SetActive(false);
                    PlayerManager.instance.elementeumParticle[i].StartPosition = Vector3.zero;
                }
            }
            else
            {
                PlayerCard.RemoveElementeum(PlayerCard.UsingElementeum);
                for (int i = 0; i < PlayerManager.instance.elementeumParticle.Length; i++)
                {
                    PlayerManager.instance.elementeumParticle[i].gameObject.SetActive(false);
                    PlayerManager.instance.elementeumParticle[i].StartPosition = Vector3.zero;
                }
            }



            if (EnemyCard.isSpActive)
            {
                EnemyCard.Cur_Elemt = 0;
                EnemyCard.RemoveElementeum(EnemyCard.CardDetail.elementeum);
                EnemyCard.RevertEnemySpAbiAnim();
                
                foreach (DigitalRuby.LightningBolt.LightningBoltScript p in PlayerManager.instance.elementeumParticleEnemy)
                {
                    p.gameObject.SetActive(false);
                    p.StartPosition = Vector3.zero;
                }
            }
            else
            {
                EnemyCard.RemoveElementeum(EnemyCard.UsingElementeum);


                foreach (DigitalRuby.LightningBolt.LightningBoltScript p in PlayerManager.instance.elementeumParticleEnemy)
                {
                    p.gameObject.SetActive(false);
                    p.StartPosition = Vector3.zero;
                }
            }
            PlayerCard.HideSpecialAbility();
            EnemyCard.HideSpecialAbility();


            for (int i = 0; i < PlayerManager.instance.CompanionCards.playerCompanionCards.Length; i++)
            {
                PlayerManager.instance.CompanionCards.AutoDeselectCompanionCardsForBattle(PlayerManager.instance.CompanionCards.playerCompanionCards[i]);

            }
            for (int i = 0; i < PlayerManager.instance.CompanionCards.enemyCompanionCards.Length; i++)
            {
                PlayerManager.instance.CompanionCards.AutoDeselectEnemyCompanionCardsForBattle(PlayerManager.instance.CompanionCards.enemyCompanionCards[i]);

            }
            for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
            {

                PlayerManager.instance.enemyCardsObj[i].companionData.Clear();
                PlayerManager.instance.enemyCardsObj[i].companionIDs.Clear();
            }





            SelectPowerManager.instance.selectedCardAnimator.SetBool("isAttack", true);
            SelectPowerManager.instance.EnemyselectedCardAnimator.SetBool("isAttack", true);

            InGameCards.instance.SetMyAttribImage(AttributeType.Attack);
            InGameCards.instance.SetOppAttribImage(AttributeType.Attack);

            Bg.gameObject.SetActive(true);
            cameraShake.ShakeCamera(3f, 4);
            FireBlast.gameObject.SetActive(true);
            FireBlast.Play();
            StartCoroutine(AnimTime());
            start = false;
            inSkirmish = true;
            _inSkirmish = 1;
            InGameCards.instance.revertAllAttributeVisualsValue();
            PlayerCard.HealthAnimatedsHeart();
            EnemyCard.HealthAnimatedsHeart();

            leftName.gameObject.SetActive(true);
            rightName.gameObject.SetActive(true);

        }

    }

    public void EndSkirmishMode()
    {
        if (!start)
        {

            //Debug.Log("End Skirmish");
            Bg.gameObject.SetActive(false);
            FireBlast.gameObject.SetActive(false);
            FireBlast.Stop();
            StartCoroutine(ReverseAnimTime());
            start = true;
            inSkirmish = false;
            _inSkirmish = 0;

            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.transform.DOScale(0, 0.5f);
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.transform.DOScale(0, 0.5f);

            leftName.gameObject.SetActive(false);
            rightName.gameObject.SetActive(false);
            for (int i = 0; i < 4; i++)
            {

                PlayerManager.instance.SelectedCard.powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Image>().raycastTarget = true;
                PlayerManager.instance.SelectedEnemyCard.powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Image>().raycastTarget = true;

            }
            DiceManager.instance.DisableDice();
            DiceManager.instance.OppDisableDice();
            
        }
    }
    // Update is called once per frame
    IEnumerator AnimTime()
    {
        //Debug.Log("Start Skirmirsh Anim");

        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = otherClip[0];
        audio.Play();

        yield return new WaitForSeconds(audio.clip.length);
        audio.clip = otherClip[1];
        audio.Play();

        yield return new WaitForSeconds(1);
        energyfield.gameObject.SetActive(true);
        energyfield.Play();

        yield return new WaitForSeconds(1);
        upperClouds.DOMove(new Vector3(0f, 8.5f, 13.0f), 2);
        lowerClouds.DOMove(new Vector3(0f, -1.19f, 13.0f), 2);

        yield return new WaitForSeconds(1);
        canvas.SetActive(true);

        yield return new WaitForSeconds(0.5f);
        text.transform.DOScale(new Vector3(1f, 1f, 1f), 1);
        
    }
    IEnumerator ReverseAnimTime()
    {
        //Debug.Log("Reverse Skirmirsh Anim");
        AudioSource audio = GetComponent<AudioSource>();

        text.transform.DOScale(new Vector3(0f, 0f, 0f), 1);
       

        energyfield.Stop();
        energyfield.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        upperClouds.DOMove(new Vector3(0f, 15.9f, 13.0f), 2);
        lowerClouds.DOMove(new Vector3(0f, -6f, 13.0f), 2);

        
        audio.clip = otherClip[1];
        audio.Stop();



        canvas.SetActive(false);
    }

}
