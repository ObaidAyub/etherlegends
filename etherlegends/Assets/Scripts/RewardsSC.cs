﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
//using Enjin.SDK.DataTypes;
//using static Enjin.SDK.Core.Enjin;
//using Enjin.SDK.Core;

public class RewardsSC : MonoBehaviour
{

    [System.Serializable]
    public struct RewardItem
    {
        public string ItemName;
        public string ItemID;      
        public int ItemValue;
    }

    // Enjin SDK operation settings.
    public string PLATFORM_URL;
    public string DEVELOPER_USERNAME;
    public string DEVELOPER_PASSWORD;
    public string DEVELOPER_AccessToken;
    public int DEVELOPER_IDENTITY_ID;
    public int APP_ID;
    public List<RewardItem> SupplyRewards = new List<RewardItem>();
    public Queue<string> ConfirmedItems;

    // Variables for controlling the game state.
    private List<System.Action> pendingActions;
    private int score;
    private string linkingCode;
    private int identityId;
    private string userAddress;
   // private User user;
    private int count;
    private int pending;
    private string tokenName;

    // Unity scene objects.
    private Text status;
    private Text tutorial;
    private GameObject authenticationPanel;
    private GameObject registrationPanel;
    private InputField registrationEmail;
    private GameObject proceedToLoginPanel;
    private GameObject loginPanel;
    private InputField loginEmail;
    private InputField loginPassword;
    private GameObject gamePanel;
    private Text inventory;
    private Image rewardTokenImage;
    private GameObject rewardMask;
    private Image rewardMaskImage;
    private List<GameObject> panelList;

    public bool startMint;

    private Queue<string> confirmedItems;


    private void Start()
    {
     //   StartEnjin();
    }

    /*public void StartEnjin()
    {
        // Start the Enjin SDK.
        User Admin = StartPlatform(PLATFORM_URL, DEVELOPER_USERNAME, DEVELOPER_PASSWORD, APP_ID);

        DEVELOPER_AccessToken = AccessToken;

        Debug.Log("<color=aqua>[Simple Game]</color> Using app with ID " + AppID);

        // Retrieve the specified reward token's metadata.
        CryptoItem rewardToken = GetCryptoItem("7000000000000797");                       

        StartCoroutine(rewardToken.GetMetadata((metadataInfo) =>
        {

            // Handle any potential errors in metadata retrieval.
            MetadataInfo.MetadataRequestState requestState = metadataInfo.state;
            switch (requestState)
            {
                case MetadataInfo.MetadataRequestState.PARSE_FAILED:
                    Debug.Log("Unable to parse the reward item's metadata.");
                    break;
                case MetadataInfo.MetadataRequestState.RETRIEVAL_FAILED:
                    Debug.Log("Unable to retrieve the reward item's metadata.");
                    break;
                case MetadataInfo.MetadataRequestState.SUCCESS:
                    {
                        tokenName = metadataInfo.metadata.name;
                        Debug.Log("Name_"+rewardToken.name);

                        Debug.Log("Balance " + rewardToken.balance);
                        StartCoroutine(rewardToken.GetImage((imageInfo) =>
                        {

                            // Handle any potential errors with retrieving the item image.
                            ImageInfo.ImageRequestState imageRequestState = imageInfo.state;
                            switch (imageRequestState)
                            {
                                case ImageInfo.ImageRequestState.BAD_METADATA:
                                    Debug.Log("Unable to handle item metadata for the image.");
                                    break;
                                case ImageInfo.ImageRequestState.RETRIEVAL_FAILED:
                                    Debug.Log("Unable to retrieve the reward item's image.");
                                    break;
                                case ImageInfo.ImageRequestState.SUCCESS:
                                    Debug.Log("image."+imageInfo.image);
                                    break;
                            }
                        }));
                        break;
                    }
            }
        }));


        User Player = UserDataController.Instance.LoginEnjin();
        Debug.Log("userAddress --" + Player.identities[0].wallet.ethAddress);
        Dictionary<string, int> totalinv = new Dictionary<string, int>();
        Dictionary<int, List<CryptoItem>> PlayerInv = GetCryptoItemsByAddress(Player.identities[0].wallet.ethAddress);

        foreach (int appid in PlayerInv.Keys)
        {
            List<CryptoItem> appinventory = PlayerInv[appid];
            foreach (CryptoItem item in appinventory)
            {
                int amount = item.balance;

                totalinv.Add(item.token_id, amount);

                Debug.Log(item.token_id + "  " + amount);
            }

        }
        userAddress = Player.identities[0].wallet.ethAddress;
        // StartCoroutine(MintItem());

     //   StartCoroutine(CreatePlayer());
    }

    public IEnumerator MintItem()
    {
        yield return new WaitForSeconds(2f);
       MintFungibleItem(DEVELOPER_IDENTITY_ID, new string[] { userAddress }, SupplyRewards[0].ItemID, 1,
        (requestdata)=>
        {
            Debug.Log("Doneeee");
            confirmedItems.Enqueue(SupplyRewards[0].ItemName);
        },true);

    }


    public IEnumerator CreatePlayer()
    {
        yield return new WaitForSeconds(2f);
        // User Player = Enjin.CreateUser("JeetNew2", "jeetnew2@gmail.com", "abcd123", "Player");
        User Player = Login("jeetnew2@gmail.com", "abcd123");

       // Debug.Log("Player:" + Player);
        //Player.identities[0].ethereum_address = "0x4e19317cc0eCa95E0CF1d3dC70E1Dd0B2F1Cdcdf";
       
       // Debug.Log(Player.identities.Length);

        var Id = Player.identities[0];

       
       // Debug.Log(Player.identities[0].id);
       
       // Id.ethereum_address = "0x4e19317cc0eCa95E0CF1d3dC70E1Dd0B2F1Cdcdf";
        //Debug.Log(Player.id);
       // Id.id = Player.identities[0].ethereum_address;
        Id.user.id = Player.id;
        // Debug.Log( Enjin.LinkIdentity(Id));
        CryptoItem item = GetCryptoItem(SupplyRewards[0].ItemID);

       Debug.Log(GetCryptoItemBalance(Player.identities[0].id, SupplyRewards[0].ItemID));

       

        Debug.Log("Enjin Bal"+  GetEnjBalance);
        Debug.Log("Eth Bal" +   GetEthBalance);
        //Debug.Log(Enjin.GetEnjBalance(Player.identities[0].id));

        //Debug.Log(Player.identities[0].ethereum_address);
        //Debug.Log("---------------------------");
        //Debug.Log("Player Id+"+Player.id);
        //Debug.Log("Player Name+" + Player.name);

        // userAddress = "0xE01C2B871eCe80001a306a004d48425C9595c787";


        SendCryptoItemRequest(Player.identities[0].id, SupplyRewards[0].ItemID, 4401, 1, (requestEvent) =>
         {


             if (requestEvent.event_type.Equals("txr_pending"))
             {
                 Debug.Log("Pending--"+requestEvent.data);
             }
             else if(requestEvent.event_type.Equals("tx_broadcast"))
             {
                 Debug.Log("Broadcast--" + requestEvent.data);
             }
            else if(requestEvent.event_type.Equals("balance_updated"))
             {
                 Debug.Log("balanceUpdated--" + requestEvent.data);
             }
            else if (requestEvent.event_type.Equals("tx_executed"))
             {
                 pending -= 1;
                 count += 1;
                 pendingActions.Add(() =>
                 {
                     Debug.Log(tokenName + "\nYou have: " + count + "\nCurrently pending: " + pending);
                 });
             }

         }, true);

       // startMint = true;
      /*  Enjin.MintFungibleItem(DEVELOPER_IDENTITY_ID, new string[] { userAddress }, SupplyRewards[0].ItemID, 1,
                   (requestEvent) =>
                   {
                         Debug.Log("contt"+requestEvent.contract);
                        if (requestEvent.event_type.Equals("txr_executed"))
                       {
                           pending -= 1;
                           count += 1;
                           pendingActions.Add(() =>
                           {
                               Debug.Log(tokenName + "\nYou have: " + count + "\nCurrently pending: " + pending);
                           });
                       }
                   });
                 

        // Debug.Log("Item Name+" + item.name + " Quantity: "+developerBalance);

    }

    public IEnumerator TransferEP()
    {
        yield return new WaitForSeconds(5f);

        /*  CryptoItem[] rewardToken = Enjin.GetAllCryptoItems();

          JSONNode data = JSON.Parse(Enjin.GetQueryResults);

          for (int i = 0; i < data["data"]["CryptoItems"].Count; i++)
          {
              string tokenID= data["data"]["CryptoItems"][i]["token_id"];
              CryptoItem Item = Enjin.GetCryptoItem(tokenID);
          }
          
        //  User Player =  Enjin.CreateUser("user1", "user1@u.com", "abcd123", "Player");
         User Player = Login("user1@u.com", "abcd1234");
         User[] usr= GetAllUsers();
         Debug.Log("Access TOken:"+Player.access_token);

        for (int i = 0; i < usr.Length; i++)
        {
            print(usr[i].name);
        }
        userAddress = "0x90cf6Df8D11Ff71AA185CeA7D2863BC9d1133197";
      // startMint = true;       
    }

    private void Update()
    {






        if (startMint)
        {           
          
                // Mint a new token directly from the developer wallet.
              //  CryptoItem item = Enjin.GetCryptoItem(SupplyRewards[0].ItemID);

               // string reserveCount = item.reserve;
                //Debug.Log("Reserve Count " + reserveCount);



                int developerBalance = GetCryptoItemBalance(DEVELOPER_IDENTITY_ID, SupplyRewards[0].ItemID);
                Debug.Log("Developer  " + developerBalance);

                if (!developerBalance.Equals(0))
                {
                    pending += 1;
                    Debug.Log(userAddress);
                   
                    MintFungibleItem(DEVELOPER_IDENTITY_ID, new string[] { userAddress }, SupplyRewards[0].ItemID, 1,
                    (requestEvent) =>
                    {
                       // Debug.Log("contt"+requestEvent.contract);
                        if (requestEvent.event_type.Equals("tx_executed"))
                        {
                            pending -= 1;
                            count += 1;
                            pendingActions.Add(() =>
                            {
                                Debug.Log(tokenName + "\nYou have: " + count + "\nCurrently pending: " + pending);
                            });
                        }
                    });
                Debug.Log(tokenName + "\nYou have: " + count + "\nCurrently pending: " + pending);
                }
                            
                // Otherwise there really is nothing of this token left for the developer to give out.
                else
                {
                    Debug.Log("Uh oh! The game developer is out of reward items!");
                }
            startMint = false;
        }
    }
*/
}
