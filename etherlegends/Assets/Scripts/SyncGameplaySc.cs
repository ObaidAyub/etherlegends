﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGameFree;

public class SyncGameplaySc : MonoBehaviour
{

    [System.Serializable]
    public class UserDataClass
    {
        public string CardClick {get; set;} 
        public string AttributeClick { get; set; }
        public string OpponentClick { get; set; }
        public string RollDice { get; set; }
    }

    [System.Serializable]
    public class TurnDataClass
    {
        //public UserDataClass MyData;
        public string MyCardClick;
        public string MyAttributeClick;
        public string MyOpponentClick;
        public string MyRollDice;

        //public UserDataClass OppData;
        public string OppCardClick;
        public string OppAttributeClick;
        public string OppOpponentClick;
        public string OppRollDice;
    }

    [System.Serializable]
    public class PlayerDataClass
    {
        public string PlayerName { get; set; }
        public string PlayerId { get; set; }
        public string SocketId { get; set; }
        public Card[] CardData;
        public Card[] TempCardData;
    }

    [System.Serializable]
    public class GameDataClass
    {
        public string SocketRoomId { get; set; }
        public string MainTurn { get; set; }
        public string Turn { get; set; }
        public string LastAction { get; set; }
        public int timer { get; set; }
        public string GameState { get; set; }
        public PlayerDataClass MyData;        
        public PlayerDataClass OppData;   
        public TurnDataClass[] TurnData;        
    }

    public GameDataClass GameData;
   
    public GameDataClass ReceivedData;

    public GameObject SyncPanel;

    string identifier = "err.dat";

    string JsonString = "";

    public static SyncGameplaySc Instance;


    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    private void Start()
    {
        string _encodePassword = "b8C54UQZEWbcZ7KLEgmFVHyuJGmeXGRBdf98eRMr";
        SaveGame.EncodePassword = _encodePassword;
        SaveGame.Encode = true;
        SaveGame.Serializer = new BayatGames.SaveGameFree.Serializers.SaveGameJsonSerializer();

        if (SaveGame.Exists(identifier))
        {
            Invoke("Load", 1);
        }

        else
        {
            Invoke("Save", 1);
        }

        StartCoroutine(TestSaveGame());

    }


    IEnumerator TestSaveGame()
    {
        yield return new WaitForSeconds(10f);
        Debug.Log("---------Initialize turn------");
        InitializeTurn();
        yield return new WaitForSeconds(2f);
        Debug.Log("---------increase size------");
        IncreaseDataSize();
        yield return new WaitForSeconds(2f);
        Debug.Log("---------Call Method------");
        SelectCard("nothinadf", true);
        Debug.Log("---------After Call Method------");

    }


    public void GenerateJson()
    {
        JsonString = JsonUtility.ToJson(GameData);
        Debug.Log(JsonString);
    }

    public void DeleteData()
    {
        SaveGame.Delete(identifier);
    }

    public void InitializeData(string SocketRoomId, string MePlayerId, string MePlayerName, string MeSocketId, string OppPlayerId, Card[] Mycards, Card[] Oppcards)
    {
        GameData.SocketRoomId = BattleLobbySC.Instance.Finaldata.user_data.socket_room;

        GameData.GameState = "running";

        Debug.Log("-->" + GameData.MyData.PlayerId);

        Debug.Log(BattleLobbySC.Instance.Finaldata.user_data.user_id);

        GameData.MyData.PlayerId = BattleLobbySC.Instance.Finaldata.user_data.user_id;

        GameData.MyData.PlayerName = BattleLobbySC.Instance.Finaldata.user_data.player_name;

        GameData.MyData.SocketId = BattleLobbySC.Instance.Finaldata.user_data.socket_id;

        GameData.OppData.PlayerId = BattleLobbySC.Instance.Finaldata.opp_user_data.user_id;

        GameData.OppData.PlayerName = BattleLobbySC.Instance.Finaldata.opp_user_data.player_name;

        GameData.OppData.SocketId = BattleLobbySC.Instance.Finaldata.opp_user_data.socket_id;

        GameData.MyData.CardData = new Card[3];

        GameData.MyData.TempCardData = new Card[3];

        GameData.OppData.CardData = new Card[3];

        GameData.OppData.TempCardData = new Card[3];


        for (int i = 0; i < 3; i++)
        {

            GameData.MyData.CardData[i] = Mycards[i];
            GameData.MyData.TempCardData[i] = Mycards[i];

        }

        for (int i = 0; i < 3; i++)
        {

            GameData.OppData.CardData[i] = Oppcards[i];
            GameData.OppData.TempCardData[i] = Oppcards[i];

        }

        Save();
    }



    public void SelectCard(string contId,bool myturn)
    {
        Debug.Log("Select Card from game sync");
        Debug.Log("Game Data Socket Id-->" + GameData.SocketRoomId);
        if (GameData.TurnData.Length == 0)
            InitializeTurn();



        if (myturn)
        {

            Debug.Log("------>Turn Data--->"+ GameData.TurnData.Length);
           // Debug.Log("Turn Data--->" + GameData.TurnData[0].MyData.ToString());

            // GameData.TurnData[GameData.TurnData.Length - 1].MyData.CardClick = contId;
            GameData.TurnData[GameData.TurnData.Length - 1].MyCardClick = contId;
            Save();
        }

        else
        {
            //GameData.TurnData[GameData.TurnData.Length - 1].OppData.CardClick = contId;
            GameData.TurnData[GameData.TurnData.Length - 1].OppCardClick = contId;
            Save();
        }
     
    }

    public void SelectAttrib(string Attrib, bool myturn)
    {
        if (myturn)
        {
            //GameData.TurnData[GameData.TurnData.Length - 1].MyData.AttributeClick = Attrib;
            Save();
        }
        else
        {
            //GameData.TurnData[GameData.TurnData.Length - 1].OppData.AttributeClick = Attrib;
            Save();
        }
    }

    public void SelectOpponentcard(string contId, bool myturn)
    {
        if (myturn)
        {
           // GameData.TurnData[GameData.TurnData.Length - 1].MyData.OpponentClick = contId;
            Save();
        }
        else
        {
          //  GameData.TurnData[GameData.TurnData.Length - 1].OppData.OpponentClick = contId;
            Save();
        }

    }

    public void DiceNumber(string DiceNo, bool myturn)
    {
        if (myturn)
        {
            //GameData.TurnData[GameData.TurnData.Length - 1].MyData.RollDice = DiceNo;
            Save();
        }
        else
        {
           // GameData.TurnData[GameData.TurnData.Length - 1].OppData.RollDice = DiceNo;
            Save();
        }

    }






    public void InitializeTurn()
    {
        Debug.Log("Initialize Turn--->");
        GameData.TurnData = new TurnDataClass[1];      

        Debug.Log("Length--->" + GameData.TurnData.Length);
    }

    public void IncreaseDataSize()
    {
        try
        {
            Debug.Log("INcrease Length--->" );

            TurnDataClass[] Tdata = new TurnDataClass[GameData.TurnData.Length + 1];
            GameData.TurnData.CopyTo(Tdata, 0);
            GameData.TurnData = Tdata;
            Save();
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex);
        }      
    }

    public IEnumerator SyncCurrentGame(string Json)
    {
        SyncPanel.SetActive(true);
        Json = JsonString;

        ReceivedData = JsonUtility.FromJson<GameDataClass>(Json);

        if(ReceivedData.TurnData.Length == GameData.TurnData.Length) //check if player device data matches with opponent data
        {

            int LastInd = ReceivedData.TurnData.Length - 1;

            if (ReceivedData.MainTurn.Equals("me") && GameData.MainTurn.Equals("opp")) //check if both players Mainturn matches or not
            {

                if (ReceivedData.Turn.Equals("me") && GameData.Turn.Equals("opp"))
                {

                    if (ReceivedData.LastAction != GameData.LastAction)
                    {

                        string[] PlayerAction = ReceivedData.LastAction.Split(':');

                        if (PlayerAction[0] == "me")
                        {

                            if (PlayerAction[1] == "mecard" || PlayerAction[1] == "mehealcard")
                            {
                              //  if (string.IsNullOrEmpty(GameData.TurnData[LastInd].OppData.CardClick))
                               // {
                               //     InGameCards.instance.EnemyAnimUpdate("card", PlayerAction[2]);
                               // }
                            }

                            else if (PlayerAction[1] == "meattribute")
                            {
                               // if (string.IsNullOrEmpty(GameData.TurnData[LastInd].OppData.AttributeClick))
                               // {
                                //    InGameCards.instance.EnemyAnimUpdate("attri", PlayerAction[2]);
                                //}
                            }

                            else if (PlayerAction[1] == "oppcard")
                            {
                               // if (string.IsNullOrEmpty(GameData.TurnData[LastInd].OppData.OpponentClick))
                                //{
                                //    InGameCards.instance.EnemyAnimUpdate("oppcard", PlayerAction[2]);
                                //}
                            }

                            else if (PlayerAction[1] == "meroll")
                            {
                               // if (string.IsNullOrEmpty(GameData.TurnData[LastInd].OppData.OpponentClick))
                               // {
                                //    InGameCards.instance.EnemyAnimUpdate("onediceroll", PlayerAction[2]);
                                //}
                            }

                            else if (PlayerAction[1] == "clickonbg")
                            {
                                PlayerManager.instance.ClickOnBG();
                            }

                        }
                    }

                }

                else if (ReceivedData.Turn.Equals(GameData.Turn))//recieveddata's "me" means "opp" turn and gamedata's "me" means "my" turn
                {
                    Debug.LogError("TODO: ChangeTurn");
                    //InGameCards.instance.ChangeTurn();
                }
                // InGameCards.instance.MainChangeTurn();
            }

            else if (ReceivedData.MainTurn.Equals("opp") && GameData.MainTurn.Equals("me")) //check if both players Mainturn matches or not
            {
                if (ReceivedData.Turn.Equals("me") && GameData.Turn.Equals("opp"))
                {

                    if (ReceivedData.LastAction != GameData.LastAction)
                    {

                        string[] PlayerAction = ReceivedData.LastAction.Split(':');

                        if (PlayerAction[0] == "me")
                        {

                            //if (PlayerAction[1] == "medisruptcard")
                            //{
                            //    if (string.IsNullOrEmpty(GameData.TurnData[LastInd].OppData.CardClick))
                            //    {
                            //        InGameCards.instance.EnemyAnimUpdate("card", PlayerAction[2]);
                            //    }
                            //}

                            //else if (PlayerAction[1] == "meattribute")
                            //{
                            //    if (string.IsNullOrEmpty(GameData.TurnData[LastInd].OppData.AttributeClick))
                            //    {
                            //        InGameCards.instance.EnemyAnimUpdate("PlayerTwoAttrib", PlayerAction[2]);
                            //    }
                            //}                        

                            //else if (PlayerAction[1] == "meroll")
                            //{
                            //    if (string.IsNullOrEmpty(GameData.TurnData[LastInd].OppData.OpponentClick))
                            //    {
                            //        InGameCards.instance.EnemyAnimUpdate("PlayerTwoDice", PlayerAction[2]);
                            //    }
                            //}

                            //else if (PlayerAction[1] == "clickonbg")
                            //{
                            //    PlayerManager.instance.ClickOnBG();
                            //}
                        }
                    }
                }
            }
            else if (ReceivedData.MainTurn.Equals(GameData.MainTurn))
            {
                //InGameCards.instance.MainChangeTurn();
            }
        }        

        yield return new WaitForSecondsRealtime(4f);

        SyncPanel.SetActive(false);

    }

    public void SendSyncJson()
    {
            
    }

    public void Save()
    {
        SaveGame.Save<GameDataClass>(identifier, GameData, SaveGame.Serializer);       
    }

    public void Load()
    {     

        GameData = SaveGame.Load<GameDataClass>(identifier,GameData, SaveGame.Serializer);

        if (GameData.GameState == "running")
        {

        }

        else
        {
            // DeleteData();
           // GameData = new GameDataClass();
          //  Save();


            //SyncGame();
        }
      
    }

}
