﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButtonSc : MonoBehaviour
{

    public enum Operation { Back, LoadLevel,Quit };

    public GameObject ObjToDisable, ObjToEnable;
    public string LvlToLoad;

    public Operation BackOperation;
    public GameObject BackBtn;

    void OnEnable()
    {
        if(BackBtn)
        BackBtn.SetActive(false);    
    }

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            switch (BackOperation)
            {
                case Operation.Back:
                    BattleLobbySC.Instance.OffSocket("afterStartPlay");
                    ObjToDisable.SetActive(false);                   
                    ObjToEnable.SetActive(true);
                    break;

                case Operation.LoadLevel:
                    SceneManager.LoadScene(LvlToLoad);
                    break;

                case Operation.Quit:

                    LoadingManager.Instance.btn_yes.onClick.RemoveAllListeners();

                    LoadingManager.Instance.btn_yes.onClick.AddListener(() => Application.Quit());

                    LoadingManager.Instance.ShowPopUp(" EXIT " + Environment.NewLine +  " Are You Sure ? ");

                    break;
                default:
                    break;
            }

        }
    }

    public void Back()
    {
        BattleLobbySC.Instance.OffSocket("afterStartPlay");
        ObjToDisable.SetActive(false);
        ObjToEnable.SetActive(true);
    }
}
