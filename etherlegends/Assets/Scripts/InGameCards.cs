﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using SimpleJSON;
using System.Linq;
using System.Collections.Generic;

public class InGameCards : MonoBehaviour
{

    public static InGameCards instance;

    public enum stats { playerone, playertwo }

    //public GameObject[] PlayerManager.instance.PlayerCardsObj;
    //public GameObject[] playerCompanionCards;
    //public GameObject[] PlayerManager.instance.enemyCardsObj;
    //public GameObject[] PlayerCompanionCardsAnim.instance.enemyCompanionCards;

    public BattleLobbySC BattleLobby;
    public GameObject DIcepanel, dice, TurnPanel;
    public TextMeshProUGUI headtext, youtxt, opptext, youdice, oppdice, resulttext, timertxt, TurnText;
    public TextMeshPro NumOfPower;
    public Text MyTimer, OppTimer;
    public GameObject BGPanel;
    public SpriteRenderer myprofile, oppProfile;
    public GameObject MyAttribImage, OppAttribImage;
    public TextMeshProUGUI MyAttribValue, OppAttribValue;
    public Sprite AttackImg, DefenseImg, DisruptImg, HealImg;

    public GameObject SyncPanel;

    public int MeCounter;
    public int Oppcounter;

    int DNum = 0;
    int DNum2 = 0;
    int selCard = 0;
    private static string SavedTime;
    //set value for players win or lost
    public stats PlayerStats;
    public stats Turn, MainTurn;

    //public string Message;
    //public EtherSocketPackets.DiceRollPacket receviedPacket;
    //string SData;

    Queue<KeyValuePair<string, object>> MessageQueue = new Queue<KeyValuePair<string, object>>();


    System.Action OnComplete = null;


    public int MeElemt, OppElement;

    bool PauseTimer;

    public GameObject MeElementeum, OppELementeum;

    public BoxCollider2D StartDiceBtn;

    public GameObject OpponentLostPanel;

    public TextMeshProUGUI TimerText;


    [Header("Player Final Value")]
    public GameObject attributeImage;
    public TextMeshProUGUI Elemtium;
    public TextMeshProUGUI Dice;
    public TextMeshProUGUI Attribute;
    public TextMeshProUGUI companion;
    public int elem, _dice, attri, comp;

    [Header("Enemy Final Value")]
    public GameObject enemyAttributeImage;
    public TextMeshProUGUI enemyElemtium;
    public TextMeshProUGUI enemyDice;
    public TextMeshProUGUI enemeyAttribute;
    public TextMeshProUGUI enemyCompanion;
    public int enemeyElem, enemeyDice, enemeyAttri, enemyComp;

    //public PlayerCompanionCardsAnim playerCompanionCardsAnim;



    [Header("Total Particle Player")]
    public ParticleSystem atkParticleMe;
    public ParticleSystem defParticleMe;
    public ParticleSystem healParticleMe;
    public ParticleSystem disParticleMe;

    [Header("Total Particle Opponent")]
    public ParticleSystem atkParticleOpp;
    public ParticleSystem defParticleOpp;
    public ParticleSystem healParticleOpp;
    public ParticleSystem disParticleOpp;

    public AudioClip drumEffect;

    public GameObject elementiumText;

    

    public void SetMyAttribImage(AttributeType Attrib)
    {

        if (Attrib == AttributeType.None)
        {
            MyAttribValue.text = "0";
            MyAttribImage.SetActive(false);
            return;
        }

        MyAttribImage.gameObject.SetActive(true);
        MyAttribValue.gameObject.SetActive(true);

        GameObject textComponent = null;
        int oldValue = 0;

        switch (Attrib)
        {
            case AttributeType.Attack:
                if (PlayerManager.instance.getSpOpp())
                    PlayerManager.instance.SelectedCard.CardDetail.attack = 0;

                MyAttribImage.GetComponent<Image>().sprite = AttackImg;
                attributeImage.GetComponent<Image>().sprite = AttackImg;

                textComponent = PlayerManager.instance.SelectedCard.txt_Attack;
                oldValue = PlayerManager.instance.SelectedCard.CardDetail.attack;

                break;

            case AttributeType.Defense:
                if (PlayerManager.instance.getSpOpp())
                    PlayerManager.instance.SelectedCard.CardDetail.defence = 0;

                MyAttribImage.GetComponent<Image>().sprite = DefenseImg;
                attributeImage.GetComponent<Image>().sprite = DefenseImg;

                textComponent = PlayerManager.instance.SelectedCard.txt_Defense;
                oldValue = PlayerManager.instance.SelectedCard.CardDetail.defence;

                break;
            case AttributeType.Heal:
                if (PlayerManager.instance.getSpOpp())
                    PlayerManager.instance.SelectedCard.CardDetail.heal = 0;

                MyAttribImage.GetComponent<Image>().sprite = HealImg;
                attributeImage.GetComponent<Image>().sprite = HealImg;

                textComponent = PlayerManager.instance.SelectedCard.txt_Heal;
                oldValue = PlayerManager.instance.SelectedCard.CardDetail.heal;

                break;

            case AttributeType.Disrupt:
                if (PlayerManager.instance.getSpOpp())
                    PlayerManager.instance.SelectedCard.CardDetail.disrupt = 0;

                MyAttribImage.GetComponent<Image>().sprite = DisruptImg;
                attributeImage.GetComponent<Image>().sprite = DisruptImg;

                textComponent = PlayerManager.instance.SelectedCard.txt_Disrupt;
                oldValue = PlayerManager.instance.SelectedCard.CardDetail.disrupt;

                break;

        }


        if (textComponent)
        {
            if (PlayerManager.instance.getSpOpp())
            {
                Debug.LogError("Enemy use special power ");
                MyAttribValue.text = "0";
                textComponent.GetComponent<TextMeshProUGUI>().text = "0";
                PlayerManager.instance.SelectedCard.CardDetail.attack = 0;

            }
            else if (PlayerManager.instance.getSpOpp() && PlayerManager.instance.getSpme())
            {
                Debug.LogError("both player use special power ");
                MyAttribValue.text = PlayerManager.instance.SelectedCard.UsingElementeum.ToString();
                textComponent.GetComponent<TextMeshProUGUI>().text = "0";
            }
            else
            {
                if (PlayerManager.instance.PrevRes == "tie")
                {
                    MyAttribValue.text = (oldValue + PlayerManager.instance.SelectedCard.UsingElementeum).ToString();
                    PlayerManager.instance.SelectedCard.TieReset();
                }
                else
                {
                    MyAttribValue.text = oldValue.ToString();
                    Attribute.text = oldValue.ToString();
                }
            }
        }

        atkParticleMe.gameObject.SetActive(Attrib == AttributeType.Attack);
        disParticleMe.gameObject.SetActive(Attrib == AttributeType.Disrupt);
        defParticleMe.gameObject.SetActive(Attrib == AttributeType.Defense);
        healParticleMe.gameObject.SetActive(Attrib == AttributeType.Heal);


        //Debug.Log("After Attribute selection value is " + MyAttribValue.text);
        //Debug.Log("After Attribute selection value is " + Attribute.text);
    }

    public void MeTieRefreshAttrib(int attribval, int elemt)
    {
        Debug.Log("METie Ref--" + attribval + " elemt " + elemt);
        var playerCard = PlayerManager.instance.SelectedCard;
        if (PlayerManager.instance.getSpOpp())
        {
            MyAttribValue.text = elemt.ToString();
            switch (SelectPowerManager.instance.selectedPowerName)
            {


                case AttributeType.Attack:
                    playerCard.txt_Attack.GetComponent<TextMeshProUGUI>().text = "0";
                    break;

                case AttributeType.Defense:
                    playerCard.txt_Defense.GetComponent<TextMeshProUGUI>().text = "0";
                    break;

                case AttributeType.Heal:
                    playerCard.txt_Heal.GetComponent<TextMeshProUGUI>().text = "0";
                    break;

                case AttributeType.Disrupt:
                    playerCard.txt_Disrupt.GetComponent<TextMeshProUGUI>().text = "0";
                    break;
            }
            Attribute.text = "0";
            Elemtium.text = elemt.ToString();

        }
        else
        {

            MyAttribValue.text = (attribval + elemt).ToString();
            Debug.Log("My attrib Val" + MyAttribValue.text);

            switch (SelectPowerManager.instance.selectedPowerName)
            {


                case AttributeType.Attack:
                    playerCard.txt_Attack.GetComponent<TextMeshProUGUI>().text = attribval.ToString();
                    break;

                case AttributeType.Defense:
                    playerCard.txt_Defense.GetComponent<TextMeshProUGUI>().text = attribval.ToString();



                    break;

                case AttributeType.Heal:
                    playerCard.txt_Heal.GetComponent<TextMeshProUGUI>().text = attribval.ToString();


                    break;

                case AttributeType.Disrupt:
                    playerCard.txt_Disrupt.GetComponent<TextMeshProUGUI>().text = attribval.ToString();


                    break;
            }
            Attribute.text = attribval.ToString();
            Elemtium.text = elemt.ToString();

        }
        DiceManager.instance.playerDiceNum = 0;
        DiceManager.instance.enemyDiceNum = 0;

    }

    public void OppTieRefreshAttrib(int attribval2, int elemt2)
    {
        Debug.Log("OppTie Ref--" + attribval2 + " elemt " + elemt2);
        var EnemyCard = PlayerManager.instance.SelectedEnemyCard;
        if (PlayerManager.instance.getSpme())
        {
            OppAttribValue.text = elemt2.ToString();
            switch (SelectPowerManager.instance.selectedEnemyPowerName)
            {
                case AttributeType.Attack:
                    EnemyCard.txt_Attack.GetComponent<TextMeshProUGUI>().text = "0";
                    break;

                case AttributeType.Defense:
                    EnemyCard.txt_Defense.GetComponent<TextMeshProUGUI>().text = "0";

                    break;

                case AttributeType.Heal:
                    EnemyCard.txt_Heal.GetComponent<TextMeshProUGUI>().text = "0";



                    break;

                case AttributeType.Disrupt:
                    EnemyCard.txt_Disrupt.GetComponent<TextMeshProUGUI>().text = "0";



                    break;
            }
            enemeyAttribute.text = "0";
            enemyElemtium.text = elemt2.ToString();
        }
        else
        {
            OppAttribValue.text = (attribval2 + elemt2).ToString();


            switch (SelectPowerManager.instance.selectedEnemyPowerName)
            {
                case AttributeType.Attack:
                    EnemyCard.txt_Attack.GetComponent<TextMeshProUGUI>().text = attribval2.ToString();

                    break;

                case AttributeType.Defense:
                    EnemyCard.txt_Defense.GetComponent<TextMeshProUGUI>().text = attribval2.ToString();


                    break;

                case AttributeType.Heal:
                    EnemyCard.txt_Heal.GetComponent<TextMeshProUGUI>().text = attribval2.ToString();


                    break;

                case AttributeType.Disrupt:
                    EnemyCard.txt_Disrupt.GetComponent<TextMeshProUGUI>().text = attribval2.ToString();


                    break;
            }

            enemeyAttribute.text = attribval2.ToString();
            enemyElemtium.text = elemt2.ToString();
        }
        DiceManager.instance.playerDiceNum = 0;
        DiceManager.instance.enemyDiceNum = 0;
    }

    public void SetOppAttribImage(AttributeType Attrib)
    {

        if (Attrib == AttributeType.None)
        {
            OppAttribValue.text = "";
            OppAttribImage.SetActive(false);
            return;
        }
        OppAttribImage.gameObject.SetActive(true);
        OppAttribValue.gameObject.SetActive(true);
        var EnemeyPlayer = PlayerManager.instance.SelectedEnemyCard;

        switch (Attrib)
        {
            case AttributeType.Attack:

                OppAttribImage.GetComponent<Image>().sprite = AttackImg;
                enemyAttributeImage.GetComponent<Image>().sprite = AttackImg;

                if (PlayerManager.instance.getSpme())
                {
                    EnemeyPlayer.CardDetail.attack = 0;
                    OppAttribValue.text = EnemeyPlayer.TotalAttack().ToString();
                    enemeyAttribute.text = EnemeyPlayer.CardDetail.attack.ToString();

                }
                else if (PlayerManager.instance.getSpOpp() && PlayerManager.instance.getSpme())
                {
                    EnemeyPlayer.CardDetail.attack = 0;
                    OppAttribValue.text = EnemeyPlayer.TotalAttack().ToString();
                    enemeyAttribute.text = EnemeyPlayer.CardDetail.attack.ToString();
                }
                else
                {
                    if (PlayerManager.instance.PrevRes == "tie")
                    {
                        OppAttribValue.text = EnemeyPlayer.TotalAttack().ToString();
                        EnemeyPlayer.TieReset();
                    }
                    else
                    {
                        OppAttribValue.text = EnemeyPlayer.TotalAttack().ToString();
                        if (enemeyAttribute)
                            enemeyAttribute.text = EnemeyPlayer.CardDetail.attack.ToString();
                    }
                }
                break;

            case AttributeType.Defense:

                OppAttribImage.GetComponent<Image>().sprite = DefenseImg;
                enemyAttributeImage.GetComponent<Image>().sprite = DefenseImg;
                if (PlayerManager.instance.getSpme())
                {
                    EnemeyPlayer.CardDetail.defence = 0;
                    OppAttribValue.text = EnemeyPlayer.TotalDefence().ToString();
                    enemeyAttribute.text = EnemeyPlayer.CardDetail.defence.ToString();

                }
                else if (PlayerManager.instance.getSpOpp() && PlayerManager.instance.getSpme())
                {
                    EnemeyPlayer.CardDetail.defence = 0;
                    OppAttribValue.text = EnemeyPlayer.TotalDefence().ToString();
                    enemeyAttribute.text = EnemeyPlayer.CardDetail.defence.ToString();
                }
                else
                {
                    if (PlayerManager.instance.PrevRes == "tie")
                    {
                        OppAttribValue.text = EnemeyPlayer.TotalDefence().ToString();
                        EnemeyPlayer.TieReset();
                    }
                    else
                    {
                        OppAttribValue.text = EnemeyPlayer.TotalDefence().ToString();
                        if (enemeyAttribute)
                            enemeyAttribute.text = EnemeyPlayer.CardDetail.defence.ToString();
                    }
                }
                break;

            case AttributeType.Heal:

                OppAttribImage.GetComponent<Image>().sprite = HealImg;
                enemyAttributeImage.GetComponent<Image>().sprite = HealImg;
                if (PlayerManager.instance.getSpme())
                {
                    EnemeyPlayer.CardDetail.heal = 0;
                    OppAttribValue.text = EnemeyPlayer.TotalHeal().ToString();
                    enemeyAttribute.text = EnemeyPlayer.CardDetail.heal.ToString();

                }
                else if (PlayerManager.instance.getSpOpp() && PlayerManager.instance.getSpme())
                {
                    EnemeyPlayer.CardDetail.heal = 0;
                    OppAttribValue.text = EnemeyPlayer.TotalHeal().ToString();
                    enemeyAttribute.text = EnemeyPlayer.CardDetail.heal.ToString();
                }
                else
                {
                    if (PlayerManager.instance.PrevRes == "tie")
                    {
                        OppAttribValue.text = EnemeyPlayer.TotalHeal().ToString();
                        EnemeyPlayer.TieReset();
                    }
                    else
                    {
                        OppAttribValue.text = EnemeyPlayer.TotalHeal().ToString();
                        if (enemeyAttribute)
                            enemeyAttribute.text = EnemeyPlayer.CardDetail.heal.ToString();
                    }
                }
                break;

            case AttributeType.Disrupt:

                OppAttribImage.GetComponent<Image>().sprite = DisruptImg;
                enemyAttributeImage.GetComponent<Image>().sprite = DisruptImg;
                if (PlayerManager.instance.getSpme())
                {
                    EnemeyPlayer.CardDetail.disrupt = 0;
                    OppAttribValue.text = EnemeyPlayer.TotalDisrupt().ToString();
                    enemeyAttribute.text = EnemeyPlayer.CardDetail.disrupt.ToString();

                }
                else if (PlayerManager.instance.getSpOpp() && PlayerManager.instance.getSpme())
                {
                    EnemeyPlayer.CardDetail.disrupt = 0;
                    OppAttribValue.text = EnemeyPlayer.TotalDisrupt().ToString();
                    enemeyAttribute.text = EnemeyPlayer.CardDetail.disrupt.ToString();
                }
                else
                {
                    if (PlayerManager.instance.PrevRes == "tie")
                    {
                        OppAttribValue.text = EnemeyPlayer.TotalDisrupt().ToString();
                        EnemeyPlayer.TieReset();
                    }
                    else
                    {
                        OppAttribValue.text = EnemeyPlayer.TotalDisrupt().ToString();
                        if (enemeyAttribute)
                            enemeyAttribute.text = EnemeyPlayer.CardDetail.disrupt.ToString();
                    }
                }
                break;

            
        }
        if (PlayerManager.instance.SelectedEnemyCard != null)
        {
            if (EnemeyPlayer.isSpActive)
            {
                AddOrRemoveOppElementeum();//Add Elementeum from here 
            }
            else if (EnemeyPlayer.UsingElementeum > 0)
            {

                AddOrRemoveOppElementeum();//Add Elementeum from here 
            }
        }
        else
        {
            var objCollection = PlayerManager.instance.enemyCardsObj.Where(x => x.GetComponent<CardObject>().DestroyedCard == false);
            if (objCollection.Any())
            {
                if (objCollection.First().GetComponent<CardObject>().isSpActive)
                {
                    AddOrRemoveOppElementeum();//Add Elementeum from here 
                }
                else if (objCollection.First().GetComponent<CardObject>().UsingElementeum > 0)
                {

                    AddOrRemoveOppElementeum();//Add Elementeum from here 
                }
            }
        }
    }

    //-------------------------------------

    public IEnumerator animateElementium(GameObject Parent)
    {
        GameObject TxtObj = Instantiate(elementiumText, Parent.gameObject.transform);
        TxtObj.SetActive(true);
        TxtObj.GetComponent<TextMeshProUGUI>().text = "1";
        TxtObj.transform.DOScale(new Vector3(1, 1, 1), 1f);
        yield return new WaitForSeconds(1);

        TxtObj.transform.DORotate(new Vector3(270, 0, 0), 0.5f);
        TxtObj.transform.DOLocalMove(new Vector3(10f, -4f, 0), 1f);
        yield return new WaitForSeconds(0.5f);

        TxtObj.transform.DOScale(Vector3.one, 0.5f);
        yield return new WaitForSeconds(0.5f);


        Destroy(TxtObj);
    }

    public void AddOrRemovePlayerElementeum()
    {
        var PlayerObj = PlayerManager.instance.SelectedCard;

        if (PlayerObj != null)
        {

            Elemtium.text = PlayerObj.UsingElementeum.ToString();
        }
        switch (SelectPowerManager.instance.selectedPowerName)
        {
            case AttributeType.Attack:
                MyAttribValue.text = PlayerObj.TotalAttack().ToString();
                break;
            case AttributeType.Heal:
                MyAttribValue.text = PlayerObj.TotalHeal().ToString();
                break;
            case AttributeType.Defense:
                MyAttribValue.text = PlayerObj.TotalDefence().ToString();
                break;
            case AttributeType.Disrupt:
                MyAttribValue.text = PlayerObj.TotalDisrupt().ToString();
                break;
        }

    }

    

    public void AddOrRemoveOppElementeum() // in this funtion if opp use tha sp ability the appEleemnt value is double
    {
        var EnemyObj = PlayerManager.instance.SelectedEnemyCard;
        if (EnemyObj != null)
        {
            enemyElemtium.text = (EnemyObj.isSpActive ? EnemyObj.CardDetail.elementeum : EnemyObj.UsingElementeum).ToString();//OppElement.ToString();
        }
        switch (SelectPowerManager.instance.selectedEnemyPowerName)
        {
            case AttributeType.Attack:
                OppAttribValue.text = EnemyObj.TotalAttack().ToString();
                break;
            case AttributeType.Heal:
                OppAttribValue.text = EnemyObj.TotalHeal().ToString();
                break;
            case AttributeType.Defense:
                OppAttribValue.text = EnemyObj.TotalDefence().ToString();
                break;
            case AttributeType.Disrupt:
                OppAttribValue.text = EnemyObj.TotalDisrupt().ToString();
                break;
        }

    }


    public void RemoveMyAttribValue()
    {
        var PlayerCardObj = PlayerManager.instance.SelectedCard;
        switch (SelectPowerManager.instance.selectedPowerName)
        {
            case AttributeType.Attack:
                MyAttribValue.text = PlayerCardObj.TotalAttack().ToString();
                break;
            case AttributeType.Defense:
                MyAttribValue.text = PlayerCardObj.TotalDefence().ToString();
                break;
            case AttributeType.Heal:
                MyAttribValue.text = PlayerCardObj.TotalHeal().ToString();
                break;
            case AttributeType.Disrupt:
                MyAttribValue.text = PlayerCardObj.TotalHeal().ToString();
                break;
        }
    }

    public void RemoveMyAttribValue(int val)  //  this function is not use in the game 
    {
        var PlayerCardObj = PlayerManager.instance.SelectedCard;

        int CurVal = 0;
        if (val != 0 && !string.IsNullOrEmpty(MyAttribValue.text))
        {
            CurVal = int.Parse(MyAttribValue.text);
        }

        if (CurVal > 0)
            CurVal -= val;

        MyAttribValue.text = CurVal.ToString();

    }

    public void RefreshMyAttribValue(int val)//when opp use special ability 
    {
        Debug.LogError("when opp use special ability ");
        var PlayerCardObj = PlayerManager.instance.SelectedCard;
        switch (SelectPowerManager.instance.selectedPowerName)
        {
            case AttributeType.Attack:
                MyAttribValue.text = (PlayerManager.instance.getSpOpp() ? "0" : (PlayerCardObj.TotalAttack().ToString()));
                break;

            case AttributeType.Defense:
                MyAttribValue.text = (PlayerManager.instance.getSpOpp() ? "0" : (PlayerCardObj.TotalDefence().ToString()));
                break;

            case AttributeType.Heal:
                MyAttribValue.text = (PlayerManager.instance.getSpOpp() ? "0" : (PlayerCardObj.TotalHeal().ToString()));
                break;

            case AttributeType.Disrupt:
                MyAttribValue.text = (PlayerManager.instance.getSpOpp() ? "0" : (PlayerCardObj.TotalDisrupt().ToString()));
                break;

            default:
                MyAttribImage.gameObject.SetActive(false);
                break;
        }
    }

    public void RefreshMyAttribValue() // When Both Player Use Special Ability
    {
        Debug.LogError("when Both player use special ability ");
        var PlayerCardObj = PlayerManager.instance.SelectedCard;
        switch (SelectPowerManager.instance.selectedPowerName)
        {
            case AttributeType.Attack:
                MyAttribValue.text = (PlayerCardObj.TotalAttack() - PlayerCardObj.CardDetail.attack).ToString();
                break;

            case AttributeType.Defense:
                MyAttribValue.text = PlayerCardObj.TotalDefence().ToString();
                break;

            case AttributeType.Heal:
                MyAttribValue.text = (PlayerCardObj.TotalHeal() - PlayerCardObj.CardDetail.heal).ToString();
                break;

            case AttributeType.Disrupt:
                MyAttribValue.text = PlayerCardObj.TotalDisrupt().ToString();
                break;

            default:
                MyAttribImage.gameObject.SetActive(false);
                break;
        }
        Attribute.text = "0";
    }

    public void RefreshOppAttribValue()
    {
        Debug.LogError(" player use special ability so opp attribute is refreshed ");

        var EnemyCardObj = PlayerManager.instance.SelectedEnemyCard;
        switch (SelectPowerManager.instance.selectedEnemyPowerName)
        {
            case AttributeType.Attack:
                OppAttribValue.text = (PlayerManager.instance.getSpOpp() ? "0" : (EnemyCardObj.TotalAttack()).ToString());
                break;

            case AttributeType.Defense:
                OppAttribValue.text = (PlayerManager.instance.getSpOpp() ? "0" : (EnemyCardObj.TotalDefence()).ToString());
                break;

            case AttributeType.Heal:
                OppAttribValue.text = (PlayerManager.instance.getSpOpp() ? "0" : (EnemyCardObj.TotalHeal()).ToString());
                break;

            case AttributeType.Disrupt:
                OppAttribValue.text = (PlayerManager.instance.getSpOpp() ? "0" : (EnemyCardObj.TotalDisrupt()).ToString());
                break;

            default:
                MyAttribImage.gameObject.SetActive(false);
                break;
        }

    }

    public void RefreshOppAttribValue(bool isplus)
    {
        Debug.LogError("Both player use special ability so opp attribute is refreshed ");
        var EnemyCardObj = PlayerManager.instance.SelectedEnemyCard;

        switch (SelectPowerManager.instance.selectedEnemyPowerName)
        {
            case AttributeType.Attack:

                OppAttribValue.text = isplus ? EnemyCardObj.TotalAttack().ToString() : (EnemyCardObj.TotalAttack() - EnemyCardObj.CardDetail.attack).ToString(); // dice value should be added in this 
                enemeyAttribute.text = isplus ? EnemyCardObj.CardDetail.attack.ToString() : "0";
                break;

            case AttributeType.Defense:

                OppAttribValue.text = EnemyCardObj.CardDetail.elementeum.ToString();
                enemeyAttribute.text = 0.ToString();
                break;

            case AttributeType.Heal:

                OppAttribValue.text = isplus ? EnemyCardObj.TotalHeal().ToString() : (EnemyCardObj.TotalHeal() - EnemyCardObj.CardDetail.heal).ToString();
                enemeyAttribute.text = isplus ? EnemyCardObj.CardDetail.heal.ToString() : "0";
                break;

            case AttributeType.Disrupt:

                OppAttribValue.text = EnemyCardObj.CardDetail.elementeum.ToString();
                enemeyAttribute.text = 0.ToString();
                break;

            default:
                MyAttribImage.gameObject.SetActive(false);
                break;
        }

    }



    public void RemoveOppAttribValue(bool isSpAbility)
    {
        var EnemyCardObj = PlayerManager.instance.SelectedEnemyCard;
       
        switch (SelectPowerManager.instance.selectedEnemyPowerName)
        {
            case AttributeType.Attack:
                OppAttribValue.text = isSpAbility ? (EnemyCardObj.TotalAttack() - EnemyCardObj.CardDetail.attack).ToString() : EnemyCardObj.TotalAttack().ToString();
                EnemyCardObj.txt_Attack.GetComponent<TextMeshProUGUI>().SetText(isSpAbility ? "0" : EnemyCardObj.CardDetail.attack.ToString());
                enemeyAttribute.SetText(isSpAbility ? "0" : EnemyCardObj.CardDetail.attack.ToString());
                break;

            case AttributeType.Defense:
                OppAttribValue.text = isSpAbility ? (EnemyCardObj.TotalDefence() - EnemyCardObj.CardDetail.defence).ToString() : EnemyCardObj.TotalDefence().ToString();
                EnemyCardObj.txt_Defense.GetComponent<TextMeshProUGUI>().SetText(isSpAbility ? "0" : EnemyCardObj.CardDetail.defence.ToString());
                enemeyAttribute.SetText(isSpAbility ? "0" : EnemyCardObj.CardDetail.defence.ToString());
                break;

            case AttributeType.Heal:
                OppAttribValue.text = isSpAbility ? (EnemyCardObj.TotalHeal() - EnemyCardObj.CardDetail.heal).ToString() : EnemyCardObj.TotalHeal().ToString();
                EnemyCardObj.txt_Heal.GetComponent<TextMeshProUGUI>().SetText(isSpAbility ? "0" : EnemyCardObj.CardDetail.heal.ToString());
                enemeyAttribute.SetText(isSpAbility ? "0" : EnemyCardObj.CardDetail.heal.ToString());

                break;

            case AttributeType.Disrupt:
                OppAttribValue.text = isSpAbility ? (EnemyCardObj.TotalDisrupt() - EnemyCardObj.CardDetail.disrupt).ToString() : EnemyCardObj.TotalDisrupt().ToString();
                EnemyCardObj.txt_Disrupt.GetComponent<TextMeshProUGUI>().SetText(isSpAbility ? "0" : EnemyCardObj.CardDetail.disrupt.ToString());
                enemeyAttribute.SetText(isSpAbility ? "0" : EnemyCardObj.CardDetail.disrupt.ToString());
                break;
        }

        

    }

    public void RemoveOppElemnteumValue(int val)
    {

        var EnemyCardObj = PlayerManager.instance.SelectedEnemyCard;
        switch (SelectPowerManager.instance.selectedPowerName)
        {
            case AttributeType.Attack:
                OppAttribValue.text = (EnemyCardObj.TotalAttack() - val).ToString();
                break;
            case AttributeType.Heal:
                OppAttribValue.text = (EnemyCardObj.TotalHeal() - val).ToString();
                break;
            case AttributeType.Defense:
                OppAttribValue.text = (EnemyCardObj.TotalDefence() - val).ToString();
                break;
            case AttributeType.Disrupt:
                OppAttribValue.text = (EnemyCardObj.TotalDisrupt() - val).ToString();
                break;
        }

    }

    void Start()
    {
        if (PlayerStats == stats.playerone)
        {
            Turn = stats.playerone;
        }
        else
        {
            Turn = stats.playertwo;

        }

        Debug.Log(PTurn());
    }

    private void Awake()
    {
        Application.runInBackground = true;
        Application.targetFrameRate = 60;

        if (instance == null)
        {
            instance = this;
        }

        dice.SetActive(false);

        timertxt.gameObject.SetActive(true);
        headtext.gameObject.SetActive(true);
        opptext.gameObject.SetActive(false);
        youtxt.gameObject.SetActive(false);
        youdice.gameObject.SetActive(false);
        oppdice.gameObject.SetActive(false);
        resulttext.gameObject.SetActive(false);

        if (GameObject.Find("SocketSession"))
        {


            BattleLobby = GameObject.Find("SocketSession").GetComponent<BattleLobbySC>();

            BattleLobbySC.Instance.UserQuitResponse();
            BattleLobbySC.Instance.UserLostFocusResponse();

            if (BattleLobbySC.Instance.Mytext != null)
            {
                myprofile.sprite = Sprite.Create(BattleLobby.Mytext, new Rect(0, 0, BattleLobby.Mytext.width, BattleLobby.Mytext.height), new Vector2(0.5f, 0.5f));
            }

            if (BattleLobbySC.Instance.opText != null)
            {
                oppProfile.sprite = Sprite.Create(BattleLobby.opText, new Rect(0, 0, BattleLobby.opText.width, BattleLobby.opText.height), new Vector2(0.5f, 0.5f));
            }


            myprofile.size = new Vector2(1, 1);
            oppProfile.size = new Vector2(1, 1);
        }



        DIcepanel.SetActive(true);

        PlayerManager.instance.CanClick = false;

        headtext.text = "Wait For Match Start";


        StartCoroutine(Countdown(3));
        StartCoroutine(DelaySync());

    }

    IEnumerator DelaySync()
    {

        yield return new WaitForSeconds(5f);

        try
        {
            Card[] Mycards = new Card[3];
            Card[] Oppcards = new Card[3];

            for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
            {
                Mycards[i] = UserDataController.Instance.profileData.best_card[i];
                Oppcards[i] = PlayerManager.instance.enemyCardsObj[i].GetComponent<CardObject>().CardDetail;
            }


            //SyncGameplaySc.Instance.InitializeData(BattleLobbySC.Instance.Finaldata.user_data.socket_room,
            //               BattleLobbySC.Instance.Finaldata.user_data.user_id, BattleLobbySC.Instance.Finaldata.user_data.player_name,
            //  BattleLobbySC.Instance.Finaldata.user_data.socket_id, BattleLobbySC.Instance.Finaldata.opp_user_data.user_id,
            //  Mycards, Oppcards);

            yield break;
            //NOTE: Not using 'SyncGameplaySc' For Now

            SyncGameplaySc.Instance.GameData.SocketRoomId = BattleLobbySC.Instance.Finaldata.user_data.socket_room;
            SyncGameplaySc.Instance.GameData.GameState = "running";
            Debug.Log("-->" + SyncGameplaySc.Instance.GameData.MyData.PlayerId);
            Debug.Log(BattleLobbySC.Instance.Finaldata.user_data.user_id);
            SyncGameplaySc.Instance.GameData.MyData.PlayerId = BattleLobbySC.Instance.Finaldata.user_data.user_id;
            SyncGameplaySc.Instance.GameData.MyData.PlayerName = BattleLobbySC.Instance.Finaldata.user_data.player_name;
            SyncGameplaySc.Instance.GameData.MyData.SocketId = BattleLobbySC.Instance.Finaldata.user_data.socket_id;
            SyncGameplaySc.Instance.GameData.OppData.PlayerId = BattleLobbySC.Instance.Finaldata.opp_user_data.user_id;
            SyncGameplaySc.Instance.GameData.OppData.PlayerName = BattleLobbySC.Instance.Finaldata.opp_user_data.player_name;
            SyncGameplaySc.Instance.GameData.OppData.SocketId = BattleLobbySC.Instance.Finaldata.opp_user_data.socket_id;
            SyncGameplaySc.Instance.GameData.MyData.CardData = new Card[3];
            SyncGameplaySc.Instance.GameData.MyData.TempCardData = new Card[3];
            SyncGameplaySc.Instance.GameData.OppData.CardData = new Card[3];
            SyncGameplaySc.Instance.GameData.OppData.TempCardData = new Card[3];

            for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
            {
                SyncGameplaySc.Instance.GameData.MyData.CardData[i] = UserDataController.Instance.profileData.best_card[i];
                SyncGameplaySc.Instance.GameData.MyData.TempCardData[i] = UserDataController.Instance.profileData.best_card[i];
            }

            for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
            {

                SyncGameplaySc.Instance.GameData.OppData.CardData[i] = PlayerManager.instance.enemyCardsObj[i].GetComponent<CardObject>().CardDetail;
                SyncGameplaySc.Instance.GameData.OppData.TempCardData[i] = PlayerManager.instance.enemyCardsObj[i].GetComponent<CardObject>().CardDetail;

            }

            SyncGameplaySc.Instance.Save();

        }
        catch (System.Exception ex)
        {
            Debug.Log("ex Error--->" + ex.ToString());
        }

    }
    private void DisruptCard(EtherSocketPackets.DiceRollPacket receviedPacket)
    {
        var allenemyCards = PlayerManager.instance.enemyCardsObj.Select(x => x.GetComponent<CardObject>()).ToList();

        if (allenemyCards.Any(x => x.CardDetail != null && x.CardDetail.card_contract_id.Equals(receviedPacket.disrupt_card_contract_id)))
        {

            var cardTemp = allenemyCards.First(x => x.CardDetail != null && x.CardDetail.card_contract_id.Equals(receviedPacket.disrupt_card_contract_id));

            Debug.Log("found");
            BGPanel.SetActive(true);
            MyAttribImage.gameObject.SetActive(true);
            MyAttribValue.gameObject.SetActive(true);
            BGPanel.GetComponent<SpriteRenderer>().sortingOrder = 5;
            cardTemp.OnSelectEnemyCardRPC();
            OnComplete?.Invoke();
        }



        Debug.Log("outside");
    }



    private void HealCard(EtherSocketPackets.DiceRollPacket receviedPacket)
    {
        var allenemyCards = PlayerManager.instance.enemyCardsObj.Select(x => x.GetComponent<CardObject>()).ToList();

        if (allenemyCards.Any(x => x.CardDetail != null && x.CardDetail.card_contract_id.Equals(receviedPacket.healing_card_contract_id)))
        {

            var cardTemp = allenemyCards.First(x => x.CardDetail != null && x.CardDetail.card_contract_id.Equals(receviedPacket.healing_card_contract_id));

            Debug.Log("found");
            BGPanel.SetActive(true);
            MyAttribImage.gameObject.SetActive(true);
            MyAttribValue.gameObject.SetActive(true);
            BGPanel.GetComponent<SpriteRenderer>().sortingOrder = 5;
            cardTemp.OnSelectEnemyCardRPC();
            // PlayerManager.instance.enemyCardsObj[i].GetComponent<CardObject>().OnEnemySelectCard();

            //allCards[1].CardDetail.card_contract_id+"A"== cardId

            OnComplete?.Invoke();
        }



        Debug.Log("outside");
    }


    private void SelectCard(EtherSocketPackets.DiceRollPacket receviedPacket)
    {

        
        var allenemyCards = PlayerManager.instance.enemyCardsObj.Select(x => x.GetComponent<CardObject>()).ToList();


        if (allenemyCards.Any(x => x.CardDetail != null && x.CardDetail.card_contract_id.Equals(receviedPacket.PlayerCardId)))
        {

            var cardTemp = allenemyCards.First(x => x.CardDetail != null && x.CardDetail.card_contract_id.Equals(receviedPacket.PlayerCardId));

            Debug.Log("found " + cardTemp);
            BGPanel.SetActive(true);
            MyAttribImage.gameObject.SetActive(true);
            MyAttribValue.gameObject.SetActive(true);
            SelectPowerManager.instance.selectedEnemyPowerName = AttributeType.None;

            BGPanel.GetComponent<SpriteRenderer>().sortingOrder = 5;
            for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
            {
                PlayerManager.instance.enemyCardsObj[i].SetAllPowerOff();
                PlayerManager.instance.enemyCardsObj[i].DiceValue = 0;
            }
            cardTemp.OnSelectEnemyCardRPC();
            // PlayerManager.instance.enemyCardsObj[i].GetComponent<CardObject>().OnEnemySelectCard();

            //allCards[1].CardDetail.card_contract_id+"A"== cardId

            OnComplete?.Invoke();
        }



        Debug.Log("outside");
    }

    /// <summary>
    /// It will handle all missing packets from PAST
    /// </summary>
    /// <param name="data"></param>
    public void RollDicePacketHandler(EtherSocketPackets.DiceRollPacket data)
    {
        MessageQueue.Enqueue(new KeyValuePair<string, object>("DiceRollBigData", data));
    }

    #region Remote RPC First Turn Functions
    public void CardHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }
        if (data == null || string.IsNullOrEmpty(data.PlayerCardId))//ID wala check
        {

            return;
        }
        else if (PlayerManager.instance.SelectedEnemyCard != null && PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id != data.PlayerCardId)
        {
            Debug.Log("Select Card " + data.PlayerCardId);
            SelectCard(data);
        }
        else if (PlayerManager.instance.SelectedEnemyCard != null)
            return;
        else
        {
            Debug.Log("Select Card " + data.PlayerCardId);
            SelectCard(data);
        }
       

        //return true;
    }
    public void PlayerAttributeHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }
        if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.None)
        {
            if (data == null || data.PlayerAttribute == AttributeType.None)
            {
                //OnComplete?.Invoke();
                return;
            }
            Debug.Log("Select Card attribute" + data.PlayerAttribute);

            SelectPowerManager.instance.SlectedEnemyPowerName(data.PlayerAttribute, 1);
            //OnComplete?.Invoke();
            //return true;
        }
    }
    public void HealCardHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }
        if (PlayerManager.instance.SelectedHealEnemyCard == null)
        {

            if (data == null || string.IsNullOrEmpty(data.healing_card_contract_id))
            {
                //OnComplete?.Invoke();
                return;
            }
            Debug.Log("Select heal Card " + data.healing_card_contract_id);

            HealCard(data);
            //return true;
        }

        if (PlayerManager.instance.SelectedCard == null)
        {
            if (data == null || string.IsNullOrEmpty(data.EnemyCardId))
            {
                //OnComplete?.Invoke();
                return;
            }

            for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
            {
                //SData = SData.Trim();

                if (PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().CardDetail.card_contract_id.Equals(data.EnemyCardId))
                {
                    Debug.Log("Opp card selected");
                    BGPanel.SetActive(true);
                    OppAttribImage.gameObject.SetActive(true);
                    OppAttribValue.gameObject.SetActive(true);
                    BGPanel.GetComponent<SpriteRenderer>().sortingOrder = 5;

                    PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().OnSelectCardRPC();

                    // PlayerManager.instance.enemyCardsObj[i].GetComponent<CardObject>().OnEnemySelectCard();
                    selCard = i;
                    Debug.Log(selCard);


                }
            }


            //OnComplete?.Invoke();
            //return true;

        }

    }
    public void OnedicerollHandler(EtherSocketPackets.DiceRollPacket data)
    {
       

        if (DiceManager.instance.playerDiceNum == 0)
        {
            if (data == null || string.IsNullOrEmpty(data.PlayerDice))
            {
                //OnComplete?.Invoke();
                return;
            }
            Debug.Log("Dice Roll num " + data.PlayerDice);

            //SData = SData.Trim();
            int num = int.Parse(data.PlayerDice);
            Debug.Log(num);
            DiceManager.instance.Btn_ClickonDiceRPC(num);
            PauseCountTimer();
            //return true;
        }
    }
    public void ElementeumHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }
        //case "elementeum":
        if (PlayerManager.instance.SelectedEnemyCard != null
            && PlayerManager.instance.SelectedEnemyCard.ElementeumStatus != null
            && data.ElementeumData != null
            &&
            (
            PlayerManager.instance.SelectedEnemyCard.ElementeumStatus.Count != data.ElementeumData.Count
            ||
            !PlayerManager.instance.SelectedEnemyCard.ElementeumStatus.All(e => data.ElementeumData.Any(x => x.Key == e.Key && x.Value == e.Value)))

            )
        {
            if (data == null || data.ElementeumData == null || data.ElementeumData.Count < 1)
            {
                //OnComplete?.Invoke();
                return;
            }

            var cObj = PlayerManager.instance.SelectedEnemyCard;
            cObj.ElementeumStatus = data.ElementeumData;

            string Name = "0";
            Debug.Log("elementeum PACKET DATA " + data.ToJObject());

            foreach (var item in data.ElementeumData)
            {
                Name = item.Key;
                int value = item.Value;


                cObj.UseElementeumRpc(Name, value);

            }

            //OnComplete?.Invoke();
            return;

        }
    }
    public void PlayerOneCompanionCardHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }
        var enemyCardObj = PlayerManager.instance.SelectedEnemyCard;
        if (data == null || data.CompanionData == null || data.CompanionData.Count < 1)
        {
            //OnComplete?.Invoke();
            return;
        }
        if (data.CompanionData.All(dc => enemyCardObj.companionIDs.Contains(dc)))
        {
            return;
        }

        Debug.LogError("companion PACKET DATA " + data);

        Debug.Log("Enemy companion cards working on the basis of the player Response" + data.CompanionData);


        enemyCardObj.companionData.Clear();
        enemyCardObj.companionIDs = data.CompanionData.ToArray().ToList();


        for (int i = 0; i < data.CompanionData.Count; i++)
        {

            EnemyComapnionCardAnim(data.CompanionData[i]); // animate
            for (int j = 0; j < PlayerCompanionCardsAnim.instance.enemyCompanionCards.Length; j++)
            {
                var EnemyComCardObj = PlayerCompanionCardsAnim.instance.enemyCompanionCards[j];
                if (EnemyComCardObj.CardDetail.card_contract_id == data.CompanionData[i])
                {
                    switch (SelectPowerManager.instance.selectedEnemyPowerName)
                    {
                        case AttributeType.Attack:
                            enemyCardObj.companionData.Add(EnemyComCardObj.CardDetail.attack);
                            instance.enemyCompanion.text = enemyCardObj.CompanionValueStatus.ToString();
                            OppAttribValue.text = enemyCardObj.TotalAttack().ToString();
                            break;
                        case AttributeType.Defense:
                            enemyCardObj.companionData.Add(EnemyComCardObj.CardDetail.defence);
                            instance.enemyCompanion.text = enemyCardObj.CompanionValueStatus.ToString();
                            OppAttribValue.text = enemyCardObj.TotalDefence().ToString();
                            break;
                        case AttributeType.Disrupt:
                            enemyCardObj.companionData.Add(EnemyComCardObj.CardDetail.disrupt);
                            instance.enemyCompanion.text = enemyCardObj.CompanionValueStatus.ToString();
                            OppAttribValue.text = enemyCardObj.TotalDisrupt().ToString();
                            break;
                        case AttributeType.Heal:
                            enemyCardObj.companionData.Add(EnemyComCardObj.CardDetail.heal);
                            instance.enemyCompanion.text = enemyCardObj.CompanionValueStatus.ToString();
                            OppAttribValue.text = enemyCardObj.TotalHeal().ToString();

                            break;
                    }
                }
            }
        }



    }

  
    public void DebuffValueHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }
        if (PlayerManager.instance.SelectedEnemyCard != null
            && SelectPowerManager.instance.selectedEnemyPowerName != AttributeType.None
            && data.debuff_value != "0")
        {
            if (data == null || string.IsNullOrEmpty(data.debuff_value))
            {
                //OnComplete?.Invoke();
                return;
            }

            //Debug.Log(SData);
            //SData = SData.Trim();
        Debug.Log("debuff PACKET DATA " + data);
            var EnemyObj = PlayerManager.instance.SelectedEnemyCard;
            int debuff_val = 0;
            if (!int.TryParse(data.debuff_value, out debuff_val))
            {
                Debug.LogError(data.debuff_value + " is not convertable to integer");
            }
            int.TryParse(data.debuff_value.ToString(), out debuff_val);
            //string attribute = SelectPowerManager.instance.selectedEnemyPowerName;

            switch (SelectPowerManager.instance.selectedEnemyPowerName)
            {
                case AttributeType.Attack:
                    EnemyObj.debuffImage.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                    EnemyObj.debuffImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1.48f, 0), 1);
                    EnemyObj.debuffAttack.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1.48f, 0), 1);

                  
                    PlayerManager.instance.SelectedEnemyCard.debuffValueAttack = debuff_val;

                    instance.enemeyAttribute.text = EnemyObj.CardDetail.attack.ToString();
                    EnemyObj.debuffAttack.text = "-" + debuff_val.ToString();
                    instance.OppAttribValue.text = EnemyObj.TotalAttack().ToString();

                    break;
                case AttributeType.Heal:
                    EnemyObj.debuffHealImage.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                    EnemyObj.debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1.48f, 0), 1);
                    EnemyObj.debuffHeal.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1.48f, 0), 1);

                    PlayerManager.instance.SelectedEnemyCard.debuffValueHeal = debuff_val;

                    instance.enemeyAttribute.text = EnemyObj.CardDetail.heal.ToString();
                    EnemyObj.debuffHeal.text = "-" + debuff_val.ToString();
                    instance.OppAttribValue.text = EnemyObj.TotalHeal().ToString();



                    break;
            }

            //OnComplete?.Invoke();
            return;


        }


    }


    public void SpecialAbilityValueHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }

        if (data == null || data.special_power == AttributeType.None)
        {
            //OnComplete?.Invoke();
            return;
        }
        //if (data.special_power_used)
        {
            for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
            {
                if (PlayerManager.instance.enemyCardsObj[i].CardDetail.card_contract_id == data.PlayerCardId)
                {
                    //if (!PlayerManager.instance.enemyCardsObj[i].isSpActive)
                    {
                        Debug.Log("Found Special Card");
                        Debug.Log("Select Card attribute" + data.special_power);
                        PlayerManager.instance.enemyCardsObj[i].UseSpecialAbilityRPC(data.special_power);
                    }
                }
            }
        }
    }

    //public void CanclesSpecaialAbilityValueHandler(EtherSocketPackets.DiceRollPacket data)
    //{
    //    if (SkirmishModeAnim.instance.inSkirmish)
    //    {
    //        return;
    //    }
    //    if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.None || SelectPowerManager.instance.selectedEnemyPowerName != data.special_power)
    //    {
    //        return;
    //    }
    //    if (data == null || data.special_power == AttributeType.None)
    //    {
    //        return;
    //    }
    //    //if (!data.special_power_used)
    //    {
    //        for (int i = 0; i < 3; i++)
    //        {

    //            if (PlayerManager.instance.enemyCardsObj[i].CardDetail.card_contract_id == data.PlayerCardId)
    //            {
    //                if (PlayerManager.instance.enemyCardsObj[i].isSpActive)
    //                {
    //                    Debug.Log("Found Special Card");
    //                    PlayerManager.instance.enemyCardsObj[i].CancelSpecialAbilityRPC();
    //                }
    //            }

    //        }
    //    }
    //}
    public void SelectOpponentCardHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }

        if (data == null || string.IsNullOrEmpty(data.EnemyCardId))
        {
            OnComplete?.Invoke();
            return;
        }
        if (PlayerManager.instance.SelectedCard != null)
            if (PlayerManager.instance.SelectedCard.CardDetail.card_contract_id == data.EnemyCardId)
                return;
        
       

        for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
        {
            //SData = SData.Trim();

            if (PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().CardDetail.card_contract_id.Equals(data.EnemyCardId))
            {
                Debug.Log("Opp card selected");
                BGPanel.SetActive(true);
                OppAttribImage.gameObject.SetActive(true);
                OppAttribValue.gameObject.SetActive(true);
                BGPanel.GetComponent<SpriteRenderer>().sortingOrder = 5;

                PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().OnSelectCardRPC();
                selCard = i;
                Debug.Log(selCard);


            }

        }
    }
    #endregion
    #region Remote RPC second Turn Functions
    public void SelectDisruptCardHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }
        if (data == null || string.IsNullOrEmpty(data.disrupt_card_contract_id))//ID wala check
        {

            return;
        }
        else if (PlayerManager.instance.SelectedEnemyCard != null && PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id != data.disrupt_card_contract_id)
        {
            Debug.Log("Select disrupt Card " + data.disrupt_card_contract_id);
            DisruptCard(data);
        }
        else if (PlayerManager.instance.SelectedEnemyCard != null)
            return;
        else
        {
            Debug.Log("Select disrupt Card " + data.PlayerCardId);
            DisruptCard(data);
        }

    }
    public void PlayerTwoAttributeHandler(EtherSocketPackets.DiceRollPacket data)
    {
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            return;
        }
        if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.None)
        {
            if (data == null || data.PlayerTwoAttribute == AttributeType.None)
        {
            
            return;
        }
        Debug.Log("Select Card attribute" + data.PlayerTwoAttribute);
            SelectPowerManager.instance.SlectedEnemyPowerName(data.PlayerTwoAttribute, 1);

            //return true;
        }


    }
    public void PlayerTwodicerollHandler(EtherSocketPackets.DiceRollPacket data)
    {

        //case "onediceroll":
        if (DiceManager.instance.enemyDiceNum == 0)
        {
            Debug.Log("Player Two Dice Roll num " + data.PlayerTwoDice);

            if (data == null || string.IsNullOrEmpty(data.PlayerTwoDice))
            {
                //OnComplete?.Invoke();
                return;
            }
            DiceManager.instance.PlayerTwoDiceNumberRPC(int.Parse(data.PlayerTwoDice));


        }
    }
    #endregion




    private void Update()
    {
       
        //timer for other user 
        OppTimer.text = Oppcounter.ToString();
        

        if (!MessageQueue.Any())
            return;



        var nextMsg = MessageQueue.Dequeue();

        if (nextMsg.Key == "DiceRollBigData")
        {

            EtherSocketPackets.DiceRollPacket data = (EtherSocketPackets.DiceRollPacket)nextMsg.Value;
            CardHandler(data);
            PlayerAttributeHandler(data);
            HealCardHandler(data);
            OnedicerollHandler(data);
            ElementeumHandler(data);
            PlayerOneCompanionCardHandler(data);
            DebuffValueHandler(data);
            SpecialAbilityValueHandler(data);
            //CanclesSpecaialAbilityValueHandler(data);

            SelectOpponentCardHandler(data);
            SelectDisruptCardHandler(data);
            PlayerTwoAttributeHandler(data);
            PlayerTwodicerollHandler(data);
            Debug.Log("All events check  ");
            return;
        }

        //if (HandleMsg(nextMsg))
        //    return;


        var MessageKey = nextMsg.Key;



        if (MessageKey == "finalresult")
        {
            var SData = ((string)nextMsg.Value).Trim();

            Debug.Log(SData);

            if (SData == "me")
            {

                //        StartCoroutine(DiceManager.instance.AttackParticle_Fun_Active(0,0));
                //PlayerManager.instance.skirmishObj.SetActive(false);



            }

            else if (SData == "opp")
            {
                //   StartCoroutine(DiceManager.instance.LostParticle_Fun_Active(0,0));
                //PlayerManager.instance.skirmishObj.SetActive(false);



            }

            else if (SData == "tie")
            {
                StartCoroutine(DiceManager.instance.TieMatchFun_Active());
            }


        }


        // logic to handle turn and main turn 
        else if (MessageKey == "plyrmsg") // logic to handle message received 
        {
            var SData = ((string)nextMsg.Value).Trim();

            Debug.Log("data" + SData);

            if (SData == "clickonbg")
            {
                PlayerManager.instance.SocketClickOnBG();
            }

            else if (SData == "turnchange")
            {
                Debug.Log("turn change " + Turn);
                StartCoroutine(ChangeTurn());
                //UpdateTime(0);
            }

            else if (SData == "mainturnchange")
            {

                PlayerManager.instance.SocketClickOnBG();
                StartCoroutine(MainChangeTurn());
            }
          
        }
        
    }

    public void EnemyAnimUpdate(string msg, EtherSocketPackets.DiceRollPacket data, System.Action OnDone = null)
    {
        Debug.Log("EnemyAnimUpdate");

        Debug.Log(data);

        Debug.LogError(msg);

        //receviedPacket = data;
        //Message = msg;

        MessageQueue.Enqueue(new KeyValuePair<string, object>(msg, data));


        OnComplete = OnDone;
    }

    public void oldEnemyAnimUpdate(string msg, string data, System.Action OnDone = null)
    {

        Debug.Log("oldEnemyAnimUpdate");

        Debug.Log(data);

        Debug.Log(msg);

        //SData = data;

        OnComplete = OnDone;

        if (msg == "msg")
        {
            msg = "plyrmsg";
        }
        MessageQueue.Enqueue(new KeyValuePair<string, object>(msg, data));

    }

    IEnumerator Countdown(int seconds)
    {
        int counter = seconds;

        timertxt.text = counter.ToString();

        while (counter > 0)
        {

            yield return new WaitForSeconds(1);
            counter--;

            timertxt.text = counter.ToString();
        }
        showrollDice();
    }


    //------------------------Timer Logic-----------------

    public void StopAllTimer()
    {
        TimerRuning = false;         StopAllCoroutines();
        PauseTimer = true;
        MyTimer.transform.parent.gameObject.SetActive(false);
        OppTimer.transform.parent.gameObject.SetActive(false);
    }

    public void PauseCountTimer()
    {
        Debug.Log("TImer Pauseeeee");
        PauseTimer = true;
    }

    public void UserLost()
    {
        StopCoroutine("WaitTime");
        Debug.Log("User Lost TImer Pauseeeee");

        PauseTimer = true;
        TimerText.text = "90";
        StartCoroutine(WaitTime(90));
        OpponentLostPanel.SetActive(true);
        PlayerManager.instance.CanClick = false;
        Time.timeScale = 0;

    }

    public void UserFound()
    {

        Debug.Log("User Lost TImer Play");
        Time.timeScale = 1;
        PauseTimer = false;

        // TimerRuning = false;         StopAllCoroutines();

        OpponentLostPanel.SetActive(false);

        PlayerManager.instance.CanClick = !InGameCards.instance.PTurn();
        //  PlayerManager.instance.CanClick = true;

    }

    IEnumerator WaitTime(float tim)
    {

        while (tim > 0)
        {
            if (PauseTimer)
            {
                yield return new WaitForSecondsRealtime(1f);
                tim -= 1;
                TimerText.text = tim.ToString();
            }
            else
            {
                tim = 0;
                yield break;
            }
        }

        if (tim <= 0)
        {
            Time.timeScale = 1;
            int meVal = 0;
            int opVal = 0;

            for (int i = 0; i < 3; i++)
            {
                if (PlayerManager.instance.PlayerCardsObj[i].GetComponent<CardObject>().CardDetail.heart <= 0)
                {
                    meVal += 1;
                }
                if (PlayerManager.instance.enemyCardsObj[i].CardDetail.heart <= 0)
                {
                    opVal += 1;
                }
            }

            OpponentLostPanel.SetActive(false);

            if (meVal > opVal)
            {
                Debug.Log("Opponent Won");
                BattleLobbySC.Instance.GameComplete("opp");
            }

            else if (meVal == opVal)
            {
                Debug.Log("Match Draw");
                BattleLobbySC.Instance.GameComplete("draw");
            }

            else
            {
                Debug.Log("Opponent Won");
                BattleLobbySC.Instance.GameComplete("me");
            }

        }
    }

    public void PlayCountTimer()
    {
        Debug.Log("TImer Play");
        PauseTimer = false;
    }

    public void showrollDice()
    {
        headtext.text = "Roll Dice To Decide Turn";
        timertxt.gameObject.SetActive(false);
        dice.SetActive(true);
        PlayerManager.instance.CanClick = false;

    }

    public void Btn_ClickOnDice()
    {

        StartDiceBtn.enabled = false;

        DNum = Random.Range(1, 7);

        headtext.text = "Wait for Opponent to Roll Dice";
        dice.GetComponent<Animator>().SetTrigger("roll");

        Debug.Log("Pressed ROllDice");
        BattleLobbySC.Instance.SendDiceNumber(DNum, this);
        PlayerManager.instance.CanClick = false; // Disabling accidental clicks while attack       
        NumOfPower.text = DNum.ToString();
        dice.GetComponent<AudioSource>().Play();
        StartCoroutine(RollDice());
    }

    public void Btn_ClickOnDiceOpp(int no)
    {

        StartDiceBtn.enabled = false;

        DNum2 = no;
        dice.GetComponent<Animator>().SetTrigger("roll");

        PlayerManager.instance.CanClick = false; // Disabling accidental clicks while attack       
        NumOfPower.text = DNum2.ToString();
        dice.GetComponent<AudioSource>().Play();
        StartCoroutine(RollDice2());
    }

    public bool PTurn()
    {
        return PlayerStats != Turn;
    }

    public void SetCounter(int TimerCount)
    {
        MeCounter = TimerCount;
    }

    public void SetCounter(int TimerCount, bool Increase)
    {
        if (Increase)
        {
            MeCounter += TimerCount;
        }
        else
        {
            MeCounter -= TimerCount;
        }

    }

    public bool GetMainTurn()
    {
        return PlayerStats != MainTurn;
    }

    public void TieTurnChange()
    {
        // SetMyAttribImage(SelectPowerManager.instance.selectedPowerName);
        //SetOppAttribImage(SelectPowerManager.instance.selectedEnemyPowerName);
        Turn = MainTurn;

        if (MainTurn == PlayerStats)
        {

        }
        else
        {
            BattleLobbySC.Instance.PlayerTwoAnim();
        }

    }


    public void DecideWhichPlayerWon(string whowon, int mysc, int oppsc)
    {

        StartCoroutine(ShowOppDice(whowon, mysc, oppsc));

    }

    IEnumerator ShowOppDice(string whowon, int mysc, int oppsc)
    {

        Debug.Log("------ Roll Dice ------->");

        yield return new WaitForSeconds(2f);

        dice.GetComponent<Animator>().SetTrigger("rollopp");

        NumOfPower.text = oppsc.ToString();

        DNum2 = oppsc;

        StartCoroutine(RollDice2());

        yield return new WaitForSeconds(1.5f);

        bool me = false;

        Debug.Log("Decidedd");
        //enemyDice.text = DNum2.ToString();
        //SyncGameplaySc.Instance.InitializeTurn();


        opptext.gameObject.SetActive(true);
        youdice.gameObject.SetActive(true);
        oppdice.gameObject.SetActive(true);
        youtxt.gameObject.SetActive((whowon == "me" || whowon == "opp"));
        resulttext.gameObject.SetActive((whowon == "me" || whowon == "opp"));

        youdice.text = mysc.ToString();
        oppdice.text = oppsc.ToString();

        me = (whowon == "me");
        if (whowon == "me" || whowon == "opp")
        {
            StartCoroutine(Turnchange(me));
        }

        PlayerManager.instance.CanClick = (whowon != "me" && whowon != "opp");
        StartDiceBtn.enabled = (whowon != "me" && whowon != "opp");

        MainTurn = PlayerStats = (whowon == "me") ? stats.playerone : (whowon == "opp") ? stats.playertwo : PlayerStats;
        MainTurn = Turn = stats.playerone;

        if (whowon == "me")
        {
            resulttext.text = "You Won";
            TurnText.text = "Your Turn";
        }
        else if (whowon == "opp")
        {
            resulttext.text = "You Lost";
            TurnText.text = "Opponent Turn";
        }
        else
        {
            headtext.text = "Please Roll Dice Again";
            resulttext.text = "Draw";
        }
    }


    public IEnumerator Turnchange(bool me)
    {

        yield return new WaitForSeconds(2f);
        DIcepanel.SetActive(false);
        TurnPanel.SetActive(true);
        TurnText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        TurnText.gameObject.SetActive(false);
        TurnPanel.SetActive(false);

        if (me)
        {
            //SyncGameplaySc.Instance.GameData.Turn = "me";
            //SyncGameplaySc.Instance.Save();
            //Turn = stats.playerone;
            TurnPanel.SetActive(false);

            PlayerManager.instance.CanClick = true;
            PlayerManager.instance.CanClickOnBG = true;
            PlayerManager.instance.CanClickOnMeCard = true;
            PlayerManager.instance.CanClickOnElemt = true;

            PlayerManager.instance.MeIndicator.SetActive(true);
            PlayerManager.instance.OppIndicator.SetActive(false);

            Debug.Log("My Turn ");
            MyTimer.transform.parent.gameObject.SetActive(true);
            OppTimer.transform.parent.gameObject.SetActive(false);

            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.text = "(Player) \n" + BattleLobbySC.Instance.Finaldata.user_data.player_name.ToString();
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.text = "(Opponent) \n" + BattleLobbySC.Instance.Finaldata.opp_user_data.player_name.ToString();
        }

        else
        {
            //SyncGameplaySc.Instance.GameData.Turn = "opp";
            //SyncGameplaySc.Instance.Save();
            // Turn = stats.playertwo;
            TurnPanel.SetActive(false);

            PlayerManager.instance.CanClick = false;
            BattleLobbySC.Instance.PlayerTwoAnim();
            PlayerManager.instance.MeIndicator.SetActive(false);
            PlayerManager.instance.OppIndicator.SetActive(true);

            Debug.Log("Opp Turn ");
            MyTimer.transform.parent.gameObject.SetActive(false);
            OppTimer.transform.parent.gameObject.SetActive(true);

            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.text = "(Opponent) \n" + BattleLobbySC.Instance.Finaldata.opp_user_data.player_name.ToString();
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.text = "(Player) \n" + BattleLobbySC.Instance.Finaldata.user_data.player_name.ToString();
        }
        if (Turn == PlayerStats)
        {

        StartTimer();
        }

    }

    

    public IEnumerator SyncTimer(string mainturn, string turn, EtherSocketPackets.DiceRollPacket LastAction)
    {
        Debug.LogError("SyncTimer " + " Main Turn---->" + mainturn + " Turn---->" + turn + " Last Action---->" + LastAction);
        SyncPanel.SetActive(true);

        MeCounter = 0;
        TimerRuning = false;
        StopAllCoroutines();

        if (mainturn == "opp")
        {
            if (PlayerStats == stats.playerone)
            {

                MainTurn = stats.playerone;
            }
            else
            {
                MainTurn = stats.playertwo;
            }
        }

        else
        {
            if (PlayerStats == stats.playerone)
            {
                MainTurn = stats.playertwo;
            }
            else
            {
                MainTurn = stats.playerone;
            }
        }

        if (turn == "opp")
        {

            if (PlayerStats == stats.playerone)
            {
                Turn = stats.playerone;
            }
            else
            {
                Turn = stats.playertwo;

            }

        }
        else
        {
                PlayerManager.instance.CanClickOnMeCard = PlayerStats == stats.playerone && (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal && SelectPowerManager.instance.selectedPowerName == AttributeType.None);

                PlayerManager.instance.CanClick = PlayerStats == stats.playerone;

                PlayerManager.instance.CanClickOnBG = false;
                PlayerManager.instance.CanClickonMeDice = false;
                PlayerManager.instance.CanClickOnEnemyCard = false;
                PlayerManager.instance.CanClickonEnemyAttrib = false;

                PlayerManager.instance.CanClickOnMeAttrib = PlayerStats == stats.playerone;
                PlayerManager.instance.CanClickonEnemyDice = PlayerStats == stats.playerone;


            if (PlayerStats == stats.playerone)
            {
                Turn = stats.playertwo;
                BattleLobbySC.Instance.PlayerOneUnsubscribeEvents();
            }

            else
            {
                Turn = stats.playerone;
                BattleLobbySC.Instance.PlayerTwoAnim();
                PlayerManager.instance.CanClickOnMeCard = false;
            }

        }

        RollDicePacketHandler(LastAction);


        //yield return new WaitForSeconds(1);

        PlayCountTimer();

        yield return new WaitForSeconds(1);

        SyncPanel.SetActive(false);
    }

    public IEnumerator MainChangeTurn()
    {
        Debug.Log("Main turn change function  ");
        //Debug.Log(Turn);
        //Debug.Log(MainTurn);
        //Debug.Log(PlayerStats);
       
        


        for (int i = 0; i < 3; i++)
        {
            PlayerManager.instance.PlayerCardsObj[i].CheckCardDisablity();
            PlayerManager.instance.enemyCardsObj[i].CheckCardDisablity();
        }

        //PlayerManager.instance.CanClick = false;
        

        TurnPanel.SetActive(true);

        TurnText.gameObject.SetActive(true);




        if (MainTurn == stats.playertwo)
        {

            if (PlayerStats == stats.playerone)
            {
                TurnText.text = "Your Turn";
                Debug.Log("Your turn");

            }
            else
            {
                Debug.Log("Opp turn");

                TurnText.text = "Opponent Turn";
            }
            MainTurn = stats.playerone;

        }

        else
        {

            if (PlayerStats == stats.playertwo)
            {
                Debug.Log("Your turn");
                TurnText.text = "Your Turn";
                PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.text = "(Player) \n" + BattleLobbySC.Instance.Finaldata.user_data.player_name.ToString();
                PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.text = "(Opponent) \n" + BattleLobbySC.Instance.Finaldata.opp_user_data.player_name.ToString();
            }
            else
            {

                Debug.Log("Opp turn");

                TurnText.text = "Opponent Turn";
                PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.text = "(Opponent) \n" + BattleLobbySC.Instance.Finaldata.opp_user_data.player_name.ToString();
                PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.text = "(Player) \n" + BattleLobbySC.Instance.Finaldata.user_data.player_name.ToString();
            }

            MainTurn = stats.playertwo;
        }
        Turn = MainTurn;
        yield return new WaitForSeconds(2f);

        TurnPanel.SetActive(false);

        TurnText.gameObject.SetActive(false);

        PlayerManager.instance.CanClickOnElemt = (MainTurn == PlayerStats);
        PlayerManager.instance.CanClickOnMeCard = (MainTurn == PlayerStats);

        PlayerManager.instance.CanClickonMeDice = false;
        PlayerManager.instance.CanClickonEnemyDice = false;
        PlayerManager.instance.CanClickOnEnemyCard = false;
        PlayerManager.instance.CanClickonEnemyAttrib = false;
        PlayerManager.instance.CanClickOnMeAttrib = false;

        PlayerManager.instance.MeIndicator.SetActive((MainTurn == PlayerStats));
        PlayerManager.instance.OppIndicator.SetActive(!(MainTurn == PlayerStats));
        

        PlayerManager.instance.ClickOnBG();
        PlayerManager.instance.CanClick = (MainTurn == PlayerStats);
        PlayerManager.instance.CanClickOnBG = (MainTurn == PlayerStats);
        if (MainTurn == PlayerStats)
        {
            BattleLobbySC.Instance.PlayerOneUnsubscribeEvents();
            MyTimer.transform.parent.gameObject.SetActive(true);
            OppTimer.transform.parent.gameObject.SetActive(false);

            Debug.Log("Your turn");
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.text = "(Player) \n" + BattleLobbySC.Instance.Finaldata.user_data.player_name.ToString();
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.text = "(Opponent) \n" + BattleLobbySC.Instance.Finaldata.opp_user_data.player_name.ToString();
        }

        else
        {
            BattleLobbySC.Instance.PlayerTwoAnim();
            Debug.Log("Opp turn");
            MyTimer.transform.parent.gameObject.SetActive(false);
            OppTimer.transform.parent.gameObject.SetActive(true);
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.text = "(Opponent) \n" + BattleLobbySC.Instance.Finaldata.opp_user_data.player_name.ToString();
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.text = "(Player) \n" + BattleLobbySC.Instance.Finaldata.user_data.player_name.ToString();
        }

        StartTimer();
    }

    public IEnumerator ChangeTurn() //TurnChange Method
    {
        Debug.Log("DEFENDER TIME LOG ");
        PlayerManager.instance.CanClick = false;

        TurnPanel.SetActive(true);

        TurnText.gameObject.SetActive(true);


        if (Turn == stats.playertwo)
        {
            if (PlayerStats == stats.playerone)
            {
                TurnText.text = "Your Turn";
            }
            else
            {
                TurnText.text = "Opponent Turn";
            }
            Turn = stats.playerone;
        }

        else
        {
            if (PlayerStats == stats.playertwo)
            {
                TurnText.text = "Your Turn";
            }
            else
            {
                TurnText.text = "Opponent Turn";
            }

            Turn = stats.playertwo;

        }

        yield return new WaitForSeconds(2f);

        TurnPanel.SetActive(false);

        TurnText.gameObject.SetActive(false);

        PlayerManager.instance.CanClick = (Turn == PlayerStats);
        PlayerManager.instance.CanClickOnBG = false;
        PlayerManager.instance.CanClickOnMeCard = false;
        PlayerManager.instance.CanClickonMeDice = false;
        PlayerManager.instance.CanClickonEnemyDice = false;
        PlayerManager.instance.CanClickOnEnemyCard = false;
        PlayerManager.instance.CanClickonEnemyAttrib = false;
        PlayerManager.instance.CanClickOnMeAttrib = (Turn == PlayerStats);


        if (Turn == PlayerStats)
        {
            PlayerManager.instance.CanClickOnMeCard = SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal;

            var truth = (PlayerManager.instance.PrevRes == "tie" && SelectPowerManager.instance.selectedPowerName != AttributeType.None);
            PlayerManager.instance.CanClickonEnemyDice = truth;
            PlayerManager.instance.CanClickOnMeAttrib = !truth;

            BattleLobbySC.Instance.PlayerOneUnsubscribeEvents();
        }
        else
        {
            BattleLobbySC.Instance.PlayerTwoAnim();
        }
        //StartTimer();
        TriggerableTimer();
        if (SkirmishModeAnim.instance.inSkirmish)
        {
            PauseCountTimer();
        }
    }

    public void StartTimer()
    {


        SavedTime = System.DateTime.Now.ToLongTimeString();

        TimerRuning = false;
        StopAllCoroutines();
        PauseTimer = false;
        StartCoroutine(TurnCounter(50, false)); // 50 sec of timer is set  for the game now for qa i set it to 120

    }

    public void TriggerableTimer()
    {
        TimerRuning = false;
        StopAllCoroutines();
        PauseTimer = false;
        StartCoroutine(TurnCounter(50, true));// 50 sec of timer is set  for the game now for qa i set it to 120
    }

    bool TimerRuning = false;

    IEnumerator TurnCounter(int seconds, bool isTrigger)
    {
        //Debug.Log(Turn);
        //Debug.Log(MainTurn);
        //Debug.Log(PlayerStats);
            MeCounter = seconds;
        
        if (TimerRuning)
            yield break;

        Text Ttext;

        if (Turn == PlayerStats)
        {
            MyTimer.transform.parent.gameObject.SetActive(true);
            OppTimer.transform.parent.gameObject.SetActive(false);
            Ttext = MyTimer;
        }
        else
        {
            MyTimer.transform.parent.gameObject.SetActive(false);
            OppTimer.transform.parent.gameObject.SetActive(true);
            Ttext = OppTimer;
        }

        Ttext.text = MeCounter.ToString();

        if (Turn == PlayerStats)
        {
            Debug.Log("your turn ");

            while (MeCounter > 0)
            {
                TimerRuning = true;
                yield return new WaitForSeconds(1);
                if (!PauseTimer)
                {

                    MeCounter--;
                    if (BattleLobbySC.Instance)
                    {
                        //if (Turn == PlayerStats && Turn == MainTurn)
                        {
                            BattleLobbySC.Instance.UserTime(MeCounter.ToString());
                            //BattleLobbySC.Instance.SocketMessage(MeCounter.ToString());
                        }
                    }
                    if (MeCounter < 3)
                    {
                        AudioSource audio = GetComponent<AudioSource>();
                        audio.clip = drumEffect;
                        audio.Play();
                    }

                }
                Ttext.text = MeCounter.ToString();
            }
        }
        else
        {
                
        Ttext.text = Oppcounter.ToString();
        }


        TimerRuning = false;
        // StartCoroutine(MainChangeTurn());


        //Logic for automatic attack while opponent is inactive and timeout

        if (isTrigger)
        {

            Debug.Log("istriger");

            PlayerManager.instance.CanClick = false;


            if (SelectPowerManager.instance.selectedPowerName == AttributeType.None)
            {

                if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Attack)
                {
                    //public void PlayerTwoDiceNumber(int dicenumber, bool elementeum, string earn_elementeum, int elementeumNumber, int companionValue, int deBuffValue, int isSkirmish)

                    SelectPowerManager.instance.AutoSlectedPowerName(AttributeType.Defense, PlayerManager.instance.SelectedCard.CardDetail.card_contract_id);

                    BattleLobbySC.Instance.PlayerTwoDiceNumber(
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                        "",
                        SelectPowerManager.instance.selectedEnemyPowerName,
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.attack.ToString(),
                        "0",
                        PlayerManager.instance.SelectedEnemyCard.ElementeumStatus,
                        PlayerManager.instance.SelectedEnemyCard.companionIDs,
                        DiceManager.instance.playerDiceNum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.isSpActive,
                        "",
                        PlayerManager.instance.SelectedEnemyCard.MaxHeartPoint.ToString(),
                        "",

                        PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                        SelectPowerManager.instance.selectedPowerName,
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.defence.ToString(),
                        "0",
                        PlayerManager.instance.SelectedCard.ElementeumStatus,
                        PlayerManager.instance.SelectedCard.companionIDs,
                        "0",
                        PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
                        PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
                        PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
                        PlayerManager.instance.SelectedCard.isSpActive,
                        "",
                        PlayerManager.instance.SelectedCard.MaxHeartPoint.ToString(),

                        PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()

                        );
                }
                else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
                {
                    ///
                    //add auto selection of card for disrupt 
                    //

                    PlayerManager.instance.CanClick = true;
                    PlayerManager.instance.CanClickOnMeCard = true;
                    if (PlayerManager.instance.SelectedCard == null)
                    {
                        var objCollection = PlayerManager.instance.PlayerCardsObj.Where(x => x != null);
                        if (objCollection.Any())
                        {
                            PlayerManager.instance.SelectedCard = objCollection.First();
                        }
                        PlayerManager.instance.SelectedCard.OnSelectCard();

                        SelectPowerManager.instance.selectedPowerName = AttributeType.Disrupt;
                    }


                    SelectPowerManager.instance.AutoSlectedPowerName(AttributeType.Disrupt, PlayerManager.instance.SelectedCard.CardDetail.card_contract_id);
                    //BattleLobbySC.Instance.PlayerTwoAttribSel(PlayerManager.instance.SelectedCard.CardDetail.card_contract_id, AttributeType.Disrupt);



                    //BattleLobbySC.Instance.PlayerTwoDiceNumber(0, false, "0", 0, 0, 0, 0);
                    BattleLobbySC.Instance.PlayerTwoDiceNumber(
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                        PlayerManager.instance.SelectedHealEnemyCard.CardDetail.card_contract_id,
                        SelectPowerManager.instance.selectedEnemyPowerName,
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.heal.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.debuffValueHeal.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.ElementeumStatus,
                        PlayerManager.instance.SelectedEnemyCard.companionIDs,
                        DiceManager.instance.playerDiceNum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.isSpActive,
                        "",
                        PlayerManager.instance.SelectedEnemyCard.MaxHeartPoint.ToString(),
                        PlayerManager.instance.SelectedHealEnemyCard.MaxHeartPoint.ToString(),

                        PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                        SelectPowerManager.instance.selectedPowerName,
                        PlayerManager.instance.SelectedCard.CardDetail.disrupt.ToString(),
                        "0",
                        PlayerManager.instance.SelectedCard.ElementeumStatus,
                        PlayerManager.instance.SelectedCard.companionIDs,
                        0.ToString(),
                        PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
                        PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
                        PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
                        PlayerManager.instance.SelectedCard.isSpActive,
                        "",
                        PlayerManager.instance.SelectedCard.MaxHeartPoint.ToString(),

                        PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
                    );

                }
                DiceManager.instance.dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                yield return new WaitForSeconds(2f);
                //Debug.Log("PLayer One DIce Number---" + DNum2 + " usingElemt " + (PlayerManager.instance.SelectedCard.UsingElementeum > 0) + " count  " + PlayerManager.instance.SelectedCard.UsingElementeum);
            }
            else
            {
                Debug.Log("Auto dice roll" + (Turn == PlayerStats));
                if (Turn == PlayerStats)
                {
                    Debug.Log("Auto dice roll" + (Turn == PlayerStats));

                    if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Attack)
                    {
                        Debug.Log(SelectPowerManager.instance.selectedPowerName);
                        BattleLobbySC.Instance.PlayerTwoDiceNumber(
                         PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                         "",
                        SelectPowerManager.instance.selectedEnemyPowerName,
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.attack.ToString(),
                        "0",
                        PlayerManager.instance.SelectedEnemyCard.ElementeumStatus,
                        PlayerManager.instance.SelectedEnemyCard.companionIDs,
                        DiceManager.instance.playerDiceNum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString(),
                        PlayerManager.instance.SelectedEnemyCard.isSpActive,
                        "",
                        PlayerManager.instance.SelectedEnemyCard.MaxHeartPoint.ToString(),
                        "",

                        PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                        SelectPowerManager.instance.selectedPowerName,
                        PlayerManager.instance.SelectedEnemyCard.CardDetail.defence.ToString(),
                        "0",
                        PlayerManager.instance.SelectedCard.ElementeumStatus,
                        PlayerManager.instance.SelectedCard.companionIDs,
                        "0",
                        PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
                        PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
                        PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
                        PlayerManager.instance.SelectedCard.isSpActive,
                        "",
                        PlayerManager.instance.SelectedCard.MaxHeartPoint.ToString(),

                        PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
                        );
                    }

                    else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
                    {
                        Debug.Log(SelectPowerManager.instance.selectedPowerName);
                        BattleLobbySC.Instance.PlayerTwoDiceNumber(
                            PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                            PlayerManager.instance.SelectedHealEnemyCard.CardDetail.card_contract_id,
                            SelectPowerManager.instance.selectedEnemyPowerName,
                            PlayerManager.instance.SelectedEnemyCard.CardDetail.heal.ToString(),
                            PlayerManager.instance.SelectedEnemyCard.debuffValueHeal.ToString(),
                            PlayerManager.instance.SelectedEnemyCard.ElementeumStatus,
                            PlayerManager.instance.SelectedEnemyCard.companionIDs,
                            DiceManager.instance.playerDiceNum.ToString(),
                            PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(),
                            PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString(),
                            PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString(),
                            PlayerManager.instance.SelectedEnemyCard.isSpActive,
                            "",
                            PlayerManager.instance.SelectedEnemyCard.MaxHeartPoint.ToString(),
                            PlayerManager.instance.SelectedHealEnemyCard.MaxHeartPoint.ToString(),

                            PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                            SelectPowerManager.instance.selectedPowerName,
                            PlayerManager.instance.SelectedCard.CardDetail.disrupt.ToString(),
                            "0",
                            PlayerManager.instance.SelectedCard.ElementeumStatus,
                            PlayerManager.instance.SelectedCard.companionIDs,
                            0.ToString(),
                            PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
                            PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
                            PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
                            PlayerManager.instance.SelectedCard.isSpActive,
                            "",
                            PlayerManager.instance.SelectedCard.MaxHeartPoint.ToString(),

                            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
                        );

                    }
                    DiceManager.instance.dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                }
            }

        }
        else
        {
            Debug.Log(Turn);
            Debug.Log(MainTurn);
            Debug.Log(PlayerStats);
            Debug.Log(MeCounter);
            if (Turn == PlayerStats && Turn == MainTurn)
            {
                if (MeCounter <= 0)
                {
                    //StartCoroutine(MainChangeTurn());

                    if (BattleLobbySC.Instance)
                        BattleLobbySC.Instance.SocketMessage("mainturnchange");

                    Debug.Log("Main turn change from turn counter  ");
                }
            }
        }
    }

    public IEnumerator RollDice()
    {
        yield return new WaitForSeconds(.5f);
        dice.GetComponent<SpriteRenderer>().sprite = DiceManager.instance.DiceFronts[DNum - 1];
        // dice.GetComponent<SpriteRenderer>().sprite = DiceFronts[DNum - 1];
        yield return new WaitForSeconds(.3f);
        youtxt.gameObject.SetActive(true);

        youdice.gameObject.SetActive(true);
        youdice.text = DNum.ToString();
        // FinalDiceFront.sprite = DiceFronts[DNum - 1];
        dice.GetComponent<AudioSource>().Pause();


    }

    public IEnumerator RollDice2()
    {
        yield return new WaitForSeconds(.5f);
        dice.GetComponent<SpriteRenderer>().sprite = DiceManager.instance.DiceFronts[DNum2 - 1];
        // dice.GetComponent<SpriteRenderer>().sprite = DiceFronts[DNum - 1];
        yield return new WaitForSeconds(.3f);
        opptext.gameObject.SetActive(true);

        oppdice.gameObject.SetActive(true);

        oppdice.text = DNum2.ToString();
        // FinalDiceFront.sprite = DiceFronts[DNum - 1];

        dice.GetComponent<AudioSource>().Pause();

    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            InGameCards.instance.PauseCountTimer();
            BattleLobbySC.Instance.PlayerLostFocus("1");
        }
        else
        {
            BattleLobbySC.Instance.PlayerLostFocus("2");
            InGameCards.instance.PlayCountTimer();
        }
        //BattleLobbySC.Instance.PlayerLostFocus("1");

        Debug.Log("isPause--" + pause);
    }
    public int EnemySelctedCardCheck = 0;

    public void EnemyComapnionCardAnim(string CompanionCardContractID)
    {
        EnemySelctedCardCheck += 1;
        //Debug.Log(CompanionCardContractID);
        for (int i = 0; i < PlayerCompanionCardsAnim.instance.enemyCompanionCards.Length; i++)
        {
            var EnemyCardObj = PlayerCompanionCardsAnim.instance.enemyCompanionCards[i];

            if (EnemyCardObj.CardDetail.card_contract_id == CompanionCardContractID)
            {
                EnemyCardObj.isSelected = true;
                switch (EnemySelctedCardCheck)
                {
                    case 1:
                        if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Attack)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 4f, 15.0f), 6, new Vector3(0, 0, 0), i);
                            StartCoroutine(PlayAttackParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));
                            Debug.Log(EnemyCardObj.CardDetail.attack);
                            //InGameCards.instance.enemyCompanion.text = playerCompanionCardsAnim.eneComcardTempValue.ToString();
                            //OppAttribValue.text = myAttri.ToString();
                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Defense)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 4f, 15.0f), 6, new Vector3(0, 0, 0), i);
                            StartCoroutine(PlayDefenceParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));
                            Debug.Log(EnemyCardObj.CardDetail.defence);
                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Disrupt)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 4f, 15.0f), 6, new Vector3(0, 0, 0), i);
                            StartCoroutine(PlayDisruptParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));
                            Debug.Log(EnemyCardObj.CardDetail.disrupt);
                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Heal)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(4.5f, 4f, 15.0f), 6, new Vector3(0, 0, 0), i);
                            StartCoroutine(PlayHealParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));
                            Debug.Log(EnemyCardObj.CardDetail.heal);
                        }
                        break;
                    case 2:
                        if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Attack)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 5f, 15.0f), 7, new Vector3(0, 0, 10), i);
                            StartCoroutine(PlayAttackParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));
                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Defense)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 5f, 15.0f), 7, new Vector3(0, 0, 10), i);
                            StartCoroutine(PlayDefenceParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));

                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Disrupt)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 5f, 15.0f), 7, new Vector3(0, 0, 10), i);
                            StartCoroutine(PlayDisruptParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));

                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Heal)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(4.5f, 5f, 15.0f), 7, new Vector3(0, 0, 10), i);
                            StartCoroutine(PlayHealParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));
                        }
                        break;
                    case 3:
                        if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Attack)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 6f, 15.0f), 8, new Vector3(0, 0, 20), i);
                            StartCoroutine(PlayAttackParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));
                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Defense)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 6f, 15.0f), 8, new Vector3(0, 0, 20), i);
                            StartCoroutine(PlayDefenceParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));

                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Disrupt)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(3.7f, 6f, 15.0f), 8, new Vector3(0, 0, 20), i);
                            StartCoroutine(PlayDisruptParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));

                        }
                        else if (EnemyCardObj.CardDetail.strength_1 == AttributeType.Heal)
                        {
                            CardAnimationAndSorting(EnemyCardObj, new Vector3(4.5f, 6f, 15.0f), 8, new Vector3(0, 0, 20), i);
                            StartCoroutine(PlayHealParticles(PlayerCompanionCardsAnim.instance.enemyCompanionCards[i]));

                        }
                        break;
                }

            }

        }
        //PlayerCompanionCardsAnim.instance.enemyCompanionCards

    }
    IEnumerator PlayAttackParticles(CompanionCardObject particleObject)
    {

        yield return new WaitForSeconds(2);
        particleObject.attackParticles.Play();
        yield return new WaitForSeconds(1);
        particleObject.attackParticles.Stop();
    }
    IEnumerator PlayDefenceParticles(CompanionCardObject particleObject)
    {
        yield return new WaitForSeconds(2);
        particleObject.defenceParticles.Play();
        yield return new WaitForSeconds(1);
        particleObject.defenceParticles.Stop();
    }
    IEnumerator PlayDisruptParticles(CompanionCardObject particleObject)
    {
        yield return new WaitForSeconds(2);
        particleObject.disruptParticles.Play();
        yield return new WaitForSeconds(1);
        particleObject.disruptParticles.Stop();
    }
    IEnumerator PlayHealParticles(CompanionCardObject particleObject)
    {
        yield return new WaitForSeconds(2);
        particleObject.HealParticles.Play();
        yield return new WaitForSeconds(1);
        particleObject.HealParticles.Stop();
    }

    public void CardAnimationAndSorting(CompanionCardObject EnemyCardObj, Vector3 Moveanim, int sortingOrder, Vector3 particleRotation, int i)
    {
        EnemyCardObj.transform.DOMove(Moveanim, 1);
        EnemyCardObj.SortingOrder(sortingOrder);

        PlayerCompanionCardsAnim.instance.enemyCompanionCards[i].attackParticles.transform.Rotate(particleRotation);
        //StartCoroutine(PlayAttackParticles(EnemyCardObj));
    }


    public void revertAllAttributeVisualsValue()
    {
        companion.text = "0";
        Dice.text = "0";
        Attribute.text = "0";
        Elemtium.text = "0";
        elem = 0;
        _dice = 0;
        attri = 0;
        comp = 0;

        enemyElemtium.text = "0";
        enemyDice.text = "0";
        enemeyAttribute.text = "0";
        enemyCompanion.text = "0";
        enemeyElem = 0;
        enemeyDice = 0;
        enemeyAttri = 0;
        enemyComp = 0;

        MyAttribValue.text = 0.ToString();
        OppAttribValue.text = 0.ToString();
    }

    
}