﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public GameObject child;
    public Vector3 scale;
    private Vector3 intialScale;
    private void Awake()
    {
        intialScale = child.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        child.transform.localScale = intialScale;
    }
    public void _OnEnter()
    {
        child.transform.localScale = scale;
    }

    public void _OnExit()
    {
        child.transform.localScale = intialScale;
    }
}
