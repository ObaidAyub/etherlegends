﻿using UnityEngine;
using UnityEngine.UI;

using TMPro;
using System.Collections;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine.Networking;
using System.Linq;

public class  CardObject : MonoBehaviour
{
    public static CardObject instance;

    public int cardNumber;

    public Card CardDetail;
    public GameObject txt_CardLvl, txt_Attack, txt_Defense, txt_Disrupt, txt_Heal, txt_CardName, txt_Life, txt_Level, txt_CardNumber;
    public SpriteRenderer CardImage;

    public Canvas canvas;



    public Animator powerAnim;

    //[HideInInspector]
    public Animator MainAnimator;//TODO: Assign while instantiating prefabs.
    public string msg;
    //[HideInInspector]
    [HideInInspector]
    public bool isPowerBtnShow = false;
    public int MaxHeartPoint;
    public GameObject DisableCanvas;

    [Header("Elementeum")]
    [Space(2)]
    public Animator elementeum;
    public Elementium[] elementeumChild;

    //public DigitalRuby.LightningBolt.LightningBoltScript[] PlayerManager.instance. elementeumParticle;

    public GameObject ElemtObj;
    public TextMeshProUGUI ElemtNo;

    //public int UsingElementeum;


    public int Cur_Elemt;
    public int remain_Elemt;
    public bool isDisable;

    public GameObject HeartPointObj;
    public TextMeshProUGUI HeartPointAdded;


    public GameObject DestPanel;

    public SpriteRenderer[] StrengthAttributes;

    [Header("Special Ability")]
    public GameObject SpAbilityObj;
    public int Spcount;
    public SpAbilityInfo SpAbility1, SpAbility2;

    public Sprite[] SpAbilitySprites;
    public bool isSpAbility;
    public AttributeType SpAbilityUsed = AttributeType.None;
    public GameObject SpAbilityParticle1, SpAbilityParticle2;

    public bool CurUnlocked, isUseSpAbility, isSpActive;

    Vector3 OldPosSp1, OldPosSp2;

    GameObject UsingAbility;


    public bool DestroyedCard;

    int DisableInt = 0;

    //string Message = "";
    //CardStrengthType Fdata = CardStrengthType.None;

    Queue<KeyValuePair<string, object>> MessageQueue = new Queue<KeyValuePair<string, object>>();


    [HideInInspector]
    public PlayerCompanionCardsAnim playerCompanionCardsAnim;

    [Header("Debuff attributes")]
    public int debuffValueAttack = 0;
    public int debuffValueHeal = 0;
    public Transform debuffImage, debuffHealImage;
    public TextMeshProUGUI debuffAttack, debuffHeal;
    public bool debuffAtk, debuffHl;

    [Header("Special ability Animator")]
    public GameObject[] coins;

    [Space(20)]
    [Header("Attribute animation")]
    public Button BtnAttack;
    public Button BtnHeal, BtnDefense, BtnDisrupt;


    private void Awake()
    {
        //PlayerManager.instance. animatedHeart.gameObject.SetActive(false);
        //PlayerSpAbiAnim();
        //EnemySpAbiAnim();
        if (instance == null) instance = this;
        OldPosSp1 = SpAbility1.transform.localPosition;
        OldPosSp2 = SpAbility2.transform.localPosition;

        playerCompanionCardsAnim = PlayerCompanionCardsAnim.instance;
        MainAnimator = playerCompanionCardsAnim.GetComponent<Animator>();

        //    public Button atk;
        //public Button heal, distrupt, def;
        if (!IsEnemyCard)
        {


            BtnAttack.onClick.RemoveAllListeners();
            BtnAttack.onClick.AddListener(() => SelectPowerManager.instance.SelectedPowerName(AttributeType.Attack));

            BtnHeal.onClick.RemoveAllListeners();
            BtnHeal.onClick.AddListener(() => SelectPowerManager.instance.SelectedPowerName(AttributeType.Heal));

            BtnDisrupt.onClick.RemoveAllListeners();
            BtnDisrupt.onClick.AddListener(() => SelectPowerManager.instance.SelectedPowerName(AttributeType.Disrupt));

            BtnDefense.onClick.RemoveAllListeners();
            BtnDefense.onClick.AddListener(() => SelectPowerManager.instance.SelectedPowerName(AttributeType.Defense));
            //SpAbility1, SpAbility2

            var btn = SpAbility1.GetComponentInChildren<Button>();
            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(() => UseSpeciaAbility(btn.transform.parent.gameObject, SpAbility1.attributeType));

            btn = SpAbility2.GetComponentInChildren<Button>();

            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(() => UseSpeciaAbility(btn.transform.parent.gameObject, SpAbility2.attributeType));
        }
        else
        {
            BtnAttack.gameObject.GetComponent<Image>().raycastTarget = false;
            BtnHeal.gameObject.GetComponent<Image>().raycastTarget = false;
            BtnDefense.gameObject.GetComponent<Image>().raycastTarget = false;
            BtnDisrupt.gameObject.GetComponent<Image>().raycastTarget = false;

            var btn = SpAbility1.GetComponentInChildren<Button>();
            btn.gameObject.GetComponent<Image>().raycastTarget = false;

            btn = SpAbility2.GetComponentInChildren<Button>();
            btn.gameObject.GetComponent<Image>().raycastTarget = false;
        }
    }  


        //UseSpeciaAbility/

    
    //private void Start()
    //{
    //     StartCoroutine(PlayerSpecialAbilityAnimation());
    //    StartCoroutine(RevertEnemySpecialAbilityAnimation());
    //}

    public void ApplyDamage(int hitPt)
    {
        hitPt = Mathf.Abs(hitPt);


        if (hitPt >= this.CardDetail.heart)
        {
            this.CardDetail.heart = 0;
            HeartPointAdded.text = "-" + this.CardDetail.heart;
            HeartPointObj.SetActive(true);

            txt_Life.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.heart.ToString());
            isDisable = true;
            canvas.gameObject.SetActive(false);

            this.GetComponent<DissoveEffectRunner>().StartBurningEffect(() =>
            {
                DestroyedCard = true;
                DestPanel.SetActive(true);
                DestPanel.GetComponent<DissoveEffectRunner>().StartApperaing();
                setCardDisable();
            });
            if (!IsEnemyCard)
            {

                PlayerManager.instance.animatedHeartPoint.text = this.CardDetail.heart.ToString();
            }
            else
            {
                PlayerManager.instance.animatedHeartPointEnemy.text = this.CardDetail.heart.ToString();

            }


            return;
        }

        if (this.CardDetail.heart > 0)
        {
            this.CardDetail.heart -= hitPt;

            HeartPointAdded.text = "-" + hitPt;

            HeartPointObj.SetActive(true);

            StartCoroutine(DisableGameObject(HeartPointObj, 1f));
   
            if (!IsEnemyCard)
            {
                Debug.Log("Player heat value ");

                this.CardDetail.heart = BattleLobbySC.Instance.heartValME;

                if (this.CardDetail.heart > MaxHeartPoint)
                {
                    this.CardDetail.heart = MaxHeartPoint;
                }
                
                txt_Life.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.heart.ToString());
                PlayerManager.instance.heartSlash.gameObject.SetActive(true);
                PlayerManager.instance.animatedHeartPoint.text = this.CardDetail.heart.ToString();

            }
            else
            {
                Debug.Log("Enemy heat value ");

                this.CardDetail.heart = BattleLobbySC.Instance.heartValOpp;

                if (this.CardDetail.heart > MaxHeartPoint)
                {
                    this.CardDetail.heart = MaxHeartPoint;
                }
                
                txt_Life.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.heart.ToString());
                PlayerManager.instance.heartSlashEnemy.gameObject.SetActive(true);
                PlayerManager.instance.animatedHeartPointEnemy.text = this.CardDetail.heart.ToString();

            }
        }

        if (this.CardDetail.heart <= 0)
        {

            isDisable = true;

            DestroyedCard = true;

            DestPanel.SetActive(true);
            Color tmp = this.GetComponent<SpriteRenderer>().color;
            tmp.a = 155f;
            this.GetComponent<SpriteRenderer>().color = tmp;
            canvas.gameObject.SetActive(false);

            setCardDisable();
        }

    }

    public void HideSpecialAbility()
    {
        //Debug.Log("Hide Special Ability");
        SpAbilityUsed = AttributeType.None;
        isUseSpAbility = false;
        SpAbilityObj.SetActive(false);
    }

    public void AddHitPoint(int hitPt)
    {

        hitPt = Mathf.Abs(hitPt);

        Debug.Log("Hit point added " +hitPt);

        //int CurHitPt = hitPt + this.CardDetail.heart;

        //if (CurHitPt >= MaxHeartPoint)
        //{
        //    hitPt = (MaxHeartPoint - this.CardDetail.heart);
        //    this.CardDetail.heart = MaxHeartPoint;
        //}
        //else
        //{
        //    this.CardDetail.heart += hitPt;
        //}

        HeartPointAdded.text = "+" + hitPt;
        HeartPointObj.SetActive(true);

        StartCoroutine(DisableGameObject(HeartPointObj, 1f));

        this.CardDetail.heart = BattleLobbySC.Instance.healCardHeartVal;

        if (this.CardDetail.heart > MaxHeartPoint)
        {
            this.CardDetail.heart = MaxHeartPoint;
        }

        txt_Life.GetComponent<TextMeshProUGUI>().text = (this.CardDetail.heart.ToString());

        if (!IsEnemyCard)
        {
            PlayerManager.instance.animatedHeartPoint.text = this.CardDetail.heart.ToString();
        }
        else
        {
            PlayerManager.instance.animatedHeartPointEnemy.SetText(this.CardDetail.heart.ToString());
        }


        Debug.Log(" Heat point " + this.CardDetail.heart);



    }
    //public void EnemyAddHitPoint(int hitPt)
    //{

    //    hitPt = Mathf.Abs(hitPt);


    //    int CurHitPt = hitPt + this.CardDetail.heart;

    //    if (CurHitPt >= MaxHeartPoint)
    //    {
    //        hitPt = (MaxHeartPoint - this.CardDetail.heart);
    //        this.CardDetail.heart = MaxHeartPoint;   
    //    }
    //    else
    //    {
    //        this.CardDetail.heart += hitPt;
    //    }
    //    HeartPointAdded.text = "+" + hitPt;
    //    HeartPointObj.SetActive(true);
    //    StartCoroutine(DisableGameObject(HeartPointObj, 1f));
    //    txt_Life.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.heart.ToString());
    //    PlayerManager.instance.animatedHeartPointEnemy.SetText(this.CardDetail.heart.ToString());
    //}

    IEnumerator DisableGameObject(GameObject Obj, float Tim)
    {
        yield return new WaitForSeconds(Tim);
        Obj.SetActive(false);
    }

    public void setCardDisable()
    {
        canvas.gameObject.SetActive(false);
        DestPanel.SetActive(true);
        DestroyedCard = true;
    }

    public void SetDisableTemp() //Temperory Disable Card When it Disrupt Enemy Heal
    {

        //Debug.Log("Disable");
        DisableCanvas.SetActive(true);
        isDisable = true;
        DisableInt = 1;
    }

    public void CheckCardDisablity()
    {
        //        Debug.Log("Set Card Disable"+ DisableInt);

        if (DisableInt > 1)
        {
            SetEnable();
        }
        else if (DisableInt == 1)
        {
            DisableCanvas.SetActive(true);
            isDisable = true;
            DisableInt += 1;
        }
    }

    public void SetEnable() // Set Enable Card To Use After Disrupt
    {
        DisableCanvas.SetActive(false);
        isDisable = false;
        DisableInt = 0;
    }

    public void ResetAttributeValue() //AfterAttack Reset Attribute Value
    {
        
        if (IsEnemyCard)
        {
            for (int i = 0; i < BattleLobbySC.Instance.Finaldata.opp_user_data.best_cards.Count; i++)
            {
                if (this.CardDetail.card_contract_id == BattleLobbySC.Instance.Finaldata.opp_user_data.best_cards[i].card_contract_id)
                {
                    this.CardDetail.attack = int.Parse(BattleLobbySC.Instance.Finaldata.opp_user_data.best_cards[i].card_attack);
                    this.CardDetail.defence = int.Parse(BattleLobbySC.Instance.Finaldata.opp_user_data.best_cards[i].card_defence);
                    this.CardDetail.disrupt = int.Parse(BattleLobbySC.Instance.Finaldata.opp_user_data.best_cards[i].card_disrupt);
                    this.CardDetail.heal = int.Parse(BattleLobbySC.Instance.Finaldata.opp_user_data.best_cards[i].card_heal);
                }
            }
            txt_Attack.GetComponent<TextMeshProUGUI>().text     = this.CardDetail.attack.ToString();
            txt_Defense.GetComponent<TextMeshProUGUI>().text    = this.CardDetail.defence.ToString();
            txt_Disrupt.GetComponent<TextMeshProUGUI>().text    = this.CardDetail.disrupt.ToString();
            txt_Heal.GetComponent<TextMeshProUGUI>().text       = this.CardDetail.heal.ToString();
        }
        else
        {
            for (int i = 0; i < BattleLobbySC.Instance.Finaldata.user_data.best_cards.Count; i++)
            {
                if (this.CardDetail.card_contract_id == BattleLobbySC.Instance.Finaldata.user_data.best_cards[i].card_contract_id)
                {
                    this.CardDetail.attack = int.Parse(BattleLobbySC.Instance.Finaldata.user_data.best_cards[i].card_attack);
                    this.CardDetail.defence = int.Parse(BattleLobbySC.Instance.Finaldata.user_data.best_cards[i].card_defence);
                    this.CardDetail.disrupt = int.Parse(BattleLobbySC.Instance.Finaldata.user_data.best_cards[i].card_disrupt);
                    this.CardDetail.heal = int.Parse(BattleLobbySC.Instance.Finaldata.user_data.best_cards[i].card_heal);
                }
            }
        txt_Attack.GetComponent<TextMeshProUGUI>().text =   this.CardDetail.attack.ToString();
        txt_Defense.GetComponent<TextMeshProUGUI>().text =  this.CardDetail.defence.ToString();
        txt_Disrupt.GetComponent<TextMeshProUGUI>().text =  this.CardDetail.disrupt.ToString();
        txt_Heal.GetComponent<TextMeshProUGUI>().text =     this.CardDetail.heal.ToString();
        }

        for (int i = 0; i < Cur_Elemt; i++)
        {
            //elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
            elementeumChild[i].gameObject.SetActive(true);
            //elementeum.transform.GetChild(i).GetChild(0).transform.position = new Vector3(0, 0, 0);



        }
        HideElementeum();
    }

    public void TieReset() //AfterAttack Reset Attribute Value
    {
        txt_Attack.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.attack.ToString());
        txt_Defense.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.defence.ToString());
        txt_Disrupt.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.disrupt.ToString());
        txt_Heal.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.heal.ToString());

        if (PlayerManager.instance.SelectedCard)
        {
            if (PlayerManager.instance.SelectedCard.UsingElementeum > 0)
            {
                InGameCards.instance.RefreshMyAttribValue(PlayerManager.instance.SelectedCard.UsingElementeum);
            }

        }

        if (PlayerManager.instance.SelectedEnemyCard)
        {
            if (PlayerManager.instance.SelectedEnemyCard.UsingElementeum > 0)
            {
                InGameCards.instance.RefreshOppAttribValue();
            }
        }
    }


    public void UnsetSpecialAbilityPosition()
    {
        SpAbility2.transform.DOLocalMove(OldPosSp2, 0.2f);
        SpAbility2.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
        SpAbility1.transform.DOLocalMove(OldPosSp1, 0.2f);
        SpAbility1.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
        SpAbilityParticle1.SetActive(false);
        SpAbilityParticle2.SetActive(false);
    }

    public void UseSpeciaAbility(GameObject AbilityBtn, AttributeType abilityType)
    {
        Debug.Log(this.IsEnemyCard + " " + this.CardDetail.name);
        if (this.IsEnemyCard)
        {
            return;
        }
        SyncUserSpecialAbility(true, AbilityBtn, abilityType);
    }

    public void SyncUserSpecialAbility(bool CanSend, GameObject AbilityBtn, AttributeType abilityType)
    {


        if (!PlayerManager.instance.CanClick)
        {
            return;
        }
        if (InGameCards.instance.MeCounter < 3)
        {
            return;

        }

        if (PlayerManager.instance.SelectedCard.UsingElementeum > 0)
        {

            var eleObj = PlayerManager.instance.SelectedCard.elementeumChild;
            for (int i = 0; i < eleObj.Length; i++)
            {
                if (!eleObj[i].selectEle)
                {
                    PlayerManager.instance.SelectedCard.CancelElementeum(eleObj[i].elemNum, eleObj[i].eName);
                    eleObj[i].transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                    eleObj[i].GetComponent<RectTransform>().DOAnchorPos(new Vector3(0, 0, 0), 0.5f);
                    eleObj[i].selectEle = true;
                }
            }

        }


        if (SelectPowerManager.instance.selectedEnemyPowerName != AttributeType.None)
        {
            Debug.Log("Enemy Use Special Ability of :" + SelectPowerManager.instance.selectedEnemyPowerName);



            switch (SelectPowerManager.instance.selectedEnemyPowerName)
            {
                case AttributeType.Attack:

                    if (SpAbility1.attributeType == AttributeType.Defense)
                    {
                        SpAbility1.gameObject.SetActive(true);
                        SpAbility2.gameObject.SetActive(false);
                    }
                    else if (SpAbility2.attributeType == AttributeType.Defense)
                    {
                        SpAbility1.gameObject.SetActive(false);
                        SpAbility2.gameObject.SetActive(true);
                    }

                    if (abilityType != AttributeType.Defense)
                    {
                        return;
                    }

                    break;

                case AttributeType.Defense:

                    if (SpAbility1.attributeType == AttributeType.Attack)
                    {
                        SpAbility1.gameObject.SetActive(true);
                        SpAbility2.gameObject.SetActive(false);
                    }
                    else if (SpAbility2.attributeType == AttributeType.Attack)
                    {
                        SpAbility1.gameObject.SetActive(false);
                        SpAbility2.gameObject.SetActive(true);
                    }
                    if (abilityType != AttributeType.Attack)
                    {
                        return;
                    }

                    break;

                case AttributeType.Disrupt:

                    if (SpAbility1.attributeType == AttributeType.Heal)
                    {
                        SpAbility1.gameObject.SetActive(true);
                        SpAbility2.gameObject.SetActive(false);
                    }
                    else if (SpAbility2.attributeType == AttributeType.Heal)
                    {
                        SpAbility1.gameObject.SetActive(false);
                        SpAbility2.gameObject.SetActive(true);
                    }

                    if (abilityType != AttributeType.Heal)
                    {
                        return;
                    }

                    break;

                case AttributeType.Heal:
                    if (SpAbility1.attributeType == AttributeType.Disrupt)
                    {
                        SpAbility1.gameObject.SetActive(true);
                        SpAbility2.gameObject.SetActive(false);
                    }
                    else if (SpAbility2.attributeType == AttributeType.Disrupt)
                    {
                        SpAbility1.gameObject.SetActive(false);
                        SpAbility2.gameObject.SetActive(true);
                    }
                    if (abilityType != AttributeType.Disrupt)
                    {
                        return;
                    }
                    break;

            }


            if (isSpActive)
            {
                Debug.Log("Player cancle cancle Special ability ");
                RevertPlayerSpecialAbilityAnimation();
                PlayerManager.instance.IsMeSp = false;
                PlayerManager.instance.UnsetSp(true);
                PlayerManager.instance.CanClickOnBG = false;
                PlayerManager.instance.CanClickOnMeCard = true;

                SpAbility1.gameObject.SetActive(true);

                if (Spcount == 2)
                    SpAbility2.gameObject.SetActive(true);

                SpAbility1.transform.DOLocalMove(OldPosSp1, 0.2f);

                isSpActive = false;
                SpAbility1.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
                SpAbility2.transform.DOLocalMove(OldPosSp2, 0.2f);
                SpAbility2.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
                SpAbilityParticle1.SetActive(false);
                SpAbilityParticle2.SetActive(false);

                Cur_Elemt = CardDetail.elementeum;
                Debug.Log(Cur_Elemt);
                isUseSpAbility = false;


                for (int i = 0; i < Cur_Elemt; i++)
                {
                    elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                }

                SelectPowerManager.instance.PlayerTwo = true;

                if (BattleLobbySC.Instance && CanSend)
                    BattleLobbySC.Instance.CancelSpecialAbility(this.CardDetail.card_contract_id,SelectPowerManager.instance.selectedPowerName);

                //InGameCards.instance.RemoveMyAttribValue(CardDetail.elementeum);

                CancelElementeum(CardDetail.elementeum, "All");
                isSpAbility = true;

                if (PlayerManager.instance.getSpOpp())
                {
                    InGameCards.instance.RefreshMyAttribValue();
                    InGameCards.instance.RefreshOppAttribValue(true);
                }
                else
                {
                    InGameCards.instance.RemoveOppAttribValue(false);
                }
                //BattleLobbySC.Instance.LastAction = "me:cancelspecial:" + abilityType;
                for (int i = 0; i < this.CardDetail.elementeum; i++)
                {
                    elementeumChild[i].canClickElementeum = true;
                }
            }
            else // defence ability is use from here 
            {
                Debug.Log("Defence Special ability ");

                if (abilityType == AttributeType.Attack || abilityType == AttributeType.Heal)
                {
                    return;
                }

                // add some condition so on the heal the spanim are not call for heal

                PlayerSpAbiAnim();


                PlayerManager.instance.setSp(true, abilityType);

                PlayerManager.instance.CanClickOnBG = false;

                PlayerManager.instance.CanClickOnMeCard = false;

                isSpActive = true;

                AbilityBtn.transform.DOLocalMove(new Vector3(0, -0.8f, -3.8f), 0.3f);
                AbilityBtn.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.3f);


                

                if (abilityType == SpAbility2.attributeType)
                {
                    SpAbility1.gameObject.SetActive(false);
                    SpAbilityParticle1.SetActive(true);
                }
                else
                {
                    SpAbility2.gameObject.SetActive(false);
                    SpAbilityParticle2.SetActive(true);
                }

                isSpAbility = false;
                SpAbilityUsed = abilityType;

                isUseSpAbility = true;
                Debug.Log(this.CardDetail.card_contract_id);


                SelectPowerManager.instance.SelectedPowerName(abilityType); // Defence and disrupt attribute selection call from here 

                if (BattleLobbySC.Instance && CanSend)
                    BattleLobbySC.Instance.UseSpecialAbility(this.CardDetail.card_contract_id, abilityType);

                //  InGameCards.instance.RemoveOppAttribValue(0, true);

                SyncUseElementeum(true, CardDetail.elementeum, "All");

                //RemoveElementeum(this.CardDetail.elementeum);

                if (PlayerManager.instance.getSpOpp())
                {
                    InGameCards.instance.RefreshMyAttribValue();
                    InGameCards.instance.RefreshOppAttribValue(false);
                }
                else
                {
                    InGameCards.instance.RemoveOppAttribValue(true);
                }


                //BattleLobbySC.Instance.LastAction = "me:usespecial:" + abilityType.ToString();
                //InGameCards.instance.AddMyAttribValue(CardDetail.elementeum);
                for (int i = 0; i < this.CardDetail.elementeum; i++)
                {

                    elementeumChild[i].canClickElementeum = false;
                }
            }

        }
        else
        {



            switch (SelectPowerManager.instance.selectedPowerName)
            {
                case AttributeType.Attack:
                    if (abilityType != AttributeType.Attack)
                    {
                        return;
                    }
                    break;

                case AttributeType.Defense:
                    if (abilityType != AttributeType.Defense)
                    {
                        return;
                    }

                    break;

                case AttributeType.Disrupt:
                    if (abilityType != AttributeType.Disrupt)
                    {
                        return;
                    }

                    break;

                case AttributeType.Heal:

                    if (abilityType != AttributeType.Heal)
                    {
                        return;
                    }
                    break;
            }

            if (isSpActive)  // from this condition player use and cancle special special ability 
            {
                //StartCoroutine(RevertPlayerSpecialAbilityAnimation());
                Debug.Log("Yahan sa special ability cancle ho rahe ha ");
                RevertPlayerSpecialAbilityAnimation();
                PlayerManager.instance.IsMeSp = false;

                PlayerManager.instance.UnsetSp(true);
                PlayerManager.instance.CanClickOnBG = true;
                PlayerManager.instance.CanClickOnMeCard = true;
                SpAbility1.gameObject.SetActive(true);

                if (Spcount == 2)
                {
                    SpAbility2.gameObject.SetActive(true);
                    SpAbility2.transform.DOLocalMove(OldPosSp2, 0.2f);
                    SpAbility2.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
                }

                isSpActive = false;
                isSpAbility = true;
                isUseSpAbility = false;
                SpAbility1.transform.DOLocalMove(OldPosSp1, 0.2f);
                SpAbility1.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
                SpAbilityParticle1.SetActive(false);
                SpAbilityParticle2.SetActive(false);
                Cur_Elemt = CardDetail.elementeum;

                for (int i = 0; i < Cur_Elemt; i++)
                {
                    elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                }
                if (BattleLobbySC.Instance && CanSend)
                    BattleLobbySC.Instance.CancelSpecialAbility(this.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName);

                //BattleLobbySC.Instance.LastAction = "me:cancelspecial:" + abilityType.ToString();
                CancelElementeum(CardDetail.elementeum, "All");
                InGameCards.instance.RefreshMyAttribValue(0);
                for (int i = 0; i < this.CardDetail.elementeum; i++)
                {
                    //elementeumChild[i].GetComponent<Button>().interactable = false;
                    elementeumChild[i].canClickElementeum = true;
                }
            }
            else
            {
                if (abilityType == AttributeType.Defense || abilityType == AttributeType.Disrupt)
                {
                    return;
                }
                Debug.Log("Player use special ability");
                if (abilityType == AttributeType.Attack)
                {
                    PlayerSpAbiAnim();
                }
                PlayerManager.instance.setSp(true, abilityType);

                PlayerManager.instance.CanClickOnBG = true;
                PlayerManager.instance.CanClickOnMeCard = true;

                isSpActive = true;

                AbilityBtn.transform.DOLocalMove(new Vector3(0, -0.8f, -3.8f), 0.3f);

                AbilityBtn.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.3f);

                if (AbilityBtn == SpAbility2)
                {
                    SpAbility1.gameObject.SetActive(false);
                    SpAbilityParticle1.SetActive(true);
                }
                else
                {
                    SpAbility2.gameObject.SetActive(false);
                    SpAbilityParticle2.SetActive(true);
                }

                isSpAbility = false;

                SpAbilityUsed = abilityType;

                isUseSpAbility = true;

                SelectPowerManager.instance.SelectedPowerName(abilityType);  /// heal and defence ability attribute is call from here 

                if (BattleLobbySC.Instance && CanSend)
                    BattleLobbySC.Instance.UseSpecialAbility(this.CardDetail.card_contract_id, abilityType);

                //BattleLobbySC.Instance.LastAction = "me:usespecial:" + abilityType.ToString();
                SyncUseElementeum(true, PlayerManager.instance.SelectedCard.CardDetail.elementeum, "All");
                //RemoveElementeum(this.CardDetail.elementeum);
                // InGameCards.instance.AddMyAttribValue(CardDetail.elementeum);
                for (int i = 0; i < PlayerManager.instance.SelectedCard.CardDetail.elementeum; i++)
                {
                    //elementeumChild[i].GetComponent<Button>().interactable = false;
                    elementeumChild[i].canClickElementeum = false;
                }
            }
        }
    }




    public void UseSpecialAbilityRPC(AttributeType AbilityName)
    {

        Debug.Log("Special Ability RPC---" + AbilityName);
        //Fdata = AbilityName;
        //Message = "specialability";
        MessageQueue.Enqueue(new KeyValuePair<string, object>("specialability", AbilityName));
    }

    public void CancelSpecialAbilityRPC()
    {
        //Message = "specialabilitycancel";
        MessageQueue.Enqueue(new KeyValuePair<string, object>("specialabilitycancel", null));
    }

    private void Update()
    {
        if (MessageQueue.Any()) {
            var msg = MessageQueue.Dequeue();
            switch (msg.Key) {
                case "specialability": {

                        Debug.Log("Update Message SpecialAbility" + isSpActive);

                        AttributeType AbilityName = (AttributeType)msg.Value;

                        GameObject AbilityObj;
                        
                        if (SpAbility1.attributeType == AbilityName)
                        {
                            AbilityObj = SpAbility1.gameObject;
                        }
                        else
                        {
                            AbilityObj = SpAbility2.gameObject;
                        }


                        if (isSpActive)
                        {
                            //StartCoroutine(EnemySpecialAbilityAnimation());
                            Debug.Log("Specail ability is cancle for RPC  " + SelectPowerManager.instance.selectedEnemyPowerName);
                            PlayerManager.instance.UnsetSp(false);
                            Debug.Log("Opponeent use IsSpActive--" + isSpActive);
                            SpAbility1.gameObject.SetActive(true);

                            if (Spcount == 2)
                                SpAbility2.gameObject.SetActive(true);

                            SpAbility1.transform.DOLocalMove(OldPosSp1, 0.2f);
                            isSpActive = false;
                            SpAbility1.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);

                            if (Spcount == 2)
                            {
                                SpAbility2.transform.DOLocalMove(OldPosSp2, 0.2f);
                                SpAbility2.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
                            }

                           
                            SpAbilityParticle1.SetActive(false);
                            SpAbilityParticle2.SetActive(false);
                            Cur_Elemt = this.CardDetail.elementeum;
                            isUseSpAbility = false;

                            for (int i = 0; i < Cur_Elemt; i++)
                            {
                                elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                            }

                            CancelElementeumRpc(this.CardDetail.elementeum, "All");

                            InGameCards.instance.RemoveOppElemnteumValue(CardDetail.elementeum);
                            //   InGameCards.instance.RemoveMyAttribValue(0, false);
                        }

                        else //special abilitu attack is used by the player 
                        {
                            Debug.Log("OPP USE SPECIAL ABILITY " + AbilityName);
                            if (AbilityName != AttributeType.Heal)
                            {

                                EnemySpAbiAnim();
                            }
                            PlayerManager.instance.IsOppSp = true;
                            PlayerManager.instance.setSp(false, AbilityName);

                            isSpActive = true;
                            AbilityObj.transform.DOLocalMove(new Vector3(0, -0.8f, -3.8f), 0.3f);
                            AbilityObj.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.3f);

                            InGameCards.instance.OppElement = 0;
                            //if (AbilityObj == SpAbility2)
                            //{
                            //    SpAbility1.gameObject.SetActive(false);
                                
                            //}

                            //else
                            //{
                            //    SpAbility2.gameObject.SetActive(false);
                                
                            //}


                            if (SpAbility1.attributeType == AbilityName)
                            {
                                //Debug.Log("This card sp1 ability is " + powerName);
                                SpAbility1.gameObject.SetActive(true);
                                SpAbility2.gameObject.SetActive(false);
                                SpAbilityParticle1.SetActive(true);
                            }
                            else if (SpAbility2.attributeType == AbilityName)
                            {
                                SpAbility1.gameObject.SetActive(false);
                                SpAbility2.gameObject.SetActive(true);
                                SpAbilityParticle2.SetActive(true);
                            }
                            else
                            {
                                SpAbility1.gameObject.SetActive(false);
                                SpAbility2.gameObject.SetActive(false);
                            }
                            

                            isSpAbility = false;

                            // SpAbilityUsed =
                            if (!System.Enum.TryParse(AbilityObj.name.ToLower(), out SpAbilityUsed))
                                SpAbilityUsed = AttributeType.None;

                            isUseSpAbility = true;


                            SelectPowerManager.instance.EnemyselectedCardAnimator = powerAnim;
                            Debug.Log("Select enememy power name " + AbilityName + "  " + PlayerManager.instance.SelectedEnemyCard.name);
                            SelectPowerManager.instance.SlectedEnemyPowerName(AbilityName, 1);

                            PlayerManager.instance.SelectedEnemyCard.SpAbilityObj.SetActive(true);
                            Debug.Log(PlayerManager.instance.getSpme());
                            if (PlayerManager.instance.getSpme())
                            {
                                InGameCards.instance.RefreshMyAttribValue();
                                InGameCards.instance.RefreshOppAttribValue(true);
                            }
                            else
                            {
                                InGameCards.instance.RefreshMyAttribValue();

                                //InGameCards.instance.AddOppAttribValue(CardDetail.elementeum);
                                //InGameCards.instance.RemoveMyAttribValue(0, true);
                            }

                            //InGameCards.instance.enemyElemtium.text = CardDetail.elementeum.ToString();
                            //InGameCards.instance.OppAttribValue.text = (int.Parse(InGameCards.instance.OppAttribValue.text) - int.Parse(debuffAttack.text)).ToString();

                        }
                    }
                    break;
                case "specialabilitycancel": {

                        Debug.LogError("specialabilitycancel");
                        for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
                        {
                            PlayerManager.instance.enemyCardsObj[i].RevertEnemySpAbiAnim();
                        }


                        SpAbility1.gameObject.SetActive(true);
                        //PlayerManager.instance.UnsetSp(true);

                        if (Spcount == 2)
                            SpAbility2.gameObject.SetActive(true);

                        PlayerManager.instance.IsOppSp = false;

                        SpAbility1.transform.DOLocalMove(OldPosSp1, 0.2f);

                        isSpActive = false;

                        isUseSpAbility = false;

                        SpAbility1.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);

                        if (Spcount == 2)
                        {
                            SpAbility2.transform.DOLocalMove(OldPosSp2, 0.2f);
                            SpAbility2.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
                        }

                        SpAbilityParticle1.SetActive(false);
                        SpAbilityParticle2.SetActive(false);

                        Cur_Elemt = this.CardDetail.elementeum;
                        //remain_Elemt -= this.CardDetail.elementeum;
                        for (int i = 0; i < Cur_Elemt; i++)
                        {
                            elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                        }

                        //CancelElementeumRpc(this.CardDetail.elementeum, "All");


                        InGameCards.instance.RemoveMyAttribValue();
                    }
                    break;

            }
        }

       

        //Message = "";
        //Fdata = CardStrengthType.None;
    }
    private Sprite cardImage;
    public Texture2D textureImg;
    private void ImageFromDataPath(string CardContractId)
    {
        if (string.IsNullOrEmpty(CardContractId)) return;
        string path = Application.persistentDataPath + "/GameCards" + "/" + CardContractId + ".png";
        string ImgLink = CardDetail.s3_Image;
        string card_contract_id = CardContractId;
        string DIRPath = Path.Combine(Application.persistentDataPath, "GameCards");
        StartCoroutine(setimage(ImgLink, card_contract_id, DIRPath, path));

    }
    IEnumerator setimage(string ImgLink, string card_contract_id, string DIRPath, string FilePath, System.Action onComplete = null)
    {

        if (System.IO.File.Exists(FilePath))
        {
            byte[] bytes = System.IO.File.ReadAllBytes(FilePath);
            textureImg = new Texture2D(1, 1);
            textureImg.LoadImage(bytes);

            if (textureImg != null)
            {
                Sprite sprite = Sprite.Create(textureImg, new Rect(0, 0, textureImg.width, textureImg.height), new Vector2(0.5f, 0.5f));
                this.CardImage.sprite = sprite;
                this.CardImage.GetComponent<DissoveEffectRunner>().UpdateTexture();
            }
            onComplete?.Invoke();
            yield break;
        }


        UnityWebRequest www = UnityWebRequestTexture.GetTexture(ImgLink);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            textureImg = new Texture2D(1, 1);
            textureImg = DownloadHandlerTexture.GetContent(www);

            if (!Directory.Exists(DIRPath))
            {

                Directory.CreateDirectory(DIRPath);

                //Debug.Log("Created Dir" + DIRPath);
            }

            byte[] bytes = textureImg.EncodeToPNG();
            File.WriteAllBytes(FilePath, bytes);

            Destroy(textureImg);
            bytes = System.IO.File.ReadAllBytes(FilePath);
            yield return new WaitForSeconds(2);
            textureImg = new Texture2D(1, 1);
            textureImg.LoadImage(bytes);

            if (textureImg != null)
            {
                Sprite sprite = Sprite.Create(textureImg, new Rect(0, 0, textureImg.width, textureImg.height), new Vector2(0.5f, 0.5f));

                cardImage = sprite;
                this.CardImage.sprite = sprite;
                this.CardImage.GetComponent<DissoveEffectRunner>().UpdateTexture();
            }


            www.Dispose();
            bytes = new byte[0];
            System.GC.Collect();

        }
    }

    public void RefreshCardDetails(string CardContractId)//displaying details of card
    {
        ElementeumStatus.Clear();
        companionData.Clear();
        companionIDs.Clear();
        ImageFromDataPath(CardContractId);

        txt_CardLvl.GetComponent<TextMeshProUGUI>().SetText("Level: " + this.CardDetail.level.ToString());
        txt_Attack.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.attack.ToString());
        txt_Defense.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.defence.ToString());
        txt_Disrupt.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.disrupt.ToString());
        txt_Heal.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.heal.ToString());

        txt_CardName.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.name);

        txt_Life.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.heart.ToString());
        txt_Level.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.level.ToString());
        txt_CardNumber.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.number.ToString());

        MaxHeartPoint = this.CardDetail.heart;

        for (int i = 0; i < 5; i++)
        {
            elementeum.transform.GetChild(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < this.CardDetail.elementeum; i++)
        {
            elementeum.transform.GetChild(i).gameObject.SetActive(true);
            elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
        }

        for (int i = 0; i < 4; i++)
        {
            StrengthAttributes[i].gameObject.SetActive(false);
        }

        switch (this.CardDetail.strength_1)
        {
            case AttributeType.Attack:
                StrengthAttributes[0].gameObject.SetActive(true);
                break;

            case AttributeType.Defense:
                StrengthAttributes[1].gameObject.SetActive(true);
                break;

            case AttributeType.Disrupt:
                StrengthAttributes[2].gameObject.SetActive(true);
                break;

            case AttributeType.Heal:
                StrengthAttributes[3].gameObject.SetActive(true);
                break;
        }

        switch (this.CardDetail.strength_2)
        {
            case AttributeType.Attack:
                StrengthAttributes[0].gameObject.SetActive(true);
                break;

            case AttributeType.Defense:
                StrengthAttributes[1].gameObject.SetActive(true);
                break;

            case AttributeType.Disrupt:
                StrengthAttributes[2].gameObject.SetActive(true);
                break;

            case AttributeType.Heal:
                StrengthAttributes[3].gameObject.SetActive(true);
                break;
        }

        if (this.CardDetail.strength_1 != AttributeType.None)
        {

            Spcount = 1;
            //Debug.Log("Strength1");

            switch (this.CardDetail.strength_1)
            {
                case AttributeType.Attack:
                    SpAbility1.gameObject.SetActive(true);
                    SpAbility1.attributeType = this.CardDetail.strength_1;
                    SpAbility1.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[0];
                    break;
                case AttributeType.Defense:
                    SpAbility1.gameObject.SetActive(true);
                    SpAbility1.attributeType = this.CardDetail.strength_1;
                    SpAbility1.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[1];
                    break;
                case AttributeType.Disrupt:
                    SpAbility1.gameObject.SetActive(true);
                    SpAbility1.attributeType = this.CardDetail.strength_1;
                    SpAbility1.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[2];
                    break;
                case AttributeType.Heal:
                    SpAbility1.gameObject.SetActive(true);
                    SpAbility1.attributeType = this.CardDetail.strength_1;
                    SpAbility1.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[3];
                    break;
            }

        }
        else
        {
            SpAbility1.gameObject.SetActive(false);
        }


        if (this.CardDetail.strength_2 != AttributeType.None && this.CardDetail.strength_1 != this.CardDetail.strength_2)
        {
            Spcount = 2;
            //Debug.Log("Strength2");
            switch (this.CardDetail.strength_2)
            {

                case AttributeType.Attack:
                    SpAbility2.gameObject.SetActive(true);
                    SpAbility2.attributeType = this.CardDetail.strength_2;
                    SpAbility2.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[0];
                    break;
                case AttributeType.Defense:
                    SpAbility2.gameObject.SetActive(true);
                    SpAbility2.attributeType = this.CardDetail.strength_2;
                    SpAbility2.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[1];
                    break;
                case AttributeType.Disrupt:
                    SpAbility2.gameObject.SetActive(true);
                    SpAbility2.attributeType = this.CardDetail.strength_2;
                    SpAbility2.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[2];
                    break;
                case AttributeType.Heal:
                    SpAbility2.gameObject.SetActive(true);
                    SpAbility2.attributeType = this.CardDetail.strength_2;
                    SpAbility2.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[3];
                    break;
            }
        }
        else
        {
            Spcount = 1;
            //Debug.Log("SpAbility 2 Disable");
            SpAbility2.gameObject.SetActive(false);
        }



        Cur_Elemt = 0;


        AddElementeum(CardDetail.elementeum);

        GetComponent<BoxCollider2D>().enabled = !IsEnemyCard;

        //AddSpecialAbility
    }



    public bool CheckElementeum()
    {
        int TempElemt = Cur_Elemt + 1;

        if (TempElemt <= CardDetail.elementeum && !isUseSpAbility)
        {
            return true;
        }

        return false;

    }


    public void AddElementeum(int num)
    {
        //Debug.Log("Add Elementeum");

        if (Cur_Elemt < this.CardDetail.elementeum)
        {
            Cur_Elemt += num;
            //Cur_Elemt = this.CardDetail.elementeum; //rm
        }
        else
        {
            CurUnlocked = false;
            return;
        }

        for (int i = 0; i < Cur_Elemt; i++)
        {
            //elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
            elementeumChild[i].gameObject.SetActive(true);
            //elementeumChild[i].GetComponent<Button>().interactable = true;
            //elementeumChild[i].selectEle = true;


        }
        //Debug.Log(Cur_Elemt);
        if (Cur_Elemt == this.CardDetail.elementeum)
        {
            isSpActive = false;
            //Debug.Log("strenght " + this.CardDetail.strength_1);
            //Reset Special Ability Icons To Its original Position
            if (this.CardDetail.strength_1 != AttributeType.None)
            {

                Spcount = 1;
                //Debug.Log("Strength1");
                //Debug.Log("strenght " + this.CardDetail.strength_1);
                switch (this.CardDetail.strength_1)
                {
                    case AttributeType.Attack:
                        SpAbility1.gameObject.SetActive(true);
                        SpAbility1.attributeType = this.CardDetail.strength_1;
                        SpAbility1.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[0];
                        break;
                    case AttributeType.Defense:
                        SpAbility1.gameObject.SetActive(true);
                        SpAbility1.attributeType = this.CardDetail.strength_1;
                        SpAbility1.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[1];
                        break;
                    case AttributeType.Disrupt:
                        SpAbility1.gameObject.SetActive(true);
                        SpAbility1.attributeType = this.CardDetail.strength_1;
                        SpAbility1.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[2];
                        break;
                    case AttributeType.Heal:
                        SpAbility1.gameObject.SetActive(true);
                        SpAbility1.attributeType = this.CardDetail.strength_1;
                        SpAbility1.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[3];
                        break;
                }

            }
            else
            {
                SpAbility1.gameObject.SetActive(false);
            }


            if (this.CardDetail.strength_2 != AttributeType.None && this.CardDetail.strength_1 != this.CardDetail.strength_2)
            {
                Spcount = 2;
                //Debug.Log("Strength2");
                switch (this.CardDetail.strength_2)
                {

                    case AttributeType.Attack:
                        SpAbility2.gameObject.SetActive(true);
                        SpAbility2.attributeType = this.CardDetail.strength_2;
                        SpAbility2.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[0];
                        break;
                    case AttributeType.Defense:
                        SpAbility2.gameObject.SetActive(true);
                        SpAbility2.attributeType = this.CardDetail.strength_2;
                        SpAbility2.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[1];
                        break;
                    case AttributeType.Disrupt:
                        SpAbility2.gameObject.SetActive(true);
                        SpAbility2.attributeType = this.CardDetail.strength_2;
                        SpAbility2.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[2];
                        break;
                    case AttributeType.Heal:
                        SpAbility2.gameObject.SetActive(true);
                        SpAbility2.attributeType = this.CardDetail.strength_2;
                        SpAbility2.transform.GetChild(0).GetComponent<Image>().sprite = SpAbilitySprites[3];
                        break;
                }
            }
            else
            {
                Spcount = 1;
                //Debug.Log("SpAbility 2 Disable");
                SpAbility2.gameObject.SetActive(false);
            }
            StartCoroutine(DelaySpAbMsg());
        }

    }

    IEnumerator DelaySpAbMsg() // Delay special ability message so it won't overlay with other messages
    {
        yield return new WaitForSeconds(1f);
        CurUnlocked = true;

        PlayerManager.instance.TextCanvas.SetActive(true);

        if (IsEnemyCard)
        {
            PlayerManager.instance.TextMessage.text = "Opponent's" + Environment.NewLine +
            "Special Ability Unlocked";
        }
        else
        {
            PlayerManager.instance.TextMessage.text = "Congratulations!!!" + Environment.NewLine +
            "Special Ability Unlocked";
        }
        yield return new WaitForSeconds(1f);
        PlayerManager.instance.TextCanvas.SetActive(false);
        isSpAbility = true;

        if (isSpActive)
        {

            SpAbilityObj.SetActive(true);
        }

    }

    public void RemoveElementeum(int count)
    {


        Debug.Log("Inside Remove Elementeum---" + Cur_Elemt + "------" + count);
        if (Cur_Elemt >= count)
        {
            Cur_Elemt -= count;
            Debug.Log("After Remove Elemt--" + Cur_Elemt);
        }
        for (int i = 0; i < this.CardDetail.elementeum; i++)
        {
            elementeum.transform.GetChild(i).GetChild(0).transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            elementeum.transform.GetChild(i).GetChild(0).GetComponent<RectTransform>().DOAnchorPos(new Vector3(0, 0, 0), 0.5f);
            elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
        }

        for (int i = 0; i < Cur_Elemt; i++)
        {
            elementeum.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
        }
    }



    public void CancelElementeum(int Number, string EleName)
    {


        if (ElementeumStatus.ContainsKey(EleName))
            ElementeumStatus[EleName] = 0;
        else
            ElementeumStatus.Add(EleName, 0);

        SyncCancelElementeum(true, Number, EleName);
    }


    //TODO: Clear this on THIRD place (2 are done in playermanager.cs)
    //When selecting other card. and deselecting this card

    //When Key is not present, it means, elementeum is UNselected (0).
    [SerializeField]
    public Dictionary<string, int> ElementeumStatus = new Dictionary<string, int>();

    public void SyncUseElementeum(bool CanSend, int elemt, string elemName)
    {
        if (!PlayerManager.instance.CanClickOnElemt || SelectPowerManager.instance.selectedPowerName == AttributeType.None && CanSend)
            return;

        if (SelectPowerManager.instance.selectedPowerName == AttributeType.None && CanSend)
            return;



        if (ElementeumStatus.ContainsKey(elemName))
            ElementeumStatus[elemName] = elemt;
        else
            ElementeumStatus.Add(elemName, elemt);



        var playercard = PlayerManager.instance.SelectedCard;

        if (CanSend)
        {
            if (PlayerManager.instance.SelectedEnemyCard != null)
            {
                var enemeyCard = PlayerManager.instance.SelectedEnemyCard;
                switch (SelectPowerManager.instance.selectedPowerName)
                {
                    case AttributeType.Attack:

                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName,
                            "0", debuffValueAttack.ToString(), enemeyCard.CardDetail.card_contract_id, ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                    case AttributeType.Heal:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName,
                            "0", debuffValueHeal.ToString(), enemeyCard.CardDetail.card_contract_id, ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                    default:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName, "0", "0", enemeyCard.CardDetail.card_contract_id, ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                }

            }
            else
            {
                switch (SelectPowerManager.instance.selectedPowerName)
                {
                    case AttributeType.Attack:

                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName, "0", debuffValueAttack.ToString(), "", ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                    case AttributeType.Heal:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName, "0", debuffValueHeal.ToString(), "", ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                    default:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName, "0", "0", "", ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                }
            }

        }

        //BattleLobbySC.Instance.LastAction = "me:elementeum:" + elemt;

        //playercard.UsingElementeum += elemt;
        Debug.Log(UsingElementeum);


        remain_Elemt = playercard.UsingElementeum;

        InGameCards.instance.AddOrRemovePlayerElementeum();//add elementeum from here

        PlayerManager.instance.CanClickOnElemt = false;

        int companioncheck = Cur_Elemt - remain_Elemt;
        Debug.Log(companioncheck);


        if (companioncheck < 2)
        {
            RevertCompanionByElementium(companioncheck);
            return;
        }

        else if (companioncheck < 3)
        {

            RevertCompanionByElementium(companioncheck);
            return;

        }
        else if (companioncheck < 4)
        {
            RevertCompanionByElementium(companioncheck);
            return;


        }
        else if (companioncheck < 5)
        {
            RevertCompanionByElementium(companioncheck);
            return;


        }
    }




    public void SyncCancelElementeum(bool CanSend, int Number, string EleName)
    {
        Debug.Log("cancle elemeneteum ");
        if (!PlayerManager.instance.CanClick || isSpActive)
        {
            if (CanSend)
                return;
        }

        if (remain_Elemt == 0)
        {
            remain_Elemt = 0;
        }
        else
        {

            remain_Elemt -= Number;
        }
        Debug.Log("Cancel ELementeum");

        PlayerManager.instance.CanClickOnElemt = true;
        elementeum.gameObject.SetActive(true);

        //UsingElementeum -= Number;
        CanSend = true;

        var playercard = PlayerManager.instance.SelectedCard;

        if (CanSend)
        {


            if (PlayerManager.instance.SelectedEnemyCard != null)
            {
                var enemeyCard = PlayerManager.instance.SelectedEnemyCard;
                switch (SelectPowerManager.instance.selectedPowerName)
                {
                    case AttributeType.Attack:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName,
                            "0", debuffValueAttack.ToString(), enemeyCard.CardDetail.card_contract_id, ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                    case AttributeType.Heal:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName,
                            "0", debuffValueHeal.ToString(), enemeyCard.CardDetail.card_contract_id, ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                    default:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName,
                            "0", "0", enemeyCard.CardDetail.card_contract_id, ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                }
            }
            else
            {
                switch (SelectPowerManager.instance.selectedPowerName)
                {
                    case AttributeType.Attack:

                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName,
                            "0", debuffValueAttack.ToString(), "", ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                    case AttributeType.Heal:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName,
                            "0", debuffValueHeal.ToString(), "", ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                    default:
                        BattleLobbySC.Instance.ElementeumSelected(playercard.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName,
                            "0", "0", "", ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                        break;
                }
            }

        }


        InGameCards.instance.AddOrRemovePlayerElementeum();//Remove Elementeum from here 

        playerCompanionCardsAnim.AutoSelectCompanionCardsForBattle(SelectPowerManager.instance.selectedPowerName);
    }



    //Multiplayer Response of Elementeum
    public void UseElementeumRpc(string OppEleName, int value)
    {

        Debug.Log(OppEleName + " revert value is " + value);
        var EnemyCard = PlayerManager.instance.SelectedEnemyCard;


        if (value == 1)
        {


            if (OppEleName.ToLower() == "all")
            {
                for (int i = 0; i < elementeumChild.Length; i++)
                {
                    elementeumChild[i].transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                    elementeumChild[i].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0.5f);
                }
            }
            else
            {

                elementeumChild[int.Parse(OppEleName)].transform.DOScale(new Vector3(2, 2, 2), 0.5f);
                elementeumChild[int.Parse(OppEleName)].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 1), 0.5f);

            }
           
        }

        else
        {
            if (OppEleName.ToLower() == "all")
            {
               // Debug.Log((EnemyCard.isSpActive ? EnemyCard.CardDetail.elementeum : EnemyCard.UsingElementeum).ToString());
               //InGameCards.instance.enemyElemtium.text = (EnemyCard.isSpActive ? EnemyCard.CardDetail.elementeum : EnemyCard.UsingElementeum).ToString();
            }
            else
            {
                elementeumChild[int.Parse(OppEleName)].transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                elementeumChild[int.Parse(OppEleName)].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0.5f);
            }
        }




        if (!isSpActive)
        {
            InGameCards.instance.AddOrRemoveOppElementeum();//Add Elementeum from here 

        }

        remain_Elemt = EnemyCard.UsingElementeum;

        int companioncheck = Cur_Elemt - remain_Elemt;

        if (PlayerManager.instance.SelectedHealEnemyCard == null)
        {
            if (companioncheck < 2)
            {
                RevertEnemyCompanionByElementium(companioncheck);

            }
            else if (companioncheck < 3)
            {

                RevertEnemyCompanionByElementium(companioncheck);
            }
            else if (companioncheck < 4)
            {
                RevertEnemyCompanionByElementium(companioncheck);

            }
            else if (companioncheck < 5)
            {
                RevertEnemyCompanionByElementium(companioncheck);

            }
        }
    }

    public void CancelElementeumRpc(int elem, string Oppname)
    {
        Debug.Log("Cancel ELementeum RPCCC " + elem + " Name " + Oppname);

        if (Oppname.ToLower() == "all")
        {
            for (int i = 0; i < elementeumChild.Length; i++)

            {
                //Debug.Log("Elementium " + elementeumChild[i].transform.name + " elemementium name " + Oppname);
                elementeumChild[i].transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                elementeumChild[i].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0.5f);
            }
        }
        else
        {
            elementeumChild[int.Parse(Oppname)].transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            elementeumChild[int.Parse(Oppname)].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0.5f);
        }

        remain_Elemt -= UsingElementeum;

        InGameCards.instance.enemyElemtium.text = UsingElementeum.ToString();
        InGameCards.instance.AddOrRemoveOppElementeum();//Remove Elementeum from here 
        playerCompanionCardsAnim.AutoSelectEnemyCompanionCardsForBattle(SelectPowerManager.instance.selectedEnemyPowerName);
    }

    public void HideElementeum()
    {
        //        Debug.Log("Hide ELement");
        for (int i = 0; i < elementeumChild.Length; i++)
        {
            elementeumChild[i].transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            elementeumChild[i].GetComponent<RectTransform>().DOAnchorPos(new Vector3(0, 0), 0f);
            elementeumChild[i].transform.DOLocalMove(new Vector3(0, 0, 0), 0.5f);
        }

        //UsingElementeum = 0;
    }


    IEnumerator enableClick()
    {
        yield return new WaitForSeconds(0.5f);
        PlayerManager.instance.CanClick = true;
    }


    //RPC
    public void OnEnemySelectCard()
    {

        if (SelectPowerManager.instance.EnemyselectedCardAnimator == powerAnim)
        {
            powerAnim.SetBool("Shopower", true);
        }

        //Set canavas Order back to panel and not PowerShow.
        for (int i = 0; i < PlayerManager.instance.CardCanvas.Length; i++)
        {
            PlayerManager.instance.CardCanvas[i].sortingOrder = 2;
            if (!powerAnim.GetBool("Shopower"))
            {
                PlayerManager.instance.powerBtns[i].SetBool("Shopower", false);
            }
        }

        CheckCardNumber();
        canvas.sortingOrder = 8;
        Invoke("PowerAnim", 0.3f);
    }


    void EnemyCardPosSet(Transform card, Vector3 pos, float v, Vector3 scale, Quaternion rot)
    {
        card.DOMove(pos, v);
        card.localScale = scale;
        card.rotation = rot;
    }


    public void CheckSpecialAb()//Method To show special ability if its unlocked
    {

        if (isSpAbility && Cur_Elemt == this.CardDetail.elementeum)
        {
            if (SelectPowerManager.instance.selectedEnemyPowerName != AttributeType.None)
            {
                var powername = SelectPowerManager.instance.selectedEnemyPowerName;

                switch (powername)
                {
                    case AttributeType.Attack:
                        if (SpAbility1.attributeType == AttributeType.Defense)
                        {
                            SpAbility1.gameObject.SetActive(true);
                            SpAbility2.gameObject.SetActive(false);
                        }
                        else if (SpAbility2.attributeType == AttributeType.Defense)
                        {
                            SpAbility1.gameObject.SetActive(false);
                            SpAbility2.gameObject.SetActive(true);
                        }
                        break;

                    case AttributeType.Defense:
                        if (SpAbility1.attributeType == AttributeType.Attack)
                        {
                            SpAbility1.gameObject.SetActive(true);
                            SpAbility2.gameObject.SetActive(false);
                        }
                        else if (SpAbility2.attributeType == AttributeType.Attack)
                        {
                            SpAbility1.gameObject.SetActive(false);
                            SpAbility2.gameObject.SetActive(true);
                        }
                        break;

                    case AttributeType.Disrupt:
                        if (SpAbility1.attributeType == AttributeType.Heal)
                        {
                            SpAbility1.gameObject.SetActive(true);
                            SpAbility2.gameObject.SetActive(false);
                        }
                        else if (SpAbility2.attributeType == AttributeType.Heal)
                        {
                            SpAbility1.gameObject.SetActive(false);
                            SpAbility2.gameObject.SetActive(true);
                        }
                        break;

                    case AttributeType.Heal:
                        if (SpAbility1.attributeType == AttributeType.Disrupt)
                        {
                            SpAbility1.gameObject.SetActive(true);
                            SpAbility2.gameObject.SetActive(false);
                        }
                        else if (SpAbility2.attributeType == AttributeType.Disrupt)
                        {
                            SpAbility1.gameObject.SetActive(false);
                            SpAbility2.gameObject.SetActive(true);
                        }
                        break;

                    default:

                        break;
                }
            }

            SpAbilityObj.SetActive(true);

        }
        else
        {
            isSpAbility = false;
        }
    }

    //  public void SetVoidCardPosition(bool IsMe)
    //{
    //    EnemyCardPosSet(this.transform, new Vector3(-2f, 1f, 14f), 0.3f, new Vector3(0.5f, 0.5f, 0.5f), Quaternion.Euler(-10f, 0f, 0));
    //}


    
    public void OnSelectCard()
    {
        if (IsEnemyCard)
        {
            //OnEnemySelectCard();
            OnSelectCard_EnemyAsVictim();
        }
        else
            SyncSelectCard(true); //True : send response to other player  || False : does not send socket call to other Player/server
    }

    public void SyncSelectCard(bool CanSend)
    {
        Debug.Log("Card Clicked...." + this.CardDetail.card_contract_id + " " + this.CardDetail.heart + " " + this.CardDetail.name);

        if (!PlayerManager.instance.CanClick || !PlayerManager.instance.CanClickOnMeCard || isDisable)
        {
            if (CanSend) //check to validate that we calling this method for gameplay syncing
                return;
        }


        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {
            if (!PlayerManager.instance.SelectedCard.isSpActive)
            {
                isSpAbility = true;
                isUseSpAbility = false;
                isSpActive = false;

            }
          
        }
        else
        {
            isSpAbility = true;
            isUseSpAbility = false;
            isSpActive = false;
        }
        SelectPowerManager.instance.checkAttrAnim = 0;

        for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
        {
            PlayerManager.instance.PlayerCardsObj[i].DiceValue = 0;
            PlayerManager.instance.PlayerCardsObj[i].SpAbilityParticle1.SetActive(false);
            PlayerManager.instance.PlayerCardsObj[i].SpAbilityParticle2.SetActive(false);

            PlayerManager.instance.PlayerCardsObj[i].SetAllPowerOff();
            PlayerManager.instance.PlayerCardsObj[i].SpAbilityObj.SetActive(false);

            //if (PlayerManager.instance.SelectedCard == null)
            if (SelectPowerManager.instance.selectedPowerName != AttributeType.Heal)
            {


                PlayerManager.instance.PlayerCardsObj[i].ElementeumStatus.Clear();
                for (int j = 0; j < PlayerManager.instance.PlayerCardsObj[i].CardDetail.elementeum; j++)
                {
                    PlayerManager.instance.PlayerCardsObj[i].elementeumChild[j].GetComponent<RectTransform>().DOAnchorPos(new Vector3(0, 0, 0), 0.5f);
                    PlayerManager.instance.PlayerCardsObj[i].elementeumChild[j].GetComponent<RectTransform>().DOScale(1, 0.5f);

                    PlayerManager.instance.PlayerCardsObj[i].elementeumChild[j].selectEle = true;
                    PlayerManager.instance.PlayerCardsObj[i].elementeumChild[i].canClickElementeum = true;

                }
            }

        }


        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {

            if (CardDetail.heart == MaxHeartPoint)
            {
                PlayerManager.instance.ShowPopUP("Already Max Health");
                return;
            }
        }


        DiceManager.instance.GetComponent<AudioSource>().clip = DiceManager.instance.CardClick;
        DiceManager.instance.GetComponent<AudioSource>().Play();




        if (SelectPowerManager.instance.selectedPowerName == AttributeType.None)
        {
            for (int i = 0; i < 3; i++)
            {
                PlayerManager.instance.PlayerCardsObj[i].SetAllPowerOff();
                PlayerManager.instance.PlayerCardsObj[i].resetTrigger();
            }
        }

        for (int i = 0; i < 4; i++)
        {
            powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = true;
        }

        powerAnim.transform.localScale = Vector3.one;

        PlayerManager.instance.CanClickOnElemt = true;

        PlayerManager.instance.CanClickOnMeAttrib = true;

        powerAnim.gameObject.SetActive(true);
        isSpAbility = true;
        //SetStrengthSortOrder(6);
        SetUpSort();

        SelectPowerManager.instance.selectedCardAnimator = powerAnim;
        //Debug.Log(isSpAbility + "  " + Cur_Elemt + "  " + this.CardDetail.elementeum);
        if (isSpAbility && Cur_Elemt == this.CardDetail.elementeum)
        {

            SpAbility1.transform.DOScale(1, 0.1f);
            SpAbility2.transform.DOScale(1, 0.1f);

            switch (Spcount)
            {
                case 1:
                    SpAbility1.gameObject.SetActive(true);
                    SpAbility2.gameObject.SetActive(false);
                    break;
                case 2:
                    SpAbility1.gameObject.SetActive(true);
                    SpAbility2.gameObject.SetActive(true);
                    break;
            }

            if (SelectPowerManager.instance.selectedEnemyPowerName != AttributeType.None)
            {



                switch (SelectPowerManager.instance.selectedEnemyPowerName)
                {
                    case AttributeType.Attack:
                        if (SpAbility1.attributeType == AttributeType.Defense)
                        {
                            SpAbility1.gameObject.SetActive(true);
                            SpAbility2.gameObject.SetActive(false);
                        }
                        else if (SpAbility2.attributeType == AttributeType.Defense)
                        {
                            SpAbility1.gameObject.SetActive(false);
                            SpAbility2.gameObject.SetActive(true);
                        }
                        break;

                    case AttributeType.Defense:
                        if (SpAbility1.attributeType == AttributeType.Attack)
                        {
                            SpAbility1.gameObject.SetActive(true);
                            SpAbility2.gameObject.SetActive(false);
                        }
                        else if (SpAbility2.attributeType == AttributeType.Attack)
                        {
                            SpAbility1.gameObject.SetActive(false);
                            SpAbility2.gameObject.SetActive(true);
                        }
                        break;

                    case AttributeType.Disrupt:
                        if (SpAbility1.attributeType == AttributeType.Heal)
                        {
                            SpAbility1.gameObject.SetActive(true);
                            SpAbility2.gameObject.SetActive(false);
                        }
                        else if (SpAbility2.attributeType == AttributeType.Heal)
                        {
                            SpAbility1.gameObject.SetActive(false);
                            SpAbility2.gameObject.SetActive(true);
                        }
                        break;

                    case AttributeType.Heal:
                        if (SpAbility1.attributeType == AttributeType.Disrupt)
                        {
                            SpAbility1.gameObject.SetActive(true);
                            SpAbility2.gameObject.SetActive(false);
                        }
                        else if (SpAbility2.attributeType == AttributeType.Disrupt)
                        {
                            SpAbility1.gameObject.SetActive(false);
                            SpAbility2.gameObject.SetActive(true);
                        }

                        break;

                    default:

                        break;
                }
            }
            SpAbilityObj.SetActive(true);

        }

        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {
            PlayerManager.instance.CanClickOnMeAttrib = false;
            if (CardDetail.heart == MaxHeartPoint)
            {
                PlayerManager.instance.ShowPopUP("Already Max Health");
                return;
            }

            MainAnimator.enabled = false;
            Debug.Log("heal icon is active ");
            PlayerManager.instance.SelectedHealCard = this;

            if (PlayerManager.instance.SelectedCard == PlayerManager.instance.SelectedHealCard)
            {
                Debug.Log("Player select the same card ");
                PlayerManager.instance.SelectedCard.powerAnim.transform.localScale = Vector3.zero;

                GetComponent<SpriteRenderer>().sortingOrder = 6;

                canvas.sortingOrder = 8;

                for (int i = 0; i < 3; i++)
                {
                    if (PlayerManager.instance.PlayerCardsObj[i] != PlayerManager.instance.SelectedHealCard)
                    {
                        EnemyCardPosSet(PlayerManager.instance.PlayerCardsObj[i].transform, Vector3.zero, 0.3f, Vector3.zero, Quaternion.Euler(Vector3.zero));
                    }
                }

                EnemyCardPosSet(this.transform, new Vector3(-2f, 1f, 14f), 0.3f, new Vector3(0.5f, 0.5f, 0.5f), Quaternion.Euler(-10f, 0f, 0));
                for (int i = 0; i < PlayerManager.instance.SelectedHealCard.elementeumChild.Length; i++)
                {

                    PlayerManager.instance.SelectedHealCard.elementeumChild[i].canClickElementeum = true;
                    PlayerManager.instance.SelectedCard.elementeumChild[i].canClickElementeum = true;
                }
                if (PlayerManager.instance.SelectedCard.isSpActive)
                {
                    PlayerManager.instance.SelectedCard.PlayerSpAbiAnim();
                    Debug.Log("Player use Special Ability for Heal ");
                }

            }

            else
            {

                // call specail animation function from here !!!!


                Debug.Log("Player select other card ");
                PlayerManager.instance.SelectedCard.powerAnim.transform.localScale = Vector3.one;

                for (int i = 0; i < 3; i++)
                {
                    PlayerManager.instance.PlayerCardsObj[i].GetComponent<SpriteRenderer>().sortingOrder = 2;
                    PlayerManager.instance.PlayerCardsObj[i].canvas.sortingOrder = 3;
                    for (int j = 0; j < 4; j++)
                    {

                        PlayerManager.instance.PlayerCardsObj[i].StrengthAttributes[j].GetComponent<SpriteRenderer>().sortingOrder = 2;


                    }

                }


                GetComponent<SpriteRenderer>().sortingOrder = 6;

                canvas.sortingOrder = 8;

                PlayerManager.instance.SelectedCard.GetComponent<SpriteRenderer>().sortingOrder = 6;

                PlayerManager.instance.SelectedCard.canvas.sortingOrder = 8;

                EnemyCardPosSet(this.transform, new Vector3(-2f, 1f, 14f), 0.3f, new Vector3(0.5f, 0.5f, 0.5f), Quaternion.Euler(-10f, 0f, 0));

                EnemyCardPosSet(PlayerManager.instance.SelectedCard.transform, new Vector3(2f, 1f, 14f), 0.2f, new Vector3(0.45f, 0.45f, 0.45f), Quaternion.Euler(-10f, 0f, 0f));


                if (PlayerManager.instance.SelectedCard.debuffHl)
                {
                    PlayerManager.instance.SelectedCard.debuffHealImage.transform.DOScale(1, 1);
                    PlayerManager.instance.SelectedCard.debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1.48f, 0), 1);
                    PlayerManager.instance.SelectedCard.debuffHeal.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1f, 0), 1);
                    Debug.Log("Heal debuff icon is animated ");
                }

            }

            DiceManager.instance.dice.DOScale(new Vector3(0.38f, 0.38f, .038f), 0.5f);
            PlayerManager.instance.TextCanvas.SetActive(false);
            PlayerManager.instance.TextMessage.text = "";
            if (BattleLobbySC.Instance && CanSend)
            {
                //BattleLobbySC.Instance.LastAction = "me:mehealcard:" + this.CardDetail.card_contract_id;

                BattleLobbySC.Instance.HealCardSelect(
                    this.CardDetail.card_contract_id,
                    PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                    SelectPowerManager.instance.selectedPowerName,
                    PlayerManager.instance.SelectedCard.debuffValueHeal.ToString()
                    );
            }

            // call specail anim for the Player if he use heal 
            if (PlayerManager.instance.SelectedCard.isSpActive)
            {
                PlayerManager.instance.SelectedCard.PlayerSpAbiAnim();
            }
        }

        else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
        {
            if (SelectPowerManager.instance.selectedCardAnimator == powerAnim)
            {
                powerAnim.SetBool("Shopower", true);
            }

            for (int i = 0; i < 4; i++)
            {
                powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = false;
            }

            powerAnim.gameObject.transform.GetChild(3).transform.GetChild(0).GetComponent<Button>().interactable = true;
            //  anim.enabled = true;

            PlayerManager.instance.TextCanvas.SetActive(false);

            PlayerManager.instance.TextMessage.text = "";

            MainAnimator.enabled = true;

            PlayerManager.instance.CanClick = false;

            StartCoroutine(enableClick());

            AllAnimCardFalse();
            // PlayerManager.instance.EnemyCardAndCanvasOrderSet();

            GameObject clickParticle = Instantiate(PlayerManager.instance.clickParticlePrefab, this.transform);

            Destroy(clickParticle, 1f);

            //Set canavas Order back to panel and not PowerShow.
            for (int i = 0; i < PlayerManager.instance.CardCanvas.Length; i++)
            {
                PlayerManager.instance.CardCanvas[i].sortingOrder = 2;
                if (!powerAnim.GetBool("Shopower"))
                {
                    PlayerManager.instance.powerBtns[i].SetBool("Shopower", false);
                }
            }

            if (BattleLobbySC.Instance && CanSend)
            {

                //  SyncGameplaySc.Instance.GameData.TurnData[SyncGameplaySc.Instance.GameData.TurnData.Length - 1].MyData.CardClick = this.CardDetail.card_contract_id;
                //BattleLobbySC.Instance.LastAction = "me:medisruptcard:" + this.CardDetail.card_contract_id;
                // SyncGameplaySc.Instance.GameData.LastAction = "me:mecard:" + this.CardDetail.card_contract_id;

                // SyncGameplaySc.Instance.Save();
                Debug.Log(this.CardDetail.name);
                BattleLobbySC.Instance.DisruptCardSelect(this.CardDetail.card_contract_id);
            }

            CheckCardNumber();
            canvas.sortingOrder = 8;
            Invoke("PowerAnim", 0.3f);
        }

        else
        {
            Debug.Log(SelectPowerManager.instance.selectedPowerName + " power is selected");
            if (SelectPowerManager.instance.selectedCardAnimator == powerAnim)
            {
                powerAnim.SetBool("Shopower", true);
            }
            else
            {
                SelectPowerManager.instance.selectedCardAnimator = powerAnim;
            }

            //anim.enabled = true;

            for (int i = 0; i < 4; i++)
            {
                powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = true;
            }

            MainAnimator.enabled = true;
            PlayerManager.instance.CanClick = false;

            StartCoroutine(enableClick());
            AllAnimCardFalse();
            PlayerManager.instance.EnemyCardAndCanvasOrderSet();
            GameObject clickParticle = Instantiate(PlayerManager.instance.clickParticlePrefab, this.transform);
            Destroy(clickParticle, 1f);

            //Set canavas Order back to panel and not PowerShow.
            for (int i = 0; i < PlayerManager.instance.CardCanvas.Length; i++)
            {
                PlayerManager.instance.CardCanvas[i].sortingOrder = 2;
                if (!powerAnim.GetBool("Shopower"))
                {
                    PlayerManager.instance.powerBtns[i].SetBool("Shopower", false);
                }
            }

            for (int i = 0; i < 4; i++)
            {
                powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = false;
            }


            if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.None)
            {
                powerAnim.gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().interactable = true;
                powerAnim.gameObject.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().interactable = true;
            }
            else
            {
                switch (SelectPowerManager.instance.selectedEnemyPowerName)
                {
                    case AttributeType.Attack:
                        powerAnim.gameObject.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().interactable = true;
                        //Debug.Log(SelectPowerManager.instance.selectedEnemyPowerName + " is Selected ");
                        break;
                    case AttributeType.Defense:
                        powerAnim.gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().interactable = true;
                        //Debug.Log(SelectPowerManager.instance.selectedEnemyPowerName + " is Selected ");

                        break;
                    case AttributeType.Disrupt:
                        powerAnim.gameObject.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().interactable = true;
                        //Debug.Log(SelectPowerManager.instance.selectedEnemyPowerName + " is Selected ");

                        break;
                    case AttributeType.Heal:
                        powerAnim.gameObject.transform.GetChild(3).transform.GetChild(0).GetComponent<Button>().interactable = true;
                        //Debug.Log(SelectPowerManager.instance.selectedEnemyPowerName + " is Selected ");

                        break;

                }
            }

            if (BattleLobbySC.Instance && CanSend)
            {
                BattleLobbySC.Instance.PlayerOneCardSelect(this.CardDetail.card_contract_id);
                SelectPowerManager.instance.selectedPowerName = AttributeType.None;
                InGameCards.instance.MyAttribImage.SetActive(false);
            }

            CheckCardNumber();

            canvas.sortingOrder = 8;

            Invoke("PowerAnim", 0.3f);

        }
        elementeum.SetTrigger("play");



        if (PlayerManager.instance.SelectedCard != null)
        {
            //Debug.Log("Seleceted card is not  null" + this.gameObject.name);



            PlayerManager.instance.SelectedCard.UnsetSpecialAbilityPosition();

            PlayerManager.instance.SelectedCard.remain_Elemt = 0;
            if (SelectPowerManager.instance.selectedPowerName != AttributeType.Heal)
            {
                for (int i = 0; i < PlayerManager.instance.SelectedCard.CardDetail.elementeum; i++)
                {
                    PlayerManager.instance.SelectedCard.elementeumChild[i].canClickElementeum = true;
                    PlayerManager.instance.SelectedCard.elementeumChild[i].GetComponent<RectTransform>().DOAnchorPos(new Vector3(0, 0, 0), 0.5f);
                    //Debug.Log("Elementium position " + i);

                    PlayerManager.instance.SelectedCard.elementeumChild[i].selectEle = true;

                }
            }
            if (PlayerManager.instance.SelectedCard.debuffValueAttack >= 1 && !PlayerManager.instance.SelectedCard.debuffAtk)
            {
                PlayerManager.instance.SelectedCard.debuffAtk = true;
                PlayerManager.instance.SelectedCard.debuffValueAttack -= 1;

            }
            if (PlayerManager.instance.SelectedCard.debuffValueHeal >= 1 && !PlayerManager.instance.SelectedCard.debuffHl)
            {
                PlayerManager.instance.SelectedCard.debuffHl = true;
                PlayerManager.instance.SelectedCard.debuffValueHeal -= 1;

            }
            if (SelectPowerManager.instance.selectedPowerName != AttributeType.Heal)
            {
                //Debug.Log("Healing card is not selecte " + CardDetail.name);

                if (SpAbilityUsed != AttributeType.None)
                {

                    PlayerManager.instance.IsMeSp = false;
                    BattleLobbySC.Instance.CancelSpecialAbility(this.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName);
                    RevertPlayerSpecialAbilityAnimation();

                    foreach (var item in ElementeumStatus)
                    {
                        ElementeumStatus[item.Key] = 0;
                    }


                    SyncCancelElementeum(false, CardDetail.elementeum, "All");
                    Debug.Log("On card Click Special ability is removed ");
                    SpAbilityUsed = AttributeType.None;
                }
            }
        }
        for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
        {
            PlayerManager.instance.PlayerCardsObj[i].debuffImage.transform.DOScale(new Vector3(0, 0, 1), 0.5f);
            PlayerManager.instance.PlayerCardsObj[i].debuffImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);

            PlayerManager.instance.PlayerCardsObj[i].debuffHealImage.transform.DOScale(new Vector3(0, 0, 1), 0.5f);
            PlayerManager.instance.PlayerCardsObj[i].debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);

            //Debug.Log("Revert Debugg Icon ");
        }
        if (PlayerManager.instance.SelectedHealCard != null)
        {
            if (PlayerManager.instance.SelectedCard.debuffHl)

            {
                PlayerManager.instance.SelectedCard.ApplyDebuffToPlayerAttribute(AttributeType.Heal);
                PlayerManager.instance.SelectedCard.debuffHealImage.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
                PlayerManager.instance.SelectedCard.debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1.48f, 0), 1);
                PlayerManager.instance.SelectedCard.debuffHeal.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-1f, 0), 1);
            }
            PlayerManager.instance.SelectedHealCard.SpAbilityObj.gameObject.SetActive(false);

            if (PlayerManager.instance.SelectedCard != PlayerManager.instance.SelectedHealCard)
            {
                for (int i = 0; i < PlayerManager.instance.SelectedHealCard.elementeumChild.Length; i++)
                {

                    PlayerManager.instance.SelectedHealCard.elementeumChild[i].canClickElementeum = false;
                }
            }


            if (PlayerManager.instance.SelectedCard.isSpActive)
            {

                for (int i = 0; i < PlayerManager.instance.SelectedCard.elementeumChild.Length; i++)
                {

                    PlayerManager.instance.SelectedCard.elementeumChild[i].canClickElementeum = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
            {
                PlayerManager.instance.PlayerCardsObj[i].remain_Elemt = 0;
                //PlayerManager.instance.MyCardsObj[i].UsingElementeum = 0;

                if (PlayerManager.instance.PlayerCardsObj[i].isUseSpAbility)
                {

                    if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
                    {
                        SelectPowerManager.instance.selectedCardAnimator.SetBool("isHeal", true);
                    }

                    PlayerManager.instance.IsMeSp = false;
                    BattleLobbySC.Instance.CancelSpecialAbility(this.CardDetail.card_contract_id, SelectPowerManager.instance.selectedPowerName);


                    PlayerManager.instance.PlayerCardsObj[i].RevertPlayerSpecialAbilityAnimation();


                }
            }
            InGameCards.instance.Elemtium.text = "0";
            InGameCards.instance.Dice.text = "0";
            InGameCards.instance.Attribute.text = "0";
            InGameCards.instance.companion.text = "0";
            InGameCards.instance.Attribute.text = "0";

          


            for (int i = 0; i < PlayerManager.instance.CompanionCards.playerCompanionCards.Length; i++)
            {
                PlayerManager.instance.CompanionCards.AutoDeselectCompanionCardsForBattle(PlayerManager.instance.CompanionCards.playerCompanionCards[i]);
                PlayerManager.instance.CompanionCards.playerCompanionCards[i].isSelected = false;
            }

            if (SelectPowerManager.instance.selectedPowerName != AttributeType.None || SelectPowerManager.instance.selectedPowerName != AttributeType.None)
            {
                for (int i = 0; i < PlayerManager.instance.CompanionCards.enemyCompanionCards.Length; i++)
                {
                    PlayerManager.instance.CompanionCards.AutoDeselectEnemyCompanionCardsForBattle(PlayerManager.instance.CompanionCards.enemyCompanionCards[i]);
                    PlayerManager.instance.CompanionCards.enemyCompanionCards[i].isSelected = false;
                }
                for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
                {

                    PlayerManager.instance.enemyCardsObj[i].companionData.Clear();
                    PlayerManager.instance.enemyCardsObj[i].companionIDs.Clear();
                }
            }

        }
    }


    public void SortingOrder(int sortingOrder)
    {
        CardImage.sortingOrder = sortingOrder;
        StrengthAttributes.ForEach(x => x.sortingOrder = sortingOrder + 1);
        canvas.sortingOrder = sortingOrder + 2;
    }


    //Sorting Layer for Strength
    public void SetUpSort()
    {
        StrengthAttributes[0].sortingOrder = 7;
        StrengthAttributes[1].sortingOrder = 7;
        StrengthAttributes[2].sortingOrder = 7;
        StrengthAttributes[3].sortingOrder = 7;
    }

    public void OppSetUpSort()
    {
        StrengthAttributes[0].sortingOrder = 7;
        StrengthAttributes[1].sortingOrder = 7;
        StrengthAttributes[2].sortingOrder = 7;
        StrengthAttributes[3].sortingOrder = 7;
    }

    public void SetDownSort()
    {
        //elementeum.enabled = false;
        StrengthAttributes[0].sortingOrder = 2;
        StrengthAttributes[1].sortingOrder = 2;
        StrengthAttributes[2].sortingOrder = 1;
        StrengthAttributes[3].sortingOrder = 1;
    }



    public void SetStrengthSortOrderRpc(int Sno)
    {


        for (int i = 0; i < 4; i++)
        {
            StrengthAttributes[i].GetComponent<SpriteRenderer>().sortingOrder = Sno;
        }
    }

    public void SetStrengthSortOrder(int attack, int defense, int disrupt, int heal)
    {
        StrengthAttributes[0].sortingOrder = attack;
        StrengthAttributes[1].sortingOrder = defense;
        StrengthAttributes[2].sortingOrder = disrupt;
        StrengthAttributes[3].sortingOrder = heal;
    }


    public void SetStrengthSortOrder(int Sno)
    {

        for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
        {
            PlayerManager.instance.PlayerCardsObj[i].SetStrengthSortOrderRpc(1);
        }

        for (int i = 0; i < 4; i++)
        {
            StrengthAttributes[i].GetComponent<SpriteRenderer>().sortingOrder = Sno;
        }

    }

    public void OnSelectCardRPC() // Multiplayer Rpc Function of onselectedCard
    {


        Debug.Log("onselected card RPC Card Clicked...." + this.CardDetail.card_contract_id);


        AllAnimCardFalse();
        //InGameCards.instance.enemyElemtium.text = "0";
        //PlayerManager.instance.EnemyCardAndCanvasOrderSet();
        GameObject clickParticle = Instantiate(PlayerManager.instance.clickParticlePrefab, this.transform);

        Destroy(clickParticle, 1f);

        //Set canavas Order back to panel and not PowerShow.
        for (int i = 0; i < PlayerManager.instance.CardCanvas.Length; i++)
        {
            PlayerManager.instance.CardCanvas[i].sortingOrder = 2;
            PlayerManager.instance.PlayerCardsObj[i].resetTrigger();

            if (!powerAnim.GetBool("Shopower"))
            {
                PlayerManager.instance.powerBtns[i].SetBool("Shopower", false);
            }
        }
        Debug.Log(isSpAbility + "  " + Cur_Elemt + " " + SelectPowerManager.instance.selectedEnemyPowerName);
        if (isSpActive && Cur_Elemt == this.CardDetail.elementeum)
        {
            SpAbilityObj.SetActive(true);
            SpAbility1.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
            SpAbility2.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);

        }
        else if (isSpAbility && Cur_Elemt == this.CardDetail.elementeum)
        {
            SpAbilityObj.SetActive(true);
            SpAbility1.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
            SpAbility2.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
        }
        else
        {
            SpAbilityObj.SetActive(false);
                
        }

        powerAnim.gameObject.SetActive(true);
        powerAnim.gameObject.transform.localScale = Vector3.one;

        powerAnim.SetBool("Shopower", true);
        SelectPowerManager.instance.selectedCardAnimator = powerAnim;
        DiceManager.instance.GetComponent<AudioSource>().clip = DiceManager.instance.CardClick;
        DiceManager.instance.GetComponent<AudioSource>().Play();


        SetUpSort();

        MainAnimator.enabled = true;

        PlayerManager.instance.SelectedCard = this;
        Debug.Log("---------------CardNumber-----------------");

        Debug.Log("card" + cardNumber);
        MainAnimator.SetBool("Card" + cardNumber, true);

       

        canvas.sortingOrder = 8;


        if (SelectPowerManager.instance.selectedEnemyPowerName != AttributeType.Heal)
        {

        DiceManager.instance.dice.DOScale(new Vector3(0.38f, 0.38f, .038f), 0.5f);
        }


        //Disable other attributes while opponet's turn
        if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Attack)
        {
            for (int i = 0; i < 4; i++)
            {
                powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = false;
            }
            powerAnim.gameObject.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().interactable = true;
            Debug.Log(SelectPowerManager.instance.selectedEnemyPowerName.ToString() + "is Selected ");
        }

        else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
        {

            for (int i = 0; i < 4; i++)
            {
                powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = false;
            }

            powerAnim.gameObject.transform.GetChild(3).transform.GetChild(0).GetComponent<Button>().interactable = true;
            Debug.Log(SelectPowerManager.instance.selectedEnemyPowerName.ToString() + "is Selected ");

        }



        elementeum.SetTrigger("play");


    }


    public void ResetAttrub()
    {
        for (int i = 0; i < 4; i++)
        {

            powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = true;

        }

        if (IsEnemyCard)
        {
            InGameCards.instance.enemyElemtium.text = "0";
            InGameCards.instance.enemyDice.text = "0";
            InGameCards.instance.enemeyAttribute.text = "0";
            InGameCards.instance.enemyCompanion.text = "0";
            SelectPowerManager.instance.checkAttrAnim = 0;
        }
    }


    //power Display
    void PowerAnim()
    {
        if (!powerAnim.GetBool("Shopower"))
        {
            //Debug.Log("PowerAnim True");
            powerAnim.SetBool("Shopower", true);
            DiceManager.instance.GetComponent<AudioSource>().clip = DiceManager.instance.CardClick;
            DiceManager.instance.GetComponent<AudioSource>().Play();
        }

        SelectPowerManager.instance.selectedCardAnimator = powerAnim;

        //Debug.Log("PowerAnim--" + powerAnim.name);
    }

    // card funtion call on card number click.
    private void CheckCardNumber()
    {

        PlayerManager.instance.SelectedCard = this;

        //Debug.Log(PlayerManager.instance.SelectedCard.transform.GetChild(6).GetChild(4));

        if (cardNumber == 0)
        {
            //Debug.Log("card" + cardNumber);
            MainAnimator.SetBool("Card0", true);
        }

        else if (cardNumber == 1)
        {
            //Debug.Log("card" + cardNumber);
            MainAnimator.SetBool("Card1", true);
        }

        else if (cardNumber == 2)
        {
            //Debug.Log("card" + cardNumber);
            MainAnimator.SetBool("Card2", true);
        }

    }

    // all card animation false for set origenal position. 
    private void AllAnimCardFalse()
    {

        if (IsEnemyCard)
        {
            MainAnimator.SetBool("Card0", false);
            MainAnimator.SetBool("Card1", false);
            MainAnimator.SetBool("Card2", false);
            MainAnimator.SetBool("CardPosBack", false);

            return;
        }

        MainAnimator.SetBool("GamePlay", false);
        MainAnimator.SetBool("Card0", false);
        MainAnimator.SetBool("Card1", false);
        MainAnimator.SetBool("Card2", false);
        MainAnimator.SetBool("CardPosBack", false);

        if (SelectPowerManager.instance.selectedCardAnimator != null)
        {
            var cardRefHolder = SelectPowerManager.instance.selectedCardAnimator.GetComponent<CardObjectRefHolder>();

            if (cardRefHolder && cardRefHolder.cardObject && cardRefHolder.cardObject.isPowerBtnShow)
            {
                SelectPowerManager.instance.AllSelectedPowerFalse();
            }
        }
    }

    public void SetAllPowerOff()
    {
        //Debug.Log("Set power OFF");
        if (this.debuffValueAttack >= 1)
        {
            debuffImage.transform.DOScale(new Vector3(0, 0, 0), 0f);
            debuffImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0);

        }
        if (this.debuffValueHeal >= 1)
        {
            debuffHealImage.transform.DOScale(new Vector3(0, 0, 0), 0f);
            debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0);

        }


        powerAnim.SetBool("Shopower", false);
        powerAnim.SetBool("isAttack", false);
        powerAnim.SetBool("isHeal", false);
        powerAnim.SetBool("isDefense", false);
        powerAnim.SetBool("isDisrupt", false);


        SetDownSort();
    }

    public void SetElemtDeault()
    {
        elementeum.gameObject.SetActive(true);

        elementeum.ResetTrigger("play");

        elementeum.ResetTrigger("reverse");

        elementeum.SetTrigger("reverse");
    }

    public void resetTrigger()
    {
        elementeum.ResetTrigger("play");
        elementeum.ResetTrigger("reverse");
    }

    public void SetAnimation(bool isEnable)
    {
        MainAnimator.SetBool("OnEnableControl", isEnable);
    }


    public void ApplyDebuffToPlayerAttribute(AttributeType AttriName)
    {


        //Debug.Log("Debuff is Apply on this card " + CardDetail.name);

        int remainingCards = 0;
        for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
        {

            if (PlayerManager.instance.PlayerCardsObj[i].DestroyedCard == false)
            {
                remainingCards += 1;

            }

        }
        if (remainingCards != 1)
        {
            switch (AttriName)
            {
                case AttributeType.Attack:
                    if (debuffValueAttack >= 3)
                    {
                        debuffAtk = false;
                        debuffValueAttack = 3;

                        InGameCards.instance.MyAttribValue.text = TotalAttack().ToString();
                        debuffAttack.text = "-" + debuffValueAttack.ToString();
                    }
                    else
                    {
                        debuffAtk = false;
                        debuffValueAttack += 1;

                        InGameCards.instance.MyAttribValue.text = TotalAttack().ToString();
                        debuffAttack.text = "-" + debuffValueAttack.ToString();
                    }

                    break;
                case AttributeType.Heal:
                    if (debuffValueHeal >= 3)
                    {
                        debuffHl = false;
                        debuffValueHeal = 3;

                        InGameCards.instance.MyAttribValue.text = TotalHeal().ToString();
                        debuffHeal.text = "-" + debuffValueHeal.ToString();
                    }
                    else
                    {
                        debuffHl = false;
                        debuffValueHeal += 1;

                        InGameCards.instance.MyAttribValue.text = TotalHeal().ToString();
                        debuffHeal.text = "-" + debuffValueHeal.ToString();
                    }
                    break;

            }
        }
    }
    public void RemoveDebuffToPlayerAttribute(AttributeType AttriName)
    {

        Debug.Log("RemoveDebuffToPlayerAttribute " + AttriName);
        switch (AttriName)
        {
            case AttributeType.Attack:
                if (!debuffAtk)
                {

                    this.debuffImage.transform.DOScale(new Vector3(0, 0, 1), 0.5f);
                    this.debuffImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);
                    InGameCards.instance.MyAttribValue.text = TotalAttack().ToString();
                    if (debuffValueAttack == 0)
                    {
                        debuffValueAttack = 0;
                    }
                    else
                    {
                        debuffValueAttack -= 1;
                    }
                    //debuffAtk = true;
                }

                break;
            case AttributeType.Heal:
                if (!debuffHl)
                {

                    this.debuffHealImage.transform.DOScale(new Vector3(0, 0, 1), 0.5f);
                    this.debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);
                    InGameCards.instance.MyAttribValue.text = TotalAttack().ToString();
                    if (debuffValueHeal == 0)
                    {
                        debuffValueHeal = 0;
                    }
                    else
                    {
                        debuffValueHeal -= 1;
                    }
                    //debuffHl = true;
                }
                break;

        }



    }



    public void RemoveDebuffToEnemyAttribute(AttributeType AttriName)
    {

        switch (AttriName)
        {
            case AttributeType.Attack:
                this.debuffImage.transform.DOScale(new Vector3(0, 0, 1), 0.5f);
                this.debuffImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);
                break;
            case AttributeType.Heal:
                this.debuffHealImage.transform.DOScale(new Vector3(0, 0, 1), 0.5f);
                this.debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);
                break;

        }
    }

    public void HealthAnimatedsHeart()
    {
        if (IsEnemyCard)
        {
            PlayerManager.instance.animatedHeartEnemy.transform.DOScale(1f, 1);
            PlayerManager.instance.animatedHeartEnemy.SetActive(true);
            PlayerManager.instance.animatedHeartPointEnemy.text = this.CardDetail.heart.ToString();
            PlayerManager.instance.animatedHeartEnemy.GetComponent<Animator>().SetBool("HeartAnim", true);

        }
        else
        {
            PlayerManager.instance.animatedHeart.transform.DOScale(1f, 1);
            PlayerManager.instance.animatedHeart.SetActive(true);
            PlayerManager.instance.animatedHeartPoint.text = this.CardDetail.heart.ToString();
            PlayerManager.instance.animatedHeart.GetComponent<Animator>().SetBool("HeartAnim", true);
        }
    }
    public void HealthAnimatedsHeartHide()
    {
        if (IsEnemyCard)
        {
            PlayerManager.instance.heartSlashEnemy.gameObject.SetActive(false);
            PlayerManager.instance.animatedHeartEnemy.transform.DOScale(0f, 1);
            PlayerManager.instance.animatedHeartEnemy.SetActive(false);
        }
        else
        {
            PlayerManager.instance.heartSlash.gameObject.SetActive(false);
            PlayerManager.instance.animatedHeart.transform.DOScale(0f, 1);
            PlayerManager.instance.animatedHeart.SetActive(false);
        }
    }
    public void PlayerSpAbiAnim()
    {
        StartCoroutine(PlayerSpecialAbilityAnimation());
    }
    float parPos = 1.5f;
    public static bool stopSpAbilityAnimation = false;

    public IEnumerator PlayerSpecialAbilityAnimation()
    {
        stopSpAbilityAnimation = true;
        PlayerManager.instance.spParticals.SetActive(true);
        for (int i = 0; i < CardDetail.elementeum; i++)
        {
            float multiplyer = i < 2 ? i : Mathf.Ceil((float)i / 2);
            elementeumChild[i].transform.DOKill();
            elementeumChild[i].transform.DOScale(new Vector3(3, 3, 3), 0.5f);
            yield return new WaitForSeconds(0.5f);
            elementeumChild[i].transform.DOMove(new Vector3((i % 2 == 0) ? (multiplyer) * parPos : multiplyer * -parPos, -2f, 14f), 0.5f);
            yield return new WaitForSeconds(0.5f);
            PlayerManager.instance.elementeumParticle[i].gameObject.SetActive(true);
            PlayerManager.instance.elementeumParticle[i].StartPosition = new Vector3(((i % 2 == 0) ? (multiplyer) * parPos : multiplyer * -parPos), 0, 0);

            if (!stopSpAbilityAnimation)
            {
                RevertPlayerSpecialAbilityAnimation(true);
                yield break;
            }

        }
        yield return new WaitForSeconds(0.5f);

    }


    public void RevertPlayerSpecialAbilityAnimation(bool Immediatly = false)
    {

        //Debug.Log("Revert Sp animation ");
        stopSpAbilityAnimation = false;
        PlayerManager.instance.spParticals.SetActive(false);

        for (int i = 0; i < CardDetail.elementeum; i++)
        {
            PlayerManager.instance.elementeumParticle[i].gameObject.SetActive(false);
            PlayerManager.instance.elementeumParticle[i].StartPosition = Vector3.zero;
            elementeumChild[i].transform.DOKill();
            elementeumChild[i].transform.DOScale(Vector3.one, Immediatly ? 0.01f : 0.2f);
            elementeumChild[i].transform.DOLocalMove(Vector3.zero, Immediatly ? 0.1f : 0.5f);



        }
        SetElemtDeault();
    }


    #region Enemy SP_Anim


    float oppParPos = 1.5f;


    bool isDoEnemySpBusy = false;
    bool isShowing = false;
    public IEnumerator DoEnemySpAnimation(bool show)
    {


        isShowing = show;
        yield return new WaitUntil(() => isDoEnemySpBusy == false);//new call should wait for old call

        isDoEnemySpBusy = true;

        if (show)
        {

            PlayerManager.instance.spParticals.SetActive(true);
            for (int i = 0; i < CardDetail.elementeum; i++)
            //for (int i = 0; i < 5; i++)
            {
                float multiplyer = i < 2 ? i : Mathf.Ceil((float)i / 2);
                elementeumChild[i].transform.DOKill();
                elementeumChild[i].transform.DOScale(new Vector3(5, 5, 5), 0.5f);
                yield return new WaitForSeconds(0.5f);
                elementeumChild[i].transform.DOMove(new Vector3((i % 2 == 0) ? (multiplyer) * oppParPos : multiplyer * -oppParPos, 8.5f, 15.0f), 0.5f);
                yield return new WaitForSeconds(0.5f);
                PlayerManager.instance.elementeumParticleEnemy[i].gameObject.SetActive(true);
                PlayerManager.instance.elementeumParticleEnemy[i].StartPosition = new Vector3(((i % 2 == 0) ? (multiplyer) * oppParPos : multiplyer * -oppParPos), 10, 0);

                if (!isShowing)
                {
                    isDoEnemySpBusy = false;
                    yield return DoEnemySpAnimation(false); ///
                    yield break;
                }
            }
        }
        else
        {

            PlayerManager.instance.spParticals.SetActive(false);

            for (int i = 0; i < CardDetail.elementeum; i++)
            {
                PlayerManager.instance.elementeumParticleEnemy[i].gameObject.SetActive(false);
                PlayerManager.instance.elementeumParticleEnemy[i].StartPosition = Vector3.zero;
                elementeumChild[i].transform.DOKill();
                elementeumChild[i].transform.DOScale(Vector3.one, 0.01f);
                elementeumChild[i].transform.DOLocalMove(Vector3.zero, 0.1f);

            }

        }


        isDoEnemySpBusy = false;

    }



    public static Queue<CardObject> enemySpCardsShowingAnim = new Queue<CardObject>();

    public void EnemySpAbiAnim()
    {
        while (enemySpCardsShowingAnim.Count > 0)
        {
            CardObject otherCard = enemySpCardsShowingAnim.Dequeue();
            if (otherCard != null)
            {
                otherCard.RevertEnemySpAbiAnim();

            }
        }

        enemySpCardsShowingAnim.Enqueue(this);

        StartCoroutine(DoEnemySpAnimation(true));
    }
    public void RevertEnemySpAbiAnim()
    {
        StartCoroutine(DoEnemySpAnimation(false));
    }



    #endregion

    public void RevertCompanionByElementium(int companioncheck)
    {
        for (int i = 0; i < playerCompanionCardsAnim.playerCompanionCards.Length; i++)
        {
            var PlayerComObj = playerCompanionCardsAnim.playerCompanionCards[i];

            switch (SelectPowerManager.instance.selectedPowerName)
            {
                case AttributeType.Attack:
                    if (PlayerComObj.CardDetail.strength_1 == SelectPowerManager.instance.selectedPowerName)
                    {

                        if (PlayerComObj.isSelected == true)
                        {
                            if (PlayerComObj.CardDetail.attack <= companioncheck)
                            {
                                if (playerCompanionCardsAnim.plyComcardTempValue <= companioncheck)
                                {
                                }
                                else
                                {
                                    Debug.Log("Here");
                                    playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementiumDeSelection(playerCompanionCardsAnim.playerCompanionCards[i]);
                                    playerCompanionCardsAnim.plyComcardTempValue -= PlayerComObj.CardDetail.attack;
                                    InGameCards.instance.companion.text = playerCompanionCardsAnim.plyComcardTempValue.ToString();
                                    int temp = int.Parse(InGameCards.instance.MyAttribValue.text) - PlayerComObj.CardDetail.attack;
                                    InGameCards.instance.MyAttribValue.text = temp.ToString();

                                }
                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.playerCompanionCards[i]);
                                playerCompanionCardsAnim.plyComcardTempValue -= PlayerComObj.CardDetail.attack;
                                int tempCom = playerCompanionCardsAnim.plyComcardTempValue;
                                InGameCards.instance.companion.text = tempCom.ToString();
                                int temp = int.Parse(InGameCards.instance.MyAttribValue.text) - PlayerComObj.CardDetail.attack;
                                InGameCards.instance.MyAttribValue.text = temp.ToString();
                            }
                        }
                        else
                        {
                            if (PlayerComObj.CardDetail.attack <= companioncheck)
                            {

                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.playerCompanionCards[i]);
                            }
                        }
                    }
                    break;
                case AttributeType.Defense:
                    if (PlayerComObj.CardDetail.strength_1 == SelectPowerManager.instance.selectedPowerName)
                    {

                        if (PlayerComObj.isSelected == true)
                        {
                            if (PlayerComObj.CardDetail.defence <= companioncheck)
                            {

                                if (playerCompanionCardsAnim.plyComcardTempValue <= companioncheck)
                                {
                                }
                                else
                                {
                                    playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementiumDeSelection(playerCompanionCardsAnim.playerCompanionCards[i]);
                                    playerCompanionCardsAnim.plyComcardTempValue -= PlayerComObj.CardDetail.defence;
                                    InGameCards.instance.companion.text = playerCompanionCardsAnim.plyComcardTempValue.ToString();
                                    int temp = int.Parse(InGameCards.instance.MyAttribValue.text) - PlayerComObj.CardDetail.defence;
                                    InGameCards.instance.MyAttribValue.text = temp.ToString();
                                }
                            }
                            else
                            {
                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.playerCompanionCards[i]);
                                playerCompanionCardsAnim.plyComcardTempValue -= PlayerComObj.CardDetail.defence;
                                int tempCom = playerCompanionCardsAnim.plyComcardTempValue;
                                InGameCards.instance.companion.text = tempCom.ToString();
                                int temp = int.Parse(InGameCards.instance.MyAttribValue.text) - PlayerComObj.CardDetail.defence;
                                InGameCards.instance.MyAttribValue.text = temp.ToString();
                            }
                        }
                        else
                        {
                            if (PlayerComObj.CardDetail.defence <= companioncheck)
                            {
                            }
                            else
                            {
                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.playerCompanionCards[i]);
                            }
                        }
                    }
                    break;
                case AttributeType.Heal:
                    if (PlayerComObj.CardDetail.strength_1 == SelectPowerManager.instance.selectedPowerName)
                    {

                        if (PlayerComObj.isSelected == true)
                        {
                            if (PlayerComObj.CardDetail.heal <= companioncheck)
                            {
                                if (playerCompanionCardsAnim.plyComcardTempValue <= companioncheck)
                                {
                                }
                                else
                                {
                                    playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementiumDeSelection(playerCompanionCardsAnim.playerCompanionCards[i]);
                                    playerCompanionCardsAnim.plyComcardTempValue -= PlayerComObj.CardDetail.heal;
                                    InGameCards.instance.companion.text = playerCompanionCardsAnim.plyComcardTempValue.ToString();
                                    int temp = int.Parse(InGameCards.instance.MyAttribValue.text) - PlayerComObj.CardDetail.heal;
                                    InGameCards.instance.MyAttribValue.text = temp.ToString();
                                }
                            }
                            else
                            {
                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.playerCompanionCards[i]);
                                playerCompanionCardsAnim.plyComcardTempValue -= PlayerComObj.CardDetail.heal;
                                int tempCom = playerCompanionCardsAnim.plyComcardTempValue;
                                InGameCards.instance.companion.text = tempCom.ToString();
                                int temp = int.Parse(InGameCards.instance.MyAttribValue.text) - PlayerComObj.CardDetail.heal;
                                InGameCards.instance.MyAttribValue.text = temp.ToString();
                            }
                        }
                        else
                        {
                            if (PlayerComObj.CardDetail.heal <= companioncheck)
                            {
                            }
                            else
                            {
                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.playerCompanionCards[i]);
                            }
                        }
                    }
                    break;
                case AttributeType.Disrupt:
                    if (PlayerComObj.CardDetail.strength_1 == SelectPowerManager.instance.selectedPowerName)
                    {
                        if (PlayerComObj.isSelected == true)
                        {
                            if (PlayerComObj.CardDetail.disrupt <= companioncheck)
                            {
                                if (playerCompanionCardsAnim.plyComcardTempValue <= companioncheck)
                                {
                                }
                                else
                                {
                                    playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementiumDeSelection(playerCompanionCardsAnim.playerCompanionCards[i]);
                                    playerCompanionCardsAnim.plyComcardTempValue -= PlayerComObj.CardDetail.disrupt;
                                    InGameCards.instance.companion.text = playerCompanionCardsAnim.plyComcardTempValue.ToString();
                                    int temp = int.Parse(InGameCards.instance.MyAttribValue.text) - PlayerComObj.CardDetail.disrupt;
                                    InGameCards.instance.MyAttribValue.text = temp.ToString();
                                }
                            }
                            else
                            {
                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.playerCompanionCards[i]);
                                playerCompanionCardsAnim.plyComcardTempValue -= PlayerComObj.CardDetail.disrupt;
                                int tempCom = playerCompanionCardsAnim.plyComcardTempValue;
                                InGameCards.instance.companion.text = tempCom.ToString();
                                int temp = int.Parse(InGameCards.instance.MyAttribValue.text) - PlayerComObj.CardDetail.disrupt;
                                InGameCards.instance.MyAttribValue.text = temp.ToString();
                            }
                        }
                        else
                        {
                            if (PlayerComObj.CardDetail.disrupt <= companioncheck)
                            {
                            }
                            else
                            {
                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.playerCompanionCards[i]);
                            }
                        }
                    }
                    break;
            }

        }

        return;
    }

    public void RevertEnemyCompanionByElementium(int companioncheck)
    {
        for (int i = 0; i < playerCompanionCardsAnim.enemyCompanionCards.Length; i++)
        {

            var EnemyComObj = playerCompanionCardsAnim.enemyCompanionCards[i];

            switch (SelectPowerManager.instance.selectedEnemyPowerName)
            {
                case AttributeType.Attack:
                    if (EnemyComObj.CardDetail.strength_1 == SelectPowerManager.instance.selectedEnemyPowerName)
                    {

                        if (EnemyComObj.isSelected == true)
                        {
                            if (EnemyComObj.CardDetail.attack <= companioncheck)
                            {
                                if (playerCompanionCardsAnim.eneComcardTempValue <= companioncheck)
                                {
                                }
                                else
                                {
                                    Debug.Log("Here");
                                    playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementiumDeSelection(playerCompanionCardsAnim.enemyCompanionCards[i]);
                                    playerCompanionCardsAnim.eneComcardTempValue -= EnemyComObj.CardDetail.attack;
                                    InGameCards.instance.enemyCompanion.text = playerCompanionCardsAnim.eneComcardTempValue.ToString();
                                    int temp = int.Parse(InGameCards.instance.OppAttribValue.text) - EnemyComObj.CardDetail.attack;
                                    InGameCards.instance.OppAttribValue.text = temp.ToString();

                                }
                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.enemyCompanionCards[i]);
                                playerCompanionCardsAnim.eneComcardTempValue -= EnemyComObj.CardDetail.attack;
                                int tempCom = playerCompanionCardsAnim.eneComcardTempValue;
                                InGameCards.instance.enemyCompanion.text = tempCom.ToString();
                                int temp = int.Parse(InGameCards.instance.OppAttribValue.text) - EnemyComObj.CardDetail.attack;
                                InGameCards.instance.OppAttribValue.text = temp.ToString();
                            }
                        }
                        else
                        {
                            if (EnemyComObj.CardDetail.attack <= companioncheck)
                            {

                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.enemyCompanionCards[i]);
                            }
                        }
                    }
                    break;
                case AttributeType.Defense:
                    if (EnemyComObj.CardDetail.strength_1 == SelectPowerManager.instance.selectedEnemyPowerName)
                    {

                        if (EnemyComObj.isSelected == true)
                        {
                            if (EnemyComObj.CardDetail.defence <= companioncheck)
                            {
                                if (playerCompanionCardsAnim.eneComcardTempValue <= companioncheck)
                                {
                                }
                                else
                                {
                                    Debug.Log("Here");
                                    playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementiumDeSelection(playerCompanionCardsAnim.enemyCompanionCards[i]);
                                    playerCompanionCardsAnim.eneComcardTempValue -= EnemyComObj.CardDetail.defence;
                                    InGameCards.instance.enemyCompanion.text = playerCompanionCardsAnim.eneComcardTempValue.ToString();
                                    int temp = int.Parse(InGameCards.instance.OppAttribValue.text) - EnemyComObj.CardDetail.defence;
                                    InGameCards.instance.OppAttribValue.text = temp.ToString();

                                }
                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.enemyCompanionCards[i]);
                                playerCompanionCardsAnim.eneComcardTempValue -= EnemyComObj.CardDetail.defence;
                                int tempCom = playerCompanionCardsAnim.eneComcardTempValue;
                                InGameCards.instance.enemyCompanion.text = tempCom.ToString();
                                int temp = int.Parse(InGameCards.instance.OppAttribValue.text) - EnemyComObj.CardDetail.defence;
                                InGameCards.instance.OppAttribValue.text = temp.ToString();
                            }
                        }
                        else
                        {
                            if (EnemyComObj.CardDetail.defence <= companioncheck)
                            {

                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.enemyCompanionCards[i]);
                            }
                        }
                    }
                    break;
                case AttributeType.Heal:
                    if (EnemyComObj.CardDetail.strength_1 == SelectPowerManager.instance.selectedEnemyPowerName)
                    {

                        if (EnemyComObj.isSelected == true)
                        {
                            if (EnemyComObj.CardDetail.heal <= companioncheck)
                            {
                                if (playerCompanionCardsAnim.eneComcardTempValue <= companioncheck)
                                {
                                }
                                else
                                {
                                    Debug.Log("Here");
                                    playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementiumDeSelection(playerCompanionCardsAnim.enemyCompanionCards[i]);
                                    playerCompanionCardsAnim.eneComcardTempValue -= EnemyComObj.CardDetail.heal;
                                    InGameCards.instance.enemyCompanion.text = playerCompanionCardsAnim.eneComcardTempValue.ToString();
                                    int temp = int.Parse(InGameCards.instance.OppAttribValue.text) - EnemyComObj.CardDetail.heal;
                                    InGameCards.instance.OppAttribValue.text = temp.ToString();

                                }
                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.enemyCompanionCards[i]);
                                playerCompanionCardsAnim.eneComcardTempValue -= EnemyComObj.CardDetail.heal;
                                int tempCom = playerCompanionCardsAnim.eneComcardTempValue;
                                InGameCards.instance.enemyCompanion.text = tempCom.ToString();
                                int temp = int.Parse(InGameCards.instance.OppAttribValue.text) - EnemyComObj.CardDetail.heal;
                                InGameCards.instance.OppAttribValue.text = temp.ToString();
                            }
                        }
                        else
                        {
                            if (EnemyComObj.CardDetail.heal <= companioncheck)
                            {

                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.enemyCompanionCards[i]);
                            }
                        }
                    }
                    break;
                case AttributeType.Disrupt:
                    if (EnemyComObj.CardDetail.strength_1 == SelectPowerManager.instance.selectedEnemyPowerName)
                    {

                        if (EnemyComObj.isSelected == true)
                        {
                            if (EnemyComObj.CardDetail.disrupt <= companioncheck)
                            {
                                if (playerCompanionCardsAnim.eneComcardTempValue <= companioncheck)
                                {
                                }
                                else
                                {
                                    Debug.Log("Here");
                                    playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementiumDeSelection(playerCompanionCardsAnim.enemyCompanionCards[i]);
                                    playerCompanionCardsAnim.eneComcardTempValue -= EnemyComObj.CardDetail.disrupt;
                                    InGameCards.instance.enemyCompanion.text = playerCompanionCardsAnim.eneComcardTempValue.ToString();
                                    int temp = int.Parse(InGameCards.instance.OppAttribValue.text) - EnemyComObj.CardDetail.disrupt;
                                    InGameCards.instance.OppAttribValue.text = temp.ToString();

                                }
                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.enemyCompanionCards[i]);
                                playerCompanionCardsAnim.eneComcardTempValue -= EnemyComObj.CardDetail.disrupt;
                                int tempCom = playerCompanionCardsAnim.eneComcardTempValue;
                                InGameCards.instance.enemyCompanion.text = tempCom.ToString();
                                int temp = int.Parse(InGameCards.instance.OppAttribValue.text) - EnemyComObj.CardDetail.disrupt;
                                InGameCards.instance.OppAttribValue.text = temp.ToString();
                            }
                        }
                        else
                        {
                            if (EnemyComObj.CardDetail.disrupt <= companioncheck)
                            {

                            }
                            else
                            {
                                Debug.Log("Here");

                                playerCompanionCardsAnim.AutoDeselectCompanionCardsFromElementium(playerCompanionCardsAnim.enemyCompanionCards[i]);
                            }
                        }
                    }
                    break;

            }


        }

        return;
    }


    public void AnimatePlayerAttributeValue(AttributeType powerName)
    {
        switch (powerName)
        {
            case AttributeType.Attack:
                //txt_Attack.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.attack.ToString());
                StartCoroutine(InstantiateObjForAnimate(txt_Attack, new Vector3(-15f, -5.8f, 0)));

                break;
            case AttributeType.Heal:
                //txt_Heal.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.Heal);
                StartCoroutine(InstantiateObjForAnimate(txt_Heal, new Vector3(-15f, -1f, 0)));

                break;
            case AttributeType.Defense:
                //txt_Defense.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.defence.ToString());
                StartCoroutine(InstantiateObjForAnimate(txt_Defense, new Vector3(-18f, -5.85f, 0)));

                break;
            case AttributeType.Disrupt:
                //txt_Disrupt.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.Disrupt);
                StartCoroutine(InstantiateObjForAnimate(txt_Disrupt, new Vector3(-17f, -1f, 0)));

                break;
        }

    }
    public void AnimateEnemyAttributeValue(AttributeType powerName)
    {
        switch (powerName)
        {
            case AttributeType.Attack:
                //txt_Attack.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.attack.ToString());
                StartCoroutine(InstantiateObjForAnimate(txt_Attack, new Vector3(18f, -1f, 0)));

                break;
            case AttributeType.Heal:
                //txt_Heal.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.Heal);
                StartCoroutine(InstantiateObjForAnimate(txt_Heal, new Vector3(18f, 5f, 0)));

                break;
            case AttributeType.Defense:
                //txt_Defense.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.defence.ToString());
                StartCoroutine(InstantiateObjForAnimate(txt_Defense, new Vector3(15f, -1f, 0)));

                break;
            case AttributeType.Disrupt:
                //txt_Disrupt.GetComponent<TextMeshProUGUI>().SetText(this.CardDetail.Disrupt);
                StartCoroutine(InstantiateObjForAnimate(txt_Disrupt, new Vector3(15f, 5f, 0)));

                break;
        }

    }


    public IEnumerator InstantiateObjForAnimate(GameObject obj, Vector3 MovePos)
    {
        GameObject TxtObj = Instantiate(obj, obj.transform);

        TxtObj.GetComponent<TextMeshProUGUI>().SetText(obj.GetComponent<TextMeshProUGUI>().text);
        TxtObj.transform.DOScale(new Vector3(3, 3, 3), 1f);
        yield return new WaitForSeconds(1);

        TxtObj.transform.DORotate(new Vector3(270, 0, 0), 0.5f);
        TxtObj.transform.DOLocalMove(MovePos, 1f);
        yield return new WaitForSeconds(0.5f);

        TxtObj.transform.DOScale(Vector3.one, 0.5f);
        yield return new WaitForSeconds(0.5f);


        Destroy(TxtObj);
    }
    #region new impelemtation 
    public int DiceValue;

    public List<int>    companionData = new List<int>();
    public List<string> companionIDs = new List<string>();

    //TODO: Edit this list when using special Activity
    public int CompanionValueStatus { get => companionData.Sum(x => x); }
    //set => usingElementeum = value; }

    //TODO: Edit this dictionary when using special Activity
    public int UsingElementeum { get => ElementeumStatus.Sum(x => x.Value); }
    //set => usingElementeum = value; }


    public int BaseAttack => CardDetail.attack;
    public int TotalAttack()
    {
        Debug.Log("total BaseAttack " + " " + BaseAttack + " " + debuffValueAttack + " " + (isSpActive ? CardDetail.elementeum : UsingElementeum) + " " + CompanionValueStatus + " " + DiceValue);
        return BaseAttack + (isSpActive ? CardDetail.elementeum : UsingElementeum) - debuffValueAttack + CompanionValueStatus + DiceValue;

    }
    public int BaseDefence => CardDetail.defence;
    public int TotalDefence()
    {
        Debug.Log("total BaseDefence " + " " + BaseDefence + " " + (isSpActive ? CardDetail.elementeum : UsingElementeum) + " " + CompanionValueStatus + " " + DiceValue);
        return BaseDefence + (isSpActive ? CardDetail.elementeum : UsingElementeum) + CompanionValueStatus + DiceValue;

    }
    public int BaseHeal => CardDetail.heal;
    public int TotalHeal()
    {
        Debug.Log("total BaseHeal " + " " + BaseHeal + " " + debuffValueAttack + " " + (isSpActive ? CardDetail.elementeum : UsingElementeum) + " " + CompanionValueStatus + " " + DiceValue);
        return BaseHeal + (isSpActive ? CardDetail.elementeum : UsingElementeum) - debuffValueHeal + CompanionValueStatus + DiceValue;

    }
    public int BaseDisrupt => CardDetail.disrupt;
    public int TotalDisrupt()
    {
        Debug.Log("total BaseDisrupt " + " " + BaseDisrupt + " " + (isSpActive ? CardDetail.elementeum : UsingElementeum) + " " + CompanionValueStatus + " " + DiceValue);
        return BaseDisrupt + (isSpActive ? CardDetail.elementeum : UsingElementeum) + CompanionValueStatus + DiceValue;

    }

    #endregion


    public bool IsEnemyCard = false;
    #region EnemyCard



    public void OnSelectCard_EnemyAsVictim()
    {

        if (!PlayerManager.instance.CanClick || !PlayerManager.instance.CanClickOnEnemyCard)
        {
            return;
        }

        if (GetComponent<CardObject>().isDisable)
        {
            return;
        }


        Debug.Log("Clicked");

        AllAnimCardFalse();

        GameObject clickParticle = Instantiate(PlayerManager.instance.clickParticlePrefab, this.transform);

        Destroy(clickParticle, 1f);


        for (int i = 0; i < PlayerManager.instance.EnemyCardCanvas.Length; i++)
        {
            PlayerManager.instance.enemyCardsObj[i].StrengthAttributes[0].GetComponent<SpriteRenderer>().sortingOrder = 3;
            PlayerManager.instance.enemyCardsObj[i].StrengthAttributes[1].GetComponent<SpriteRenderer>().sortingOrder = 3;
            PlayerManager.instance.enemyCardsObj[i].StrengthAttributes[2].GetComponent<SpriteRenderer>().sortingOrder = 3;
            PlayerManager.instance.enemyCardsObj[i].StrengthAttributes[3].GetComponent<SpriteRenderer>().sortingOrder = 3;
            PlayerManager.instance.EnemyCardCanvas[i].sortingOrder = 4;
            PlayerManager.instance.enemyCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
            PlayerManager.instance.EnemypowerBtns[i].SetBool("Shopower", false);
        }


        if (BattleLobbySC.Instance)
        {

            string contId = GetComponent<CardObject>().CardDetail.card_contract_id;

            //SyncGameplaySc.Instance.GameData.TurnData[SyncGameplaySc.Instance.GameData.TurnData.Length - 1].MyData.OpponentClick = contId;

            //BattleLobbySC.Instance.LastAction = "me:oppcard:" + contId;
            var playerCard = PlayerManager.instance.SelectedCard;
            //SyncGameplaySc.Instance.Save();
            switch (SelectPowerManager.instance.selectedPowerName)
            {
                case AttributeType.Attack:
                    BattleLobbySC.Instance.OppositeCardSelect(
                    contId,
                    playerCard.CardDetail.card_contract_id,
                    SelectPowerManager.instance.selectedPowerName,
                    playerCard.debuffValueAttack.ToString(), playerCard.ElementeumStatus,playerCompanionCardsAnim.SelectedCompanionIds);
                    break;
                case AttributeType.Heal:
                    BattleLobbySC.Instance.OppositeCardSelect(
                    contId,
                    playerCard.CardDetail.card_contract_id,
                    SelectPowerManager.instance.selectedPowerName,
                    playerCard.debuffValueHeal.ToString(), playerCard.ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                    break;
                default:
                    BattleLobbySC.Instance.OppositeCardSelect(contId,
                    playerCard.CardDetail.card_contract_id,
                    SelectPowerManager.instance.selectedPowerName,
                    "0", playerCard.ElementeumStatus, playerCompanionCardsAnim.SelectedCompanionIds);
                    break;
            }

        }

        SelectPowerManager.instance.EnemyselectedCardAnimator = powerAnim;

        //Disable other attributes while opponet's turn
        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Attack)
        {
            for (int i = 0; i < 4; i++)
            {
                powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = false;
            }

            powerAnim.gameObject.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().interactable = true;
            Debug.Log("Enemy attack ability Seleted");
        }

        else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {
            for (int i = 0; i < 4; i++)
            {
                powerAnim.gameObject.transform.GetChild(i).transform.GetChild(0).GetComponent<Button>().interactable = false;
            }
            powerAnim.gameObject.transform.GetChild(3).transform.GetChild(0).GetComponent<Button>().interactable = true;
            Debug.Log("Enemy Heal ability Seleted");
        }

        CheckCardNumber(true);

        canvas.sortingOrder = 8;
        //GetComponent<CardObject>().CheckSpecialAb();
        GetComponent<CardObject>().OppSetUpSort();
    }





    public void OnSelectEnemyCardRPC()  //Multiplayer Card Function
    {
        Debug.Log("enemy card move  power ");
        ResetAttrub();


        AllAnimCardFalse();

        for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
        {
            if (SelectPowerManager.instance.selectedPowerName != AttributeType.Heal)
            {
                PlayerManager.instance.enemyCardsObj[i].ElementeumStatus.Clear();
                for (int j = 0; j < PlayerManager.instance.enemyCardsObj[i].CardDetail.elementeum; j++)
                {
                    PlayerManager.instance.enemyCardsObj[i].elementeumChild[j].GetComponent<RectTransform>().DOAnchorPos(new Vector3(0, 0, 0), 0.5f);
                    PlayerManager.instance.enemyCardsObj[i].elementeumChild[j].GetComponent<RectTransform>().DOScale(1, 0.5f);
                }
            }
        
            PlayerManager.instance.enemyCardsObj[i].debuffImage.transform.DOScale(new Vector3(0, 0, 1), 0.5f);
            PlayerManager.instance.enemyCardsObj[i].debuffImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);
            PlayerManager.instance.enemyCardsObj[i].debuffAttack.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);

            PlayerManager.instance.enemyCardsObj[i].debuffHealImage.transform.DOScale(new Vector3(0, 0, 1), 0.5f);
            PlayerManager.instance.enemyCardsObj[i].debuffHealImage.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0), 1);
            PlayerManager.instance.enemyCardsObj[i].debuffHeal.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 1);

        }
        powerAnim.gameObject.SetActive(true);
        GameObject clickParticle = Instantiate(PlayerManager.instance.clickParticlePrefab, this.transform);
        DiceManager.instance.GetComponent<AudioSource>().clip = DiceManager.instance.CardClick;
        DiceManager.instance.GetComponent<AudioSource>().Play();

        Destroy(clickParticle, 1f);
        GetComponent<CardObject>().CheckSpecialAb();

        if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
        {

            Debug.Log("INside OnSelectCard.......Heal");

            MainAnimator.enabled = false;

            PlayerManager.instance.SelectedHealEnemyCard = this;
            if (PlayerManager.instance.SelectedHealEnemyCard == null)
            {
                Debug.Log("healer card " + this.gameObject.name);
                for (int i = 0; i < PlayerManager.instance.CompanionCards.enemyCompanionCards.Length; i++)
                {
                    PlayerManager.instance.CompanionCards.AutoDeselectEnemyCompanionCardsForBattle(PlayerManager.instance.CompanionCards.enemyCompanionCards[i]);
                    PlayerManager.instance.CompanionCards.enemyCompanionCards[i].isSelected = false;
                }
                for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
                {

                    PlayerManager.instance.enemyCardsObj[i].companionData.Clear();
                    PlayerManager.instance.enemyCardsObj[i].companionIDs.Clear();
                }
            }
            else
            {
                Debug.Log("heal card " + this.gameObject.name);
                
                InGameCards.instance.enemyElemtium.text = PlayerManager.instance.SelectedEnemyCard.isSpActive ? PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(): PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString();
                InGameCards.instance.enemeyAttribute.text = PlayerManager.instance.SelectedEnemyCard.txt_Heal.GetComponent<TextMeshProUGUI>().text.ToString();
                InGameCards.instance.enemyCompanion.text = PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString();
            }

            if (PlayerManager.instance.SelectedEnemyCard == PlayerManager.instance.SelectedHealEnemyCard)
            {
                GetComponent<SpriteRenderer>().sortingOrder = 6;

                canvas.sortingOrder = 8;
                for (int i = 0; i < 3; i++)
                {
                    if (PlayerManager.instance.enemyCardsObj[i] != PlayerManager.instance.SelectedHealEnemyCard)
                    {
                        EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[i].transform, Vector3.zero, 0.3f, Vector3.zero, Quaternion.Euler(Vector3.zero));
                    }
                }

                EnemyCardPosSet(this.transform, new Vector3(-2f, 5f, 14f), 0.3f, new Vector3(0.5f, 0.5f, 0.5f), Quaternion.Euler(0, 0f, 0));
                // powerAnim.gameObject.SetActive(false);
                Debug.Log("Enemy Heal ability Seleted");
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    PlayerManager.instance.enemyCardsObj[i].SortingOrder(2);
                }

                GetComponent<SpriteRenderer>().sortingOrder = 6;
                canvas.sortingOrder = 8;
                PlayerManager.instance.SelectedEnemyCard.GetComponent<SpriteRenderer>().sortingOrder = 6;
                PlayerManager.instance.SelectedEnemyCard.canvas.sortingOrder = 8;

                EnemyCardPosSet(PlayerManager.instance.SelectedEnemyCard.transform, new Vector3(2f, 5f, 14f), 0.3f, new Vector3(0.45f, 0.45f, 0.45f), Quaternion.Euler(0f, 0f, 0));
                EnemyCardPosSet(this.transform, new Vector3(-2f, 5f, 14f), 0.3f, new Vector3(0.55f, 0.55f, 0.55f), Quaternion.Euler(0f, 0f, 0));

                powerAnim.gameObject.SetActive(true);
            }
            if (PlayerManager.instance.SelectedEnemyCard.isSpActive)
            {
                PlayerManager.instance.SelectedEnemyCard.EnemySpAbiAnim();
            }

            DiceManager.instance.dice.DOScale(new Vector3(0.38f, 0.38f, .038f), 0.5f);
        }
        else
        {
            Debug.Log(SelectPowerManager.instance.selectedEnemyPowerName + " is used");
            InGameCards.instance.OppAttribImage.gameObject.SetActive(false);
            InGameCards.instance.enemyElemtium.text = "0";
            InGameCards.instance.enemyDice.text = "0";
            InGameCards.instance.enemeyAttribute.text = "0";
            InGameCards.instance.enemyCompanion.text = "0";
            InGameCards.instance.OppAttribValue.text = "0";

            for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
            {
                PlayerManager.instance.enemyCardsObj[i].UnsetSpecialAbilityPosition();
                PlayerManager.instance.enemyCardsObj[i].HideSpecialAbility();
            }
            for (int i = 0; i < PlayerManager.instance.EnemyCardCanvas.Length; i++)
            {
                PlayerManager.instance.EnemyCardCanvas[i].sortingOrder = 4;
                PlayerManager.instance.enemyCardsObj[i].SortingOrder(3);

                PlayerManager.instance.EnemypowerBtns[i].SetBool("Shopower", false);
            }

            // powerAnim.SetBool("Shopower", true);

            //PlayerManager.instance.EnemyCardAndCanvasOrderSet();

            for (int i = 0; i < PlayerManager.instance.EnemyCardCanvas.Length; i++)
            {
                PlayerManager.instance.enemyCardsObj[i].GetComponent<BoxCollider2D>().enabled = true;
            }
            SelectPowerManager.instance.EnemyselectedCardAnimator = powerAnim;
            //print("PowerAnim---" + powerAnim);
            CheckCardNumber(false);
            canvas.sortingOrder = 8;
        }

        GetComponent<CardObject>().OppSetUpSort();
        if (PlayerManager.instance.SelectedHealEnemyCard == null)
        {

            for (int i = 0; i < PlayerManager.instance.CompanionCards.enemyCompanionCards.Length; i++)
            {
                PlayerManager.instance.CompanionCards.AutoDeselectEnemyCompanionCardsForBattle(PlayerManager.instance.CompanionCards.enemyCompanionCards[i]);
                PlayerManager.instance.CompanionCards.enemyCompanionCards[i].isSelected = false;
            }
            for (int i = 0; i < PlayerManager.instance.enemyCardsObj.Count; i++)
            {

                PlayerManager.instance.enemyCardsObj[i].companionData.Clear();
                PlayerManager.instance.enemyCardsObj[i].companionIDs.Clear();
            }
        }
    }

    // card funtion call on card number click.
    private void CheckCardNumber(bool isEnemy)
    {
        SelectPowerManager.instance.EnemyselectedCardAnimator = powerAnim;

        if (cardNumber == 0)
        {
            MainAnimator.enabled = false;
            PlayerManager.instance.SelectedEnemyCard = this;
            PlayerManager.instance.SelectedEnemyCard.GetComponent<SpriteRenderer>().sortingOrder = 6;
            PlayerManager.instance.enemyCardsObj[1].GetComponent<SpriteRenderer>().sortingOrder = 2;
            PlayerManager.instance.enemyCardsObj[2].GetComponent<SpriteRenderer>().sortingOrder = 2;
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[0].transform, new Vector3(0f, 5f, 14f), 0.3f, new Vector3(0.55f, 0.55f, 0.55f), Quaternion.Euler(0f, 0f, 0));
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[1].transform, new Vector3(-1.8f, 5f, 14f), 0.2f, new Vector3(0.5f, 0.45f, 0.45f), Quaternion.Euler(8f, 0f, 0f));
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[2].transform, new Vector3(1.8f, 5f, 14f), 0.2f, new Vector3(0.5f, 0.45f, 0.45f), Quaternion.Euler(8f, 0f, 0f));
        }

        if (cardNumber == 1)
        {
            MainAnimator.enabled = false;
            PlayerManager.instance.SelectedEnemyCard = this;
            PlayerManager.instance.SelectedEnemyCard.GetComponent<SpriteRenderer>().sortingOrder = 6;
            PlayerManager.instance.enemyCardsObj[0].GetComponent<SpriteRenderer>().sortingOrder = 2;
            PlayerManager.instance.enemyCardsObj[2].GetComponent<SpriteRenderer>().sortingOrder = 2;
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[0].transform, new Vector3(-1.8f, 5f, 14f), 0.3f, new Vector3(0.5f, 0.45f, 0.45f), Quaternion.Euler(8f, 0f, 0));
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[1].transform, new Vector3(0f, 5f, 14f), 0.3f, new Vector3(0.55f, 0.55f, 0.55f), Quaternion.Euler(0f, 0f, 0f));
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[2].transform, new Vector3(1.8f, 5f, 14f), 0.2f, new Vector3(0.5f, 0.45f, 0.45f), Quaternion.Euler(8f, 0f, 0f));
        }

        if (cardNumber == 2)
        {
            MainAnimator.enabled = false;
            PlayerManager.instance.SelectedEnemyCard = this;
            PlayerManager.instance.SelectedEnemyCard.GetComponent<SpriteRenderer>().sortingOrder = 6;
            PlayerManager.instance.enemyCardsObj[1].GetComponent<SpriteRenderer>().sortingOrder = 2;
            PlayerManager.instance.enemyCardsObj[0].GetComponent<SpriteRenderer>().sortingOrder = 2;
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[0].transform, new Vector3(-1.8f, 5f, 14f), 0.3f, new Vector3(0.5f, 0.45f, 0.45f), Quaternion.Euler(8f, 0f, 0));
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[1].transform, new Vector3(1.8f, 5f, 14f), 0.3f, new Vector3(0.5f, 0.45f, 0.45f), Quaternion.Euler(8f, 0f, 0f));
            EnemyCardPosSet(PlayerManager.instance.enemyCardsObj[2].transform, new Vector3(0f, 5f, 14f), 0.2f, new Vector3(0.55f, 0.55f, 0.55f), Quaternion.Euler(0f, 0f, 0f));
        }

        if (isEnemy)
        {
            DiceManager.instance.dice.DOScale(new Vector3(0.38f, 0.38f, .038f), 0.5f);
        }


    }



    #endregion
}