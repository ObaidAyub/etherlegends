﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class Elementium : MonoBehaviour
{
    //public static Elementium instance;
    public string eName;
    public int elemNum;
    public bool selectEle;


    public CardObject cardObject;
    public Button button;
    public bool canClickElementeum;
    public GameObject text;

    InGameCards inGameCards;
    PlayerManager playerManager;

    private void Awake()
    {
        //instance = this;
        button = GetComponent<Button>();

        inGameCards = InGameCards.instance;
        playerManager = PlayerManager.instance;
    }
    private void Start()
    {
        button.onClick.AddListener(SelectElementium);
        canClickElementeum = true;
        selectEle = true;
        elemNum = 1;
    }
    public void SelectElementium()
    {
        Debug.LogError("Click on elementeum ");
        if (playerManager.SelectedCard == null )
        {
            return;
        }
        if (!canClickElementeum)
        {
            return;
        }

        if (playerManager.SelectedCard.CardDetail.card_contract_id != cardObject.CardDetail.card_contract_id)
        {
            return;
        }
        eName = this.gameObject.name;
        Debug.Log(" elementium  " + eName);
        if (SelectPowerManager.instance.selectedPowerName == AttributeType.None)
            return;

        if (selectEle)
        {
            ////Debug.Log("Select elementium ");
            playerManager.CanClickOnElemt = true;
            this.gameObject.transform.DOScale(new Vector3(2, 2, 2), 0.5f);
            this.gameObject.GetComponent<RectTransform>().DOAnchorPos(new Vector3(0, 1,0), 0.5f);
            playerManager.SelectedCard.SyncUseElementeum(true, elemNum,eName);

            AnimateElementeumValue();

            selectEle = false;

        }
        else
        {
            ////Debug.Log("cancle elementium ");
            playerManager.SelectedCard.CancelElementeum(elemNum, eName);
            this.gameObject.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            this.gameObject.GetComponent<RectTransform>().DOAnchorPos(new Vector3(0, 0, 0), 0.5f);
            selectEle = true;
        }
    }

    public void AnimateElementeumValue()
    {
        StartCoroutine(animateElementium());
    }


    public IEnumerator animateElementium()
    {
        GameObject TxtObj = Instantiate(text, this.gameObject.transform);
        TxtObj.SetActive(true);
        TxtObj.GetComponent<TextMeshProUGUI>().text = "1";
        TxtObj.transform.DOScale(new Vector3(1, 1, 1), 1f);
        yield return new WaitForSeconds(1);

        TxtObj.transform.DORotate(new Vector3(270, 0, 0), 0.5f);
        TxtObj.transform.DOLocalMove(new Vector3(-10f, -0.5f, 0), 1f);
        yield return new WaitForSeconds(0.5f);

        TxtObj.transform.DOScale(Vector3.one, 0.5f);
        yield return new WaitForSeconds(0.5f);

        //Debug.Log("Animate Elementium Text ");

        Destroy(TxtObj);
    }

}
