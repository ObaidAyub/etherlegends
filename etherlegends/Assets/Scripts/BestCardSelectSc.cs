﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.UI;
using System;
using Newtonsoft.Json;

public class BestCardSelectSc : MonoBehaviour
{
    public static BestCardSelectSc Instance;
    string TokenId;
    public Transform cardScrollView;
    public SelectedCardSc cardPrefab;
    public Transform companionScrollView;
    public SelectedCardSc companionPrefab;
    public GameObject ProgressBar;

    public GameObject Card1, Card2, Card3;
    public GameObject[] companionCard;
    public TextMeshProUGUI MMRText;
    public int mMR_Val;
    public Text EPText;

    public GameObject CardLevelWarning,MatchMakingWindow;

    string Message = "";
    public int charCardSeleNum = 0;

    public int selectedCompanionCardNum = 0;
    public Text companionCardText;

    public GameObject profileScr, bestCardScr;
    private void Awake()
    {
        Instance = this;
        if (EPText)
        {
            EPText.text = UserDataController.Instance.GetEP() + " EP";
        }

        //FetchingCards();
        RefreshCards();
        StartCoroutine(DelayCheck());

    }
    private void Update()
    {

        //selectedCompanionCardNum = UserDataController.Instance.profileData.companion_card.Count;
        companionCardText.text = "Companion \n" + selectedCompanionCardNum.ToString()+"/8";
    }

    public void CalculateMMR()
    {
        int finalmmr = (Card1.GetComponent<SelectedCardSc>().allocated ? Card1.GetComponent<SelectedCardSc>().CardDetail.level + Card1.GetComponent<SelectedCardSc>().CardDetail.rarity_value : 0) +
                       (Card2.GetComponent<SelectedCardSc>().allocated ? Card2.GetComponent<SelectedCardSc>().CardDetail.level + Card2.GetComponent<SelectedCardSc>().CardDetail.rarity_value : 0) +
                       (Card3.GetComponent<SelectedCardSc>().allocated ? Card3.GetComponent<SelectedCardSc>().CardDetail.level + Card3.GetComponent<SelectedCardSc>().CardDetail.rarity_value : 0);
        mMR_Val = finalmmr;
        MMRText.text ="MMR: "+ mMR_Val.ToString();
    }

    public void ClearCard()
    {
        for (int i = 0; i < cardScrollView.childCount; i++)
        {
           Destroy(cardScrollView.GetChild(i).gameObject);
        }
        UserDataController.Instance.cardDeck.Clear();
    }
    public async void RefreshCards()
    {
        
        if (string.IsNullOrEmpty(SessionManager.Instance.userToken))
        {
            LoadingManager.Instance.ShowInfoPopup("User Not Logged In", true);
            return;
        }
        LoadingManager.Instance.ShowLoader("Getting cards...");
        string response = await SessionManager.Instance.Post(ConstantManager._url_UpdateCardContractBalance, null, SessionManager.Instance.userToken, true);

        if (!string.IsNullOrEmpty(response))
        {


            JSONNode jnode = JSON.Parse(response);

            if (jnode["status"] == 1)
            {
                FetchingCards();

            }

            else if (jnode["status"] == 0)
            {
                //Debug.Log("get profile failed: " + jnode["message"]);
                RefreshCards();
                return;

            }
        }
        else
        {
            RefreshCards();
            LoadingManager.Instance.HideLoader();
            return;
        }
            LoadingManager.Instance.HideLoader();

    }
    private void OnEnable()
    {
    }

    IEnumerator DelayCheck()
    {

        yield return new WaitForSeconds(2f);
        BattleLobbySC.Instance.LogoutImmediately();
        SeasonRewards.Instance.GetCurrentSeasonTrophies();
        
    }

    public void ShowBattleScreen()
    {
        LoadingManager.Instance.ShowLoader("Getting cards...");
        bestCardScr.gameObject.SetActive(true);
        profileScr.gameObject.SetActive(false);
        selectedCompanionCardNum = 0;
        companionCardText.text = "Companion \n" + selectedCompanionCardNum.ToString() + "/8";

        if (EPText)
            EPText.text = UserDataController.Instance.GetEP() + " EP";



        StartCoroutine(setCardsInPanel());
    }

    IEnumerator setCardsInPanel()
    {
        if (UserDataController.Instance.cardDeck.Count <= 0)
        {
            LoadingManager.Instance.HideLoader();
            yield break;
        }
        if (UserDataController.Instance.sortedCards.Count <= 0)
        {
            LoadingManager.Instance.HideLoader();

            yield break;
        }
        yield return new WaitForEndOfFrame();

        if (ShowCharactersAndCompanions.instance.companionScrollView.gameObject.activeInHierarchy)
        {
            ShowCharactersAndCompanions.instance.ClearCard(ShowCharactersAndCompanions.instance.companionScrollView);
        }
        else if (ShowCharactersAndCompanions.instance.characterCardScrollView.gameObject.activeInHierarchy)
        {
            ShowCharactersAndCompanions.instance.ClearCard(ShowCharactersAndCompanions.instance.characterCardScrollView);
        }
        if (cardScrollView.childCount <= 0)
        {
            StartCoroutine(Characters());
            StartCoroutine(Companions());
        }
    }


    public void BackToProfileScr()
    {

        Card1.GetComponent<SelectedCardSc>().UnassignCard();
        Card2.GetComponent<SelectedCardSc>().UnassignCard();
        Card3.GetComponent<SelectedCardSc>().UnassignCard();
        for (int i = 0; i < companionCard.Length; i++)
        {
            companionCard[i].GetComponent<SelectedCardSc>().UnassignCompanionCard(companionCard[i].GetComponent<SelectedCardSc>().CardDetail);
        }
        if (cardScrollView.gameObject.activeInHierarchy)
        {
            ClearCard(cardScrollView);
        }
        if (companionScrollView.gameObject.activeInHierarchy)
        {
            ClearCard(companionScrollView);
        }
        profileScr.gameObject.SetActive(true);
        bestCardScr.gameObject.SetActive(false);
        ShowCharactersAndCompanions.instance.ChangePanel(0);
    }


    

    public async void FetchingCards()
    {

        ClearCard();

        ProgressBar.SetActive(true);


        string response = await SessionManager.Instance.Post(ConstantManager._url_GetUserCardBalance, null, SessionManager.Instance.userToken, true);
        if (string.IsNullOrEmpty(response))
        {
            FetchingCards();
            return;
        }
        else
        {



            JSONNode data = JSON.Parse(response);

            //Debug.Log("GetUser Card Response =---" + response);

            FetchCardsResponse fetchCardsResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<FetchCardsResponse>
                   (response);

            if (fetchCardsResponse.status == 1)
            {
                

                UserDataController.Instance.clearSortedCards();
                


                fetchCardsResponse.data.user_card_details.ForEach( catKv =>
                {
                    catKv.Value.ForEach(y => y.Value.Card_Type = catKv.Key);

                    var allMyCards = catKv.Value.Select(x => x.Value).Where(c => c.value != 0 && !string.IsNullOrEmpty(c.s3_Image)).ToList();

                    allMyCards.ForEach(x=>{
                        //SelectedCardSc NewCard = Instantiate(cardPrefab, cardScrollView);
                        //NewCard.gameObject.SetActive(true);
                        //NewCard.CardDetail = x;
                        UserDataController.Instance.cardDeck.Add(x);
                        //NewCard.RefreshCardDetails(true);

                    });


                    UserDataController.Instance.AddCardsToCollection(catKv.Key, catKv.Value.Select(x=>x.Value).ToList(), false);
                    UserDataController.Instance.AddCardsToCollection(catKv.Key, allMyCards,true);
                }

                );
                //!= "0" && !string.IsNullOrEmpty(data["data"]["user_card_details"][i][j]["s3_image"]))
                        //{
                    //UserDataController.Instance.AddCategory("CatName", SortCard);
                    //UserDataController.Instance.AllCardsAddCategory(AllCatName, AllSortCard);

                


            }
            else
            {

                Debug.Log(data["message"]);
            }

            ProgressBar.SetActive(false);
            ClearCard(cardScrollView);
        }

    }
    public async void SendBestCards()
    {
        UserDataController.Instance.profileData.companion_card.Clear();
        UserDataController.Instance.profileData.best_card.Clear();

        UserDataController.Instance.profileData.best_card.Add(Card1.GetComponent<SelectedCardSc>().CardDetail);
        UserDataController.Instance.profileData.best_card.Add(Card2.GetComponent<SelectedCardSc>().CardDetail);
        UserDataController.Instance.profileData.best_card.Add(Card3.GetComponent<SelectedCardSc>().CardDetail);


        List<CardDetail> cards = new List<CardDetail>();

        cards.AddRange(new List<CardDetail>
        {
            new CardDetail(Card1.GetComponent<SelectedCardSc>().CardDetail.Card_Type,
            Card1.GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                Card1.GetComponent<SelectedCardSc>().CardDetail.level,
                Card1.GetComponent<SelectedCardSc>().CardDetail.rarity,
                Card1.GetComponent<SelectedCardSc>().CardDetail.s3_Image),

            new CardDetail(Card2.GetComponent<SelectedCardSc>().CardDetail.Card_Type,Card2.GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                Card2.GetComponent<SelectedCardSc>().CardDetail.level,Card2.GetComponent<SelectedCardSc>().CardDetail.rarity,
                Card2.GetComponent<SelectedCardSc>().CardDetail.s3_Image),

            new CardDetail(Card3.GetComponent<SelectedCardSc>().CardDetail.Card_Type,Card3.GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                Card3.GetComponent<SelectedCardSc>().CardDetail.level,Card3.GetComponent<SelectedCardSc>().CardDetail.rarity,
                Card3.GetComponent<SelectedCardSc>().CardDetail.s3_Image)

        });

        for (int i = 0; i < 8; i++)
        {
            UserDataController.Instance.profileData.companion_card.Add(companionCard[i].GetComponent<SelectedCardSc>().CardDetail);
        }


        List<CompanionCardDetail> companioncCards = new List<CompanionCardDetail>();

        companioncCards.AddRange(new List<CompanionCardDetail>
        {
            new CompanionCardDetail(companionCard[0].GetComponent<SelectedCardSc>().CardDetail.Card_Type,companionCard[0].GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                companionCard[0].GetComponent<SelectedCardSc>().CardDetail.level,companionCard[0].GetComponent<SelectedCardSc>().CardDetail.rarity,companionCard[0].GetComponent<SelectedCardSc>().CardDetail.s3_Image),

             new CompanionCardDetail(companionCard[1].GetComponent<SelectedCardSc>().CardDetail.Card_Type,companionCard[1].GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                companionCard[1].GetComponent<SelectedCardSc>().CardDetail.level,companionCard[1].GetComponent<SelectedCardSc>().CardDetail.rarity,companionCard[1].GetComponent<SelectedCardSc>().CardDetail.s3_Image),

              new CompanionCardDetail(companionCard[2].GetComponent<SelectedCardSc>().CardDetail.Card_Type,companionCard[2].GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                companionCard[2].GetComponent<SelectedCardSc>().CardDetail.level,companionCard[2].GetComponent<SelectedCardSc>().CardDetail.rarity,companionCard[2].GetComponent<SelectedCardSc>().CardDetail.s3_Image),

               new CompanionCardDetail(companionCard[3].GetComponent<SelectedCardSc>().CardDetail.Card_Type,companionCard[3].GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                companionCard[3].GetComponent<SelectedCardSc>().CardDetail.level,companionCard[3].GetComponent<SelectedCardSc>().CardDetail.rarity,companionCard[3].GetComponent<SelectedCardSc>().CardDetail.s3_Image),

                new CompanionCardDetail(companionCard[4].GetComponent<SelectedCardSc>().CardDetail.Card_Type,companionCard[4].GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                companionCard[4].GetComponent<SelectedCardSc>().CardDetail.level,companionCard[4].GetComponent<SelectedCardSc>().CardDetail.rarity,companionCard[4].GetComponent<SelectedCardSc>().CardDetail.s3_Image),

                 new CompanionCardDetail(companionCard[5].GetComponent<SelectedCardSc>().CardDetail.Card_Type,companionCard[5].GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                companionCard[5].GetComponent<SelectedCardSc>().CardDetail.level,companionCard[5].GetComponent<SelectedCardSc>().CardDetail.rarity,companionCard[5].GetComponent<SelectedCardSc>().CardDetail.s3_Image),

                  new CompanionCardDetail(companionCard[6].GetComponent<SelectedCardSc>().CardDetail.Card_Type,companionCard[6].GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                companionCard[6].GetComponent<SelectedCardSc>().CardDetail.level,companionCard[6].GetComponent<SelectedCardSc>().CardDetail.rarity,companionCard[6].GetComponent<SelectedCardSc>().CardDetail.s3_Image),

                   new CompanionCardDetail(companionCard[7].GetComponent<SelectedCardSc>().CardDetail.Card_Type,companionCard[7].GetComponent<SelectedCardSc>().CardDetail.card_contract_id,
                companionCard[7].GetComponent<SelectedCardSc>().CardDetail.level,companionCard[7].GetComponent<SelectedCardSc>().CardDetail.rarity,companionCard[7].GetComponent<SelectedCardSc>().CardDetail.s3_Image),




        });

        RootObject robj = new RootObject();

        robj.card_details = cards;// UserDataController.Instance.profileData.best_card;// cards;
        robj.companionCardDetail = companioncCards;

        //string Jutil = JsonUtility.ToJson(robj);

        var dataStr = Newtonsoft.Json.JsonConvert.SerializeObject(robj);
        Debug.Log(dataStr);
        Debug.Log(SessionManager.Instance.userToken);

        //#if UNITY_EDITOR
        //        System.IO.File.WriteAllText("./SENT.txt", dataStr);
        //#endif
        ////Debug.Log("Jutil " + Jutil);

        string Response = await SessionManager.Instance.Post(ConstantManager._url_bestCards, dataStr, SessionManager.Instance.userToken, true);

        //////Debug.Log("Send Best Card---"+Response);

        if (string.IsNullOrEmpty(Response))
        {
            LoadingManager.Instance.ShowInfoPopUp("Error Connecting To Server Please Try Again...");
            return;
        }

        var res = JSON.Parse(Response);

        if (res["status"] == 1)
        {
            MatchMakingWindow.SetActive(true);
            GameObject.Find("SocketSession").GetComponent<BattleLobbySC>().StartMatchMaking();
        }
        else if (res["status"] == 2)
        {
            MatchMakingWindow.SetActive(false);
            LoadingManager.Instance.ShowInfoPopUp("Please Select Appropriate Card Level"
                                                    + Environment.NewLine+
                                                    "Note:"+Environment.NewLine+
                                                    "Level of all 3 cards should be + 1 or - 1 with each other.");
        
        }
        else
        {
            Debug.Log(res["message"]);
        }

    }
    

    public void SaveAll()
    {

        if (Card1.GetComponent<SelectedCardSc>().isSaveOrReset)
        {
            Card1.GetComponent<SelectedCardSc>().FinalSave();
        }
        else if (Card2.GetComponent<SelectedCardSc>().isSaveOrReset)
        {
            Card2.GetComponent<SelectedCardSc>().FinalSave();
        }
        else if (Card3.GetComponent<SelectedCardSc>().isSaveOrReset)
        { 
            Card3.GetComponent<SelectedCardSc>().FinalSave();
        }
        //if(companionCard[0].GetComponent<SelectedCardSc>().isSaveOrReset)
        //{
        //    companionCard[0].GetComponent<SelectedCardSc>().FinalSave();
        //}
        //if (companionCard[1].GetComponent<SelectedCardSc>().isSaveOrReset)
        //{
        //    companionCard[1].GetComponent<SelectedCardSc>().FinalSave();
        //}
        //if (companionCard[2].GetComponent<SelectedCardSc>().isSaveOrReset)
        //{
        //    companionCard[2].GetComponent<SelectedCardSc>().FinalSave();
        //}
        //if (companionCard[3].GetComponent<SelectedCardSc>().isSaveOrReset)
        //{
        //    companionCard[3].GetComponent<SelectedCardSc>().FinalSave();
        //}
        //if (companionCard[4].GetComponent<SelectedCardSc>().isSaveOrReset)
        //{
        //    companionCard[4].GetComponent<SelectedCardSc>().FinalSave();
        //}
        //if (companionCard[5].GetComponent<SelectedCardSc>().isSaveOrReset)
        //{
        //    companionCard[5].GetComponent<SelectedCardSc>().FinalSave();
        //}
        //if (companionCard[6].GetComponent<SelectedCardSc>().isSaveOrReset)
        //{
        //    companionCard[6].GetComponent<SelectedCardSc>().FinalSave();
        //}
        //if (companionCard[7].GetComponent<SelectedCardSc>().isSaveOrReset)
        //{
        //    companionCard[7].GetComponent<SelectedCardSc>().FinalSave();
        //}
        
        else
        {
            LoadingManager.Instance.ShowInfoPopUp("Saving Data...");
        }
    }


    public void ResetConfirm()
    {
        LoadingManager.Instance.HidePopUp();
        StartCoroutine(ResetCards());
    }


    IEnumerator ResetCards()
    {
        LoadingManager.Instance.ShowLoader("Reseting Cards...");

        if (Card1.GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            Card1.GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);

        if (Card2.GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            Card2.GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }
        yield return new WaitForSeconds(1f);

        if (Card3.GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            Card3.GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }
        if (companionCard[0].GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            companionCard[0].GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);
        if (companionCard[1].GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            companionCard[1].GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);
        if (companionCard[2].GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            companionCard[2].GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);
        if (companionCard[3].GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            companionCard[3].GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);
        if (companionCard[4].GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            companionCard[4].GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);
        if (companionCard[5].GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            companionCard[5].GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);
        if (companionCard[6].GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            companionCard[6].GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);
        if (companionCard[7].GetComponent<SelectedCardSc>().CardDetail.level > 0)
        {
            companionCard[7].GetComponent<SelectedCardSc>().ResetDefaultAttrib();
        }

        yield return new WaitForSeconds(1f);
       

        LoadingManager.Instance.HideLoader();
        if (Card1.GetComponent<SelectedCardSc>().CardDetail.level < 0 && Card2.GetComponent<SelectedCardSc>().CardDetail.level < 0 && Card3.GetComponent<SelectedCardSc>().CardDetail.level < 0 )
        {
            //  LoadingManager.Instance.ShowInfoPopUp("Nothing To Reset");
        }
    }

    public void ResetAll()
    {
        LoadingManager.Instance.btn_yes.onClick.RemoveAllListeners();

        LoadingManager.Instance.btn_yes.onClick.AddListener(()=>ResetConfirm());
        LoadingManager.Instance.ShowPopUp("All Cards Values Will Get Reset To It's Default Value" + Environment.NewLine + "Are You Sure?");
    }

    public IEnumerator Characters()
    {
        ClearCard(cardScrollView);

        yield return new WaitForEndOfFrame();
        if (UserDataController.Instance.cardDeck.Count > 0 || !string.IsNullOrEmpty(UserDataController.Instance.cardDeck[0].s3_Image))
        {
            var CharacterCards = UserDataController.Instance.sortedCards;
            for (int i = 0; i < CharacterCards.Count; i++)
            {
                if (CharacterCards[i].Name.ToLower() != "companion")
                {
                    for (int j = 0; j < CharacterCards[i].Cards.Count; j++)
                    {
                        yield return new WaitForEndOfFrame();
                        SelectedCardSc NewCard = Instantiate(cardPrefab, cardScrollView);
                        NewCard.gameObject.SetActive(true);
                        NewCard.CardDetail = CharacterCards[i].Cards[j];
                        NewCard.RefreshCardDetails(true);

                    }
                }
            }
        }
        LoadingManager.Instance.HideLoader();

    }
    public IEnumerator Companions()
    {
        ////////Debug.Log("Show Companions");
        ClearCard(companionScrollView);
        yield return new WaitForEndOfFrame();
        if (UserDataController.Instance.cardDeck.Count > 0 || !string.IsNullOrEmpty(UserDataController.Instance.cardDeck[0].s3_Image))
        {
            for (int i = 0; i < UserDataController.Instance.sortedCards.Count; i++)
            {
                if (UserDataController.Instance.sortedCards[i].IsCompanion())
                {
                    var CompanionCards = UserDataController.Instance.sortedCards[i].Cards;
                    for (int j = 0; j < CompanionCards.Count; j++)
                    {
                        yield return new WaitForEndOfFrame();
                        SelectedCardSc NewCard = Instantiate(companionPrefab, companionScrollView);
                        NewCard.gameObject.SetActive(true);
                        NewCard.CardDetail = CompanionCards[j];


                        if (NewCard.CardDetail.heal > 0)
                        {
                            NewCard.txt_Heal.gameObject.SetActive(true);
                            NewCard.txt_Attack.gameObject.SetActive(false);
                            NewCard.txt_Defense.gameObject.SetActive(false);
                            NewCard.txt_Disrupt.gameObject.SetActive(false);

                        }
                       else if (NewCard.CardDetail.attack > 0)
                        {
                            NewCard.txt_Heal.gameObject.SetActive(false);
                            NewCard.txt_Attack.gameObject.SetActive(true);
                            NewCard.txt_Defense.gameObject.SetActive(false);
                            NewCard.txt_Disrupt.gameObject.SetActive(false);

                        }
                       else if (NewCard.CardDetail.defence > 0)
                        {
                            NewCard.txt_Heal.gameObject.SetActive(false);
                            NewCard.txt_Attack.gameObject.SetActive(false);
                            NewCard.txt_Defense.gameObject.SetActive(true);
                            NewCard.txt_Disrupt.gameObject.SetActive(false);

                        }
                       else if (NewCard.CardDetail.disrupt > 0)
                        {
                            NewCard.txt_Heal.gameObject.SetActive(false);
                            NewCard.txt_Attack.gameObject.SetActive(false);
                            NewCard.txt_Defense.gameObject.SetActive(false);
                            NewCard.txt_Disrupt.gameObject.SetActive(true);

                        }
                        NewCard.txt_CardNumber.gameObject.SetActive(false);
                        NewCard.txt_Level.gameObject.SetActive(false);
                        NewCard.txt_Life.gameObject.SetActive(false);


                        NewCard.RefreshCardDetails(true);
                    }
                }
            }
            
            

        }

       
    }
    public void ClearCard(Transform Content)
    {
        
        while (Content.childCount > 0)
        {
            var child = Content.GetChild(0);
            child.SetParent(null);
            Destroy(child.gameObject);
        }
    }
}
