﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
//using Enjin.SDK.DataTypes;
//using static Enjin.SDK.Core.Enjin;
public class LootDropSC : MonoBehaviour
{

    public Light DLight;
    public GameObject[] ObjToDisable;
    public SpriteRenderer Item1,Item2,FinalItem;
    public TextMeshProUGUI IName;
    int MinVal, MaxVal;

    //Final Reward
    string ItemId, ItemName;
    Texture2D ItemImg;

    // Enjin SDK operation settings.
    public string PLATFORM_URL;
    public string DEVELOPER_USERNAME;
    public string DEVELOPER_PASSWORD;
    public string DEVELOPER_AccessToken;
    public int DEVELOPER_IDENTITY_ID;
    public int APP_ID;
   // private User Admin;
    bool Candisable = false;

    private void Awake()
    {
        StartLoot();
    }

    public void StartLoot()
    {
        DLight.intensity = 0;
        for (int i = 0; i < ObjToDisable.Length; i++)
        {

            ObjToDisable[i].SetActive(false);
        }
        MinVal = 0;
        MaxVal = UserDataController.Instance.SupplyRewards.Count;
       
    }


    //GetImage By Item
    public Texture2D ReturnCardImg(string ItemId)
    {
        for (int i = 0; i < UserDataController.Instance.SupplyRewards.Count; i++)
        {
            if (UserDataController.Instance.SupplyRewards[i].ItemID==ItemId)
            {
                return UserDataController.Instance.SupplyRewards[i].ItemImg;
            }

        }
        return null;
    }
    //Return Random Img for Items
    public Texture2D ReturnCardImg()
    {
        int Val = Random.Range(MinVal, MaxVal - 1);
        return UserDataController.Instance.SupplyRewards[0].ItemImg;

    }


    public void setItem1()
    {
        Texture2D CurTexture = ReturnCardImg();
        Item1.sprite = Sprite.Create(CurTexture, new Rect(0, 0, CurTexture.width, CurTexture.height), new Vector2(0.5f, 0.5f));
    }

    public void setItem2()
    {
        Texture2D CurTexture = ReturnCardImg();
        Item2.sprite = Sprite.Create(CurTexture, new Rect(0, 0, CurTexture.width, CurTexture.height), new Vector2(0.5f, 0.5f));
    }

    public void finalItem()
    {
        int Val = Random.Range(MinVal, MaxVal - 1);
        ItemImg= UserDataController.Instance.SupplyRewards[Val].ItemImg;
        ItemName= UserDataController.Instance.SupplyRewards[Val].ItemName;
        ItemId = UserDataController.Instance.SupplyRewards[Val].ItemID;
        IName.text = "You got 1 " + ItemName;

        Texture2D CurTexture = ItemImg;

        FinalItem.sprite = Sprite.Create(CurTexture, new Rect(0, 0, CurTexture.width, CurTexture.height), new Vector2(0.5f, 0.5f));

        StartCoroutine(TransferERC1155(ItemId));

    }

    IEnumerator TransferERC1155(string itemID)
    {
        yield break;
        /*
        Admin = StartPlatform(PLATFORM_URL, DEVELOPER_USERNAME, DEVELOPER_PASSWORD, APP_ID);

        yield return new WaitForSeconds(1f);
        User Player = UserDataController.Instance.LoginEnjin();

        int AdminId = 0;
        int UserId = 0;

        for (int i = 0; i < Admin.identities.Length; i++)
        {
            if (Admin.identities[i].app.id ==APP_ID)
            {
              AdminId =  Admin.identities[i].id;
            }
        }

        for (int i = 0; i < Player.identities.Length; i++)
        {
            if (Player.identities[i].app.id == APP_ID)
            {
                UserId = Player.identities[i].id;
            }
        }

        int CurBal = GetCryptoItemBalance(AdminId, itemID);

        if (CurBal >= 1)
        {
           SendCryptoItemRequest(AdminId, itemID, UserId, 1, (requestEvent) =>
            {
                if (requestEvent.event_type.Equals("txr_pending"))
                {
                    Debug.Log("Pending--" + requestEvent.data);
                }
                else if (requestEvent.event_type.Equals("tx_broadcast"))
                {
                    Debug.Log("Broadcast--" + requestEvent.data);
                }
                else if (requestEvent.event_type.Equals("balance_updated"))
                {
                    Debug.Log("balanceUpdated--" + requestEvent.data);
                }
                else if (requestEvent.event_type.Equals("tx_executed"))
                {
                }
            }, true);
        }
        else
        {
            Debug.Log("Not enough Balance");
        }
        */

    }


    public void DisableThis()
    {
        if (Candisable) { }
        //this.gameObject.SetActive(false);
    }
    public void changeDisable()
    {
        Candisable = true;
    }

}
