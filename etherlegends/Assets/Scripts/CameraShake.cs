﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.
    public Transform camTransform;

    // How long the object should shake for.
     float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 0.1f;
    public float decreaseFactor = 1f;

    public Vector3 originalPos;

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent(typeof(Transform)) as Transform;
        }
    }

    /* void OnEnable()
        {
            originalPos = camTransform.localPosition;
        }
    */

    void Update()
    {
        if (shakeDuration > 0)
        {

            camTransform.localPosition = Vector3.Lerp(camTransform.localPosition, originalPos + Random.insideUnitSphere * shakeAmount, Time.deltaTime * 3);

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            camTransform.localPosition = originalPos;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (InGameCards.instance.PTurn())
            {
                PlayerManager.instance.ShowPopUP("Opponent's Turn!");
            }
        }
    }

    public void ShakeCamera(float Duration)
    {
        originalPos = camTransform.localPosition;
        shakeDuration =Duration;
    }

    public void ShakeCamera(float Duration,float shakeAmt)
    {
        originalPos = camTransform.localPosition;
        shakeAmount = shakeAmt;
        shakeDuration = Duration;
    }

}
