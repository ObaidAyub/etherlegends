﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class CreateHtmlFile : MonoBehaviour
{

    public void CreateFile()
    {

        string path = Application.persistentDataPath + "/web.html";

        if (!File.Exists(path))
        {
            File.WriteAllText(path,
            "<html>\n<body>\n <iframe src='https://opensea.io/assets/ether-legends?embed=true' width='600' height='1000' frameborder='0'></iframe>\n    </body>\n</html>\n");
        }
       
    }

    // Start is called before the first frame update
    void Start()
    {
        CreateFile();
    }


}
