﻿

public class ConstantManager
{


    //public const string _url_baseApi = //"https://etherlegendsapi.etherlegends.com/api"; //"http://192.168.10.125:5040/api"; 

    //public const string _url_SocketUrl = "https://etherlegendsapi.etherlegends.com:5050";  // "https://192.168.10.125:5050";

    //public const string _url_OpenGameSocketUrl = "https://etherlegendsapi.etherlegends.com:5051";  //"https://192.168.10.125:5051";

    //public const string _url_ETHMainNet = "https://mainnet.infura.io/v3/1b57fe1cae5e4973ba7b15eef3a2768e";

    /// <summary>
    /// live _url_baseApi = "https://game.etherlegends.com/api";
    /// Live _url_SocketUrl = "https://socket.etherlegends.com";
    /// staging _url_baseApi = "https://etherlegend.tekstaging.com/api/";
    /// staging _url_SocketUrl = "https://socketetherlegend.tekstaging.com";
    /// </summary>

    public const string _url_baseApi = "https://etherlegend.tekstaging.com/api";// "https://etherlegend.tekstaging.com/api"; //"http://54.157.45.103:5040/api"; 

    public const string _url_SocketUrl = "https://socketetherlegend.tekstaging.com";//"https://etherlegend.tekstaging.com:5050";  // "https://192.168.10.125:5050";  https://etherlegend.etherlegend.co/api

    public const string _url_OpenGameSocketUrl = "https://socketetherlegend.tekstaging.com";//"https://etherlegend.tekstaging.com:5051";  //"https://192.168.10.125:5051";

    public const string _url_ETHMainNet = "https://mainnet.infura.io/v3/a76103c6ae194fe493701fb4f4104419";//"https://mainnet.infura.io/v3/1b57fe1cae5e4973ba7b15eef3a2768e";

    public const string _url_Login = "/users/login";
    public const string _url_ForceLogin = "/users/forceLogin";

    public const string _url_Register = "/users/register";
    public const string _url_ChangePassword = "/users/changePassword";
    public const string _url_GetProfile = "/users/getProfile";
    public const string _url_RemoveProfile = "/users/removeAvatar";
    public const string _url_userlogout = "/users/logout";
    public const string _url_LeaderBoard = "/users/getUserLeaderBoard";
    public const string _url_getOtherUserProfile = "/users/getOtherUserProfile";
    public const string _url_UpdateDisplayName = "/users/updateDisplayName";



    public const string _url_confirmUserToken = "/users/confirmUserToken";

    public const string _url_ConfirmWalletAddress = "/wallet/walletAddressConfirm";

    public const string _url_CreateImportNewWallet = "/wallet/createImportNewWallet";

    public const string _url_CheckEmailWallet = "/wallet/walletEmailConfirm";

    public const string _url_ForceNewWallet = "/wallet/forceNewWallet";

    public const string _url_GetImageUrl = "/wallet/getImageFromJson";

    public const string _url_getAllTokenBalance = "/wallet/getAllTokenBalance";

    public const string _url_getAllCardContractBalance = "/wallet/getAllCardContractBalance";   /// <summary>
    /// ys apis bharwi ha login honaka fron bad isko chala na ha 
    /// </summary>

    public const string _url_UpdateCardContractBalance = "/wallet/updateAllCardContractBalance";

    public const string _url_GetUserCardBalance = "/card/getUserCardBalance";

    public const string _url_getMetaDataOfCards = "/wallet/getMetaDataOfCards";

    public const string _url_getTransactionConfirmation = "/wallet/getTransactionConfirmation";

    public const string _url_GetAllCardDetailsFromStore = "/wallet/getAllCardDetailsForStore";

    public const string _url_getCardsVersion = "/card/getCardsVersion";

    public const string _url_bestCards = "/card/bestCards";

    public const string _url_UpdateCard = "/card/updateCardLevel";

    public const string _url_UploadProfilePic = "/users/updateAvatar";

    public const string _url_GetbaseValue = "/card/getCardBaseValue";

    public const string _url_GetAllUserTransaction = "/wallet/getAllUserTransaction";

    public const string _url_StoreTransaction = "/wallet/storeTransaction";

    public const string _url_Get_Active_Season = "/seasons/get-active-season";

    public const string _url_All_Season = "/seasons/all-seasons";

    public const string _url_SeasonLeaderboard = "/seasons/leaderboard";

    public const string _url_userTrophies = "/seasons/user-trophies";

}
