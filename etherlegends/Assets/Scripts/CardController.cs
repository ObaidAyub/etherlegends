﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CardController : MonoBehaviour
{

    public static CardController Instance;

    public GameObject[] cardDeck;

    public Transform[] cardDeckDefaultPos;

    public Transform[] cardDisabledPos;

    public float startAnimationDuration;

    public GameObject CardSelectPos;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }   

    private void Awake()
    {
        Instance = this;
    }

    public bool CanClick = false;

    public void StartGame()
    {

        /*   for (int i = 0; i < cardDeck.Length; i++)
        {
            cardDeck[i].transform.position = cardDisabledPos[i].position;
            cardDeck[i].SetActive(true);
            cardDeck[i].transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        }
        Invoke("SetCard", 1);
        */ 
              
    }

    int i = 0;
    void SetCard()
    {
        Sequence s = DOTween.Sequence();
        s.Append(cardDeck[i].transform.DOMove(cardDeckDefaultPos[i].position, startAnimationDuration));
        s.Join(cardDeck[i].transform.DOScale(new Vector3(0.52f, 0.52f, 0.52f), startAnimationDuration));
        s.OnComplete(() => oncompleteAnim());

    }

    void oncompleteAnim()
    {
        if (i < 2)
        {
            i++;
            startAnimationDuration -= 0.03f;
            SetCard();
        }
        if (i == 2)
        {
            CanClick = true;
        }
    }




    public void OnSelectCard(int cardnumber)
    {
        if (!CanClick)
        {
            return;
        }

        Sequence s = DOTween.Sequence();
        s.Append(cardDeck[cardnumber].transform.DOMove(CardSelectPos.transform.position, 0.2f));
        s.Join(cardDeck[cardnumber].transform.DOScale(new Vector3(0.64f, 0.64f, 0.64f), 0.2f));
        s.OnComplete(() => SetAnim(cardnumber));
        int count = 0;
        for (int j = 0; j < cardDeck.Length; j++)
        {
            if (j != cardnumber)
            {
                Sequence s2 = DOTween.Sequence();
                s2.Append(cardDeck[j].transform.DOMove(cardDisabledPos[count].position, 0.2f));
                s2.Join(cardDeck[j].transform.DOScale(new Vector3(0.3f, 0.3f, 0.3f), 0.2f));
                cardDeck[j].GetComponent<CardObject>().SetAnimation(false);
                count++;
            }
        }
    }

    public void SetAnim(int number)
    {
        cardDeck[number].GetComponent<CardObject>().SetAnimation(true);
    }


}
