﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{

    public Text text_loadingMessage, txt_MainloadingMessage, text_infoMessage,txt_PopUpMessage , TxtInfoSingleBtnMsg;

    public GameObject loadingPanel, infoPanel, PopUpWindow, MainLoadingPanel, InfoPopupSingleBtn, OnScreenKeyboard, networkErrorWindow;

    public int defaultTime = 1;

    IEnumerator coroutine;

    public Button btn_yes;

    public static LoadingManager Instance;

    public AudioClip btn_click;

    [Header("Authentication")]
    public GameObject AuthenticationPopup;

    [SerializeField]
    InputField AuthenticationInput;

    [SerializeField]
    Text AuthenError;

    public delegate void FunctionToCall();

    FunctionToCall CallingFunction;

    public Slider CardLoadingBar;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
    public void ShowLoader(string message)
    {       
        text_loadingMessage.text = message;
        loadingPanel.SetActive(true);
    }
    public void ShowLoader(string message,float DisableTime)
    {
        text_loadingMessage.text = message;
        loadingPanel.SetActive(true);

        Invoke("HideLoader", DisableTime);
    }
    public void Btn_ClickSound()
    {
        GetComponent<AudioSource>().clip = btn_click;
        GetComponent<AudioSource>().Play();
    }

    public void ShowInfoPopUp(string Message)
    {
        TxtInfoSingleBtnMsg.text = Message;
        InfoPopupSingleBtn.SetActive(true);
    }

    public void HideInfoPopup()
    {
        StartCoroutine(DisablePopUp(InfoPopupSingleBtn));
        InfoPopupSingleBtn.transform.GetChild(0).GetComponent<Animator>().SetTrigger("reverse");
    }

    IEnumerator DisablePopUp(GameObject PopUpToHide)
    {
        yield return new WaitForSeconds(0.7f);
        PopUpToHide.SetActive(false);
    }

    public void ShowPopUp()
    {
        PopUpWindow.SetActive(true);
    }

    public void ShowPopUp(string message)
    {
        print(message);
        txt_PopUpMessage.text = message;
        PopUpWindow.SetActive(true);
    }

    public void HidePopUp()
    {
        StartCoroutine(DisablePopUp());
        PopUpWindow.transform.GetChild(0).GetComponent<Animator>().SetTrigger("reverse");
    }

    public void ShowAuthentication(FunctionToCall function)
    {
        AuthenError.gameObject.SetActive(false);
        AuthenticationPopup.SetActive(true);
        AuthenticationInput.text = "";
        CallingFunction = function;
    }

    public void AuthenticateIt()
    {
        if( AuthenticationInput.text.Equals(UserDataController.Instance.Myprefs.UPass))
        {
            AuthenticationInput.text = "";
            AuthenticationPopup.SetActive(false);
            CallingFunction();
        }
        else
        {
            AuthenError.gameObject.SetActive(true);            
        }
    }

    IEnumerator DisablePopUp()
    {
        yield return new WaitForSeconds(0.7f);
        PopUpWindow.SetActive(false);
    }

    public void HideLoader()
    {
        text_loadingMessage.text = "";
        loadingPanel.SetActive(false);
    }

    public void ShowInfoPopup(string message, bool canCloseLoding = false)
    {
        if (canCloseLoding)
            HideLoader();
        text_infoMessage.text = message;
        coroutine = _show(defaultTime);
        infoPanel.SetActive(true);
        StartCoroutine(coroutine);
    }

    private IEnumerator _show(int duration)
    {
        yield return new WaitForSeconds(duration);
        if (infoPanel.activeInHierarchy)
        {
            infoPanel.SetActive(false);
        }
    }



    public void ShowMainLoading(string text) { 
        //Debug.Log("Show");
        CardLoadingBar.gameObject.SetActive(false);
        txt_MainloadingMessage.text = text;
        MainLoadingPanel.SetActive(true);
    }

    public void ShowMainLoading(string LoadingText, bool load = false)
    {
        txt_MainloadingMessage.text = LoadingText;
        MainLoadingPanel.SetActive(true);
        ////Debug.Log("enable");
        if (load)
        {
            

            CardLoadingBar.gameObject.SetActive(true);


        }
        else
        {
           

            CardLoadingBar.gameObject.SetActive(false);
            
        }

    }
    public void HideMainLoading()
    {
        //Debug.Log("Hide");
        MainLoadingPanel.SetActive(false);
        CardManagerSC.Instance.downloadingText.gameObject.SetActive(false);
    }
    System.Action OnConnectionResume;
    public void ShowNetworkErrorWindow(System.Action argOnConnectionResume= null)
    {
        OnConnectionResume = argOnConnectionResume;

        networkErrorWindow.SetActive(true);
    }
    public void HideNetworkErrorWindow()
    {
        networkErrorWindow.SetActive(false);

        if (OnConnectionResume != null )
        {
            OnConnectionResume.Invoke();
        }
    }
    public void RetryForNetwork()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ShowLoaderScreenForCardsFetching(string LoadingText, int Count)
    {

    }
}
