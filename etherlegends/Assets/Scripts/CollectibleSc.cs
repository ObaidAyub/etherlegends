﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleSc : MonoBehaviour
{

    [System.Serializable]
    public struct RewardItem
    {
        public string ItemName;
        public string ItemID;
        public int ItemValue;
        public string ImgString;
        public Texture2D ItemImg;
    }

    [System.Serializable]
    class ImgUrl
    {
        public string url;
    }

    public List<RewardItem> SupplyRewards = new List<RewardItem>();

    public List<RewardItem> TempSupplyRewards = new List<RewardItem>();


    [Header("Collectibles Object")]
    public GameObject ERC721Cat;
    public Transform ERC721CatParent;
    public GameObject ERC721;
    public Transform ERC721Parent;

    public GameObject ERC1155;
    public Transform ERC1155Parent;

    public GameObject ERC20;
    public Transform ERC20Parent;
   

    public void RefreshERC721()
    {
        UserDataController.Instance.RefreshCard721(ERC721Cat, ERC721CatParent, ERC721, ERC721Parent);
    }

    public void RefreshERC721Delay()
    {


        for (int i = 0; i < ERC721Parent.childCount; i++)
        {
            Destroy(ERC721Parent.GetChild(i).gameObject);
        }
        for (int i = 0; i < ERC721CatParent.childCount; i++)
        {
            Destroy(ERC721CatParent.GetChild(i).gameObject);
        }

        StartCoroutine(DelayCard());
    }

    IEnumerator DelayCard()
    {
        yield return new WaitForSeconds(5f);
        UserDataController.Instance.RefreshCard721(ERC721Cat, ERC721CatParent, ERC721, ERC721Parent);
    }

    public void RefreshERC20()
    {

        UserDataController.Instance.RefreshERC20(ERC20Parent);

    }

    public void GetCardByCategory(GameObject CatButton)
    {
        UserDataController.Instance.RefreshCard721(ERC721Cat, ERC721CatParent, ERC721, ERC721Parent,CatButton);
    }
   
}
