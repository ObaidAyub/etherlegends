﻿
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.Common;

public class TransactSc : MonoBehaviour
{
    public GameObject TransfObj, ReceiveObjWallet, ReceiveObjEnjin;
    public RawImage QRImageId,QRImageAdd;
    public Text walletAddress, IdentityId;
    public bool isCreated;
    public Transform Container;
    [Header("Item UI")]
    public Text TxtNo;
    public RawImage ItemImage;
    public Text TxtName;
    public Text TxtValue;

    //Item Value
    string I_ID;
    string I_Name;
    Texture2D I_Image;
    int I_Value;


    public void SetValue(int No,string ItemId,string Name,int Val,Texture2D Img)
    {
        I_ID = ItemId;
        I_Name = Name;
        I_Image = Img;
        I_Value = Val;
        TxtNo.text = No.ToString();
        ItemImage.texture= Img;
        TxtName.text =Name;
        TxtValue.text = Val.ToString();
    }



    public void showQRCOde()
    {
        TransfObj.SetActive(false);

        ReceiveObjEnjin.SetActive(true);

        IdentityId.text = UserDataController.Instance.IdentityID;
              
        QRImageId.texture = GenerateBarcode(IdentityId.text, BarcodeFormat.QR_CODE, QRImageId.mainTexture.width, QRImageId.mainTexture.height);
    }

    public void showWalletQRCOde()
    {
        TransfObj.SetActive(false);
        ReceiveObjWallet.SetActive(true);

        walletAddress.text = UserDataController.Instance.GetWalletAddress();
              
        QRImageAdd.texture = GenerateBarcode(UserDataController.Instance.GetWalletAddress(), BarcodeFormat.QR_CODE, QRImageAdd.mainTexture.width, QRImageAdd.mainTexture.height);
    }


    private Texture2D GenerateBarcode(string data, BarcodeFormat format, int width, int height)
    {

        // Generate the BitMatrix
        BitMatrix bitMatrix = new MultiFormatWriter()
            .encode(data, format, width, height);

        // Generate the pixel array
        Color[] pixels = new Color[bitMatrix.Width * bitMatrix.Height];

        int pos = 0;

        for (int y = 0; y < bitMatrix.Height; y++)
        {
            for (int x = 0; x < bitMatrix.Width; x++)
            {
                pixels[pos++] = bitMatrix[x, y] ? Color.black : Color.white;
            }
        }

        // Setup the texture
        Texture2D tex = new Texture2D(bitMatrix.Width, bitMatrix.Height);
        tex.SetPixels(pixels);
        tex.Apply();
        isCreated = true;
        return tex;
    }

   /* public void TransferEther()
    {
        TransfObj.GetComponent<TransferErc1155>().setValues(I_ID, I_Name, I_Value.ToString());
        TransfObj.SetActive(true);
        ReceiveObjWallet.SetActive(false);
        ReceiveObjEnjin.SetActive(false);
    }
    */
   

}
