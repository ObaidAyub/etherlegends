﻿using ZXing;
using ZXing.Common;
using UnityEngine;
using UnityEngine.UI;
using BayatGames.SaveGameFree;
//using Enjin;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System;
using System.Linq;
//using Enjin.SDK.DataTypes;
//using static Enjin.SDK.Core.Enjin;

public class UserDataController : MonoBehaviour
{

    [System.Serializable]
    public class UserPrefs
    {
        public string PKey, Mnemonic, Address, EnjinAddress, UserToken, UserID;
        public int EP;
        public float Elemt, enjin, Ether,LP,Arena;
        public string UName, UPass;
        public string user_token;
        //public int version;
    }

    [System.Serializable]
    public struct RewardItem
    {
        public string ItemName;
        public string ItemID;
        public int ItemValue;
        public Texture2D ItemImg;
    }
    
    [System.Serializable]
    public class CardCategoryClass
    {
        public string Name;
        public List<Card> Cards;
        public bool IsCompanion()
        {
            return this.Name.Equals("Companion");
        }    
    }
    
    [System.Serializable]
    public struct CardImageStored
    {
        public string cardname, contractId;
        public Texture2D CardImage;
    }

    public static UserDataController Instance;

    public ProfileData profileData;

    public List<Card> cardDeck;

    public List<RewardItem> SupplyRewards = new List<RewardItem>();

    public UserTokenData userTokenData;

    public Text Ether, Elemt, Enjin, EP, LP,Arena;

    public GameObject ProfileObj;

    //Save Game Data
    private string _encodePassword;

    public string identifier = "sys.dat";

    public List<CardCategoryClass> sortedCards;

    public List<CardCategoryClass> AllsortedCards;

    public List<CardImageStored> CardImgs;

    public UserPrefs Myprefs;

    public string IdentityID;

    private void Awake()
    {
        //deleteAllData();

        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Instance.Ether = this.Ether;
            Instance.Elemt = this.Elemt;
            Instance.Enjin = this.Enjin;
            Instance.EP = this.EP;
            Instance.LP = this.LP;
            Instance.Arena = this.Arena;
            Instance.CardImgs = this.CardImgs;
            Instance.sortedCards = this.sortedCards;
            Instance.SupplyRewards = this.SupplyRewards;
            Destroy(this.gameObject);
        }

       
        StartCoroutine(DelayStart());
    }

    public bool IsNotToken()
    {
        return string.IsNullOrEmpty(Myprefs.user_token);
    }

    public void setToken(string token)
    {
        Myprefs.user_token = token;
        Save();
    }

    public string gettoken()
    {
        Load();
        return Myprefs.user_token;
    }

    public void deleteAllData()
    {
        SaveGame.Delete(identifier);
    }


    public void AddSuppyRewards(string itemname, string itemid, int Itemval, Texture2D itemimg)
    {

        for (int i = 0; i < SupplyRewards.Count; i++)
        {
            if (SupplyRewards[i].ItemID == itemid)
            {
                return;
            }
        }

        RewardItem rw = new RewardItem();
        rw.ItemName = itemname;
        rw.ItemID = itemid;
        rw.ItemValue = Itemval;
        rw.ItemImg = itemimg;
        SupplyRewards.Add(rw);

    }



    IEnumerator DelayStart()
    {

        yield return new WaitForSeconds(1f);
        _encodePassword = "12345678910abcdef12345678910abcdef";
        SaveGame.EncodePassword = _encodePassword;
        SaveGame.Encode = true;
        SaveGame.Serializer = new BayatGames.SaveGameFree.Serializers.SaveGameBinarySerializer();

        if (SaveGame.Exists(identifier))
        {
            Load();
        }
        else
        {
            Save();
        }

    }

    //Method to get wallet Address
    public string GetWalletAddress()
    {
        return Myprefs.Address;
    }

    //Method to get Private Key
    public string GetPrivateKey()
    {
        return Myprefs.PKey;
    }

    public string GetSeedPhrase()
    {
        return Myprefs.Mnemonic;
    }

    /*
    public User LoginEnjin()
    {

        string Uname = Myprefs.UName;

        string Upass = Myprefs.UPass;
        
        var Ply = Login(Uname, Upass);

        //////Debug.Log("Login Name---"+Ply.name);
        //////Debug.Log("Id---" + Ply.id);
        //////Debug.Log("ActiveIdentity---" + Ply.identities[0].id);
        IdentityID = Ply.identities[0].id.ToString();
        return Ply;
    }
    */
    public void SetInfoP(string PString) // method to change pass 
    {
        Myprefs.UPass = PString;
        Save();
    }

    public void setTOken20(float elemnt, float EJC, float ether, int EPower, float Lp, float arena)
    {
        Myprefs.Elemt = elemnt;
        Myprefs.Ether = ether;
        Myprefs.enjin = EJC;
        Myprefs.EP = EPower;
        Myprefs.LP = Lp;
        Myprefs.Arena = arena;
        Ether.text = ether.ToString();
        Elemt.text = elemnt.ToString();
        Enjin.text = EJC.ToString();

        EP.text = EPower.ToString();
        LP.text = Lp.ToString();
        Arena.text = arena.ToString();
        Save();

    }
    //--------Getter Setter For User Login Token

    public void SetUserID(string UserID)
    {
        //////Debug.Log("set user TOken " + Token);
        Myprefs.UserID = UserID;
        Save();
    }

    public string GetUserID()
    {
        Load();
        return Myprefs.UserID;
    }

    //------------------------------------------

    //--------Getter Setter For User Login Token

    public void SetUserToken(string Token)
    {
        //////Debug.Log("set user TOken " + Token);
        Myprefs.UserToken = Token;
        Save();
    }

    public string GetUserToken()
    {
        Load();
        return Myprefs.UserToken;
    }

    //------------------------------------------

    //Getter Setter for Ethereal power
    public int GetEP()
    {
        Load();
        return Myprefs.EP;
    }

    public void SetEPower(int Val)
    {

        Myprefs.EP = Val;
        Save();
    }
    //---------------------------

    //Getter Setter for Ethereum
    public float GetEther()
    {
        Load();
        return Myprefs.Ether;
    }

    public void SetEther(float Val)
    {

        Myprefs.Ether = Val;
        Save();
    }
    //---------------------------


    public bool ValidateUser(string Unam, string pass)
    {
        // return true;

        Load();

        string Uname = Myprefs.UName;
        string Upass = Myprefs.UPass;

        if (Uname.ToLower() == Unam.ToLower() && Upass == pass)
        {
            return true;
        }

        return false;
    }

    public void setUser(string Unam, string Pass)
    {
        if (Unam == "" || Pass == "")
        {
            return;
        }

        //////Debug.Log("Set User" + Unam);

        Myprefs.UName = Unam;
        //////Debug.Log("After Set User--" + Myprefs.UName);
        Myprefs.UPass = Pass;

        Save();
    }
    public int GetVersion()
    {

        //Load();
        //Myprefs.version = Version;
        return PlayerPrefs.GetInt("version", 0);
    }
    public void SetVersion(int Version)
    {
        //Myprefs.version = Version;
        //Save();

        PlayerPrefs.SetInt("version", Version);
        PlayerPrefs.Save();
    }

    public void CategorizeCards()
    {

        for (int i = 0; i < cardDeck.Count; i++)
        {

            //////Debug.Log("cardDeck");

            foreach (var cardItem in sortedCards)
            {
                //////Debug.Log("cardItem" + cardItem.Name);

                if (cardItem.Name == cardDeck[i].Card_Type)
                {
                    //////Debug.Log("card" + cardDeck[i].name);

                    cardItem.Cards.Add(cardDeck[i]);

                    return;
                }
            }
        }

    }


    public Texture2D GenerateBarcode(string data, int width, int height)
    {

        BarcodeFormat format = BarcodeFormat.QR_CODE;

        // Generate the BitMatrix
        BitMatrix bitMatrix = new MultiFormatWriter()
            .encode(data, format, width, height);

        // Generate the pixel array
        Color[] pixels = new Color[bitMatrix.Width * bitMatrix.Height];

        int pos = 0;

        for (int y = 0; y < bitMatrix.Height; y++)
        {
            for (int x = 0; x < bitMatrix.Width; x++)
            {
                pixels[pos++] = bitMatrix[x, y] ? Color.black : Color.white;
            }
        }

        // Setup the texture
        Texture2D tex = new Texture2D(bitMatrix.Width, bitMatrix.Height);
        tex.SetPixels(pixels);
        tex.Apply();

        return tex;
    }

    public void clearSortedCards()
    {
        sortedCards.Clear();
    }

    //public void AddCategory(string CatName, List<Card> cardsToAdd)
    //{

    //    if (string.IsNullOrEmpty(CatName))
    //    {
    //        return;
    //    }

    //    //////Debug.Log("Add Cat--" + CatName);

    //    foreach (var cardItem in sortedCards)
    //    {
    //        if (cardItem.Name == CatName)
    //        {
    //            return;
    //        }
    //    }

    //    if (sortedCards.Any(c => c.Name == CatName))
    //    {
    //        sortedCards.First(c => c.Name == CatName).Cards.AddRange(cardsToAdd);
    //    }
    //    else
    //    {
    //        CardCategoryClass cd3 = new CardCategoryClass();
    //        cd3.Name = CatName;
    //        cd3.Cards = cardsToAdd;
    //        sortedCards.Add(cd3);
    //    }
    //}

    /// <summary>
    /// Send True if you want to add into sortedCards i.e. PlayerCards
    /// </summary>
    /// <param name="CatName"></param>
    /// <param name="cardsToAdd"></param>
    /// <param name="isMyCollection"></param>
    public void AddCardsToCollection(string CatName, List<Card> cardsToAdd,bool isMyCollection) {
        if (string.IsNullOrEmpty(CatName))
        {
            return;
        }

        var collection = (isMyCollection) ? sortedCards : AllsortedCards;

        if (collection.Any(c => c.Name == CatName))
        {
            collection.First(c => c.Name == CatName).Cards.AddRange(cardsToAdd);
        }
        else
        {

            CardCategoryClass cd3 = new CardCategoryClass();
            cd3.Name = CatName;
            cd3.Cards = cardsToAdd;
            collection.Add(cd3);
        }

    }

    //public void AllCardsAddCategory(string CatName, List<Card> cardsToAdd)
    //{
    //    if (string.IsNullOrEmpty(CatName))
    //    {
    //        return;
    //    }

    //    //////Debug.Log("All sorted Cat--" + CatName);

    //    foreach (var cardItem in AllsortedCards)
    //    {
    //        if (cardItem.Name == CatName)
    //        {
    //            return;
    //        }
    //    }

    //    if (AllsortedCards.Any(c => c.Name == CatName))
    //    {
    //        AllsortedCards.First(c => c.Name == CatName).Cards.AddRange(cardsToAdd);
    //    }
    //    else
    //    {

    //        CardCategoryClass cd3 = new CardCategoryClass();
    //        cd3.Name = CatName;
    //        cd3.Cards = cardsToAdd;
    //        AllsortedCards.Add(cd3);
    //    }
    //}

    public string GetCardContract(string CName)
    {
        for (int i = 0; i < AllsortedCards.Count; i++)
        {
            for (int j = 0; j < AllsortedCards[i].Cards.Count; j++)
            {
                if (AllsortedCards[i].Cards[j].name.Equals(CName) || AllsortedCards[i].Cards[j].name.Contains(CName))
                {
                    //////Debug.Log("CardName " + CName);

                    return AllsortedCards[i].Cards[j].card_contract_id;
                }
            }

        }
        return null;
    }


    public void RefreshCard721(GameObject catObj, Transform CatobjTrans, GameObject CardObj, Transform CardobjTrans)
    {
        //LoadingManager.Instance.ShowMainLoading();


        for (int i = 0; i < CardobjTrans.childCount; i++)
        {
            Destroy(CardobjTrans.GetChild(i).gameObject);
        }

        for (int i = 0; i < CatobjTrans.childCount; i++)
        {
            Destroy(CatobjTrans.GetChild(i).gameObject);
        }

        //////Debug.Log("Refresh ERC721");

        try
        {
            if (AllsortedCards.Count <= 0)
            {
                //////Debug.Log("Less SOrted Cards");
                //   ProfileObj.GetComponent<CollectibleSc>().RefreshERC721();
                StartCoroutine(DelayFetchERC721(catObj, CatobjTrans, CardObj, CardobjTrans));

            }
            else
            {
                foreach (var item in sortedCards) //save and spawn Cards which user have 
                {
                    // var category = Instantiate(catObj, CatobjTrans);
                    // category.name = item.Name;

                    //category.transform.GetChild(0).GetComponent<Text>().text = item.Name.Replace('_', ' ');
                    // category.SetActive(true);

                    for (int i = 0; i < item.Cards.Count; i++)
                    {

                        bool canset = false;
                        var CardObject = Instantiate(CardObj, CardobjTrans);

                        for (int j = 0; j < CardImgs.Count; j++)
                        {
                            if (CardImgs[j].contractId.Trim() == item.Cards[i].card_contract_id.Trim())
                            {
                                CardObject.GetComponent<CardInfoSc>().SetCardvalue(CardImgs[j].CardImage, item.Cards[i]);
                                CardObject.GetComponent<RawImage>().texture = CardImgs[j].CardImage;
                                //////Debug.Log("Card Check ---->" + item.Cards[i].value);

                                CardObject.SetActive(true);
                                canset = true;
                                break;
                            }
                        }

                        if (!canset)
                            StartCoroutine(CardImage(CardObject, item.Cards[i].Image, item.Cards[i]));



                    }
                }

                foreach (var item in AllsortedCards)  //spawn cards after user cards spawned
                {
                    var category = Instantiate(catObj, CatobjTrans);
                    category.name = item.Name;

                    category.transform.GetChild(0).GetComponent<Text>().text = item.Name.Replace('_', ' ');
                    category.SetActive(true);

                    for (int i = 0; i < item.Cards.Count; i++)
                    {
                        if (item.Cards[i].value == 0)
                        {
                            bool canset = false;
                            var CardObject = Instantiate(CardObj, CardobjTrans);

                            for (int j = 0; j < CardImgs.Count; j++)
                            {
                                if (CardImgs[j].contractId.Trim() == item.Cards[i].card_contract_id.Trim())
                                {
                                    CardObject.GetComponent<CardInfoSc>().SetCardvalue(CardImgs[j].CardImage, item.Cards[i]);
                                    CardObject.GetComponent<RawImage>().texture = CardImgs[j].CardImage;
                                    //////Debug.Log("Card Check ---->" + item.Cards[i].value);
                                    //////Debug.Log("Card Name ---->" + item.Cards[i].name);
                                    Color CardColor = CardObject.GetComponent<RawImage>().color;
                                    CardColor.a = 0.5f;
                                    CardObject.GetComponent<RawImage>().color = CardColor;
                                    CardObject.SetActive(true);
                                    canset = true;
                                    break;
                                }
                            }

                            if (!canset)
                                StartCoroutine(CardImage(CardObject, item.Cards[i].Image, item.Cards[i]));
                        }


                    }
                }

            }
        }
        catch (Exception ex)
        {
            //////Debug.Log(ex);
            StartCoroutine(hideLoading());
        }

        StartCoroutine(hideLoading());

    }

    IEnumerator DelayFetchERC721(GameObject catObj, Transform CatobjTrans, GameObject CardObj, Transform CardobjTrans)
    {
        yield return new WaitForSeconds(4f);

        foreach (var item in sortedCards)
        {
            var category = Instantiate(catObj, CatobjTrans);
            category.name = item.Name;

            category.transform.GetChild(0).GetComponent<Text>().text = item.Name.Replace('_', ' ');
            category.SetActive(true);

            for (int i = 0; i < item.Cards.Count; i++)
            {
                bool canset = false;
                var CardObject = Instantiate(CardObj, CardobjTrans);

                for (int j = 0; j < CardImgs.Count; j++)
                {
                    if (CardImgs[j].contractId.Trim() == item.Cards[i].card_contract_id.Trim())
                    {
                        CardObject.GetComponent<CardInfoSc>().SetCardvalue(CardImgs[j].CardImage, item.Cards[i]);
                        CardObject.GetComponent<RawImage>().texture = CardImgs[j].CardImage;
                        CardObject.SetActive(true);
                        canset = true;
                        break;
                    }
                }

                if (!canset)
                    StartCoroutine(CardImage(CardObject, item.Cards[i].Image, item.Cards[i]));

            }

        }
        yield return new WaitForSeconds(1f);
        LoadingManager.Instance.HideMainLoading();
    }

    public void RefreshCard721(GameObject catObj, Transform CatobjTrans, GameObject CardObj, Transform CardobjTrans, GameObject CatName)
    {


        for (int i = 0; i < CardobjTrans.childCount; i++)
        {
            Destroy(CardobjTrans.GetChild(i).gameObject);
        }



        //////Debug.Log("Refresh ERC721");


        foreach (var item in sortedCards)
        {
            if (item.Name == CatName.name)
            {
                /*  var category = Instantiate(catObj, CatobjTrans);
                  category.name = item.Name;
                  category.transform.GetChild(0).GetComponent<Text>().text = item.Name.Replace('_',' ') ;
                  category.SetActive(true);*/

                for (int i = 0; i < item.Cards.Count; i++)
                {
                    bool canset = false;
                    var CardObject = Instantiate(CardObj, CardobjTrans);

                    for (int j = 0; j < CardImgs.Count; j++)
                    {
                        if (CardImgs[j].contractId.Trim() == item.Cards[i].card_contract_id.Trim())
                        {
                            CardObject.GetComponent<CardInfoSc>().SetCardvalue(CardImgs[j].CardImage, item.Cards[i]);
                            CardObject.GetComponent<RawImage>().texture = CardImgs[j].CardImage;
                            CardObject.SetActive(true);
                            canset = true;
                            break;
                        }
                    }

                    if (!canset)
                        StartCoroutine(CardImage(CardObject, item.Cards[i].Image, item.Cards[i]));

                }
            }

        }

        foreach (var item in AllsortedCards)
        {

            if (item.Name == CatName.name)
            {

                /*  var category = Instantiate(catObj, CatobjTrans);
                  category.name = item.Name;
                  category.transform.GetChild(0).GetComponent<Text>().text = item.Name.Replace('_',' ') ;
                  category.SetActive(true);*/

                for (int i = 0; i < item.Cards.Count; i++)
                {
                    if (item.Cards[i].value == 0)
                    {
                        bool canset = false;
                        var CardObject = Instantiate(CardObj, CardobjTrans);

                        for (int j = 0; j < CardImgs.Count; j++)
                        {

                            if (CardImgs[j].contractId.Trim() == item.Cards[i].card_contract_id.Trim())
                            {
                                CardObject.GetComponent<CardInfoSc>().SetCardvalue(CardImgs[j].CardImage, item.Cards[i]);
                                CardObject.GetComponent<RawImage>().texture = CardImgs[j].CardImage;
                                CardObject.SetActive(true);


                                //////Debug.Log("Card Name ---->" + item.Cards[i].name);
                                Color CardColor = CardObject.GetComponent<RawImage>().color;
                                CardColor.a = 0.5f;
                                CardObject.GetComponent<RawImage>().color = CardColor;

                                canset = true;
                                break;

                            }

                        }

                        if (!canset)
                            StartCoroutine(CardImage(CardObject, item.Cards[i].Image, item.Cards[i]));
                    }

                }
            }
            //   StartCoroutine(hideLoading());
        }

    }
    IEnumerator hideLoading()
    {
        yield return new WaitForSeconds(4f);
        LoadingManager.Instance.HideMainLoading();
    }

    public async void RefreshERC20(Transform ItemParent)
    {

        //Compare password and return if doesn't match.
        if (SessionManager.Instance.userToken == null)
        {
            //////Debug.Log("User Not Logged In");
            LoadingManager.Instance.ShowInfoPopup("User Not Logged In", true);
            return;
        }

        //string bodyJson = "";

        string response = await SessionManager.Instance.Post(ConstantManager._url_GetProfile, null, null, true);

        GetProfileResponse getProfileResponse = JsonUtility.FromJson<GetProfileResponse>(response);

        //////Debug.Log(SessionManager.Instance.userToken);

        string GetTokenBalResponse = await SessionManager.Instance.Post(ConstantManager._url_getAllTokenBalance, null, SessionManager.Instance.userToken, true);

        GetTokenBal TokenResponse = JsonUtility.FromJson<GetTokenBal>(GetTokenBalResponse);

        ////Debug.Log("Token Response" + GetTokenBalResponse);


        if (TokenResponse.status == 1)
        {

            Ether.text = TokenResponse.data.ether.ToString();

            Elemt.text = TokenResponse.data.elementeum.ToString(); 
            Enjin.text = TokenResponse.data.enjin.ToString();
            EP.text = TokenResponse.data.ep.ToString();
            LP.text = TokenResponse.data.lp.ToString();
            Arena.text = TokenResponse.data.arena.ToString();

            setTOken20(float.Parse(TokenResponse.data.elementeum), float.Parse(TokenResponse.data.enjin), float.Parse(TokenResponse.data.ether), TokenResponse.data.ep, float.Parse(TokenResponse.data.lp), float.Parse(TokenResponse.data.arena));
        }

        ItemParent.GetChild(0).GetComponent<ERCTokenItem>().setValue("Enjin", Myprefs.enjin);
        ItemParent.GetChild(1).GetComponent<ERCTokenItem>().setValue("Elementeum", Myprefs.Elemt);
    }

    public async void RefreshERC20()
    {
        //Compare password and return if doesn't match.
        if (SessionManager.Instance.userToken == null)
        {
            //////Debug.Log("User Not Logged In");
            LoadingManager.Instance.ShowInfoPopup("User Not Logged In", true);
            return;
        }


        string GetTokenBalResponse = await SessionManager.Instance.Post(ConstantManager._url_getAllTokenBalance, null, SessionManager.Instance.userToken, true);

        GetTokenBal TokenResponse = JsonUtility.FromJson<GetTokenBal>(GetTokenBalResponse);

        ////Debug.Log("Token Response" + GetTokenBalResponse);

        if (TokenResponse.status == 1)
        {
            //Ether.text = Math.Round(double.Parse(TokenResponse.data.ether), 8).ToString();
            Ether.text = TokenResponse.data.ether.ToString(); //(float.Parse(TokenResponse.data.ether) % 1 == 0 ? "0" : float.Parse(TokenResponse.data.ether).ToString("#.00000000"));

            Elemt.text = TokenResponse.data.elementeum.ToString(); //(float.Parse(TokenResponse.data.elementeum) % 1 == 0 ? TokenResponse.data.elementeum : float.Parse(TokenResponse.data.elementeum).ToString("#.00000000"));
            Enjin.text = TokenResponse.data.enjin.ToString(); //(float.Parse(TokenResponse.data.enjin) % 1 == 0 ? TokenResponse.data.enjin : float.Parse(TokenResponse.data.enjin).ToString("#.00000000"));
            EP.text = TokenResponse.data.ep.ToString();
            LP.text = TokenResponse.data.lp.ToString();
            Arena.text = TokenResponse.data.arena.ToString();

            setTOken20(float.Parse(TokenResponse.data.elementeum), float.Parse(TokenResponse.data.enjin), float.Parse(TokenResponse.data.ether), TokenResponse.data.ep, float.Parse(TokenResponse.data.lp), float.Parse(TokenResponse.data.arena));
        }

    }

    IEnumerator CardImage(GameObject Cobj, string Imglink, Card CardInfo)
    {

        if (!Cobj)
        {
            //////Debug.Log("Card Image Null--");
            yield break;
        }

        //////Debug.Log("Card Link " + Imglink);

        Texture2D ImgText = BlockChainCardSC.Instance.GetCardTexture(CardInfo.card_contract_id);

        if (ImgText != null)
        {
            Texture2D CardTexture = ImgText;
            Cobj.GetComponent<RawImage>().texture = CardTexture;
            Cobj.GetComponent<CardInfoSc>().SetCardvalue(CardTexture, CardInfo);
            Cobj.SetActive(true);

            if (CardImgs.Count <= 0)
            {
                CardImageStored cardImageStored = new CardImageStored();
                cardImageStored.cardname = CardInfo.name;
                cardImageStored.contractId = CardInfo.card_contract_id;
                cardImageStored.CardImage = CardTexture;
                CardImgs.Add(cardImageStored);
            }

            for (int i = 0; i < CardImgs.Count; i++)
            {
                if (CardImgs[i].contractId.Trim() != CardInfo.card_contract_id.Trim())
                {
                    CardImageStored cardImageStored = new CardImageStored();
                    cardImageStored.cardname = CardInfo.name;
                    cardImageStored.contractId = CardInfo.card_contract_id;
                    cardImageStored.CardImage = CardTexture;
                    CardImgs.Add(cardImageStored);
                    break;
                }
            }
        }

        else
        {

            yield return new WaitForSeconds(0.1f);


            UnityWebRequest www = UnityWebRequestTexture.GetTexture(Imglink);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                //////Debug.Log(www.error);
            }
            else
            {
                Texture2D CardTexture = DownloadHandlerTexture.GetContent(www);
                Cobj.GetComponent<RawImage>().texture = CardTexture;
                Cobj.GetComponent<CardInfoSc>().SetCardvalue(CardTexture, CardInfo);

                Cobj.SetActive(true);

                BlockChainCardSC.Instance.SaveCard(CardInfo.card_contract_id, CardTexture, false);

                if (CardImgs.Count <= 0)
                {
                    CardImageStored cardImageStored = new CardImageStored();
                    cardImageStored.cardname = CardInfo.name;
                    cardImageStored.contractId = CardInfo.card_contract_id;
                    cardImageStored.CardImage = CardTexture;
                    CardImgs.Add(cardImageStored);
                }

                for (int i = 0; i < CardImgs.Count; i++)
                {
                    if (CardImgs[i].contractId.Trim() != CardInfo.card_contract_id.Trim())
                    {
                        CardImageStored cardImageStored = new CardImageStored();
                        cardImageStored.cardname = CardInfo.name;
                        cardImageStored.contractId = CardInfo.card_contract_id;
                        cardImageStored.CardImage = CardTexture;
                        CardImgs.Add(cardImageStored);
                        break;
                    }
                }

            }
        }
        if (CardInfo.value == 0)
        {
            //////Debug.Log("Card Name ---->" + CardInfo.name);
            Color CardColor = Cobj.GetComponent<RawImage>().color;
            CardColor.a = 0.5f;
            Cobj.GetComponent<RawImage>().color = CardColor;
        }
    }

    public async void DoGetProfile()
    {

        //Compare password and return if doesn't match.
        if (SessionManager.Instance.userToken == null)
        {
            //////Debug.Log("User Not Logged In");
            LoadingManager.Instance.ShowInfoPopup("User Not Logged In", true);
            return;
        }

        //string bodyJson = "";

        

        //////Debug.Log(SessionManager.Instance.userToken);

        string GetTokenBalResponse = await SessionManager.Instance.Post(ConstantManager._url_getAllTokenBalance, null, SessionManager.Instance.userToken, true);

        //Debug.Log("Token Response" + GetTokenBalResponse);

        if (string.IsNullOrEmpty(GetTokenBalResponse))
        {
            //Debug.Log("Token balance is empty or null");


            DoGetProfile();
            return;
        }

        GetTokenBal TokenResponse = JsonUtility.FromJson<GetTokenBal>(GetTokenBalResponse);

        ////Debug.Log("Token Response" + TokenResponse);



        if (TokenResponse.status == 1)
        {
           


            if (string.IsNullOrEmpty(TokenResponse.data.ether))
            {
                TokenResponse.data.ether = "0";
            }

            ////Debug.Log("Got itttt" + TokenResponse.data.ether);
            Ether.text = TokenResponse.data.ether.ToString();

            if (string.IsNullOrEmpty(TokenResponse.data.elementeum))
            {
                TokenResponse.data.elementeum = "0";
            }
            ////Debug.Log("Got itttt" + TokenResponse.data.elementeum);
            Elemt.text = TokenResponse.data.elementeum.ToString(); 
            if (string.IsNullOrEmpty(TokenResponse.data.enjin))
            {
                TokenResponse.data.enjin = "0";
            }
            ////Debug.Log("Got itttt" + TokenResponse.data.enjin);
            Enjin.text = TokenResponse.data.enjin.ToString(); 
            if (string.IsNullOrEmpty(TokenResponse.data.lp))
            {
                TokenResponse.data.lp = "0";
            }
            ////Debug.Log("Got itttt" + TokenResponse.data.lp);
            LP.text = TokenResponse.data.lp.ToString();
            if (string.IsNullOrEmpty(TokenResponse.data.arena))
            {
                TokenResponse.data.arena = "0";
            }
            ////Debug.Log("Got itttt" + TokenResponse.data.arena);
            Arena.text = TokenResponse.data.arena.ToString();

            EP.text = TokenResponse.data.ep.ToString();


            setTOken20(float.Parse(TokenResponse.data.elementeum), float.Parse(TokenResponse.data.enjin), float.Parse(TokenResponse.data.ether), TokenResponse.data.ep, float.Parse(TokenResponse.data.lp), float.Parse(TokenResponse.data.arena));

        }
        string response = await SessionManager.Instance.Post(ConstantManager._url_GetProfile, null, null, true);

        GetProfileResponse getProfileResponse = JsonUtility.FromJson<GetProfileResponse>(response);

        if (string.IsNullOrEmpty(response))
        {
            DoGetProfile();
            return;
        }


        // LoadingManager.Instance.ShowInfoPopup(getProfileResponse.message, true);
        if (getProfileResponse.status == 1)
        {
            // //////Debug.Log(getProfileResponse.message + " Need to open last screen..");
            profileData = getProfileResponse.data;
            UserDataController.Instance.Myprefs.Address = profileData.publicAddress;
            ////Debug.Log(profileData.battles + " " + profileData.wins + " " + profileData.max_streak);

            ProfileManager.Instance.text_totBattles.text = profileData.battles.ToString();
            ProfileManager.Instance.text_totWin.text = profileData.wins.ToString();
            ProfileManager.Instance.text_totWinStreak.text = profileData.max_streak.ToString();

            if (string.IsNullOrEmpty(profileData.publicAddress) || !Myprefs.Address.Equals(profileData.publicAddress))
            {

                LoadingManager.Instance.HideMainLoading();
                //////Debug.Log("link wallet screen is active fro here");

                LoadingManager.Instance.ShowInfoPopUp("User Public Address not found!");
                AuthenticationManager.Instance.ChangeScreen(AuthenticationManager.AuthenticationScreen.LOGIN);
                ProfileManager.Instance.profilePanel.SetActive(false);
                //UserDataController.Instance.DoGetTokenBalance();
            }
            else
            {

                ProfileManager.Instance.SetProfile();
                DoGetTokenBalance();
                ProfileManager.Instance.BestCard.SetActive(true);
            }

        }
        else
        {
            //////Debug.Log("get profile failed: " + getProfileResponse.message);
        }

        if (profileData.best_card.Count > 0) ;
        profileData.best_card.RemoveRange(0, profileData.best_card.Count);
    }

    public void DoGetTokenBalance()
    {
        DoUpdateCardBalance();
    }

    public async void DoUpdateCardBalance()
    {

        //////Debug.Log("UPdate Card Balance called=----- " + SessionManager.Instance.userToken);
        //LoadingManager.Instance.ShowLoader("Getting Token Balance..");
        if (SessionManager.Instance.userToken == null)
        {
            //////Debug.Log("User Not Logged In");
            LoadingManager.Instance.ShowInfoPopup("User Not Logged In", true);
            return;
        }

        //string bodyJson = "";
        string response = await SessionManager.Instance.Post(ConstantManager._url_UpdateCardContractBalance, null, SessionManager.Instance.userToken, true);
        ////////Debug.Log("test "+ response);
        ///
        if (!string.IsNullOrEmpty(response))
        {
            JSONNode jnode = JSON.Parse(response);


            //LoadingManager.Instance.ShowInfoPopup(getAllTokenBalanceResponse.message, true);
            //////Debug.Log(jnode);
            if (jnode["status"] == 1)
            {
                //            //////Debug.Log(getAllTokenBalanceResponse.message + " Need to open last screen..");
                DoGetUserTokenBalance();
            }

            else
            {
                //////Debug.Log("get profile failed: " + jnode["message"]);

            }
        }
        else
        {
            ////Debug.Log(ConstantManager._url_UpdateCardContractBalance+ " Response is null ");
        }

    }

    public async void DoGetUserTokenBalance()
    {

        //LoadingManager.Instance.ShowLoader("Getting Token Balance..");
        if (SessionManager.Instance.userToken == null)
        {
            //////Debug.Log("User Not Logged In");
            LoadingManager.Instance.ShowInfoPopup("User Not Logged In", true);
            return;
        }

        //string bodyJson = "";
        string response = await SessionManager.Instance.Post(ConstantManager._url_getAllTokenBalance, null, SessionManager.Instance.userToken, true);
        if (string.IsNullOrEmpty(response))
        {
            DoGetUserTokenBalance();
            return;
        }
        else
        {


            GetAllTokenBalanceResponse getAllTokenBalanceResponse = JsonUtility.FromJson<GetAllTokenBalanceResponse>(response);
            ////Debug.Log("GetAlltokenBalance working " + response);
            //LoadingManager.Instance.ShowInfoPopup(getAllTokenBalanceResponse.message, true);
            if (getAllTokenBalanceResponse.status == 1)
            {
                // //////Debug.Log(getAllTokenBalanceResponse.message + " Need to open last screen..");
                ProfileManager.Instance.SetTokenBalance();
                ProfileManager.Instance.BestCard.SetActive(true);
            }

            else
            {
                //////Debug.Log("get profile failed: " + getAllTokenBalanceResponse.message);
                userTokenData = getAllTokenBalanceResponse.data;
            }
        }
    }

    public async void DoGetAllCardTokenBalance()
    {

        //LoadingManager.Instance.ShowLoader("Getting Token Balance..");
        if (SessionManager.Instance.userToken == null)
        {
            //////Debug.Log("User Not Logged In");
            LoadingManager.Instance.ShowInfoPopup("User Not Logged In", true);
            return;
        }

        //string bodyJson = "";
        string response = await SessionManager.Instance.Post(ConstantManager._url_getAllCardContractBalance, null, SessionManager.Instance.userToken, true);
        if (!string.IsNullOrEmpty(response))
        {


            JSONNode jnode = JSON.Parse(response);

            //LoadingManager.Instance.ShowInfoPopup(getAllTokenBalanceResponse.message, true);

            if (jnode["status"] == 1)
            {
                //            //////Debug.Log(getAllTokenBalanceResponse.message + " Need to open last screen..");
                DoGetUserTokenBalance();
            }

            else
            {
                //////Debug.Log("get profile failed: " + jnode["message"]);

            }
        }
        else
        {
            ////Debug.Log(ConstantManager._url_getAllCardContractBalance + " response is null ");
        }

    }

    public void setUserPref(string Pk, string Mnemonic, string Address)
    {

        Myprefs.PKey = Pk;
        Myprefs.Mnemonic = Mnemonic;
        Myprefs.Address = Address;

        Save();
    }

    public void setEnjinWallet(string EnjinAdd)
    {
        Myprefs.EnjinAddress = EnjinAdd;
        Save();
    }

    public void SetEP(int val)
    {
        //////Debug.Log("set EP-=---->" + val);
        if (val > 0)
        {

            Myprefs.EP = val;

            Save();
        }


    }

    public void Save()
    {
        //////Debug.Log("----Save--->" + Myprefs.EP);
        SaveGame.Save<UserPrefs>(identifier, Myprefs, SaveGame.Serializer);

        //        //////Debug.Log("User Save TOken--"+Myprefs.UserToken);
    }

    public void Load()
    {
        Myprefs = SaveGame.Load<UserPrefs>(
             identifier,
             Myprefs,
             SaveGame.Serializer);


    }

}
