﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;
using TMPro;
using System.Linq;

public class PlayerCompanionCardsAnim : MonoBehaviour
{

    public static PlayerCompanionCardsAnim instance;

    public CompanionCardObject CompanionPrefab;
    public Transform PlayerCompanionHolder, EnemyCompanionHolder;
    public float CompanionLocalScaleFactor = 0.3F;
    public Vector3 playerCompanionLocalPos, enemyCompanionLocalPos;

    public CompanionCardObject[] playerCompanionCards;
    public CompanionCardObject[] enemyCompanionCards;

    public List<string> SelectedCompanionIds = new List<string>();


public int SelctedCardCheck = 0;
public int plyComcardTempValue;
public int eneComcardTempValue;

    public bool canClickCompanion;
    //public string selectedPlayerCardContractId;

    public void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {

        var playerCompanions = BattleLobbySC.Instance.Finaldata.user_data.companionCardDetail.Where(c => !string.IsNullOrEmpty(c.card_contract_id)).ToList();
        var enemyCompanions = BattleLobbySC.Instance.Finaldata.opp_user_data.companionCardDetail.Where(c => !string.IsNullOrEmpty(c.card_contract_id)).ToList();

        playerCompanionCards = new CompanionCardObject[playerCompanions.Count];
        enemyCompanionCards = new CompanionCardObject[enemyCompanions.Count];


        for (int i = 0; i < playerCompanions.Count; i++)
        {
            var x = playerCompanions[i];
            var cCard = Instantiate(CompanionPrefab, PlayerCompanionHolder);
            cCard.transform.localPosition = playerCompanionLocalPos;
            cCard.transform.localScale = CompanionLocalScaleFactor * Vector3.one;
            cCard.CardDetail = Card.FromFetchedCard(x);
            cCard.name = x.card_name;
            //cCard.GetComponent<BoxCollider2D>().enabled = true;
            playerCompanionCards[i] = cCard;
        }

        for (int i = 0; i < enemyCompanions.Count; i++)
        {
            var x = enemyCompanions[i];
            var cCard = Instantiate(CompanionPrefab, EnemyCompanionHolder);
            cCard.transform.localPosition = enemyCompanionLocalPos;
            cCard.transform.localScale = CompanionLocalScaleFactor * Vector3.one;
            cCard.CardDetail = Card.FromFetchedCard(x);
            cCard.name = x.card_name;
            enemyCompanionCards[i] = cCard;
        }


        yield return new WaitForSeconds(1);

        PlayerCards();
        EnemyCards();
        canClickCompanion = true;

    }

    IEnumerator PlayAttackParticles(CompanionCardObject particleObject)
    {
        yield return new WaitForSeconds(2);
        particleObject.attackParticles.Play();
        yield return new WaitForSeconds(1);
        particleObject.attackParticles.Stop();
    }
    IEnumerator PlayDefenceParticles(CompanionCardObject particleObject)
    {
        yield return new WaitForSeconds(2);
        particleObject.defenceParticles.Play();
        yield return new WaitForSeconds(1);
        particleObject.defenceParticles.Stop();
    }
    IEnumerator PlayDisruptParticles(CompanionCardObject particleObject)
    {
        yield return new WaitForSeconds(2);
        particleObject.disruptParticles.Play();
        yield return new WaitForSeconds(1);
        particleObject.disruptParticles.Stop();
    }
    IEnumerator PlayHealParticles(CompanionCardObject particleObject)
    {
        yield return new WaitForSeconds(2);
        particleObject.HealParticles.Play();
        yield return new WaitForSeconds(1);
        particleObject.HealParticles.Stop();
    }





    #region Player Companion cards

    int TotalCompanionAbilityValue()
    {
        return PlayerSelectedCompanionCards().Sum(x => x.CardDetail.AbilityValue(true));
    }

    private List<CompanionCardObject> PlayerSelectedCompanionCards()
    {
        return playerCompanionCards.Where(x => SelectedCompanionIds.Any(y => y == x.CardDetail.card_contract_id)).ToList();
    }


    public int test;
    public void CompanionCards(CompanionCardObject CardObject)
    {

        if (canClickCompanion)
        {
            //Possible Companion Sum
           int ElementiumValue = PlayerManager.instance.SelectedCard.Cur_Elemt - PlayerManager.instance.SelectedCard.remain_Elemt;


           //var AbilityWiseCompanions =  playerCompanionCards.Where(x => x.CardDetail.strength_1 == CardObject.CardDetail.strength_1).ToList();

            var AbilityWiseSelectedCompanions = PlayerSelectedCompanionCards().Where(x=>x.CardDetail.strength_1 == SelectPowerManager.instance.selectedPowerName).ToList(); ;
            var temp1 = AbilityWiseSelectedCompanions.Sum(x => x.CardDetail.AbilityValue(true));


            if (ElementiumValue < 2 || temp1 >= ElementiumValue)
            {
                return;
            }




            if (ElementiumValue >= 2 && ElementiumValue < 4 && SelctedCardCheck == 1)
            {
                return;
            }


            if (ElementiumValue >= 2 && SelctedCardCheck == 2)
            {
                return;
            }


            var cardStrength = CardObject.CardDetail.strength_1;
            switch (cardStrength)
            {
                case AttributeType.Attack:
                    test = CardObject.CardDetail.attack + plyComcardTempValue;
                    break;
                case AttributeType.Defense:
                    test = CardObject.CardDetail.defence + plyComcardTempValue;
                    break;
                case AttributeType.Disrupt:
                    test = CardObject.CardDetail.disrupt + plyComcardTempValue;
                    break;
                case AttributeType.Heal:
                    test = CardObject.CardDetail.heal + plyComcardTempValue;
                    break;
            }
            if (ElementiumValue < test)
            {
                return;
            }

            SelctedCardCheck += 1;
            CardObject.attackParticles.transform.rotation = Quaternion.Euler(0, 0, -105);
            CardObject.GetComponent<BoxCollider2D>().enabled = false;
            CardObject.isSelected = true;

            switch (SelctedCardCheck)
            {
                case 1:

                    if (cardStrength == AttributeType.Attack)
                    {
                        Attack(CardObject, new Vector3(-3.5f, 2f, 15.0f), 6, new Vector3(0, 0, 0), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Defense)
                    {
                        Defense(CardObject, new Vector3(-3.5f, 2f, 15.0f), 6, new Vector3(0, 0, 0), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Disrupt)
                    {
                        Disrupt(CardObject, new Vector3(-3.5f, 2f, 15.0f), 6, new Vector3(0, 0, 0), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Heal)
                    {
                        Heal(CardObject, new Vector3(-6.5f, 2f, 15.0f), 6, new Vector3(0, 0, 0), cardStrength);
                    }


                    break;
                case 2:

                    if (cardStrength == AttributeType.Attack)
                    {
                        Attack(CardObject, new Vector3(-3.5f, 1f, 15.0f), 7, new Vector3(0, 0, 15), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Defense)
                    {
                        Defense(CardObject, new Vector3(-3.5f, 1f, 15.0f), 7, new Vector3(0, 0, 15), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Disrupt)
                    {
                        Disrupt(CardObject, new Vector3(-3.5f, 1f, 15.0f), 7, new Vector3(0, 0, 15), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Heal)
                    {
                        Heal(CardObject, new Vector3(-6.5f, 1f, 15.0f), 7, new Vector3(0, 0, 15), cardStrength);
                    }
                    break;

                case 3:

                    if (cardStrength == AttributeType.Attack)
                    {
                        Attack(CardObject, new Vector3(-3.5f, 0f, 15.0f), 8, new Vector3(0, 0, 30), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Defense)
                    {
                        Defense(CardObject, new Vector3(-3.5f, 0f, 15.0f), 8, new Vector3(0, 0, 30), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Disrupt)
                    {
                        Disrupt(CardObject, new Vector3(-3.5f, 0f, 15.0f), 8, new Vector3(0, 0, 30), cardStrength);
                    }
                    else if (cardStrength == AttributeType.Heal)
                    {
                        Heal(CardObject, new Vector3(-6.5f, 0f, 15.0f), 8, new Vector3(0, 0, 30), cardStrength);
                    }
                    break;


            }



        }
    }
    int highlightCompanion;
    public void AutoSelectCompanionCardsForBattle(AttributeType Ability)
    {
        var localSelectedCard = (PlayerManager.instance.SelectedCard ?? PlayerManager.instance.PlayerCardsObj.FirstOrDefault(x => x.GetComponent<CardObject>().DestroyedCard == false));

        int ElementiumValue = localSelectedCard.Cur_Elemt - localSelectedCard.remain_Elemt;

        if (localSelectedCard || !localSelectedCard.isSpActive)
        {
            if (ElementiumValue >= 2)
            {
                playerCompanionCards.ForEach(x =>
                {
                    if (x.CardDetail.strength_1 == Ability && x.IsCurrentAbilityLessThan(ElementiumValue))
                        CardOrderAndSelection(x);
                    else
                        x.transform.DOScale(new Vector3(0.25f, 0.25f, 0.25f), 1);
                }
                );
            }
        }
    }


    public void AutoDeselectCompanionCardsForBattle(CompanionCardObject CardObject)
    {
        SelectedCompanionIds.Remove(CardObject.CardDetail.card_contract_id);
        for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
        {
            PlayerManager.instance.PlayerCardsObj[i].companionIDs.Remove(CardObject.CardDetail.card_contract_id);
            PlayerManager.instance.PlayerCardsObj[i].companionData.Clear();
        }
        int attrib = int.Parse(InGameCards.instance.MyAttribValue.text) - plyComcardTempValue;
        InGameCards.instance.MyAttribValue.text = attrib.ToString();

        plyComcardTempValue = 0;
        CardObject.AllParticles().ForEach(x => x.Stop());
        PlayerCards();
        CardObject.GetComponent<BoxCollider2D>().enabled = false;
        CardObject.SortingOrder(1);
        CardObject.transform.DOScale(new Vector3(0.3f, 0.3f, 0.3f), 1);
        SelctedCardCheck = 0;
    }
    public void AutoDeselectCompanionCardsFromElementium(CompanionCardObject CardObject)
    {
        CardObject.transform.DOMove((CardObject.cardPosition), 1);
        CardObject.AllParticles().ForEach(x => x.Stop());
        CardObject.GetComponent<BoxCollider2D>().enabled = false;
        CardObject.isSelected = false;
        CardObject.SortingOrder(1);
        CardObject.transform.DOScale(new Vector3(0.3f, 0.3f, 0.3f), 1);
        SelctedCardCheck = System.Math.Max(0, SelctedCardCheck - 1);

    }
    public void AutoDeselectCompanionCardsFromElementiumDeSelection(CompanionCardObject CardObject)
    {
        SelectedCompanionIds.Remove(CardObject.CardDetail.card_contract_id);
        PlayerManager.instance.SelectedCard.companionIDs.Remove(CardObject.CardDetail.card_contract_id);
        PlayerManager.instance.SelectedCard.companionData.Clear();
        CardObject.attackParticles.Stop();
        CardObject.defenceParticles.Stop();
        CardObject.HealParticles.Stop();
        CardObject.disruptParticles.Stop();
        CardObject.transform.DOMove((CardObject.cardPosition), 1);
        SelctedCardCheck = System.Math.Max(0, SelctedCardCheck - 1);

        CardObject.SortingOrder(6);
        CardObject.transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 1);
        CardObject.GetComponent<BoxCollider2D>().enabled = !CardObject.isUsed;
    }



    Vector3[] playerCompanionCardPositions = new Vector3[] {
        new Vector3(-6.5f, -2f, 15.0f),new Vector3(-4.8f, -2f, 15.0f),new Vector3(-3.1f, -2f, 15.0f),
        new Vector3(-1.4f, -2f, 15.0f),new Vector3(0.3f, -2f, 15.0f),new Vector3(2f, -2f, 15.0f),
        new Vector3(3.7f, -2f, 15.0f),new Vector3(5.4f, -2f, 15.0f)
    };
    public void PlayerCards()
    {
        for (int i = 0; i < playerCompanionCards.Length; i++)
        {
            playerCompanionCards[i].transform.DOMove(playerCompanionCardPositions[i], 1);
            playerCompanionCards[i].cardPosition = playerCompanionCardPositions[i];
        }


    }
    #endregion


    #region Enemy Companion cards

    public void AutoSelectEnemyCompanionCardsForBattle(AttributeType Ability)
    {

        //var CardToBe = (PlayerManager.instance.SelectedEnemyCard
        //    ??
        //    PlayerManager.instance.enemyCardsObj.FirstOrDefault(x => x.GetComponent<CardObject>().DestroyedCard == false));

        CardObject CardToBe;
        if (PlayerManager.instance.SelectedEnemyCard)
            CardToBe = PlayerManager.instance.SelectedEnemyCard;
        else
            CardToBe = PlayerManager.instance.enemyCardsObj.FirstOrDefault(x => x.GetComponent<CardObject>().DestroyedCard == false);


        int ElementiumValue = CardToBe.Cur_Elemt - CardToBe.remain_Elemt;

        if (CardToBe || !CardToBe.isSpActive)
        {
            if (CardToBe.Cur_Elemt >= 2)
            {
                enemyCompanionCards.ForEach(x =>
                {
                    if (x.CardDetail.strength_1 == Ability && x.IsCurrentAbilityLessThan(ElementiumValue))
                        EnemyCardOrderAndSelection(x);
                    else
                        x.transform.DOScale(new Vector3(0.25f, 0.25f, 0.25f), 1);
                }
                );

                //enemyCompanionCards.Where(x => x.CardDetail.strength_1 == Ability && x.IsCurrentAbilityLessThan(ElementiumValue)).ForEach(x => EnemyCardOrderAndSelection(x));
                //enemyCompanionCards.Where(x => x.CardDetail.strength_1 != Ability).ForEach(x => x.transform.DOScale(new Vector3(0.25f, 0.25f, 0.25f), 1));
            }
        }

    }

    public void AutoDeselectEnemyCompanionCardsForBattle(CompanionCardObject CardObject)
    {
        int attrib = int.Parse(InGameCards.instance.OppAttribValue.text) - eneComcardTempValue;
        InGameCards.instance.OppAttribValue.text = attrib.ToString();
        eneComcardTempValue = 0;
        CardObject.AllParticles().ForEach(x => x.Stop());
        CardObject.SortingOrder(1);

        CardObject.transform.DOScale(new Vector3(0.3f, 0.3f, 0.3f), 1);
        EnemyCards();

        InGameCards.instance.EnemySelctedCardCheck = 0;
    }


    Vector3[] enemyCompanionCardPositions = new Vector3[] {
                new Vector3(5.4f, 8.5f, 15.0f),
                new Vector3(3.7f, 8.5f, 15.0f),
                new Vector3(2, 8.5f, 15.0f),
                new Vector3(0.3F, 8.5f, 15.0f) ,
                new Vector3(-1.4f, 8.5f, 15.0f),
                new Vector3(-3.1f, 8.5f, 15.0f),
                new Vector3(-4.8f, 8.5f, 15.0f),
                new Vector3(-6.5f, 8.5f, 15.0f)
    };


    private void EnemyCards()
    {
        for (int i = 0; i < enemyCompanionCards.Length; i++)
        {
            enemyCompanionCards[i].transform.DOMove(enemyCompanionCardPositions[i], 1);
            enemyCompanionCards[i].cardPosition = enemyCompanionCardPositions[i];
        }

    }
    #endregion




    public void Attack(CompanionCardObject CardObject, Vector3 Moveanim, int sortingOrder, Vector3 particleRotation, AttributeType PowerName)
    {

        CardObject.transform.DOMove(Moveanim, 1);
        CardObject.SortingOrder(sortingOrder);

        CardObject.attackParticles.transform.Rotate(particleRotation);
        StartCoroutine(PlayAttackParticles(CardObject));


        int atk = CardObject.CardDetail.attack;
        plyComcardTempValue += atk;

        InGameCards.instance.companion.text = plyComcardTempValue.ToString();

        SelectedCompanionIds.Add(CardObject.CardDetail.card_contract_id);

        PlayerManager.instance.SelectedCard.companionIDs = SelectedCompanionIds;
        PlayerManager.instance.SelectedCard.companionData.Add(CardObject.CardDetail.attack);

        if (PlayerManager.instance.SelectedEnemyCard == null)
        {
            BattleLobbySC.Instance.CompanionCardSelected(
                SelectedCompanionIds,
                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                AttributeType.Attack,
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                "",
                PlayerManager.instance.SelectedCard.ElementeumStatus
                );
        }
        else
        {
            BattleLobbySC.Instance.CompanionCardSelected(
                SelectedCompanionIds,
                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                AttributeType.Attack,
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                PlayerManager.instance.SelectedCard.ElementeumStatus
                );
        }
        atk = int.Parse(InGameCards.instance.MyAttribValue.text) + atk;
        ////Debug.Log(atk);
        InGameCards.instance.MyAttribValue.text = atk.ToString();

        AnimatePlayerCompanionValue(PowerName, CardObject.txt_Attack);
    }

    public void Defense(CompanionCardObject CardObject, Vector3 Moveanim, int sortingOrder, Vector3 particleRotation, AttributeType PowerName)
    {
        CardObject.transform.DOMove(Moveanim, 1);

        CardObject.SortingOrder(sortingOrder);


        CardObject.defenceParticles.transform.Rotate(particleRotation);
        StartCoroutine(PlayDefenceParticles(CardObject));


        int def = CardObject.CardDetail.defence;
        plyComcardTempValue += def;
        InGameCards.instance.companion.text = plyComcardTempValue.ToString();

        SelectedCompanionIds.Add(CardObject.CardDetail.card_contract_id);

        PlayerManager.instance.SelectedCard.companionIDs = SelectedCompanionIds;
        PlayerManager.instance.SelectedCard.companionData.Add(CardObject.CardDetail.defence);


        if (PlayerManager.instance.SelectedEnemyCard == null)
        {
            BattleLobbySC.Instance.CompanionCardSelected(
                SelectedCompanionIds,
                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                AttributeType.Defense,
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                "",
                PlayerManager.instance.SelectedCard.ElementeumStatus
                );
        }
        else
        {
            BattleLobbySC.Instance.CompanionCardSelected(
                SelectedCompanionIds,
                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                AttributeType.Defense,
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                PlayerManager.instance.SelectedCard.ElementeumStatus
                );
        }

        def = int.Parse(InGameCards.instance.MyAttribValue.text) + def;

        InGameCards.instance.MyAttribValue.text = def.ToString();

        AnimatePlayerCompanionValue(PowerName, CardObject.txt_Defense);
    }

    public void Heal(CompanionCardObject CardObject, Vector3 Moveanim, int sortingOrder, Vector3 particleRotation, AttributeType PowerName)
    {
        CardObject.transform.DOMove(Moveanim, 1);
        CardObject.SortingOrder(sortingOrder);
        CardObject.HealParticles.transform.Rotate(0, 0, 0);
        StartCoroutine(PlayHealParticles(CardObject));



        int heal = CardObject.CardDetail.heal;
        plyComcardTempValue += heal;
        InGameCards.instance.companion.text = plyComcardTempValue.ToString();

        SelectedCompanionIds.Add(CardObject.CardDetail.card_contract_id);

        PlayerManager.instance.SelectedCard.companionIDs = SelectedCompanionIds.ToArray().ToList();
        PlayerManager.instance.SelectedCard.companionData.Add(CardObject.CardDetail.heal);


        if (PlayerManager.instance.SelectedEnemyCard == null)
        {
            BattleLobbySC.Instance.CompanionCardSelected(
                SelectedCompanionIds,
                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                AttributeType.Heal,
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                "",
                PlayerManager.instance.SelectedCard.ElementeumStatus
                );
        }
        else
        {
            BattleLobbySC.Instance.CompanionCardSelected(
                SelectedCompanionIds,
                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                AttributeType.Heal,
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                PlayerManager.instance.SelectedCard.ElementeumStatus
                );
        }
        heal = int.Parse(InGameCards.instance.MyAttribValue.text) + heal;

        InGameCards.instance.MyAttribValue.text = heal.ToString();
        AnimatePlayerCompanionValue(PowerName, CardObject.txt_Heal);

    }

    public void Disrupt(CompanionCardObject CardObject, Vector3 Moveanim, int sortingOrder, Vector3 particleRotation, AttributeType PowerName)
    {
        CardObject.transform.DOMove(Moveanim, 1);
        CardObject.SortingOrder(sortingOrder);



        CardObject.disruptParticles.transform.Rotate(0, 0, 0);
        StartCoroutine(PlayDisruptParticles(CardObject));


        int dist = CardObject.CardDetail.disrupt;

        plyComcardTempValue += dist;
        InGameCards.instance.companion.text = plyComcardTempValue.ToString();

        SelectedCompanionIds.Add(CardObject.CardDetail.card_contract_id);

        PlayerManager.instance.SelectedCard.companionIDs = SelectedCompanionIds.ToArray().ToList();
        PlayerManager.instance.SelectedCard.companionData.Add(CardObject.CardDetail.disrupt);



        if (PlayerManager.instance.SelectedEnemyCard == null)
        {
            BattleLobbySC.Instance.CompanionCardSelected(
                SelectedCompanionIds,
                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                AttributeType.Disrupt,
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                "",
                PlayerManager.instance.SelectedCard.ElementeumStatus
                );
        }
        else
        {
            BattleLobbySC.Instance.CompanionCardSelected(
                SelectedCompanionIds,
                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                AttributeType.Disrupt,
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                PlayerManager.instance.SelectedCard.ElementeumStatus
                );
        }

        dist = int.Parse(InGameCards.instance.MyAttribValue.text) + dist;

        InGameCards.instance.MyAttribValue.text = dist.ToString();
        AnimatePlayerCompanionValue(PowerName, CardObject.txt_Disrupt);
    }



    public void CardOrderAndSelection(CompanionCardObject CardObject)
    {
        CardObject.SortingOrder(6);
        CardObject.transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 1);
        CardObject.GetComponent<BoxCollider2D>().enabled = !CardObject.isUsed;
    }

    public void EnemyCardOrderAndSelection(CompanionCardObject CardObject)
    {
        CardObject.SortingOrder(6);
        CardObject.transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 1);
    }





    public void AnimatePlayerCompanionValue(AttributeType powerName, GameObject txtObj)
    {
        switch (powerName)
        {
            case AttributeType.Attack:
                StartCoroutine(InstantiateObjForAnimate(txtObj, new Vector3(-15f, -5.8f, 0)));
                break;
            case AttributeType.Heal:
                StartCoroutine(InstantiateObjForAnimate(txtObj, new Vector3(-15f, -1f, 0)));
                break;
            case AttributeType.Defense:
                StartCoroutine(InstantiateObjForAnimate(txtObj, new Vector3(-18f, -5.85f, 0)));
                break;
            case AttributeType.Disrupt:
                StartCoroutine(InstantiateObjForAnimate(txtObj, new Vector3(-17f, -1f, 0)));
                break;
        }

    }


    public IEnumerator InstantiateObjForAnimate(GameObject obj, Vector3 MovePos)
    {
        GameObject TxtObj = Instantiate(obj, obj.transform);
        TxtObj.transform.DOScale(new Vector3(3, 3, 3), 1f);
        yield return new WaitForSeconds(1);

        TxtObj.transform.DORotate(new Vector3(270, 0, 0), 0.5f);
        TxtObj.transform.DOLocalMove(MovePos, 1f);
        yield return new WaitForSeconds(0.5f);

        TxtObj.transform.DOScale(Vector3.one, 0.5f);
        yield return new WaitForSeconds(0.5f);


        Destroy(TxtObj);
    }
}

