﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

//Structure For Sequencing Attribute Particles and can be edited directly from inspector
[Serializable]
public class AttribSequence
{
    public string AttribName;

    [Header("Attribute Start")]
    public float AttribStartDelay;
    public GameObject AttribStart; // particles when starting an Attrib
    public Vector3 AttribStartPositionMe;
    public Vector3 AttribStartPositionOpp;
    public Vector3 AttribStartRotationMe;
    public Vector3 AttribStartRotationOpp;
    public AudioClip StartAudio;

    [Header("Attribute Hit")]
    public float AttribHitDelay;
    public GameObject AttribHit;
    public Vector3 AttribHitPositionMe;
    public Vector3 AttribHitPositionOpp;
    public Vector3 AttribHitRotationMe;
    public Vector3 AttribHitRotationOpp;
    public AudioClip HitAudio;

    [Header("Attribute After Hit")]
    public float AfterAttribHitDelay;
    public GameObject AttribAfterHit;
    public Vector3 AttribAfterHitPositionMe;
    public Vector3 AttribAfterHitPositionOpp;
    public Vector3 AttribAfterHitRotationMe;
    public Vector3 AttribAfterHitRotationOpp;
    public AudioClip AfterHitAudio;

    [Header("Attribute Final")]
    public float AttribAfterFinalDelay;
    public GameObject AttribAfterFinal;
    public Vector3 AttribAfterFinalPositionMe;
    public Vector3 AttribAfterFinalPositionOpp;
    public Vector3 AttribAfterFinalRotationMe;
    public Vector3 AttribAfterFinalRotationOpp;
    public AudioClip FinalAudio;


}


public class DiceManager : MonoBehaviour
{
    public static DiceManager instance;

    public Transform dice,dice2;
    public GameObject numberOfPower;
    public TextMeshPro NumOfPower, NumOfPower2;
    public AudioClip DiceRollMusic;
    public AudioClip CardClick;
    public AudioClip CardReset;

    //New Battle particle system
    public GameObject electicStrom;
    public GameObject[] attackParticle;

    public PlayerCompanionCardsAnim playerCompanionCardsAnim;

   
   

    /*//--------------------------------------------------
    [Header("Attack Particles")]
    public GameObject[] AttackStart; // particles when starting an attack
    public AudioClip[] StartAudio;
    public GameObject[] AttackHit;
    public AudioClip[] HitAudio;
    public GameObject[] AttackAfterHit;
    public AudioClip[] AfterHitAudio;
    public GameObject[] AttackAfterFinal;
    public AudioClip[] FinalAudio;

    [Header("Defense Particles")]
    public GameObject[] DefenseStart; // particles when starting an Defense
    public AudioClip[] DefenseStartAudio;
    public GameObject[] DefenseHit;
    public AudioClip[] DefenseHitAudio;
    public GameObject[] DefenseAfterHit;
    public AudioClip[] DefenseAfterHitAudio;
    public GameObject[] DefenseAfterFinal;
    public AudioClip[] DefenseFinalAudio;

    [Header("Heal Particles")]
    public GameObject[] HealStart; // particles when starting an Heal
    public AudioClip[] HealStartAudio;
    public GameObject[] HealHit;
    public AudioClip[] HealHitAudio;
    public GameObject[] HealAfterHit;
    public AudioClip[] HealAfterHitAudio;
    public GameObject[] HealAfterFinal;
    public AudioClip[] HealFinalAudio;

    [Header("Disrupt Particles")]
    public GameObject[] DisruptStart; // particles when starting an Disrupt
    public AudioClip[] DisruptStartAudio;
    public GameObject[] DisruptHit;
    public AudioClip[] DisruptHitAudio;
    public GameObject[] DisruptAfterHit;
    public AudioClip[] DisruptAfterHitAudio;
    public GameObject[] DisruptAfterFinal;
    public AudioClip[] DisruptFinalAudio;*/

    [Header("Attribute Sequence")]

    public AttribSequence[] AttackSequence, DefenseSequence, HealSequence, DisruptSequence;

    [Header("Special Ability Sequence")]

    public AttribSequence[] SpecialSequence;


   [Header("After Battle Text ")]
    public string[] WinSlang;
    public string[] LostSlang;
    public string[] SpecialSlang;
    public AudioClip[] winSound;
    public AudioClip[] lostSound;
    public Sprite[] DiceFronts;

    public SpriteRenderer FinalDiceFront,Player2DiceFront;

    int Attack_Value, Defense_Value, Disrupt_value, Heal_value;

    CardObject SelectedCard;

    public GameObject ElEarnMe, ELEarnOpp;

    public int playerDiceNum = 0;
    public int enemyDiceNum = 0;

    public TextMeshProUGUI WinText, LostText, SpecialText,MeEp,OppEP,MeTrophies,OppTrophies;
    public GameObject finalScreenBg;
    public GameObject Cloud;

    public int TestMeint, TestOppint;

    public GameObject LootMessage, LootObject;
    public GameObject[] FinalObjToDisable;

    public TextMeshProUGUI dice1Num, dice2Num;
    public Image dice1Attack, dice2Attack;
    private void Awake()
    {
        if (instance == null) instance = this;
       
    }



    public void Btn_ClickOnDice()
    {
        //Debug.Log("click on dice one ");
        if (!PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish)
        {
            //Debug.Log("Click on dice one ");
            if (InGameCards.instance.MeCounter < 3)
            {
                return;

            }
        }
        InGameCards.instance.PauseCountTimer();

       

        if (!PlayerManager.instance.CanClick || !PlayerManager.instance.CanClickonMeDice)
        {
            return;
        }
      

        PlayerManager.instance.CompanionCards.canClickCompanion = false;
        for (int i = 0; i < PlayerManager.instance.SelectedCard.elementeumChild.Length; i++)
        {

            PlayerManager.instance.SelectedCard.elementeumChild[i].GetComponent<Elementium>().canClickElementeum = false;

        }
        PlayerManager.instance.CanClickonMeDice = false;
        PlayerManager.instance.CanClickOnMeCard = false;
        PlayerManager.instance.CanClickOnElemt = false;
        PlayerManager.instance.CanClickOnBG = false;

        playerDiceNum = UnityEngine.Random.Range(1, 7);
        //playerDiceNum = 1;
        PlayerManager.instance.SelectedCard.DiceValue = playerDiceNum;
        bool UsingElementeum;
        if (PlayerManager.instance.SelectedCard.UsingElementeum > 0)
        {
            UsingElementeum = true;
        }
        else
        {
            UsingElementeum = false;
        }

        //Debug.Log(UsingElementeum);
        if (PlayerManager.instance.SelectedCard.isUseSpAbility)
        {
            PlayerManager.instance.SelectedCard.isUseSpAbility = false;

        }
        ////Debug.Log("PLayer One DIce Number---" + DNum + " usingElemt " + UsingElementeum + " count  " + PlayerManager.instance.SelectedCard.UsingElementeum);
        InGameCards.instance.Dice.text = playerDiceNum.ToString();

        //SyncGameplaySc.Instance.GameData.TurnData[SyncGameplaySc.Instance.GameData.TurnData.Length - 1].MyData.RollDice = DNum.ToString();

//BattleLobbySC.Instance.LastAction = "me:onediceroll:" + playerDiceNum;

        //SyncGameplaySc.Instance.Save();

        ////Debug.Log(SelectPowerManager.instance.selectedPowerName);
        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().text.transform.DOScale(new Vector3(0f, 0f, 0f), 1);
        }

        switch (SelectPowerManager.instance.selectedPowerName)
        {
            case AttributeType.Attack:
                for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
                {
                    if (PlayerManager.instance.PlayerCardsObj[i].CardDetail.card_contract_id == PlayerManager.instance.SelectedCard.CardDetail.card_contract_id)
                    {
                        PlayerManager.instance.SelectedCard.debuffAtk = true;
                        ////Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);

                    }
                    else
                    {
                        PlayerManager.instance.PlayerCardsObj[i].debuffValueAttack = 0;
                        PlayerManager.instance.PlayerCardsObj[i].debuffAtk = false;

                        PlayerManager.instance.PlayerCardsObj[i].debuffValueHeal = 0;
                        PlayerManager.instance.PlayerCardsObj[i].debuffHl = false;

                        PlayerManager.instance.SelectedCard.debuffValueHeal = 0;
                        PlayerManager.instance.SelectedCard.debuffHl = false;

                        ////Debug.Log(PlayerManager.instance.PlayerCardsObj[i].CardDetail.name);
                        ////Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);
                    }
                }
                break;

            case AttributeType.Heal:
                for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
                {
                    if (PlayerManager.instance.PlayerCardsObj[i].CardDetail.card_contract_id == PlayerManager.instance.SelectedCard.CardDetail.card_contract_id)
                    {
                        PlayerManager.instance.SelectedCard.debuffHl = true;
                        ////Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);


                    }
                    else
                    {
                        PlayerManager.instance.PlayerCardsObj[i].debuffValueHeal = 0;
                        PlayerManager.instance.PlayerCardsObj[i].debuffHl = false;

                        PlayerManager.instance.PlayerCardsObj[i].debuffValueAttack = 0;
                        PlayerManager.instance.PlayerCardsObj[i].debuffAtk = false;


                        PlayerManager.instance.SelectedCard.debuffValueAttack = 0;
                        PlayerManager.instance.SelectedCard.debuffAtk = false;

                        ////Debug.Log(PlayerManager.instance.PlayerCardsObj[i].CardDetail.name);
                        ////Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);
                    }
                }
                break;
        }

       
        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Attack )
        {

            ////Debug.Log("Dice num = " + DNum + " Elementeum boolean = " + UsingElementeum + " Elementeum = " +
           //PlayerManager.instance.SelectedCard.UsingElementeum +
           //" Companion Value = " + playerCompanionCardsAnim.plyComcardTempValue + " debuff value " +
           //PlayerManager.instance.SelectedCard.debuffValueAttack +
           //" skirmish " + PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish);

            BattleLobbySC.Instance.PlayerOneDiceNumber(
            PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
            "",
            AttributeType.Attack,
            PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
            PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
            PlayerManager.instance.SelectedCard.ElementeumStatus,
            playerCompanionCardsAnim.SelectedCompanionIds,
            playerDiceNum.ToString(),
            PlayerManager.instance.SelectedCard.CardDetail.attack.ToString(),
            UsingElementeum,
            PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
            PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
            PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
            );
        }
        else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {
            BattleLobbySC.Instance.PlayerOneDiceNumber(
           PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
           PlayerManager.instance.SelectedHealCard.CardDetail.card_contract_id,
           AttributeType.Heal,
           PlayerManager.instance.SelectedCard.debuffValueHeal.ToString(),
           "",
           PlayerManager.instance.SelectedCard.ElementeumStatus,
           playerCompanionCardsAnim.SelectedCompanionIds,
           playerDiceNum.ToString(),
           PlayerManager.instance.SelectedCard.CardDetail.attack.ToString(),
           UsingElementeum,
           PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
           PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
           PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
           PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
           );
        }
        else
        {
            BattleLobbySC.Instance.PlayerOneDiceNumber(
          PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
          "",
          AttributeType.Heal,
          "0",
          PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
          PlayerManager.instance.SelectedCard.ElementeumStatus,
          playerCompanionCardsAnim.SelectedCompanionIds,
          playerDiceNum.ToString(),
          PlayerManager.instance.SelectedCard.CardDetail.attack.ToString(),
          UsingElementeum,
          PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
          PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
          PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
          PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
          );
        }

        

        PlayerManager.instance.CanClick = false; // Disabling accidental clicks while attack

        GetComponent<AudioSource>().clip = DiceRollMusic;

        GetComponent<AudioSource>().Play();

        SelectedCard = PlayerManager.instance.SelectedCard;

        dice.GetComponent<Animator>().SetTrigger("roll");

        StartCoroutine(ChangeTUrn());

        StartCoroutine(RollDice());

        StartCoroutine(UpdateDiceAttribValue(playerDiceNum));

        // StartCoroutine(AttackParticle_Fun_Active());      
    }


    public void PlayerTwoDiceNumber()
    {
        if (!PlayerManager.instance.CanClick || !PlayerManager.instance.CanClickonEnemyDice)
        {
            return;
        }
        if (!PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish)
        {
            if (InGameCards.instance.MeCounter < 3)
            {
                return;

            }
        }
        PlayerManager.instance.CompanionCards.canClickCompanion = false;
        for (int i = 0; i < PlayerManager.instance.SelectedCard.elementeumChild.Length; i++)
        {

            PlayerManager.instance.SelectedCard.elementeumChild[i].GetComponent<Elementium>().canClickElementeum = false;

        }
        PlayerManager.instance.CanClickonEnemyDice = false;

        enemyDiceNum = UnityEngine.Random.Range(1, 7);
        //int enemyTwoDiceNum = 2;

        PlayerManager.instance.SelectedCard.DiceValue = enemyDiceNum;

        bool UsingElementeum;

        if (PlayerManager.instance.SelectedCard.UsingElementeum > 0)
        {
            UsingElementeum = true;
        }
        else
        {
            UsingElementeum = false;
        }
        //Debug.Log(PlayerManager.instance.SelectedCard.isUseSpAbility);
        if (PlayerManager.instance.SelectedCard.isUseSpAbility)
        {
            PlayerManager.instance.SelectedCard.isUseSpAbility = false;

        }
        ////Debug.Log("PLayer One DIce Number---" + DNum2 + " usingElemt " + UsingElementeum + " count  " + PlayerManager.instance.SelectedCard.UsingElementeum);

        InGameCards.instance.Dice.text = enemyDiceNum.ToString();

        ////Debug.Log(SelectPowerManager.instance.selectedPowerName);// = CardStrengthType.Attack;
        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().text.transform.DOScale(new Vector3(0f, 0f, 0f), 1);
        }
        switch (SelectPowerManager.instance.selectedPowerName)
        {
            case AttributeType.Attack:
                for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
                {
                    if (PlayerManager.instance.PlayerCardsObj[i].CardDetail.card_contract_id == PlayerManager.instance.SelectedCard.CardDetail.card_contract_id)
                    {
                        PlayerManager.instance.SelectedCard.debuffAtk = true;
                        ////Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);
                    }
                    else
                    {
                        PlayerManager.instance.PlayerCardsObj[i].debuffValueAttack = 0;
                        PlayerManager.instance.PlayerCardsObj[i].debuffAtk = false;

                        PlayerManager.instance.PlayerCardsObj[i].debuffValueHeal = 0;
                        PlayerManager.instance.PlayerCardsObj[i].debuffHl = false;

                        PlayerManager.instance.SelectedCard.debuffValueHeal = 0;
                        PlayerManager.instance.SelectedCard.debuffHl = false;


                        ////Debug.Log(PlayerManager.instance.PlayerCardsObj[i].CardDetail.name);
                        ////Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);
                    }
                }
                break;

            case AttributeType.Heal:
                for (int i = 0; i < PlayerManager.instance.PlayerCardsObj.Count; i++)
                {
                    if (PlayerManager.instance.PlayerCardsObj[i].CardDetail.card_contract_id == PlayerManager.instance.SelectedCard.CardDetail.card_contract_id)
                    {
                        PlayerManager.instance.SelectedCard.debuffHl = true;
                        ////Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);
                    }
                    else
                    {
                        PlayerManager.instance.PlayerCardsObj[i].debuffValueAttack = 0;
                        PlayerManager.instance.PlayerCardsObj[i].debuffAtk = false;

                        PlayerManager.instance.PlayerCardsObj[i].debuffValueHeal = 0;
                        PlayerManager.instance.PlayerCardsObj[i].debuffHl = false;

                        PlayerManager.instance.SelectedCard.debuffValueAttack = 0;
                        PlayerManager.instance.SelectedCard.debuffAtk = false;

                        ////Debug.Log(PlayerManager.instance.PlayerCardsObj[i].CardDetail.name);
                        ////Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);

                    }
                }
                break;
        }




        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Attack)
        {
            Debug.Log("player two use " + SelectPowerManager.instance.selectedPowerName + " attribute");
                BattleLobbySC.Instance.PlayerTwoDiceNumber(
                PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                "",
                SelectPowerManager.instance.selectedEnemyPowerName,
                PlayerManager.instance.SelectedEnemyCard.CardDetail.attack.ToString(),
                PlayerManager.instance.SelectedEnemyCard.debuffValueAttack.ToString(),
                PlayerManager.instance.SelectedEnemyCard.ElementeumStatus,
                PlayerManager.instance.SelectedEnemyCard.companionIDs,
                playerDiceNum.ToString(),
                PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(),
                PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString(),
                PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString(),
                PlayerManager.instance.SelectedEnemyCard.isSpActive,
                BattleLobbySC.Instance.heartValOpp.ToString(),
                PlayerManager.instance.SelectedEnemyCard.MaxHeartPoint.ToString(),
                "",

                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                SelectPowerManager.instance.selectedPowerName,
                PlayerManager.instance.SelectedCard.CardDetail.attack.ToString(),
                PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                PlayerManager.instance.SelectedCard.ElementeumStatus,
                PlayerManager.instance.SelectedCard.companionIDs,
                enemyDiceNum.ToString(),
                PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
                PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
                PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
                PlayerManager.instance.SelectedCard.isSpActive,
                BattleLobbySC.Instance.heartValME.ToString(),
                PlayerManager.instance.SelectedCard.MaxHeartPoint.ToString(),

                PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
            );
        }
        else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {
            Debug.Log("player two use " + SelectPowerManager.instance.selectedPowerName + " attribute");
            BattleLobbySC.Instance.PlayerTwoDiceNumber(
                 PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                 PlayerManager.instance.SelectedHealEnemyCard.CardDetail.card_contract_id,
                 SelectPowerManager.instance.selectedEnemyPowerName,
                 PlayerManager.instance.SelectedEnemyCard.CardDetail.heal.ToString(),
                 PlayerManager.instance.SelectedEnemyCard.debuffValueHeal.ToString(),
                 PlayerManager.instance.SelectedEnemyCard.ElementeumStatus,
                 PlayerManager.instance.SelectedEnemyCard.companionIDs,
                 playerDiceNum.ToString(),
                 PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(),
                 PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString(),
                 PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString(),
                 PlayerManager.instance.SelectedEnemyCard.isSpActive,
                 "",
                 PlayerManager.instance.SelectedEnemyCard.MaxHeartPoint.ToString(),
                 PlayerManager.instance.SelectedHealEnemyCard.MaxHeartPoint.ToString(),

                 PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                 SelectPowerManager.instance.selectedPowerName,
                 PlayerManager.instance.SelectedEnemyCard.CardDetail.heal.ToString(),

                 PlayerManager.instance.SelectedCard.debuffValueAttack.ToString(),
                 PlayerManager.instance.SelectedCard.ElementeumStatus,
                 PlayerManager.instance.SelectedCard.companionIDs,
                 enemyDiceNum.ToString(),
                 PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
                 PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
                 PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
                 PlayerManager.instance.SelectedCard.isSpActive,
                 "",
                 PlayerManager.instance.SelectedCard.MaxHeartPoint.ToString(),

                 PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
            );
        }
        else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Defense)
        {
            Debug.Log("player two use " + SelectPowerManager.instance.selectedPowerName + " attribute");
           BattleLobbySC.Instance.PlayerTwoDiceNumber(
               PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
               "",
               SelectPowerManager.instance.selectedEnemyPowerName,
               PlayerManager.instance.SelectedEnemyCard.CardDetail.attack.ToString(),
               PlayerManager.instance.SelectedEnemyCard.debuffValueAttack.ToString(),
               PlayerManager.instance.SelectedEnemyCard.ElementeumStatus,
               PlayerManager.instance.SelectedEnemyCard.companionIDs,
               playerDiceNum.ToString(),
               PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(),
               PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString(),
               PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString(),
               PlayerManager.instance.SelectedEnemyCard.isSpActive,
               BattleLobbySC.Instance.heartValOpp.ToString(),
               PlayerManager.instance.SelectedEnemyCard.MaxHeartPoint.ToString(),
               "",

               PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
               SelectPowerManager.instance.selectedPowerName,
               PlayerManager.instance.SelectedCard.CardDetail.defence.ToString(),
               "0",
               PlayerManager.instance.SelectedCard.ElementeumStatus,
               PlayerManager.instance.SelectedCard.companionIDs,
               enemyDiceNum.ToString(),
               PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
               PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
               PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
               PlayerManager.instance.SelectedCard.isSpActive,
               BattleLobbySC.Instance.heartValME.ToString(),
               PlayerManager.instance.SelectedCard.MaxHeartPoint.ToString(),

               PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
           );
        }
        else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Disrupt)
        {
            Debug.Log("player two use " + SelectPowerManager.instance.selectedPowerName + " attribute");
            BattleLobbySC.Instance.PlayerTwoDiceNumber(
                PlayerManager.instance.SelectedEnemyCard.CardDetail.card_contract_id,
                PlayerManager.instance.SelectedHealEnemyCard.CardDetail.card_contract_id,
                SelectPowerManager.instance.selectedEnemyPowerName,
                PlayerManager.instance.SelectedEnemyCard.CardDetail.heal.ToString(),
                PlayerManager.instance.SelectedEnemyCard.debuffValueHeal.ToString(),
                PlayerManager.instance.SelectedEnemyCard.ElementeumStatus,
                PlayerManager.instance.SelectedEnemyCard.companionIDs,
                playerDiceNum.ToString(),
                PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum.ToString(),
                PlayerManager.instance.SelectedEnemyCard.UsingElementeum.ToString(),
                PlayerManager.instance.SelectedEnemyCard.CompanionValueStatus.ToString(),
                PlayerManager.instance.SelectedEnemyCard.isSpActive,
                "",
                PlayerManager.instance.SelectedEnemyCard.MaxHeartPoint.ToString(),
                PlayerManager.instance.SelectedHealEnemyCard.MaxHeartPoint.ToString(),

                PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                SelectPowerManager.instance.selectedPowerName,
                PlayerManager.instance.SelectedCard.CardDetail.disrupt.ToString(),
                "0",
                PlayerManager.instance.SelectedCard.ElementeumStatus,
                PlayerManager.instance.SelectedCard.companionIDs,
                enemyDiceNum.ToString(),
                PlayerManager.instance.SelectedCard.CardDetail.elementeum.ToString(),
                PlayerManager.instance.SelectedCard.UsingElementeum.ToString(),
                PlayerManager.instance.SelectedCard.CompanionValueStatus.ToString(),
                PlayerManager.instance.SelectedCard.isSpActive,
                "",
                PlayerManager.instance.SelectedCard.MaxHeartPoint.ToString(),

                PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>()._inSkirmish.ToString()
            );
        }
        PlayerManager.instance.CanClick = false; // Disabling accidental clicks while attack

        dice2.GetComponent<Animator>().SetTrigger("rollreverseme");

        GetComponent<AudioSource>().clip = DiceRollMusic;
        GetComponent<AudioSource>().Play();
        Attack_Value = PlayerManager.instance.SelectedCard.CardDetail.attack;
        SelectedCard = PlayerManager.instance.SelectedCard;

        //dice2.GetComponent<Animator>().SetTrigger("roll");

        StartCoroutine(RollDice2(enemyDiceNum));

        StartCoroutine(UpdateDiceAttribValue(enemyDiceNum)); // Delay Card Attribute Update For After Dice to roll

    }


    IEnumerator UpdateDiceAttribValue(int Dnumber)
    {

        yield return new WaitForSeconds(1.2f);


        var PlayerCardObj = PlayerManager.instance.SelectedCard;
        switch (SelectPowerManager.instance.selectedPowerName)
        {
            case AttributeType.Attack:

                if (SkirmishModeAnim.instance.inSkirmish)
                {
                    InGameCards.instance.MyAttribValue.text = Dnumber.ToString();

                }
                else
                {

                    InGameCards.instance.MyAttribValue.text = PlayerCardObj.TotalAttack().ToString();
                }

                int valAtk = PlayerCardObj.CardDetail.attack;

                if (PlayerManager.instance.getSpOpp())
                {
                    valAtk = 0;
                }
                break;

            case AttributeType.Heal:
                if (SkirmishModeAnim.instance.inSkirmish)
                {
                    InGameCards.instance.MyAttribValue.text = Dnumber.ToString();

                }
                else
                {
                    InGameCards.instance.MyAttribValue.text = PlayerCardObj.TotalHeal().ToString();
                }
                int valHeal = PlayerCardObj.CardDetail.heal;

                if (PlayerManager.instance.getSpOpp())
                {
                    valHeal = 0;
                }
                StartCoroutine(HealAnimStart(Dnumber, true));
                break;

            case AttributeType.Defense:
                if (SkirmishModeAnim.instance.inSkirmish)
                {
                    InGameCards.instance.MyAttribValue.text = Dnumber.ToString();

                }
                else
                {
                    InGameCards.instance.MyAttribValue.text = PlayerCardObj.TotalDefence().ToString();
                }
                int valDef = PlayerCardObj.CardDetail.defence;

                if (PlayerManager.instance.getSpOpp())
                {
                    valDef = 0;
                }
                break;

            case AttributeType.Disrupt:
                if (SkirmishModeAnim.instance.inSkirmish)
                {
                    InGameCards.instance.MyAttribValue.text = Dnumber.ToString();

                }
                else
                {
                    InGameCards.instance.MyAttribValue.text = PlayerCardObj.TotalDisrupt().ToString();
                }
                int valDis = PlayerCardObj.CardDetail.disrupt;

                if (PlayerManager.instance.getSpOpp())
                {
                    valDis = 0;
                }
                break;
        }

    }

    public void PlayerTwoDiceNumberRPC(int RPCDiceNumber) //Player Two DIce CLick Animation on Multiplayer
    {
        //Debug.Log("Opp dice roll");
        PlayerManager.instance.SelectedEnemyCard.DiceValue = RPCDiceNumber;
        InGameCards.instance.PauseCountTimer();


        PlayerManager.instance.CanClick = false; // Disabling accidental clicks while attack

        if (RPCDiceNumber == 0)
        {
            dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
            return;
        }
        GetComponent<AudioSource>().clip = DiceRollMusic;

        GetComponent<AudioSource>().Play();
        if (PlayerManager.instance.SelectedEnemyCard.isUseSpAbility)
        {
            PlayerManager.instance.SelectedEnemyCard.isUseSpAbility = false;

        }

        InGameCards.instance.enemyDice.text = RPCDiceNumber.ToString();
        if (InGameCards.instance.PTurn())
        {
            dice2.GetComponent<Animator>().SetTrigger("rollreverseopp");
        }

        else
        {
            dice2.GetComponent<Animator>().SetTrigger("rollreverseme");
        }

        // dice2.GetComponent<Animator>().SetTrigger("roll");

        StartCoroutine(RollDice2(RPCDiceNumber));

        StartCoroutine(UpdateAttribValueOpponent(RPCDiceNumber));


    }


    public IEnumerator RollDice2(int Dnumr)
    {
        if (Dnumr==0)
        {
            yield break;  
        }

        //print("Dice Number---" + Dnumr);
        //Debug.Log("Roll DIce 2  IEnum");
        yield return new WaitForSeconds(.5f);

        Player2DiceFront.sprite = DiceFronts[Dnumr - 1];
        //print(DiceFronts[Dnumr - 1]);
        NumOfPower2.text = (Dnumr.ToString());

        yield return new WaitForSeconds(.3f);
        Player2DiceFront.sprite = DiceFronts[Dnumr - 1];
        GetComponent<AudioSource>().Pause();

       
        //Dice Disable 31/12/19
        yield return new WaitForSeconds(1f);
        if (!PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish)
        {

            dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
            //InGameCards.instance.enemyDice.text = Dnumr.ToString();

        }
        else
        {
            dice2Num.gameObject.SetActive(true);
            dice2Attack.gameObject.SetActive(true);
            dice2Num.text = Dnumr.ToString();
            dice2Num.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-20, -5.5f), 1);
            dice2Attack.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-20, -21f), 1);
            //dice1Attack, dice2Attack;
        }

    }

    IEnumerator ChangeTUrn()
    {
        yield return new WaitForSeconds(2f);
        BattleLobbySC.Instance.SocketMessage("turnchange");
    }

    public void Btn_ClickonDiceRPC(int DNumber)
    {
        Debug.Log("Btn_ClickonDiceRPC " + DNumber);
        playerDiceNum = DNumber;
        PlayerManager.instance.SelectedEnemyCard.DiceValue = DNumber;
        GetComponent<AudioSource>().clip = DiceRollMusic;

        GetComponent<AudioSource>().Play();

        SelectedCard = PlayerManager.instance.SelectedCard;

        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().text.transform.DOScale(new Vector3(0f, 0f, 0f), 1);
        }

        if (PlayerManager.instance.SelectedEnemyCard.isUseSpAbility)
        {
            PlayerManager.instance.SelectedEnemyCard.isUseSpAbility = false;

        }

        if (InGameCards.instance.PTurn())
        {

            dice.GetComponent<Animator>().SetTrigger("rollopp");
        }

        else
        {
            dice.GetComponent<Animator>().SetTrigger("roll");
        }

        SelectPowerManager.instance.PlayerTwo = true;

        StartCoroutine(RollDice());

        StartCoroutine(UpdateAttribValueOpponent(playerDiceNum));

        if (SelectPowerManager.instance.selectedPowerName != AttributeType.None)
        {

            StartCoroutine(TieTUrnChange(true));
        }
    }

    IEnumerator UpdateAttribValueOpponent(int Dnumber)
    {
        int diceVal = Dnumber;

        yield return new WaitForEndOfFrame();

        var EnemyPlayer = PlayerManager.instance.SelectedEnemyCard;

        if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Attack)
        {

            if (SkirmishModeAnim.instance.inSkirmish)
            {

                InGameCards.instance.OppAttribValue.text = diceVal.ToString();

            }
            else 
            {
                
                InGameCards.instance.OppAttribValue.text = (EnemyPlayer.TotalAttack()).ToString();
            }

        }
        else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Defense)
        {
            
            if (SkirmishModeAnim.instance.inSkirmish)
            {
                InGameCards.instance.OppAttribValue.text = diceVal.ToString();
            }
            else
            {

                InGameCards.instance.OppAttribValue.text = (EnemyPlayer.TotalDefence()).ToString();
            }
        }
        else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
        {
            
            StartCoroutine(HealAnimStart(diceVal, false));
            if (SkirmishModeAnim.instance.inSkirmish)
            {
                InGameCards.instance.OppAttribValue.text = diceVal.ToString();

            }
            else
            {
                InGameCards.instance.OppAttribValue.text = (EnemyPlayer.TotalHeal()).ToString();
            }
        }

        else if (SelectPowerManager.instance.selectedEnemyPowerName== AttributeType.Disrupt)
        {

            
            if (SkirmishModeAnim.instance.inSkirmish)
            {
                InGameCards.instance.OppAttribValue.text = diceVal.ToString();
            }
            else
            {
                InGameCards.instance.OppAttribValue.text = (EnemyPlayer.TotalDisrupt()).ToString();
            }
        }


        if (Dnumber != 0)
        {
            InGameCards.instance.enemyDice.text = diceVal.ToString();
        }


    }



    public IEnumerator DefeatAnim(string fData)
    {
        Debug.LogError("user Defeat ");
        InGameCards.instance.PauseCountTimer();
        InGameCards.instance.OpponentLostPanel.SetActive(false);
        //Debug.Log("Defeat/////////");
        yield return new WaitForSeconds(2f);
        PlayerManager.instance.DefeatObj.SetActive(true);
        yield return new WaitForSeconds(4f);
        PlayerManager.instance.DefeatObj.SetActive(false);
        PlayerManager.instance.FinalBattleScreenSHow("opp");
        PlayerManager.instance.CanClickOnBG = false;

        //var rewardData = BattleLobbySC.Instance.gameCompleteData;
        JSONNode Jnode = JSON.Parse(fData);
        //Debug.Log(rewardData);

        if (Jnode["status"] == 1)
        {
            MeTrophies.text = Jnode["data"]["loserTrophies"];
            OppTrophies.text = Jnode["data"]["winnerTrophies"];

            MeEp.text = Jnode["data"]["loserEp"];
            OppEP.text = Jnode["data"]["winnerEp"]; 
        }
        else
        {
            MeEp.text = "+1";
            OppEP.text = "2";

            MeTrophies.text = "0";
            OppTrophies.text = "100";
        }
       
        finalScreenBg.gameObject.SetActive(true);

        for (int i = 0; i < FinalObjToDisable.Length; i++)
        {
            FinalObjToDisable[i].SetActive(false);
        }

        
        FinalObjToDisable[10].SetActive(true);
        BattleLobbySC.Instance.DisconnectSocket();
    }

    public IEnumerator DrawAnim()
    {
        InGameCards.instance.PauseCountTimer();
        InGameCards.instance.OpponentLostPanel.SetActive(false);
        //Debug.Log("Draw/////////");
        yield return new WaitForSeconds(2f);
        PlayerManager.instance.DrawObj.SetActive(true);
        yield return new WaitForSeconds(3f);
        PlayerManager.instance.DrawObj.SetActive(false);
        PlayerManager.instance.FinalBattleScreenSHow("draw");

        MeEp.text = "1";
        OppEP.text = "1";

        MeTrophies.text = "0";

        OppTrophies.text = "0";
        finalScreenBg.gameObject.SetActive(true);
        for (int i = 0; i < FinalObjToDisable.Length; i++)
        {
            FinalObjToDisable[i].SetActive(false);
        }

        FinalObjToDisable[10].SetActive(true);
        BattleLobbySC.Instance.DisconnectSocket();
    }



    public IEnumerator WinAnim(string rewardData)
    {
        Debug.Log("WinAnim" + rewardData);
        InGameCards.instance.PauseCountTimer();
        InGameCards.instance.OpponentLostPanel.SetActive(false);
        //Debug.Log("Victory/////////");
        yield return new WaitForSeconds(2f);
        PlayerManager.instance.VictoryObj.SetActive(true);
        yield return new WaitForSeconds(4f);

        PlayerManager.instance.VictoryObj.SetActive(false);
        PlayerManager.instance.FinalBattleScreenSHow("me");
        PlayerManager.instance.CanClickOnBG = true;
        //var rewardData = BattleLobbySC.Instance.gameCompleteData;
        JSONNode Jnode = JSON.Parse(rewardData);
        Debug.Log("WinAnim" + Jnode);

        if (Jnode["status"] == 1)
        {
            MeTrophies.text = Jnode["data"]["winnerTrophies"];
            OppTrophies.text = Jnode["data"]["loserTrophies"];

            MeEp.text = Jnode["data"]["winnerEp"];
            OppEP.text = Jnode["data"]["loserEp"];
        }
        else
        {
            MeEp.text = "+2";
            OppEP.text = "1";

            MeTrophies.text = "+100";
            OppTrophies.text = "00";
        }
        for (int i = 0; i < FinalObjToDisable.Length; i++)
        {
            FinalObjToDisable[i].SetActive(false);
        }

        FinalObjToDisable[10].SetActive(true);

        yield return new WaitForSeconds(0.5f);
        finalScreenBg.gameObject.SetActive(false);
        PlayerManager.instance.bgFinalEndScreen.enabled = false;
        PlayerManager.instance.canvasFinalEndScreen.SetActive(true);
        //LootMessage.SetActive(true);
        LootObject.SetActive(true);

        yield return new WaitForSeconds(2f);



        BattleLobbySC.Instance.DisconnectSocket();
    }


    public IEnumerator RollDice()
    {
        //Debug.Log("Roll DIce IEnum");

        yield return new WaitForSeconds(.5f);
       
        FinalDiceFront.sprite = DiceFronts[playerDiceNum - 1];
   
        NumOfPower.text = (playerDiceNum.ToString());
        
        yield return new WaitForSeconds(.3f);

        FinalDiceFront.sprite = DiceFronts[playerDiceNum - 1];

        GetComponent<AudioSource>().Pause();
       
        if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
        {
            PlayerManager.instance.TextCanvas.SetActive(true);

            PlayerManager.instance.TextMessage.text = "Select Card to Disrupt Opponent Heal";

            for (int i = 0; i < 3; i++)
            {
                if (!PlayerManager.instance.PlayerCardsObj[i].DestroyedCard && !PlayerManager.instance.PlayerCardsObj[i].isDisable)
                {
                    PlayerManager.instance.PlayerCardsObj[i].GetComponent<SpriteRenderer>().sortingOrder = 6;
                    PlayerManager.instance.PlayerCardsObj[i].canvas.sortingOrder = 8;
                    PlayerManager.instance.PlayerCardsObj[i].OppSetUpSort();
                }
            }
        }
        //Dice Disable 31/12/19
        yield return new WaitForSeconds(1f);
        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {
            dice1Num.gameObject.SetActive(true);
            dice1Attack.gameObject.SetActive(true);
            dice1Num.text = playerDiceNum.ToString();
            dice1Num.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(20, 5.4f), 1);
            dice1Attack.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(20, 18f), 1);
        }
        else
        {

            dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);       
        }

    }

    public IEnumerator AttackParticle_Fun_Active(int MeInt,int OppInt,bool isMeSp,bool isOppSp)
    {
        ////Debug.Log(MeInt  + " After win condition in update player comes here and My selected power is  " + SelectPowerManager.instance.selectedPowerName);
        ////Debug.LogError("Player power name " + SelectPowerManager.instance.selectedPowerName + " and card Name  " + PlayerManager.instance.SelectedCard.name);
        if (PlayerManager.instance.SelectedCard != null)
        {

            for (int i = 0; i < PlayerManager.instance.SelectedCard.CardDetail.elementeum; i++)
            {

                PlayerManager.instance.elementeumParticle[i].StartPosition = Vector3.zero;
                PlayerManager.instance.elementeumParticle[i].gameObject.SetActive(false);
            }
        }
        
        if (PlayerManager.instance.SelectedEnemyCard != null)
        {


            for (int i = 0; i < PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum; i++)
            {
                //elementeumParticleEnemy
                PlayerManager.instance.elementeumParticleEnemy[i].StartPosition = Vector3.zero;
                PlayerManager.instance.elementeumParticleEnemy[i].gameObject.SetActive(false);
            }
        }
       
        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Disrupt)
        {
            int CardInt = 0;

            for (int i = 0; i < 3; i++)
            {
                if (PlayerManager.instance.PlayerCardsObj[i].DestroyedCard)
                {
                    CardInt += 1;
                }
            }

            if (CardInt < 2 )
            {
                //Debug.Log("distrupt working and card is locked");
                PlayerManager.instance.SelectedCard.SetDisableTemp();
            }

        }

        if (SelectPowerManager.instance.selectedEnemyPowerName== AttributeType.Disrupt)
        {

            int CardInt = 0;

            for (int i = 0; i < 3; i++)
            {
                if (PlayerManager.instance.enemyCardsObj[i].DestroyedCard)
                {
                    CardInt += 1;
                }
            }
            if (CardInt < 2)
            {
                //Debug.Log("distrupt working and card is locked");
                if (PlayerManager.instance.SelectedEnemyCard != null)
                {

                PlayerManager.instance.SelectedEnemyCard.SetDisableTemp();
                }
                else
                {
                    var objCollection = PlayerManager.instance.enemyCardsObj.Where(x => x.DestroyedCard== false);
                    if (objCollection.Any())
                    {
                        objCollection.First().SetDisableTemp();
                    }
                    //PlayerManager.instance.MyCardsObj[0].SetDisableTemp();

                }
            }

        }
        // MeInt = TestMeint;
        //OppInt = TestOppint;

        if(BattleLobbySC.Instance)
        BattleLobbySC.Instance.PauseCountTimer();

        PlayerManager.instance.CanClick = false;

        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {

            WinText.text = WinSlang[4];
            //Debug.Log("Win Slang  " + WinSlang[4]);
        }
        else
        {
            switch (SelectPowerManager.instance.selectedPowerName)
            {
                case AttributeType.Attack:
                    if (isMeSp)
                    {
                        WinText.text = SpecialSlang[0];
                    }
                    else
                    {
                        WinText.text = WinSlang[0];
                    }

                    break;

                case AttributeType.Defense:
                    if (isMeSp)
                    {
                        WinText.text = SpecialSlang[1];
                    }
                    else
                    {
                        WinText.text = WinSlang[1];
                    }

                    break;

                case AttributeType.Disrupt:
                    if (isMeSp)
                    {
                        WinText.text = SpecialSlang[2];
                    }
                    else
                    {
                        WinText.text = WinSlang[2];
                    }

                    break;

                case AttributeType.Heal:
                    if (isMeSp)
                    {
                        WinText.text = SpecialSlang[3];
                    }
                    else
                    {
                        WinText.text = WinSlang[3];
                    }
                    break;
            }
        }

        if (isMeSp)// && !PlayerManager.instance.getSpOpp())
        {
            yield return new WaitForSeconds(0.8f);

            //Debug.Log("Special attack Used-----");

            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Attack)
            {

                DefenseSequence[OppInt].AttribStart.transform.localPosition = DefenseSequence[OppInt].AttribStartPositionOpp;// new Vector3(DefenseSequence[OppInt].AttribStart.transform.localPosition.x, -2, DefenseSequence[OppInt].AttribStart.transform.localPosition.z);


                DefenseSequence[OppInt].AttribStart.transform.localRotation = Quaternion.Euler(DefenseSequence[OppInt].AttribStartRotationOpp);
                DefenseSequence[OppInt].AttribStart.SetActive(true);


                if (DefenseSequence[OppInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = DefenseSequence[OppInt].StartAudio;
                    GetComponent<AudioSource>().Play();
                }

                yield return new WaitForSeconds(1f);
                dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);

                yield return new WaitForSeconds(SpecialSequence[0].AttribStartDelay); //1.4

                if (SpecialSequence[0].AttribStartPositionMe != Vector3.zero)
                    SpecialSequence[0].AttribStart.transform.localPosition = SpecialSequence[0].AttribStartPositionMe;
              //  if (SpecialSequence[0].AttribStartRotationMe != Vector3.zero)
                    SpecialSequence[0].AttribStart.transform.localRotation = Quaternion.Euler(SpecialSequence[0].AttribStartRotationMe);

                SpecialSequence[0].AttribStart.SetActive(true);


                if (SpecialSequence[0].StartAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[0].StartAudio;
                    GetComponent<AudioSource>().Play();


                }

                //Attrib Hit Particle-------------
                yield return new WaitForSeconds(SpecialSequence[0].AttribHitDelay); //0.8

                SpecialSequence[0].AttribStart.SetActive(false);

                if (SpecialSequence[0].AttribHitPositionMe != Vector3.zero)
                    SpecialSequence[0].AttribHit.transform.localPosition = SpecialSequence[0].AttribHitPositionMe; //new Vector3(AttackHit[0].transform.localPosition.x, -2f, AttackHit[0].transform.localPosition.z);

                //if (SpecialSequence[0].AttribHitRotationMe != Vector3.zero)
                    SpecialSequence[0].AttribHit.transform.localRotation = Quaternion.Euler(SpecialSequence[0].AttribHitRotationMe);

                if (SpecialSequence[0].HitAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[0].HitAudio;
                    GetComponent<AudioSource>().Play();
                }


                SpecialSequence[0].AttribHit.SetActive(true);
                DefenseSequence[OppInt].AttribStart.SetActive(false);
                Camera.main.GetComponent<CameraShake>().ShakeCamera(1f);

                //Attrib After Hit Particle
                yield return new WaitForSeconds(SpecialSequence[0].AfterAttribHitDelay);//0.5


                if (SpecialSequence[0].AttribAfterHitPositionMe != Vector3.zero)
                    SpecialSequence[0].AttribAfterHit.transform.localPosition = SpecialSequence[0].AttribAfterHitPositionMe;// new Vector3(AttackAfterHit[0].transform.localPosition.x, -2f, AttackAfterHit[0].transform.localPosition.z);

               // if (SpecialSequence[0].AttribAfterHitRotationMe != Vector3.zero)
                    SpecialSequence[0].AttribAfterHit.transform.localRotation = Quaternion.Euler(SpecialSequence[0].AttribAfterHitRotationMe);

                SpecialSequence[0].AttribAfterHit.SetActive(true);

                if (SpecialSequence[0].AfterHitAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[0].AfterHitAudio;
                    GetComponent<AudioSource>().Play();

                }
               

                Cloud.SetActive(false);


                //Attrib Final Attack Particle----------------
                yield return new WaitForSeconds(SpecialSequence[0].AttribAfterFinalDelay); //0.7

                SpecialSequence[0].AttribAfterHit.SetActive(false);
                SpecialSequence[0].AttribHit.SetActive(false);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);

                if (SpecialSequence[0].AttribAfterFinalPositionMe != Vector3.zero)
                    SpecialSequence[0].AttribAfterFinal.transform.localPosition = SpecialSequence[0].AttribAfterFinalPositionMe; // new Vector3(AttackAfterFinal[0].transform.localPosition.x, -2f, AttackAfterFinal[0].transform.localPosition.z);

               // if (SpecialSequence[0].AttribAfterFinalRotationMe != Vector3.zero)
                    SpecialSequence[0].AttribAfterFinal.transform.localRotation = Quaternion.Euler(SpecialSequence[0].AttribAfterFinalRotationMe);

                SpecialSequence[0].AttribAfterFinal.SetActive(true);


                if (SpecialSequence[0].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[0].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }



                //--------------------------WinAnimation-------------------
                yield return new WaitForSeconds(0.5f);
                PlayerManager.instance.PrevRes = "won";

                PlayerManager.instance.winAnimator.gameObject.SetActive(true);
                PlayerManager.instance.winAnimator.enabled = true;
                GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(2f);
                PlayerManager.instance.winAnimator.gameObject.SetActive(false);
                PlayerManager.instance.winAnimator.enabled = false;

                SpecialSequence[0].AttribAfterFinal.SetActive(false);
                DefenseSequence[OppInt].AttribStart.SetActive(false);

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 15, 13), 0.1f);
                }

                if (PlayerManager.instance.SelectedCard.CurUnlocked || PlayerManager.instance.SelectedEnemyCard.CurUnlocked)
                {
                    yield return new WaitForSeconds(3f);
                }

                yield return new WaitForSeconds(2f);

                MoveToNextRound();

                BattleLobbySC.Instance.SocketMessage("mainturnchange");
                //StartCoroutine(AttackParticle_Fun_DeActive());
            }
            else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Defense)
            {
                yield return new WaitForSeconds(0.8f);


                SpecialSequence[1].AttribStart.transform.localPosition = SpecialSequence[1].AttribStartPositionMe;// new Vector3(DefenseSequence[OppInt].AttribStart.transform.localPosition.x, 3, DefenseSequence[OppInt].AttribStart.transform.localPosition.z);

               // if (SpecialSequence[1].AttribStartRotationMe != Vector3.zero)
                    SpecialSequence[1].AttribStart.transform.localRotation = Quaternion.Euler(SpecialSequence[1].AttribStartRotationMe);

                SpecialSequence[1].AttribStart.SetActive(true);


                if (SpecialSequence[1].StartAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[1].StartAudio;

                    GetComponent<AudioSource>().Play();
                }
               


                yield return new WaitForSeconds(1f);

                dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 17f, 13), 0.01f);
                    Cloud.SetActive(true);
                    Cloud.transform.DOMove(new Vector3(0, 8.5f, 13), 0.7f);
                }


                yield return new WaitForSeconds(AttackSequence[OppInt].AttribStartDelay); //1.4

                if (AttackSequence[OppInt].AttribStartPositionMe != Vector3.zero)
                    AttackSequence[OppInt].AttribStart.transform.localPosition = AttackSequence[OppInt].AttribStartPositionMe;
              //  if (AttackSequence[OppInt].AttribStartRotationMe != Vector3.zero)
                    AttackSequence[OppInt].AttribStart.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribStartRotationMe);

                AttackSequence[OppInt].AttribStart.SetActive(true);

                if (AttackSequence[OppInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].StartAudio;

                    GetComponent<AudioSource>().Play();
                }


                yield return new WaitForSeconds(AttackSequence[OppInt].AttribHitDelay); //0.8

                AttackSequence[OppInt].AttribStart.SetActive(false);

                if (AttackSequence[OppInt].AttribHitPositionMe != Vector3.zero)
                    AttackSequence[OppInt].AttribHit.transform.localPosition = AttackSequence[OppInt].AttribHitPositionMe;
               // if (AttackSequence[OppInt].AttribHitRotationMe != Vector3.zero)
                    AttackSequence[OppInt].AttribHit.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribHitRotationMe);

                //new Vector3(AttackHit[0].transform.localPosition.x, -2f, AttackHit[0].transform.localPosition.z);

                if (AttackSequence[OppInt].HitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].HitAudio;
                    GetComponent<AudioSource>().Play();
                }


                AttackSequence[OppInt].AttribHit.SetActive(true);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(1f);

                yield return new WaitForSeconds(AttackSequence[OppInt].AfterAttribHitDelay);//0.5

                AttackSequence[OppInt].AttribHit.SetActive(false);

                if (AttackSequence[OppInt].AttribAfterHitPositionOpp != Vector3.zero)
                    AttackSequence[OppInt].AttribAfterHit.transform.localPosition = AttackSequence[OppInt].AttribAfterHitPositionOpp;

              //  if (AttackSequence[OppInt].AttribAfterHitRotationOpp != Vector3.zero)
                    AttackSequence[OppInt].AttribAfterHit.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribAfterHitRotationOpp);
                // new Vector3(AttackAfterHit[0].transform.localPosition.x, -2f, AttackAfterHit[0].transform.localPosition.z);

                if (AttackSequence[OppInt].AfterHitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].AfterHitAudio;

                    GetComponent<AudioSource>().Play();
                }
               
                AttackSequence[OppInt].AttribAfterHit.SetActive(true);

                Cloud.SetActive(false);

                yield return new WaitForSeconds(AttackSequence[OppInt].AttribAfterFinalDelay); //0.7

                AttackSequence[OppInt].AttribAfterHit.SetActive(false);
                PlayerManager.instance.winAnimator.gameObject.SetActive(true);
                PlayerManager.instance.winAnimator.enabled = true;
                GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();

                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);
                if (AttackSequence[OppInt].AttribAfterFinal)
                {
                    if (AttackSequence[OppInt].AttribAfterFinalPositionOpp != Vector3.zero)
                        AttackSequence[OppInt].AttribAfterFinal.transform.localPosition = AttackSequence[OppInt].AttribAfterFinalPositionOpp;

                  //  if (AttackSequence[OppInt].AttribAfterFinalRotationOpp != Vector3.zero)
                        AttackSequence[OppInt].AttribAfterFinal.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribAfterFinalRotationOpp);

                    // new Vector3(AttackAfterFinal[0].transform.localPosition.x, -2f, AttackAfterFinal[0].transform.localPosition.z);
                    AttackSequence[OppInt].AttribAfterFinal.SetActive(true);
                }

                if (AttackSequence[OppInt].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }
               

                yield return new WaitForSeconds(0.5f);
                SpecialSequence[1].AttribStart.SetActive(false);
                PlayerManager.instance.winAnimator.gameObject.SetActive(true);
                PlayerManager.instance.winAnimator.enabled = true;
                PlayerManager.instance.PrevRes = "won";
                GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(2f);
                if (AttackSequence[OppInt].AttribAfterFinal)
                    AttackSequence[OppInt].AttribAfterFinal.SetActive(false);

                PlayerManager.instance.winAnimator.gameObject.SetActive(false);
                PlayerManager.instance.winAnimator.enabled = false;



                if (PlayerManager.instance.SelectedCard.CurUnlocked || PlayerManager.instance.SelectedEnemyCard.CurUnlocked)
                {
                    yield return new WaitForSeconds(3f);
                }

                yield return new WaitForSeconds(2f);

                MoveToNextRound();

                if (MeInt == 0)
                    Cloud.transform.DOMove(new Vector3(0, 15, 13), 0.1f);

                BattleLobbySC.Instance.SocketMessage("mainturnchange");
                //StartCoroutine(AttackParticle_Fun_DeActive());
            }
            else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Disrupt)
            {
                WinText.text = SpecialSlang[2];
               
                yield return new WaitForSeconds(1.5f);
                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 2f);
                SpecialSequence[2].AttribStart.transform.localPosition = SpecialSequence[2].AttribStartPositionOpp;

                SpecialSequence[2].AttribStart.SetActive(true);


                if (SpecialSequence[2].StartAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[2].StartAudio;
                    GetComponent<AudioSource>().Play();
                }
               

                yield return new WaitForSeconds(3.5f);

                //Debug.Log("call moveenext");

                Camera.main.GetComponent<CameraShake>().ShakeCamera(2f, 1f);

                yield return new WaitForSeconds(4f);
                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 3f);
               
                PlayerManager.instance.winAnimator.gameObject.SetActive(true);
                PlayerManager.instance.winAnimator.enabled = true;
                GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                if (SpecialSequence[2].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[2].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }

                yield return new WaitForSeconds(2f);
                PlayerManager.instance.PrevRes = "won";
                PlayerManager.instance.winAnimator.gameObject.SetActive(false);
                PlayerManager.instance.winAnimator.enabled = false;
              
                 BattleLobbySC.Instance.SocketMessage("mainturnchange");
                MoveToNextRound();
               
            }

            else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
            {
                WinText.text = SpecialSlang[3];

                if (SpecialSequence[3].AttribStartPositionMe != Vector3.zero)
                    SpecialSequence[3].AttribStart.transform.localPosition = SpecialSequence[3].AttribStartPositionMe;

                SpecialSequence[3].AttribStart.SetActive(true);

                if (SpecialSequence[3].StartAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[3].StartAudio;

                    GetComponent<AudioSource>().Play();
                }
               
                yield return new WaitForSeconds(4f);

                //Debug.Log("call moveenext");
                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);


                yield return new WaitForSeconds(4f);
              
                SpecialSequence[3].AttribStart.SetActive(false);
               
                yield return new WaitForSeconds(3f);
                PlayerManager.instance.winAnimator.gameObject.SetActive(true);
                PlayerManager.instance.winAnimator.enabled = true;
                PlayerManager.instance.PrevRes = "won";
                GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();

                if (SpecialSequence[3].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[3].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }

                yield return new WaitForSeconds(2f);
                PlayerManager.instance.winAnimator.gameObject.SetActive(false);
                PlayerManager.instance.winAnimator.enabled = false;

                BattleLobbySC.Instance.SocketMessage("mainturnchange");

                MoveToNextRound();
            }

        }

        else
        {
            //Debug.Log("Normal AttackParticle_Fun_Active" + SelectPowerManager.instance.selectedPowerName);

            PlayerManager.instance.TextMessage.text = "";


            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Attack)
            {
                //Debug.Log("in attack this conditon of particels");

                yield return new WaitForSeconds(0.8f);

                DefenseSequence[OppInt].AttribStart.transform.localPosition = DefenseSequence[OppInt].AttribStartPositionOpp;

                DefenseSequence[OppInt].AttribStart.transform.localRotation = Quaternion.Euler( DefenseSequence[OppInt].AttribStartRotationOpp);
                              
                DefenseSequence[OppInt].AttribStart.SetActive(true);


                if (DefenseSequence[OppInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = DefenseSequence[OppInt].StartAudio;
                    GetComponent<AudioSource>().Play();
                }


                yield return new WaitForSeconds(1f);

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 17f, 13), 0.01f);
                    Cloud.SetActive(true);
                    Cloud.transform.DOMove(new Vector3(0, 8.5f, 13), 0.7f);
                }
                if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
                {

                 
                }
                else
                {
                    dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                    dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                }
                //Attrib Start Particle-----------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribStartDelay); //1.4

                if (AttackSequence[MeInt].AttribStartPositionMe != Vector3.zero)
                    AttackSequence[MeInt].AttribStart.transform.localPosition = AttackSequence[MeInt].AttribStartPositionMe;
               // if (AttackSequence[MeInt].AttribStartRotationMe != Vector3.zero)
                    AttackSequence[MeInt].AttribStart.transform.localRotation = Quaternion.Euler( AttackSequence[MeInt].AttribStartRotationMe);

              
                AttackSequence[MeInt].AttribStart.SetActive(true);

                if (AttackSequence[MeInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].StartAudio;
                    GetComponent<AudioSource>().Play();
                }
               


                //Attrib Hit Particle-------------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribHitDelay); //0.8

                AttackSequence[MeInt].AttribStart.SetActive(false);


                if (AttackSequence[MeInt].AttribHitPositionMe != Vector3.zero)
                    AttackSequence[MeInt].AttribHit.transform.localPosition = AttackSequence[MeInt].AttribHitPositionMe; //new Vector3(AttackHit[0].transform.localPosition.x, -2f, AttackHit[0].transform.localPosition.z);

               //if (AttackSequence[MeInt].AttribHitRotationMe != Vector3.zero)
                    AttackSequence[MeInt].AttribHit.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribHitRotationMe);


                if (AttackSequence[MeInt].HitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].HitAudio;
                    GetComponent<AudioSource>().Play();
                }


                AttackSequence[MeInt].AttribHit.SetActive(true);
                DefenseSequence[OppInt].AttribStart.SetActive(false);
                Camera.main.GetComponent<CameraShake>().ShakeCamera(1f);

                //Attrib After Hit Particle
                yield return new WaitForSeconds(AttackSequence[MeInt].AfterAttribHitDelay);//0.5

                AttackSequence[MeInt].AttribHit.SetActive(false);

                if (AttackSequence[MeInt].AttribAfterHitPositionOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribAfterHit.transform.localPosition = AttackSequence[MeInt].AttribAfterHitPositionOpp;// new Vector3(AttackAfterHit[0].transform.localPosition.x, -2f, AttackAfterHit[0].transform.localPosition.z);
               // if (AttackSequence[MeInt].AttribAfterHitRotationOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribAfterHit.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribAfterHitRotationOpp);

                //Debug.Log(AttackSequence[MeInt].AttribHit.transform.localPosition);
                //Debug.Log(AttackSequence[MeInt].AttribHit.transform.localRotation);
                AttackSequence[MeInt].AttribAfterHit.SetActive(true);

                if (AttackSequence[MeInt].AfterHitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].AfterHitAudio;
                    GetComponent<AudioSource>().Play();
                }
               
                Cloud.SetActive(false);


                //Attrib Final Attack Particle----------------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribAfterFinalDelay); //0.7

                AttackSequence[MeInt].AttribAfterHit.SetActive(false);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);

                if (AttackSequence[MeInt].AttribAfterFinal)
                {
                    if (AttackSequence[MeInt].AttribAfterFinalPositionOpp != Vector3.zero)
                        AttackSequence[MeInt].AttribAfterFinal.transform.localPosition = AttackSequence[MeInt].AttribAfterFinalPositionOpp; // new Vector3(AttackAfterFinal[0].transform.localPosition.x, -2f, AttackAfterFinal[0].transform.localPosition.z);
                  //  if (AttackSequence[MeInt].AttribAfterFinalRotationOpp != Vector3.zero)
                        AttackSequence[MeInt].AttribAfterFinal.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribAfterFinalRotationOpp);

                    AttackSequence[MeInt].AttribAfterFinal.SetActive(true);
                }

                if (AttackSequence[MeInt].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }



                //--------------------------WinAnimation-------------------
                yield return new WaitForSeconds(0.5f);
                PlayerManager.instance.PrevRes = "won";
                PlayerManager.instance.winAnimator.gameObject.SetActive(true);
                PlayerManager.instance.winAnimator.enabled = true;
                GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(2f);
                PlayerManager.instance.winAnimator.gameObject.SetActive(false);
                PlayerManager.instance.winAnimator.enabled = false;

                if (AttackSequence[MeInt].AttribAfterFinal)
                    AttackSequence[MeInt].AttribAfterFinal.SetActive(false);
               

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 15, 13), 0.1f);
                }

                if (PlayerManager.instance.SelectedCard.CurUnlocked || PlayerManager.instance.SelectedEnemyCard.CurUnlocked)
                {
                    yield return new WaitForSeconds(3f);
                }

                yield return new WaitForSeconds(2f);

                MoveToNextRound();

                BattleLobbySC.Instance.SocketMessage("mainturnchange");
                //StartCoroutine(AttackParticle_Fun_DeActive());

            }

            else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Defense)
            {
               
                 yield return new WaitForSeconds(0.8f);


                DefenseSequence[MeInt].AttribStart.transform.localPosition = DefenseSequence[MeInt].AttribStartPositionMe;// new Vector3(DefenseSequence[OppInt].AttribStart.transform.localPosition.x, 3, DefenseSequence[OppInt].AttribStart.transform.localPosition.z);

                DefenseSequence[MeInt].AttribStart.transform.localRotation = Quaternion.Euler(DefenseSequence[MeInt].AttribStartRotationMe);

                DefenseSequence[MeInt].AttribStart.SetActive(true);

                if (DefenseSequence[MeInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = DefenseSequence[MeInt].StartAudio;
                    GetComponent<AudioSource>().Play();
                }
               
                yield return new WaitForSeconds(1f);

                if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
                {


                }
                else
                {
                    dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                    dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                }

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 17f, 13), 0.01f);
                    Cloud.SetActive(true);
                    Cloud.transform.DOMove(new Vector3(0, 8.5f, 13), 0.7f);
                }


                yield return new WaitForSeconds(AttackSequence[OppInt].AttribStartDelay); //1.4

                if (AttackSequence[OppInt].AttribStartPositionMe != Vector3.zero)
                    AttackSequence[OppInt].AttribStart.transform.localPosition = AttackSequence[OppInt].AttribStartPositionMe;
               // if (AttackSequence[OppInt].AttribStartRotationMe != Vector3.zero)
                    AttackSequence[OppInt].AttribStart.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribStartRotationMe);

                AttackSequence[OppInt].AttribStart.SetActive(true);

                if (AttackSequence[OppInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].StartAudio;
                    GetComponent<AudioSource>().Play();
                }
               

                yield return new WaitForSeconds(AttackSequence[OppInt].AttribHitDelay); //0.8

                AttackSequence[OppInt].AttribStart.SetActive(false);

                if (AttackSequence[OppInt].AttribHitPositionMe != Vector3.zero)
                    AttackSequence[OppInt].AttribHit.transform.localPosition = AttackSequence[OppInt].AttribHitPositionMe;
               // if (AttackSequence[OppInt].AttribHitRotationMe   != Vector3.zero)
                    AttackSequence[OppInt].AttribHit.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribHitRotationMe);

                //new Vector3(AttackHit[0].transform.localPosition.x, -2f, AttackHit[0].transform.localPosition.z);

                if (AttackSequence[OppInt].HitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].HitAudio;
                    GetComponent<AudioSource>().Play();
                }


                AttackSequence[OppInt].AttribHit.SetActive(true);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(1f);

                yield return new WaitForSeconds(AttackSequence[OppInt].AfterAttribHitDelay);//0.5

                AttackSequence[OppInt].AttribHit.SetActive(false);

                if (AttackSequence[OppInt].AttribAfterHitPositionOpp != Vector3.zero)
                    AttackSequence[OppInt].AttribAfterHit.transform.localPosition = AttackSequence[OppInt].AttribAfterHitPositionOpp;

                //if (AttackSequence[OppInt].AttribAfterHitRotationOpp != Vector3.zero)
                    AttackSequence[OppInt].AttribAfterHit.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribAfterHitRotationOpp);
                // new Vector3(AttackAfterHit[0].transform.localPosition.x, -2f, AttackAfterHit[0].transform.localPosition.z);


                if (AttackSequence[OppInt].AfterHitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].AfterHitAudio;
                    GetComponent<AudioSource>().Play();
                }
               
                AttackSequence[OppInt].AttribAfterHit.SetActive(true);

                Cloud.SetActive(false);

                yield return new WaitForSeconds(AttackSequence[OppInt].AttribAfterFinalDelay); //0.7

                AttackSequence[OppInt].AttribAfterHit.SetActive(false);
               


                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);
                if (AttackSequence[OppInt].AttribAfterFinal)
                {
                    if (AttackSequence[OppInt].AttribAfterFinalPositionOpp != Vector3.zero)
                        AttackSequence[OppInt].AttribAfterFinal.transform.localPosition = AttackSequence[OppInt].AttribAfterFinalPositionOpp;

                  //  if (AttackSequence[OppInt].AttribAfterFinalRotationOpp != Vector3.zero)
                        AttackSequence[OppInt].AttribAfterFinal.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribAfterFinalRotationOpp);

                    // new Vector3(AttackAfterFinal[0].transform.localPosition.x, -2f, AttackAfterFinal[0].transform.localPosition.z);
                    AttackSequence[OppInt].AttribAfterFinal.SetActive(true);
                }
                DefenseSequence[MeInt].AttribStart.SetActive(false);


                if (AttackSequence[OppInt].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }


                yield return new WaitForSeconds(0.5f);

                PlayerManager.instance.winAnimator.gameObject.SetActive(true);
                PlayerManager.instance.winAnimator.enabled = true;
                PlayerManager.instance.PrevRes = "won";
                GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(2f);
                if (AttackSequence[OppInt].AttribAfterFinal)
                    AttackSequence[OppInt].AttribAfterFinal.SetActive(false);

                PlayerManager.instance.winAnimator.gameObject.SetActive(false);
                PlayerManager.instance.winAnimator.enabled = false;



                if (PlayerManager.instance.SelectedCard.CurUnlocked || PlayerManager.instance.SelectedEnemyCard.CurUnlocked)
                {
                    yield return new WaitForSeconds(3f);
                }

                yield return new WaitForSeconds(2f);

                MoveToNextRound();

                if (MeInt == 0)
                    Cloud.transform.DOMove(new Vector3(0, 15, 13), 0.1f);

                BattleLobbySC.Instance.SocketMessage("mainturnchange");
                //StartCoroutine(AttackParticle_Fun_DeActive());

            }

            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
            {
                StartCoroutine(HealSuccessfull(MeInt,OppInt, true));
              
              /*  InGameCards.instance.PauseCountTimer();
                PlayerManager.instance.TextMessage.text = "";
                PlayerManager.instance.CanClick = false;
                HealSequence[MeInt].AttribAfterFinal.SetActive(true);

                yield return new WaitForSeconds(4f);

                //Debug.Log("call moveenext");
                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);


                yield return new WaitForSeconds(4f);

                DisruptSequence[OppInt].AttribHit.SetActive(false);
                HealSequence[MeInt].AttribStart.SetActive(false);
                HealSequence[MeInt].AttribHit.SetActive(false);
                HealSequence[MeInt].AttribAfterHit.SetActive(false);

                yield return new WaitForSeconds(3f);
                PlayerManager.instance.winAnimator.gameObject.SetActive(true);
                PlayerManager.instance.winAnimator.enabled = true;
                PlayerManager.instance.PrevRes = "won";
                yield return new WaitForSeconds(4f);
                PlayerManager.instance.winAnimator.gameObject.SetActive(false);
                PlayerManager.instance.winAnimator.enabled = false;

                BattleLobbySC.Instance.SocketMessage("mainturnchange");

                MoveToNextRound();
                */               
            }
            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Disrupt)
            {
                StartCoroutine(DisruptAnimStart(MeInt,OppInt,true));
            }
        }
    }


    public IEnumerator HealAnimStart(int diceNum,bool isme) //Start Heal Particle Once Player ROll the DIce
    {

        int Meval = 0;
       

        if (diceNum == 3 || diceNum == 4)
        {
            Meval = 1;
        }
        else if (diceNum == 5 || diceNum == 6)
        {
            Meval = 2;
        }
        yield return new WaitForSeconds(0.1f);

        if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal) 
        {

            HealSequence[Meval].AttribStart.transform.localPosition = HealSequence[Meval].AttribStartPositionOpp;

           // if (HealSequence[Meval].AttribStartRotationOpp!= Vector3.zero)
           // {
                HealSequence[Meval].AttribStart.transform.localRotation =Quaternion.Euler(HealSequence[Meval].AttribStartRotationOpp);
           // }

            HealSequence[Meval].AttribStart.SetActive(true);


            yield return new WaitForSeconds(.5f);

            HealSequence[Meval].AttribHit.transform.localPosition = HealSequence[Meval].AttribHitPositionOpp;

           // if (HealSequence[Meval].AttribHitRotationOpp != Vector3.zero)
           // {
                HealSequence[Meval].AttribHit.transform.localRotation = Quaternion.Euler(HealSequence[Meval].AttribHitRotationOpp);
           // }

            HealSequence[Meval].AttribHit.SetActive(true);

            if (HealSequence[Meval].StartAudio)
            {
                GetComponent<AudioSource>().clip = HealSequence[Meval].StartAudio;
                GetComponent<AudioSource>().Play();
            }

        }

        else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {
            HealSequence[Meval].AttribStart.transform.localPosition = HealSequence[Meval].AttribStartPositionMe;

           // if (HealSequence[Meval].AttribStartRotationMe != Vector3.zero)
           // {
                HealSequence[Meval].AttribStart.transform.localRotation = Quaternion.Euler(HealSequence[Meval].AttribStartRotationMe);
          //  }

            HealSequence[Meval].AttribStart.SetActive(true);

            yield return new WaitForSeconds(.5f);
            HealSequence[Meval].AttribHit.transform.localPosition = HealSequence[Meval].AttribHitPositionMe;

          //  if (HealSequence[Meval].AttribHitRotationMe != Vector3.zero)
          //  {
                HealSequence[Meval].AttribHit.transform.localRotation = Quaternion.Euler(HealSequence[Meval].AttribHitRotationMe);
          //  }

            HealSequence[Meval].AttribHit.SetActive(true);

            if (HealSequence[Meval].StartAudio)
            {
                GetComponent<AudioSource>().clip = HealSequence[Meval].StartAudio;
                GetComponent<AudioSource>().Play();
            }

        }
    }


    public IEnumerator HealSuccessfull(int DiceNum,int OppDiceNum, bool isme)
    {
       

        InGameCards.instance.PauseCountTimer();
        PlayerManager.instance.TextMessage.text = "";
        PlayerManager.instance.CanClick = false;

        if (isme)
        {
            WinText.text = WinSlang[3];

            if (HealSequence[DiceNum].AttribAfterFinalPositionMe != Vector3.zero)
                HealSequence[DiceNum].AttribAfterFinal.transform.localPosition = HealSequence[DiceNum].AttribAfterFinalPositionMe;

           // if (HealSequence[DiceNum].AttribAfterFinalRotationMe != Vector3.zero)
                HealSequence[DiceNum].AttribAfterFinal.transform.localRotation = Quaternion.Euler(HealSequence[DiceNum].AttribAfterFinalRotationMe);


            HealSequence[DiceNum].AttribAfterFinal.SetActive(true);

            if (HealSequence[DiceNum].FinalAudio)
            {
                GetComponent<AudioSource>().clip = HealSequence[DiceNum].FinalAudio;
                GetComponent<AudioSource>().Play();
            }

            Camera.main.GetComponent<CameraShake>().ShakeCamera(1f, 2f);
            yield return new WaitForSeconds(4f);

            Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);                       

            HealSequence[DiceNum].AttribStart.SetActive(false);
            HealSequence[DiceNum].AttribHit.SetActive(false);
            HealSequence[DiceNum].AttribAfterFinal.SetActive(false);
            PlayerManager.instance.winAnimator.gameObject.SetActive(true);
            PlayerManager.instance.winAnimator.enabled = true;
            GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
            GetComponent<AudioSource>().Play();
            PlayerManager.instance.PrevRes = "won";
            yield return new WaitForSeconds(2f);
            PlayerManager.instance.winAnimator.gameObject.SetActive(false);
            PlayerManager.instance.winAnimator.enabled = false;
            yield return new WaitForSeconds(3f);
            BattleLobbySC.Instance.SocketMessage("mainturnchange");
        }
        else
        {
            LostText.text = LostSlang[2];

            if (HealSequence[OppDiceNum].AttribAfterFinalPositionOpp != Vector3.zero)
                HealSequence[OppDiceNum].AttribAfterFinal.transform.localPosition = HealSequence[OppDiceNum].AttribAfterFinalPositionOpp;

            //if (HealSequence[OppDiceNum].AttribAfterFinalRotationOpp != Vector3.zero)
                HealSequence[OppDiceNum].AttribAfterFinal.transform.localRotation = Quaternion.Euler(HealSequence[OppDiceNum].AttribAfterFinalRotationOpp);

            HealSequence[OppDiceNum].AttribAfterFinal.SetActive(true);
            Camera.main.GetComponent<CameraShake>().ShakeCamera(1f, 2f);

            if (HealSequence[OppDiceNum].FinalAudio)
            {
                GetComponent<AudioSource>().clip = HealSequence[OppDiceNum].FinalAudio;
                GetComponent<AudioSource>().Play();
            }
           
            yield return new WaitForSeconds(4f);

            Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);
                  
            HealSequence[OppDiceNum].AttribStart.SetActive(false);
            HealSequence[OppDiceNum].AttribHit.SetActive(false);
            HealSequence[OppDiceNum].AttribAfterFinal.SetActive(false);
            PlayerManager.instance.LostAnimator.gameObject.SetActive(true);
            GetComponent<AudioSource>().clip = lostSound[UnityEngine.Random.Range(0, 3)];
            GetComponent<AudioSource>().Play();
            PlayerManager.instance.PrevRes = "lost";
            yield return new WaitForSeconds(2f);
            PlayerManager.instance.LostAnimator.gameObject.SetActive(false);
            yield return new WaitForSeconds(3f);
        }

      
        MoveToNextRound();             
    }


    public IEnumerator DisruptAnimStart(int DiceNum,int OppDiceNUm,bool IsMe)
    {
       

        InGameCards.instance.PauseCountTimer();

        PlayerManager.instance.CanClick = false;

        yield return new WaitForSeconds(0.1f);

        if (IsMe)
        {
            WinText.text = WinSlang[2];

            if (DisruptSequence[DiceNum].AttribStartPositionMe != Vector3.zero)
                DisruptSequence[DiceNum].AttribStart.transform.localPosition = DisruptSequence[DiceNum].AttribStartPositionMe;

            //if (DisruptSequence[DiceNum].AttribStartRotationMe != Vector3.zero)
                DisruptSequence[DiceNum].AttribStart.transform.localRotation = Quaternion.Euler(DisruptSequence[DiceNum].AttribStartRotationMe);
           
            DisruptSequence[DiceNum].AttribStart.SetActive(true);

            if (DisruptSequence[DiceNum].StartAudio)
            {
                GetComponent<AudioSource>().clip = DisruptSequence[DiceNum].StartAudio;
                GetComponent<AudioSource>().Play();
            }



            yield return new WaitForSeconds(DisruptSequence[DiceNum].AttribHitDelay);

            Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 2f);

            HealSequence[OppDiceNUm].AttribStart.SetActive(false);
            HealSequence[OppDiceNUm].AttribHit.SetActive(false);

            DisruptSequence[DiceNum].AttribStart.SetActive(false);

            if (DisruptSequence[DiceNum].AttribHitPositionOpp != Vector3.zero)
                DisruptSequence[DiceNum].AttribHit.transform.localPosition = DisruptSequence[DiceNum].AttribHitPositionOpp;

           // if (DisruptSequence[DiceNum].AttribHitRotationOpp != Vector3.zero)
                DisruptSequence[DiceNum].AttribHit.transform.localRotation = Quaternion.Euler(DisruptSequence[DiceNum].AttribHitRotationOpp);

            DisruptSequence[DiceNum].AttribHit.SetActive(true);

            if (DisruptSequence[DiceNum].HitAudio)
            {
                GetComponent<AudioSource>().clip = DisruptSequence[DiceNum].HitAudio;
                GetComponent<AudioSource>().Play();
            }

           

            yield return new WaitForSeconds(3.5f);

            //Debug.Log("call moveenext");

            Camera.main.GetComponent<CameraShake>().ShakeCamera(2f, 1f);

            yield return new WaitForSeconds(4f);
            Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 3f);
            DisruptSequence[DiceNum].AttribHit.SetActive(false);

            if (DisruptSequence[DiceNum].FinalAudio)
            {
                GetComponent<AudioSource>().clip = DisruptSequence[DiceNum].FinalAudio;
                GetComponent<AudioSource>().Play();
            }

            PlayerManager.instance.winAnimator.gameObject.SetActive(true);
            PlayerManager.instance.winAnimator.enabled = true;
            PlayerManager.instance.PrevRes = "won";
            GetComponent<AudioSource>().clip = winSound[UnityEngine.Random.Range(0, 3)];
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(3f);

            PlayerManager.instance.winAnimator.gameObject.SetActive(false);
            PlayerManager.instance.winAnimator.enabled = false;

            BattleLobbySC.Instance.SocketMessage("mainturnchange");


            //Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);
            PlayerManager.instance.SelectedEnemyCard.ApplyDamage(1);

            MoveToNextRound();
            //StartCoroutine(AttackParticle_Fun_DeActive());
        }
       else
        {
            LostText.text = LostSlang[3];

            if (DisruptSequence[OppDiceNUm].AttribStartPositionOpp != Vector3.zero)
                DisruptSequence[OppDiceNUm].AttribStart.transform.localPosition = DisruptSequence[OppDiceNUm].AttribStartPositionOpp;

           // if (DisruptSequence[OppDiceNUm].AttribStartRotationMe != Vector3.zero)
                DisruptSequence[OppDiceNUm].AttribStart.transform.localRotation = Quaternion.Euler(DisruptSequence[OppDiceNUm].AttribStartRotationOpp);

            DisruptSequence[OppDiceNUm].AttribStart.SetActive(true);

            if (DisruptSequence[OppDiceNUm].StartAudio)
            {
                GetComponent<AudioSource>().clip = DisruptSequence[OppDiceNUm].StartAudio;
                GetComponent<AudioSource>().Play();
            }

            yield return new WaitForSeconds(DisruptSequence[OppDiceNUm].AttribHitDelay);

            Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 2f);

            HealSequence[DiceNum].AttribStart.SetActive(false);
            HealSequence[DiceNum].AttribHit.SetActive(false);

            DisruptSequence[OppDiceNUm].AttribStart.SetActive(false);

            if (DisruptSequence[OppDiceNUm].AttribHitPositionMe != Vector3.zero)
                DisruptSequence[OppDiceNUm].AttribHit.transform.localPosition = DisruptSequence[OppDiceNUm].AttribHitPositionMe;

           // if (DisruptSequence[OppDiceNUm].AttribHitRotationMe != Vector3.zero)
                DisruptSequence[OppDiceNUm].AttribHit.transform.localRotation = Quaternion.Euler(DisruptSequence[OppDiceNUm].AttribHitRotationMe);

            DisruptSequence[OppDiceNUm].AttribHit.SetActive(true);

            if (DisruptSequence[OppDiceNUm].HitAudio)
            {
                GetComponent<AudioSource>().clip = DisruptSequence[OppDiceNUm].HitAudio;
                GetComponent<AudioSource>().Play();
            }

            yield return new WaitForSeconds(3.5f);

            DisruptSequence[OppDiceNUm].AttribHit.SetActive(false);

            Camera.main.GetComponent<CameraShake>().ShakeCamera(2f, 1f);

            yield return new WaitForSeconds(4f);

            Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 3f);

            if (DisruptSequence[OppDiceNUm].FinalAudio)
            {
                GetComponent<AudioSource>().clip = DisruptSequence[OppDiceNUm].FinalAudio;
                GetComponent<AudioSource>().Play();
            }

            PlayerManager.instance.LostAnimator.gameObject.SetActive(true);

            PlayerManager.instance.PrevRes = "lost";
            GetComponent<AudioSource>().clip = lostSound[UnityEngine.Random.Range(0, 3)];
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(3f);
            PlayerManager.instance.LostAnimator.gameObject.SetActive(false);

            //Debug.Log(PlayerManager.instance.SelectedCard.CardDetail.name);
            PlayerManager.instance.SelectedCard.ApplyDamage(1);

            MoveToNextRound();         
           
            //StartCoroutine(AttackParticle_Fun_DeActive());          
        }


    }

    public IEnumerator TieTUrnChange(bool isme)
    {
        InGameCards.instance.TieTurnChange();
        PlayerManager.instance.DisableParticle();
        PlayerManager.instance.PrevRes = "tie";

        //Debug.Log("Tie dice Rolled From here" + isme);

        if(PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {
            dice.DOScale(new Vector3(0.7f, 0.7f, 0.7f), 0.5f);
            dice2.DOScale(new Vector3(0.7f, 0.7f, 0.7f), 0.5f);
        }
        else
        {
            dice.DOScale(new Vector3(0.38f, 0.38f, 0.38f), 0.5f);
            dice2.DOScale(new Vector3(0.38f, 0.38f, 0.38f), 0.5f);

        }
        

        if (isme)
        {

            PlayerManager.instance.TextCanvas.SetActive(true);
            PlayerManager.instance.TextMessage.text = "Your Turn";
            yield return new WaitForSeconds(1.5f);
            PlayerManager.instance.CanClick = true;
            PlayerManager.instance.CanClickOnBG = false;
            PlayerManager.instance.CanClickOnMeCard = false;
            PlayerManager.instance.CanClickonMeDice = true;
            PlayerManager.instance.CanClickonEnemyDice = false;
            PlayerManager.instance.CanClickOnEnemyCard = false;
            PlayerManager.instance.CanClickonEnemyAttrib = false;
            PlayerManager.instance.CanClickOnMeAttrib = false;
            PlayerManager.instance.TextCanvas.SetActive(true);
            PlayerManager.instance.TextMessage.text = "Please Roll Dice Again";

            //Debug.Log("My name on left "+ BattleLobbySC.Instance.Finaldata.user_data.player_name);
            //Debug.Log("My name on Right " + BattleLobbySC.Instance.Finaldata.opp_user_data.player_name);

            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.transform.DOScale(0.1f, 0.5f);
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.transform.DOScale(0.1f, 0.5f);

            

            yield return new WaitForSeconds(1.5f);
            PlayerManager.instance.TextCanvas.SetActive(false);
        }
        else
        {
            PlayerManager.instance.TextCanvas.SetActive(true);
            PlayerManager.instance.CanClick = false;
            PlayerManager.instance.CanClickOnBG = false;
            PlayerManager.instance.CanClickOnMeCard = false;
            PlayerManager.instance.CanClickonMeDice = false;
            PlayerManager.instance.CanClickonEnemyDice = false;
            PlayerManager.instance.CanClickOnEnemyCard = false;
            PlayerManager.instance.CanClickonEnemyAttrib = false;
            PlayerManager.instance.CanClickOnMeAttrib = false;
            PlayerManager.instance.TextMessage.text = "Opponent Turn";

            //Debug.Log("My name on Right " + BattleLobbySC.Instance.Finaldata.user_data.player_name);
            //Debug.Log("My name on Left " + BattleLobbySC.Instance.Finaldata.opp_user_data.player_name);

            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().leftName.transform.DOScale(0.1f, 0.5f);
            PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().rightName.transform.DOScale(0.1f, 0.5f);

            

            BattleLobbySC.Instance.PlayerTwoAnim();
            yield return new WaitForSeconds(1.5f);
            PlayerManager.instance.TextCanvas.SetActive(false);
        }

    }

    public IEnumerator TieMatchFun_Active()
    {

        InGameCards.instance.PauseCountTimer();

         //HealStart[0].SetActive(false);
        //HealHit[0].SetActive(false);
       //DisruptStart[0].SetActive(false);

        yield return new WaitForSeconds(0.5f);
        PlayerManager.instance.TieObj.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        PlayerManager.instance.TieObj.SetActive(false);
        PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().StartSkirmishMode();
        yield return new WaitForSeconds(3f);
        dice.DOScale(new Vector3(0.38f, 0.38f, 0.38f), 0.5f);
        dice2.DOScale(new Vector3(0.38f, 0.38f, 0.38f), 0.5f);

        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Attack || SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
        {
                      
            StartCoroutine(TieTUrnChange(true));
        }
        else
        {
            StartCoroutine(TieTUrnChange(false));
        }
             
    }


    public IEnumerator LostParticle_Fun_Active(int MeInt,int OppInt, bool isMeSp, bool isOppSp)
    {
        Debug.Log("Lost partical funactive ");

        if (PlayerManager.instance.SelectedCard != null)
        { 
            for (int i = 0; i < PlayerManager.instance.SelectedCard.CardDetail.elementeum; i++)
            {

                PlayerManager.instance.elementeumParticle[i].StartPosition = Vector3.zero;
                PlayerManager.instance.elementeumParticle[i].gameObject.SetActive(false);
            }
        }
        if (PlayerManager.instance.SelectedEnemyCard != null)
        {
            for (int i = 0; i < PlayerManager.instance.SelectedEnemyCard.CardDetail.elementeum; i++)
            {

                PlayerManager.instance.elementeumParticleEnemy[i].StartPosition = Vector3.zero;
                PlayerManager.instance.elementeumParticleEnemy[i].gameObject.SetActive(false);
            }
        }

        if (SelectPowerManager.instance.selectedPowerName == AttributeType.Disrupt)
        {
            int CardInt = 0;

            for (int i = 0; i < 3; i++)
            {
                if (PlayerManager.instance.PlayerCardsObj[i].DestroyedCard)
                {
                    CardInt += 1;
                }
            }
            if (CardInt < 2)
            {
                if (PlayerManager.instance.SelectedCard != null)
                {

                    PlayerManager.instance.SelectedCard.SetDisableTemp();
                }
                else
                {
                    PlayerManager.instance.PlayerCardsObj[0].SetDisableTemp();
                }
            }
        }

        if(SelectPowerManager.instance.selectedEnemyPowerName== AttributeType.Disrupt)
        {

            int CardInt = 0;

            for (int i = 0; i < 3; i++)
            {
                if (PlayerManager.instance.enemyCardsObj[i].DestroyedCard)
                {
                    CardInt += 1;
                }
            }
            if (CardInt < 2)
            {
                PlayerManager.instance.SelectedEnemyCard.SetDisableTemp();
            }
               
        }

        PlayerManager.instance.TextMessage.text = "";
        InGameCards.instance.PauseCountTimer();
        PlayerManager.instance.CanClick = false;
        if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
        {

            LostText.text = LostSlang[4];
        }
        else
        {


            switch (SelectPowerManager.instance.selectedPowerName)
            {
                case AttributeType.Attack:

                    LostText.text = LostSlang[0];
                    break;

                case AttributeType.Defense:
                    LostText.text = LostSlang[1];
                    break;

                case AttributeType.Disrupt:
                    LostText.text = LostSlang[2];
                    break;

                case AttributeType.Heal:
                    LostText.text = LostSlang[3];
                    break;
            }
        }

        if (isOppSp)
        {
            switch (SelectPowerManager.instance.selectedEnemyPowerName)
            {
                case AttributeType.Attack:
                    LostText.text = SpecialSlang[0];
                    break;

                case AttributeType.Defense:
                    LostText.text = SpecialSlang[1];                   
                    break;

                case AttributeType.Disrupt:
                    LostText.text = SpecialSlang[2];                  
                    break;

                case AttributeType.Heal:
                    LostText.text = SpecialSlang[3];                   
                    break;
            }
            yield return new WaitForSeconds(0.8f);

            //Debug.Log("Special attack Used-----");
            if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Attack)
            {
                //Debug.Log("Special Attack");

               
                DefenseSequence[MeInt].AttribStart.transform.localPosition = DefenseSequence[MeInt].AttribStartPositionMe;// new Vector3(DefenseSequence[OppInt].AttribStart.transform.localPosition.x, -2, DefenseSequence[OppInt].AttribStart.transform.localPosition.z);

                DefenseSequence[MeInt].AttribStart.transform.localRotation = Quaternion.Euler(DefenseSequence[MeInt].AttribStartRotationMe);

                DefenseSequence[MeInt].AttribStart.SetActive(true);

                yield return new WaitForSeconds(1f);
                dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);

                yield return new WaitForSeconds(SpecialSequence[0].AttribStartDelay); //1.4

                if (SpecialSequence[0].AttribStartPositionOpp != Vector3.zero)
                    SpecialSequence[0].AttribStart.transform.localPosition = SpecialSequence[0].AttribStartPositionOpp;

              //  if (SpecialSequence[0].AttribStartPositionOpp != Vector3.zero)
                    SpecialSequence[0].AttribStart.transform.localRotation = Quaternion.Euler( SpecialSequence[0].AttribStartRotationOpp);

                SpecialSequence[0].AttribStart.SetActive(true);

                if (SpecialSequence[0].StartAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[0].StartAudio;
                    GetComponent<AudioSource>().Play();
                }



                //Attrib Hit Particle-------------
                yield return new WaitForSeconds(SpecialSequence[0].AttribHitDelay); //0.8

                SpecialSequence[0].AttribStart.SetActive(false);

                if (SpecialSequence[0].AttribHitPositionOpp != Vector3.zero)
                    SpecialSequence[0].AttribHit.transform.localPosition = SpecialSequence[0].AttribHitPositionOpp; //new Vector3(AttackHit[0].transform.localPosition.x, -2f, AttackHit[0].transform.localPosition.z);

               // if (SpecialSequence[0].AttribAfterHitRotationOpp != Vector3.zero)
                    SpecialSequence[0].AttribHit.transform.localRotation = Quaternion.Euler(SpecialSequence[0].AttribAfterHitRotationOpp);

                if (SpecialSequence[0].HitAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[0].HitAudio;
                    GetComponent<AudioSource>().Play();
                }

                SpecialSequence[0].AttribHit.SetActive(true);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(1f);

                //Attrib After Hit Particle
                yield return new WaitForSeconds(SpecialSequence[0].AfterAttribHitDelay);//0.5


              
                if (SpecialSequence[0].AttribAfterHitPositionOpp != Vector3.zero)
                    SpecialSequence[0].AttribAfterHit.transform.localPosition = SpecialSequence[0].AttribAfterHitPositionOpp;// new Vector3(AttackAfterHit[0].transform.localPosition.x, -2f, AttackAfterHit[0].transform.localPosition.z);

              //  if (SpecialSequence[0].AttribAfterHitRotationOpp != Vector3.zero)
                    SpecialSequence[0].AttribAfterHit.transform.localRotation = Quaternion.Euler(SpecialSequence[0].AttribAfterHitRotationOpp);

                if (SpecialSequence[0].AfterHitAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[0].AfterHitAudio;
                    GetComponent<AudioSource>().Play();
                }

                SpecialSequence[0].AttribAfterHit.SetActive(true);

                Cloud.SetActive(false);

                DefenseSequence[OppInt].AttribStart.SetActive(false);
                //Attrib Final Attack Particle----------------
                yield return new WaitForSeconds(SpecialSequence[0].AttribAfterFinalDelay); //0.7

                SpecialSequence[0].AttribAfterHit.SetActive(false);
                SpecialSequence[0].AttribHit.SetActive(false);
                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);

                if (SpecialSequence[0].AttribAfterFinalPositionOpp != Vector3.zero)
                    SpecialSequence[0].AttribAfterFinal.transform.localPosition = SpecialSequence[0].AttribAfterFinalPositionOpp; // new Vector3(AttackAfterFinal[0].transform.localPosition.x, -2f, AttackAfterFinal[0].transform.localPosition.z);

              //  if (SpecialSequence[0].AttribAfterFinalRotationOpp != Vector3.zero)
                    SpecialSequence[0].AttribAfterFinal.transform.localRotation = Quaternion.Euler(SpecialSequence[0].AttribAfterFinalRotationOpp);

                SpecialSequence[0].AttribAfterFinal.SetActive(true);


                if (SpecialSequence[0].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[0].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }
               


                //--------------------------Lost Animation-------------------
                yield return new WaitForSeconds(0.5f);
                PlayerManager.instance.PrevRes = "lost";
                PlayerManager.instance.LostAnimator.gameObject.SetActive(true);
                GetComponent<AudioSource>().clip = lostSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();

                yield return new WaitForSeconds(2f);
                PlayerManager.instance.LostAnimator.gameObject.SetActive(false);
               

                SpecialSequence[0].AttribAfterFinal.SetActive(false);
                DefenseSequence[OppInt].AttribStart.SetActive(false);

               
                if (PlayerManager.instance.SelectedCard.CurUnlocked || PlayerManager.instance.SelectedEnemyCard.CurUnlocked)
                {
                    yield return new WaitForSeconds(3f);
                }

                yield return new WaitForSeconds(2f);

                MoveToNextRound();

              
                //StartCoroutine(AttackParticle_Fun_DeActive());
            }
            else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Defense)
            {

                yield return new WaitForSeconds(0.8f);
                //Debug.Log("Special Defense");

                SpecialSequence[1].AttribStart.transform.localPosition = SpecialSequence[1].AttribStartPositionOpp;// new Vector3(DefenseSequence[OppInt].AttribStart.transform.localPosition.x, 3, DefenseSequence[OppInt].AttribStart.transform.localPosition.z);
               // if (SpecialSequence[1].AttribStartRotationOpp != Vector3.zero)
                    SpecialSequence[1].AttribStart.transform.localRotation =Quaternion.Euler( SpecialSequence[1].AttribStartRotationOpp);

                SpecialSequence[1].AttribStart.SetActive(true);


                yield return new WaitForSeconds(1f);

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 17f, 13), 0.01f);
                    Cloud.SetActive(true);
                    Cloud.transform.DOMove(new Vector3(0, 8.5f, 13), 0.7f);
                }

                dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);

                //Attrib Start Particle-----------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribStartDelay); //1.4

                if (AttackSequence[MeInt].AttribStartPositionOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribStart.transform.localPosition = AttackSequence[MeInt].AttribStartPositionOpp;
               // if (AttackSequence[MeInt].AttribStartRotationOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribStart.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribStartRotationOpp);



                AttackSequence[MeInt].AttribStart.SetActive(true);

                if (AttackSequence[MeInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].StartAudio;
                    GetComponent<AudioSource>().Play();
                }



                //Attrib Hit Particle-------------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribHitDelay); //0.8

                AttackSequence[MeInt].AttribStart.SetActive(false);


                if (AttackSequence[MeInt].AttribHitPositionOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribHit.transform.localPosition = AttackSequence[MeInt].AttribHitPositionOpp; //new Vector3(AttackHit[0].transform.localPosition.x, -2f, AttackHit[0].transform.localPosition.z);

            //    if (AttackSequence[MeInt].AttribHitRotationOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribHit.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribHitRotationOpp);

                AttackSequence[MeInt].AttribHit.SetActive(true);

                if (AttackSequence[MeInt].HitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].HitAudio;
                    GetComponent<AudioSource>().Play();
                }

                Camera.main.GetComponent<CameraShake>().ShakeCamera(1f);

                //Attrib After Hit Particle
                yield return new WaitForSeconds(AttackSequence[MeInt].AfterAttribHitDelay);//0.5

                AttackSequence[MeInt].AttribHit.SetActive(false);

                if (AttackSequence[MeInt].AttribAfterHitPositionMe != Vector3.zero)
                    AttackSequence[MeInt].AttribAfterHit.transform.localPosition = AttackSequence[MeInt].AttribAfterHitPositionMe;// new Vector3(AttackAfterHit[0].transform.localPosition.x, -2f, AttackAfterHit[0].transform.localPosition.z);
               // if (AttackSequence[MeInt].AttribAfterHitRotationMe != Vector3.zero)
                    AttackSequence[MeInt].AttribAfterHit.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribAfterHitRotationMe);

                AttackSequence[MeInt].AttribAfterHit.SetActive(true);
                DefenseSequence[OppInt].AttribStart.SetActive(false);
                Cloud.SetActive(false);

                if (AttackSequence[MeInt].AfterHitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].AfterHitAudio;
                    GetComponent<AudioSource>().Play();
                }


                //Attrib Final Attack Particle----------------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribAfterFinalDelay); //0.7

                AttackSequence[MeInt].AttribAfterHit.SetActive(false);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);
                if (AttackSequence[MeInt].AttribAfterFinal)
                {
                    if (AttackSequence[MeInt].AttribAfterFinalPositionMe != Vector3.zero)
                        AttackSequence[MeInt].AttribAfterFinal.transform.localPosition = AttackSequence[MeInt].AttribAfterFinalPositionMe; // new Vector3(AttackAfterFinal[0].transform.localPosition.x, -2f, AttackAfterFinal[0].transform.localPosition.z);
                    //if (AttackSequence[MeInt].AttribAfterFinalRotationMe != Vector3.zero)
                        AttackSequence[MeInt].AttribAfterFinal.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribAfterFinalRotationMe);                        
                    AttackSequence[MeInt].AttribAfterFinal.SetActive(true);
                }
                SpecialSequence[1].AttribStart.SetActive(false);

                if (AttackSequence[MeInt].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }



                //-------------------------- Lost Animation ----------------------------------
                yield return new WaitForSeconds(0.5f);
                PlayerManager.instance.PrevRes = "lost";
                PlayerManager.instance.LostAnimator.gameObject.SetActive(true);

                GetComponent<AudioSource>().clip = lostSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(2f);
                PlayerManager.instance.LostAnimator.gameObject.SetActive(false);

                if (AttackSequence[MeInt].AttribAfterFinal)
                    AttackSequence[MeInt].AttribAfterFinal.SetActive(false);
            

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 15, 13), 0.1f);
                }

                if (PlayerManager.instance.SelectedCard.CurUnlocked || PlayerManager.instance.SelectedEnemyCard.CurUnlocked)
                {
                    yield return new WaitForSeconds(3f);
                }

                yield return new WaitForSeconds(2f);

                MoveToNextRound();

                //StartCoroutine(AttackParticle_Fun_DeActive());
            }

            else if (SelectPowerManager.instance.selectedEnemyPowerName== AttributeType.Disrupt)
            {
                WinText.text = SpecialSlang[2];
                //Debug.Log("Special Disrupt");
                yield return new WaitForSeconds(1.5f);
                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 2f);

                SpecialSequence[2].AttribStart.transform.localPosition = SpecialSequence[2].AttribStartPositionMe;

                SpecialSequence[2].AttribStart.SetActive(true);


                if (SpecialSequence[2].StartAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[2].StartAudio;
                    GetComponent<AudioSource>().Play();
                }
               

                yield return new WaitForSeconds(3.5f);

                //Debug.Log("call moveenext");

                Camera.main.GetComponent<CameraShake>().ShakeCamera(2f, 1f);

                yield return new WaitForSeconds(4f);

                if (SpecialSequence[2].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[2].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }

                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 3f);

                PlayerManager.instance.LostAnimator.gameObject.SetActive(true);
                GetComponent<AudioSource>().clip = lostSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(2f);
                PlayerManager.instance.PrevRes = "lost";
                PlayerManager.instance.LostAnimator.gameObject.SetActive(false);

              
               
                MoveToNextRound();

            }

            else if (SelectPowerManager.instance.selectedEnemyPowerName == AttributeType.Heal)
            {
                //Debug.Log("Special Heal");
                WinText.text = SpecialSlang[3];

                if (SpecialSequence[3].AttribStartPositionOpp != Vector3.zero)
                    SpecialSequence[3].AttribStart.transform.localPosition = SpecialSequence[3].AttribStartPositionOpp;

                SpecialSequence[3].AttribStart.SetActive(true);

                if (SpecialSequence[3].StartAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[3].StartAudio;
                    GetComponent<AudioSource>().Play();
                }
               

                yield return new WaitForSeconds(4f);

                //Debug.Log("call moveenext");
                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);


                yield return new WaitForSeconds(4f);

                SpecialSequence[3].AttribStart.SetActive(false);
               
                yield return new WaitForSeconds(3f);

                if (SpecialSequence[3].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = SpecialSequence[3].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }


                PlayerManager.instance.LostAnimator.gameObject.SetActive(true);
                GetComponent<AudioSource>().clip = lostSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                PlayerManager.instance.PrevRes = "lost";
                yield return new WaitForSeconds(2f);
                PlayerManager.instance.LostAnimator.gameObject.SetActive(false);


              
                MoveToNextRound();
            }
        }

        else
        {
            ////Debug.Log("We are in the particle condition from loat " + SelectPowerManager.instance.selectedPowerName);
            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Attack)
            {

                yield return new WaitForSeconds(0.8f);

                DefenseSequence[OppInt].AttribStart.transform.localPosition = DefenseSequence[OppInt].AttribStartPositionOpp;// new Vector3(DefenseSequence[OppInt].AttribStart.transform.localPosition.x, -2, DefenseSequence[OppInt].AttribStart.transform.localPosition.z);

                DefenseSequence[OppInt].AttribStart.transform.localRotation = Quaternion.Euler(DefenseSequence[OppInt].AttribStartRotationOpp);
               
                DefenseSequence[OppInt].AttribStart.SetActive(true);

                //DefenseStart[0].transform.localPosition = new Vector3(DefenseStart[0].transform.localPosition.x, -2, DefenseStart[0].transform.localPosition.z);
                //DefenseStart[0].SetActive(true);

                yield return new WaitForSeconds(1f);

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 17f, 13), 0.01f);
                    Cloud.SetActive(true);
                    Cloud.transform.DOMove(new Vector3(0, 8.5f, 13), 0.7f);
                }

                if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
                {


                }
                else
                {
                    dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                    dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                }

                //Attrib Start Particle-----------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribStartDelay); //1.4

                if (AttackSequence[MeInt].AttribStartPositionOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribStart.transform.localPosition = AttackSequence[MeInt].AttribStartPositionOpp;
              //  if (AttackSequence[MeInt].AttribStartRotationOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribStart.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribStartRotationOpp);



                AttackSequence[MeInt].AttribStart.SetActive(true);


                if (AttackSequence[MeInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].StartAudio;
                    GetComponent<AudioSource>().Play();
                }



                //Attrib Hit Particle-------------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribHitDelay); //0.8

                AttackSequence[MeInt].AttribStart.SetActive(false);


                if (AttackSequence[MeInt].AttribHitPositionOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribHit.transform.localPosition = AttackSequence[MeInt].AttribHitPositionOpp; //new Vector3(AttackHit[0].transform.localPosition.x, -2f, AttackHit[0].transform.localPosition.z);

                //if (AttackSequence[MeInt].AttribHitRotationOpp != Vector3.zero)
                    AttackSequence[MeInt].AttribHit.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribHitRotationOpp);


                if (AttackSequence[MeInt].HitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].HitAudio;
                    GetComponent<AudioSource>().Play();
                }

                AttackSequence[MeInt].AttribHit.SetActive(true);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(1f);

                //Attrib After Hit Particle
                yield return new WaitForSeconds(AttackSequence[MeInt].AfterAttribHitDelay);//0.5

                AttackSequence[MeInt].AttribHit.SetActive(false);

                if (AttackSequence[MeInt].AttribAfterHitPositionMe != Vector3.zero)
                    AttackSequence[MeInt].AttribAfterHit.transform.localPosition = AttackSequence[MeInt].AttribAfterHitPositionMe;// new Vector3(AttackAfterHit[0].transform.localPosition.x, -2f, AttackAfterHit[0].transform.localPosition.z);
               // if (AttackSequence[MeInt].AttribAfterHitRotationMe != Vector3.zero)
                    AttackSequence[MeInt].AttribAfterHit.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribAfterHitRotationMe);

                AttackSequence[MeInt].AttribAfterHit.SetActive(true);
                  DefenseSequence[OppInt].AttribStart.SetActive(false);

                if (AttackSequence[MeInt].AfterHitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].AfterHitAudio;
                    GetComponent<AudioSource>().Play();
                }
               

                Cloud.SetActive(false);


                //Attrib Final Attack Particle----------------
                yield return new WaitForSeconds(AttackSequence[MeInt].AttribAfterFinalDelay); //0.7

                AttackSequence[MeInt].AttribAfterHit.SetActive(false);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);
                if (AttackSequence[MeInt].AttribAfterFinal)
                {
                    if (AttackSequence[MeInt].AttribAfterFinalPositionMe != Vector3.zero)
                        AttackSequence[MeInt].AttribAfterFinal.transform.localPosition = AttackSequence[MeInt].AttribAfterFinalPositionMe; // new Vector3(AttackAfterFinal[0].transform.localPosition.x, -2f, AttackAfterFinal[0].transform.localPosition.z);
                   // if (AttackSequence[MeInt].AttribAfterFinalRotationMe != Vector3.zero)
                        AttackSequence[MeInt].AttribAfterFinal.transform.localRotation = Quaternion.Euler(AttackSequence[MeInt].AttribAfterFinalRotationMe);


                    AttackSequence[MeInt].AttribAfterFinal.SetActive(true);
                }



                if (AttackSequence[MeInt].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[MeInt].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }
               


                //-------------------------- Lost Animation ----------------------------------
                yield return new WaitForSeconds(0.5f);
                PlayerManager.instance.PrevRes = "lost";
                PlayerManager.instance.LostAnimator.gameObject.SetActive(true);
                GetComponent<AudioSource>().clip = lostSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();

                yield return new WaitForSeconds(2f);
                PlayerManager.instance.LostAnimator.gameObject.SetActive(false);

                if (AttackSequence[MeInt].AttribAfterFinal)
                    AttackSequence[MeInt].AttribAfterFinal.SetActive(false);
               

                if (MeInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 15, 13), 0.1f);
                }

                if (PlayerManager.instance.SelectedCard.CurUnlocked || PlayerManager.instance.SelectedEnemyCard.CurUnlocked)
                {
                    yield return new WaitForSeconds(3f);
                }

                yield return new WaitForSeconds(2f);

                MoveToNextRound();

            }

            else if (SelectPowerManager.instance.selectedPowerName == AttributeType.Defense)
            {

                yield return new WaitForSeconds(0.8f);

                DefenseSequence[MeInt].AttribStart.transform.localPosition = DefenseSequence[MeInt].AttribStartPositionMe;// new Vector3(DefenseSequence[OppInt].AttribStart.transform.localPosition.x, -2, DefenseSequence[OppInt].AttribStart.transform.localPosition.z);

                DefenseSequence[MeInt].AttribStart.transform.localRotation = Quaternion.Euler(DefenseSequence[MeInt].AttribStartRotationMe);

                DefenseSequence[MeInt].AttribStart.SetActive(true);

                //DefenseStart[0].transform.localPosition = new Vector3(DefenseStart[0].transform.localPosition.x, -2, DefenseStart[0].transform.localPosition.z);
                //DefenseStart[0].SetActive(true);

                yield return new WaitForSeconds(1f);

                if (OppInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 17f, 13), 0.01f);
                    Cloud.SetActive(true);
                    Cloud.transform.DOMove(new Vector3(0, 8.5f, 13), 0.7f);
                }

                if (PlayerManager.instance.skirmishObj.GetComponent<SkirmishModeAnim>().inSkirmish == true)
                {


                }
                else
                {
                    dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                    dice2.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
                }

                //Attrib Start Particle-----------
                yield return new WaitForSeconds(AttackSequence[OppInt].AttribStartDelay); //1.4

                if (AttackSequence[OppInt].AttribStartPositionOpp != Vector3.zero)
                    AttackSequence[OppInt].AttribStart.transform.localPosition = AttackSequence[OppInt].AttribStartPositionOpp;
              //  if (AttackSequence[OppInt].AttribStartRotationOpp != Vector3.zero)
                    AttackSequence[OppInt].AttribStart.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribStartRotationOpp);



                AttackSequence[OppInt].AttribStart.SetActive(true);

                if (AttackSequence[OppInt].StartAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].StartAudio;
                    GetComponent<AudioSource>().Play();
                }



                //Attrib Hit Particle-------------
                yield return new WaitForSeconds(AttackSequence[OppInt].AttribHitDelay); //0.8

                AttackSequence[OppInt].AttribStart.SetActive(false);

                DefenseSequence[MeInt].AttribStart.SetActive(false);

                if (AttackSequence[OppInt].AttribHitPositionOpp != Vector3.zero)
                    AttackSequence[OppInt].AttribHit.transform.localPosition = AttackSequence[OppInt].AttribHitPositionOpp; //new Vector3(AttackHit[0].transform.localPosition.x, -2f, AttackHit[0].transform.localPosition.z);

               // if (AttackSequence[OppInt].AttribHitRotationOpp != Vector3.zero)
                    AttackSequence[OppInt].AttribHit.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribHitRotationOpp);

                AttackSequence[OppInt].AttribHit.SetActive(true);


                if (AttackSequence[OppInt].HitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].HitAudio;
                    GetComponent<AudioSource>().Play();
                }


                Camera.main.GetComponent<CameraShake>().ShakeCamera(1f);

                //Attrib After Hit Particle
                yield return new WaitForSeconds(AttackSequence[OppInt].AfterAttribHitDelay);//0.5

                AttackSequence[OppInt].AttribHit.SetActive(false);
                DefenseSequence[OppInt].AttribStart.SetActive(false);
                if (AttackSequence[OppInt].AttribAfterHitPositionMe != Vector3.zero)
                    AttackSequence[OppInt].AttribAfterHit.transform.localPosition = AttackSequence[OppInt].AttribAfterHitPositionMe;// new Vector3(AttackAfterHit[0].transform.localPosition.x, -2f, AttackAfterHit[0].transform.localPosition.z);
             //   if (AttackSequence[OppInt].AttribAfterHitRotationMe != Vector3.zero)
                    AttackSequence[OppInt].AttribAfterHit.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribAfterHitRotationMe);

                AttackSequence[OppInt].AttribAfterHit.SetActive(true);

                if (AttackSequence[OppInt].AfterHitAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].AfterHitAudio;
                    GetComponent<AudioSource>().Play();
                }


                Cloud.SetActive(false);


                //Attrib Final Attack Particle----------------
                yield return new WaitForSeconds(AttackSequence[OppInt].AttribAfterFinalDelay); //0.7

                AttackSequence[OppInt].AttribAfterHit.SetActive(false);

                Camera.main.GetComponent<CameraShake>().ShakeCamera(0.5f, 1f);
                if (AttackSequence[OppInt].AttribAfterFinal)
                {
                    if (AttackSequence[OppInt].AttribAfterFinalPositionMe != Vector3.zero)
                        AttackSequence[OppInt].AttribAfterFinal.transform.localPosition = AttackSequence[OppInt].AttribAfterFinalPositionMe; // new Vector3(AttackAfterFinal[0].transform.localPosition.x, -2f, AttackAfterFinal[0].transform.localPosition.z);
                  //  if (AttackSequence[OppInt].AttribAfterFinalRotationMe != Vector3.zero)
                        AttackSequence[OppInt].AttribAfterFinal.transform.localRotation = Quaternion.Euler(AttackSequence[OppInt].AttribAfterFinalRotationMe);


                    AttackSequence[OppInt].AttribAfterFinal.SetActive(true);
                }

                if (AttackSequence[OppInt].FinalAudio)
                {
                    GetComponent<AudioSource>().clip = AttackSequence[OppInt].FinalAudio;
                    GetComponent<AudioSource>().Play();
                }



                //-------------------------- Lost Animation ----------------------------------
                yield return new WaitForSeconds(0.5f);
                PlayerManager.instance.PrevRes = "lost";
                PlayerManager.instance.LostAnimator.gameObject.SetActive(true);

                GetComponent<AudioSource>().clip = lostSound[UnityEngine.Random.Range(0, 3)];
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(2f);
                PlayerManager.instance.LostAnimator.gameObject.SetActive(false);

                if (AttackSequence[OppInt].AttribAfterFinal)
                    AttackSequence[OppInt].AttribAfterFinal.SetActive(false);
             

                if (OppInt == 0)
                {
                    Cloud.transform.DOMove(new Vector3(0, 15, 13), 0.1f);
                }

                if (PlayerManager.instance.SelectedCard.CurUnlocked || PlayerManager.instance.SelectedEnemyCard.CurUnlocked)
                {
                    yield return new WaitForSeconds(3f);
                }

                yield return new WaitForSeconds(2f);

                MoveToNextRound();

            }

            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Heal)
            {
                StartCoroutine(DisruptAnimStart(MeInt,OppInt,false));
            }

            if (SelectPowerManager.instance.selectedPowerName == AttributeType.Disrupt)
            {

                StartCoroutine(HealSuccessfull(MeInt, OppInt, false));

                //StartCoroutine(AttackParticle_Fun_DeActive());

            }
        }
        //StartCoroutine(AttackParticle_Fun_DeActive());
    }

    
    public void DisableDice()
    {
        dice1Num.gameObject.SetActive(false);
        dice1Attack.gameObject.SetActive(false);
        dice.DOScale(new Vector3(0f, 0f, 0f), 0.5f);
        dice1Num.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 20f), 1);
        dice1Attack.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, -21f), 1);
    }
    public void OppDisableDice()
    {
        dice2Num.gameObject.SetActive(false);
        dice2Attack.gameObject.SetActive(false);
        dice2.DOScale(new Vector3(0, 0, 0),0.5f);
        dice2Num.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, -20f), 1);
        dice2Attack.transform.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, -21f), 1);

    }

    void MoveToNextRound()
    {
        if (PlayerManager.instance.SelectedCard != null)
        {
            PlayerManager.instance.SelectedCard.isSpActive = false;
            PlayerManager.instance.SelectedCard.ResetAttributeValue();

        }
       
        if (PlayerManager.instance.SelectedEnemyCard != null)
        {
            PlayerManager.instance.SelectedEnemyCard.isSpActive = false;
            PlayerManager.instance.SelectedEnemyCard.ResetAttributeValue();

        }
        
        ElEarnMe.SetActive(false);
        ELEarnOpp.SetActive(false);


        

        for (int i = 0; i < PlayerCompanionCardsAnim.instance.playerCompanionCards.Length; i++)
        {
            if (PlayerCompanionCardsAnim.instance.playerCompanionCards[i].isSelected)
            {
                PlayerCompanionCardsAnim.instance.playerCompanionCards[i].isUsed = true;
                PlayerCompanionCardsAnim.instance.playerCompanionCards[i].lockImage.SetActive(true);
                PlayerCompanionCardsAnim.instance.playerCompanionCards[i].GetComponent<BoxCollider2D>().enabled = false;
                PlayerCompanionCardsAnim.instance.playerCompanionCards[i].isSelected = false;

                //Debug.Log(PlayerCompanionCardsAnim.instance.playerCompanionCards[i].CardDetail.name + " Working " + PlayerCompanionCardsAnim.instance.playerCompanionCards[i].isUsed);

                //RevertCompanionValues(i);

            }
        }

        for (int i = 0; i < PlayerCompanionCardsAnim.instance.enemyCompanionCards.Length; i++)
        {
            if (PlayerCompanionCardsAnim.instance.enemyCompanionCards[i].isSelected)
            {
                PlayerCompanionCardsAnim.instance.enemyCompanionCards[i].isUsed = true;
                PlayerCompanionCardsAnim.instance.enemyCompanionCards[i].lockImage.SetActive(true);
                PlayerCompanionCardsAnim.instance.enemyCompanionCards[i].GetComponent<BoxCollider2D>().enabled = false;
                PlayerCompanionCardsAnim.instance.enemyCompanionCards[i].isSelected = false;
            }

        }


        //Debug.Log("move to next round");
        PlayerManager.instance.winAnimator.gameObject.SetActive(false);
        PlayerManager.instance.winAnimator.enabled = false;
        PlayerManager.instance.LostAnimator.SetActive(false);
        PlayerManager.instance.CanClick = true;
        //PlayerManager.instance.CanClickOnBG = true;


        PlayerManager.instance.ClickOnBG();

        

    }

    public IEnumerator waitforAnimReturn(GameObject cardobject)
    {

        cardobject.GetComponent<CompanionCardObject>().lockImage.SetActive(true);
        cardobject.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(1f);
    }


    public void RevertCompanionValues(int i)
    {
        if (PlayerCompanionCardsAnim.instance.playerCompanionCards[i].isSelected)
        {
            //PlayerCompanionCardsAnim.instance.playerCompanionCards[i].isSelected = false;
            switch (PlayerCompanionCardsAnim.instance.playerCompanionCards[i].CardDetail.strength_1)
            {
                case AttributeType.Attack:

                    //BattleLobbySC.Instance.CompanionCardSelected(

                    //    PlayerCompanionCardsAnim.instance.playerCompanionCards[i].CardDetail.card_contract_id,
                    //    CardStrengthType.Attack,//CardObject.CardDetail.strength_1,
                    //    PlayerManager.instance.SelectedCard.CardDetail.attack.ToString(),
                    //    PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                    //    "1"
                    //    );

                    InGameCards.instance.MyAttribValue.text = PlayerManager.instance.SelectedCard.CardDetail.attack.ToString();
                    break;
                case AttributeType.Defense:
                    //BattleLobbySC.Instance.CompanionCardSelected(

                    //    PlayerCompanionCardsAnim.instance.playerCompanionCards[i].CardDetail.card_contract_id,
                    //    CardStrengthType.Defense,//CardObject.CardDetail.strength_1,
                    //    PlayerManager.instance.SelectedCard.CardDetail.defence.ToString(),
                    //    PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                    //    "1"
                    //    );
                    InGameCards.instance.MyAttribValue.text = PlayerManager.instance.SelectedCard.CardDetail.defence.ToString();

                    break;
                case AttributeType.Heal:
                    //BattleLobbySC.Instance.CompanionCardSelected(

                    //    PlayerCompanionCardsAnim.instance.playerCompanionCards[i].CardDetail.card_contract_id,
                    //    CardStrengthType.Heal,//CardObject.CardDetail.strength_1,
                    //   PlayerManager.instance.SelectedCard.CardDetail.heal.ToString(),
                    //    PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                    //    "1"
                    //    );
                    InGameCards.instance.MyAttribValue.text = PlayerManager.instance.SelectedCard.CardDetail.heal.ToString();

                    break;
                case AttributeType.Disrupt:
                    //BattleLobbySC.Instance.CompanionCardSelected(

                    //    PlayerCompanionCardsAnim.instance.playerCompanionCards[i].CardDetail.card_contract_id,
                    //    "disrupt",//CardObject.CardDetail.strength_1,
                    //    PlayerManager.instance.SelectedCard.CardDetail.disrupt.ToString(),
                    //    PlayerManager.instance.SelectedCard.CardDetail.card_contract_id,
                    //    "1"
                    //    );
                    InGameCards.instance.MyAttribValue.text = PlayerManager.instance.SelectedCard.CardDetail.disrupt.ToString();

                    break;


            }
        }
            
    }

}
